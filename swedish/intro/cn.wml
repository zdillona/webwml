#use wml::debian::template title="Debians webbplats i olika språk" MAINPAGE="true"
#use wml::debian::translation-check translation="6f2dd223c8dc504e001d0a01f1a665a5423e0213"
#use wml::debian::toc

<define-tag toc-title-formatting endtag="required">%body</define-tag>
<define-tag toc-item-formatting endtag="required">[%body]</define-tag>
<define-tag toc-display-begin><p></define-tag>
<define-tag toc-display-end></p></define-tag>

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<div id="toc">
  <ul class="toc">
    <li><a href="#intro">Innehållsförhandling</a></li>
    <li><a href="#howtoset">Hur du ställer in en webbläsares språk</a></li>
    <li><a href="#override">Hur du åsidosätter inställningarna</a></li>
    <li><a href="#fix">Felsökning</a></li>
  </ul>
</div>

<h2><a id="intro">Innehållsförhandling</a></h2>

<p>
En grupp <a href="../devel/website/translating">översättare</a>
arbetar på Debians webbplats för att konvertera den för en växande
mängd språk. Men hur sker språkväxlingen i webbläsaren?
En standard kallad
<a href="$(HOME)/devel/website/content_negotiation">innehållsförhandling</a>
tillåter användare att sätta sitt föredragna språk för webbinnehåll.
Versionen som dom ser förhandlas mellan webbläsaren och webbservern:
webbläsaren skickar inställningarna till servern, och servern väljer
då vilket version som dom levererar (baserat på användarens inställningar och
tillgängliga versioner).
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="http://www.w3.org/International/questions/qa-lang-priorities">Läs mer hos W3C</a></button></p>

<p>
Alla känner inte till innehållsförhandling, så länkar på nederkanten av
alla Debians webbsidor pekar till tillgängliga versioner. Vänligen notera
att om du väljer ett annat språk från denna lista kommer det endast att
påverka aktuell sida. Det ändrar inte standardspråket för din webbläsare.
Om du följer en länk till en annan sida kommer du se den i standardspråket
igen.
</p>

<p>
För att ändra ditt förinställda språk, har du två alternativ:
</p>

<ul>
  <li><a href="#howtoset">Konfigurera din webbläsare</a></li>
  <li><a href="#override">Åsidosätt din webbläsares språkinställning</a></li>
</ul>

<p>
Hoppa direkt till konfigurationsinstruktionerna för dessa webbläsare:</p>

<toc-display />

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Ursprungsspråket på
Debianwebben är engelska. Därför är det en god idé att lägga till engelska
(<code>en</code>) på slutet av din lista över språk. Detta fungerar som en
backup om en sida inte har översatts till något av dina föredragna språk.</p>
</aside>

<h2><a id="howtoset">Hur du ställer in en webbläsares språk</a></h2>

<p>
Innan vi beskriver hur du konfigurerar språkinställningen i olika
webbläsare följer några allmänna kommentarer. Först är det en god idé
att inkludera alla språk som du talar i listan över föredragna språk.
Exempelvis, om du har franska som modersmål kan du använda <code>fr</code>
som ditt första språk, följt av engelska med språkkoden <code>en</code>.
</p>

<p>
För det andra - i vissa webbläsare kan du lägga till språkkoder istället
för att välja från en meny. Om det är fallet, kom ihåg att om du skapar en
lista i stil med <code>fr, en</code> så anger den inte vad du föredrar,
utan definierar lika rankade alternativ, och webbläsaren kan välja att
ignorera ordningen och bara välja ett av språken. Om du vill ange en
verklig preferens så måste du jobba med s.k. kvalitetsvärden,
dvs. flyttalsvärden mellan 0 och 1. Ett högre värde indikerar en
högre preferens. Om vi går tillbaks till exemplet med franska och engelska
så kan du modifiera det exemplet så här:
</p>

<pre>
fr; q=1.0, en; q=0.5
</pre>

<h3>Var försiktig med landskoder</h3>

<p>
En webbserver som får en förfrågan efter ett dokument med föredraget
språk <code>en-GB, fr</code> levererar <strong>inte alltid</strong> den
engelska versionen före den franska. Den kommer endast att göra det om en
sida med språkkoden <code>en-gb</code> finns. Det fungerar dock på andra
hållet: en server kan returnera en sida med språkkod <code>en-us</code> om
bara <code>en</code> är inkluderad i listan på föredragna språk.
</p>

<p>
Vi rekommenderar därför att inte lägga till två-bokstavs-språkkoder som
<code>en-GB</code> eller <code>en-US</code>, om du inte har en väldigt god
anledning. Om du lägger till en sådan, säkerställ att du lägger till språkkoden
utan tillägget också: <code>en-GB, en, fr</code>
</p>

<p style="text-align:center"><button type="button"><span
class="fas fa-book-open fa-2x"></span> <a
href="https://httpd.apache.org/docs/current/content-negotiation.html">Mer om innehållsförhandling</a></button></p>

<h3>Instruktioner för olika webbläsare</h3>

<p>
Vi har sammanställt en lista på populära webbläsare med instruktioner för hur
man ställer in föredraget språk för webbinnehåll i dess inställningar:
</p>

<div class="row">
   <!-- left column -->
   <div class="column column-left">
      <div style="text-align: left">
         <ul>
            <li><strong><toc-add-entry name="chromium">Chrome/Chromium</toc-add-entry></strong><br>
               Högst upp till höger, öppna menyn och klicka på <em>Inställningar</em> -&gt;
               <em>Avancerade inställningar</em> -&gt; <em>Språk</em>. Öppna menyn
               <em>Språk</em> för att se en lista på språk. Klicka på dom tre punkterna bredvid
               varje post för att ändra på ordningen. Du kan även lägga till nya språk
               om det skulle behövas.</li>
            <li><strong><toc-add-entry name="elinks">ELinks</toc-add-entry></strong><br>
               Att sätta standardspråket via <em>Inställningar</em> -&gt; <em>Språk</em> kommer
               att även ändra efterfrågat språk från webbsidor. Du kan ändra detta beteende
               och finjustera <em>Accept-language-rubriken</em> vid <em>Inställningar</em>
               -&gt; <em>Alternativhanterare</em> -&gt; <em>Protokoll</em> -&gt; <em>HTTP</em></li>
            <li><strong><toc-add-entry name="epiphany">Epiphany</toc-add-entry></strong><br>
               Öppna <em>Inställningar</em> från huvudmenyn och växla till
               <em>Språk</em>-fliken. Här kan du lägga till, ta bort och arrangera språk.</li>
            <li><strong><toc-add-entry name="mozillafirefox">Firefox</toc-add-entry></strong><br>
               I menyn överst, öppna <em>Inställningar</em>, scrolla ner till
               <em>Språk och utseense</em> -&gt; <em>Språk</em> i <em>Allmänt</em>-panelen.
               Klicka på knappen <em>Välj</em> för att sätta ditt förvalda språk att visa
               webbsidor i. I samma dialog kan du även lägga till, ta bort eller byta
               ordning på språk.</li>
            <li><strong><toc-add-entry name="ibrowse">IBrowse</toc-add-entry></strong><br>
               Gå till <em>Konfiguration</em> -&gt; <em>Inställningar</em> -&gt; <em>Nätverk</em>.
               <em>Acceptera språk</em> visa antagligen en *, vilket är standard. Om du
               klickar på knappen <em>Plats</em> skall du kunna lägga till ditt föredragna
               språk. Om inte, kan du lägga till det manuellt.</li>
            <li><strong><toc-add-entry name="icab">iCab</toc-add-entry></strong><br>
               <em>Redigera</em> -&gt; <em>Inställningar</em> -&gt; <em>Webbläsare</em> -&gt;
               <em>Typsnitt, Språk</em></li>
            <li><strong><toc-add-entry name="iceweasel">IceCat
               (Iceweasel)</toc-add-entry></strong><br>
               <em>Redigera</em> -&gt; <em>Inställningar</em> -&gt; <em>Innehåll</em> -&gt;
               <em>Språk</em> -&gt; <em>Välj</em></li>
            <li><strong><toc-add-entry name="ie">Internet Explorer</toc-add-entry></strong><br>
               Klicka på <em>Verktygs</em>-ikonen, Välj <em>Internetalternativ</em>, växla till
               <em>Almmänt</em>-fliken, och klicka på knappen <em>Språk</em>. Klicka
               <em>Sätt Språkval</em>. och i följande dialog kan du lägga till, ta bort
               och ändra ordningen på språk.</li>
         </ul>
      </div>
   </div>

<!-- right column -->
   <div class="column column-right">
      <div style="text-align: left">
         <ul>
            <li><strong><toc-add-entry name="konqueror">Konqueror</toc-add-entry></strong><br>
               Redigera filen <em>~/.kde/share/config/kio_httprc</em> och inkludera
               följande nya rad:<br>
               <code>Languages=fr;q=1.0, en;q=0.5</code></li>
            <li><strong><toc-add-entry name="lynx">Lynx</toc-add-entry></strong><br>
               Redigera filen <em>~/.lynxrc</em> och lägg till följande rad:<br>
               <code>preferred_language=fr; q=1.0, en; q=0.5</code><br>
               Alternativt kan du öppna webbläsarens inställningar genom att
               trycka [O]. Scrolla ner till <em>Föredragna språk</em> och lägg
               till raden ovan.</li>
            <li><strong><toc-add-entry name="edge">Microsoft Edge</toc-add-entry></strong><br>
               <em>Inställningar</em>  -&gt;
               <em>Språk</em> -&gt; <em>Lägg till språk</em><br>
               Klicka på knappen med tre punkter bredvid varje språk för mer
               alternativ och för att ändra på ordningen.</li>
            <li><strong><toc-add-entry name="opera">Opera</toc-add-entry></strong><br>
               <em>Inställningar</em> -&gt; <em>Webbläsare</em> -&gt; <em>Språk</em> -&gt; <em>Föredragna språk</em></li>
            <li><strong><toc-add-entry name="safari">Safari</toc-add-entry></strong><br>
               Safari använder systemomfattande inställningar på macOS och iOS,
               så för att ändra ditt föredragna språk var vänlig öppna
               <em>Systeminställningar</em> (macOS) eller <em>Inställningar</em>
               (iOS).</li>
            <li><strong><toc-add-entry name="w3m">W3M</toc-add-entry></strong><br>
               Tryck [O] för att öppna <em>Panel för alternativinställningar</em>,
               scrolla ner till <em>Nätverksinställningar</em> -&gt;
               <em>Acceptera-Språk header</em>. Tryck [Enter] för att ändra
               inställningen (exempelvis <code>fr; q=1.0, en; q=0.5</code>) och
               bekräfta med [Enter]. Scrolla hela vägen ner till [OK] för att
               spara dina inställningar.</li>
            <li><strong><toc-add-entry name="vivaldi">Vivaldi</toc-add-entry></strong><br>
               Gå till <em>Inställningar</em> -&gt; <em>Allmänt</em> -&gt; <em>Språk</em>
               -&gt; <em>Accepterade språk</em>, klicka på <em>Lägg till språk</em> och
               välj ett från menyn. Använd pilarna för att ändra ordningen på
               dina val.</li>
         </ul>
      </div>
   </div>
</div>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Även om det alltid är bättre
att välja ditt föredragna språk i webbläsarens inställningar, så finns det ett
alternativ att åsidosätta inställningarna med en cookie.</p>
</aside>

<h2><a id="override">Hur du åsidosätter inställningarna</a></h2>

<p>
Om du av någon anledning inte har möjlighet att definiera dina föredragna
språk i din webbläsares inställningar, enhet eller datormiljö, kan du
åsidosätta inställningarna med hjälp av en cookie som en sista utväg. Klicka
på någon av knapparna nedan för att sätta ett enstaka språk på toppen av den
listan.
</p>

<p>
Vänligen notera att detta kommer att sätta en <a
href="https://en.wikipedia.org/wiki/HTTP_cookie">cookie</a>. Din
webbläsare kommer att ta bort den automatiskt om du inte besöker denna
webbplats på en månad. Självklart kan du ta bort denna cookie automatiskt
i din webbläsare eller genom att trycka på knappen <em>webbläsarstandard</em>.
</p>

<protect pass=2>
<: print language_selector_buttons(); :>
</protect>


<h2><a id="fix">Felsökning</a></h2>

<p>
Ibland visas Debians webbplats med fel språk trots alla dina insatser
för att sätta ett förvalt språk. Vårt första förslag är att rensa all
lokal cache (både på disk och i minnet) i din webbläsare innan du försöker
ladda om webbsidan. Om du är helt säker på att du har
<a href="#howtoset">konfigurerat din webbläsare</a> ordentligt så kan
en trasig eller felkonfigurerad cache vara problemet. Detta kan vara
ett allvarligt problem i dessa dagar, eftersom fler och fler
internetleverantörer ser cachning som ett sätt att minska sin nättrafik. Läs
<a href="#cache">sektionen</a> om proxyservrar även om du inte tror att du
använder någon.
</p>

<p>
Men för all del, det är alltid möjligt att det var ett problem med
<a href="https://www.debian.org/">www.debian.org</a>. Även om endast
en handfull av språkproblem som rapporterats de senaste åren orsakats
av ett fel på vår sida så är det inte alls omöjligt. Vi föreslår därför
att du undersöker dina egna inställningar och ett eventuellt cachningsproblem
först, innan du <a href="../contact">kontaktar</a> oss. Om
<a href="https://www.debian.org/">https://www.debian.org/</a> fungerar,
men inte en av <a href="https://www.debian.org/mirror/list">speglingarna</a>,
var vänlig rapporerar detta så att vi kan kontakta dom ansvariga för denna
spegling.
</p>

<h3><a name="cache">Eventuella problem med proxyservrar</a></h3>

<p>
Proxyservrar är huvudsakligen webbservrar som inte har något eget
innehåll. Dom är placerade mitt emellan användare och riktiga
webbservrar, fångar förfrågningar efter webbsidor, och hämtar sidorna.
Efter detta vidarebefordrar de innehållet till användarens webbläsare,
men gör även en lokal cachad kopia som används för efterföljande
förfrågningar. Detta kan skära ner på nätverkstrafik när det finns många
användare efterfrågar samma sida.
</p>

<p>
Även om detta kan vara en god idé för det mesta, orsakar det även misslyckanden
när cachen är buggig. I synnerhet förstår inte vissa äldre proxyservrar
innehållsförhandling. Detta resulterar i att dom cachar en sida och
levererar denna trots att ett annat språk efterfrågas senare. Den enda
lösningen på detta är att uppgradera eller ersätta cachningsmjukvaran.
</p>

<p>
Historiskt användes endast proxyservrar när folk konfigurerade deras
webbläsare i enlighet med detta. Detta är dock inte längre fallet. Din
internetleverantör kan omdirigera alla HTTP-förfrågningar genom en
transparant proxy. Om proxyn inte hanterar innehållsförhandling ordentligt
kan användare få cachade sidor på fel språk. Detta enda sättet att korrigera
detta är att klaga hos din internetleverantör för att få dem att uppgradera
eller ersätta sin mjukvara.
</p>
