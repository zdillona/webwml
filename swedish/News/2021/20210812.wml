#use wml::debian::translation-check translation="a48c50faef33185d0ae2746854bc99435817bad1"
<define-tag pagetitle>Debianprojektet sörjer förlusten av Robert Lemmen, Karl Ramm och Rogério Theodoro de Brito</define-tag>
<define-tag release_date>2021-08-12</define-tag>
#use wml::debian::news

<p>Debianprojektet har förlorat flera medlemmar av sin gemenskap under
det senaste året.</p>

<p>
I juni 2020 avled Robert Lemmen efter en allvarlig sjukdom.
Robert hade regelbundet besökt Debian Munich-mötena sedan tidigt
2000-tal och har hjälpt till med lokala presentationsbås.
Han hade varit Debianutvecklare sedan 2007. Bland andra bidrag
paketerade han moduler för Raku (Perl6 vid den tidpunkten) och hjälpte
andra bidragslämnare att bli involverade i Raku-gruppen.
Han lade även ner tid på att spåra cirkulära beroenden i Debian.
</p>

<p>
Karl Ramm avled i juni 2020, efter komplikationer på grund av metastatisk
tjocktarmscancer. Han hade varit Debianutvecklare sedan 2001 och paketerade
flera komponenter av MITs <a 
href="https://en.wikipedia.org/wiki/Project_Athena">Project Athena</a>.
Han brann för teknik och Debian, och alltid intresserad av att hjälpa
andra att hitta och marknadsföra sina passioner.
</p>

<p>
I aprl 2021 förlorade vi Rogério Theodoro de Brito på grund av COVID-19-pandemin.
Rogério tyckte om att koda små verktyg och hade varit bidragslämnare till
Debian i mer än 15 år. Bland hans projekt bidrag han till användningen av
Kurobox/Linkstation-enheter i Debian och underhöll verktyget youtube-dl.
Han deltog även i och var <q>debiankontakt</q> i många uppströmsprojekt.
</p>

<p>
Debianprojektet hedrar Robert, Karl och Rogérios goda arbete och starka
engagemang i Debian och fri mjukvara. Deras bidrag kommer inte att glömmas
och deras arbetes höga kvalitet kommer att fortsätta att fungera som en
inspiration för andra.</p>

<h2>Om Debian</h2>

<p>
Debianprojektet är en sammanslutning av utvecklare av fri mjukvara som
ger frivilligt av sin tid och insats för att producera det helt fria
operativsystemet Debian.
</p>


<h2>Kontaktinformation</h2>

<p>
För mer information, vänligen besök Debians webbplats på
<a href="$(HOME)/">https://www.debian.org/</a> eller skicka e-post (på 
engelska) till &lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.
</p>
