#use wml::debian::template title="Debian-Installer" NOHEADER="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="a22870164df5007ae4f4e356dfe54983be0f1e9e"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"

<h1>Nyheter</h1>

<p>
<:= get_recent_list('News/$(CUR_YEAR)', '2',
'$(ENGLISHDIR)/devel/debian-installer', '', '\d+\w*' ) :>
<a href="News">Äldre nyheter</a>
</p>

<h1>Installera med Debian-Installer</h1>


<p>
<if-stable-release release="bullseye">
<strong>För information och officiella installationsmedia för
<current_release_bullseye></strong>, se
<a href="$(HOME)/releases/bullseye/debian-installer">sidan för Bullseye</a>.
</if-stable-release>
<if-stable-release release="bookworm">
<strong>För information och officiella installationsmedia för
<current_release_bookworm></strong>, se
<a href="$(HOME)/releases/bookworm/debian-installer">sidan för Bookworm</a>.
</if-stable-release>
</p>

<div class="tip">
<p>
Alla avbildningar som länkas nedan är för den version av Debianinstalleraren som
utvecklas för nästa Debianutgåva och kommer som standard att installera uttestningsutgåvan
(<q><:print ucfirst("<current_testing_name>"):></q>).
</p>
</div>

<!-- Shown in the beginning of the release cycle: no Alpha/Beta/RC released yet. -->
<if-testing-installer released="no">

<p>
<strong>För att installera uttestningsutgåvan</strong> rekommenderar vi att du
använder <strong>de dagliga utgåvorna</strong> av installationsprogrammet.
Följande avbildningar är tillgängliga för de dagliga utgåvorna:
</p>

</if-testing-installer>

<!-- Shown later in the release cycle: Alpha/Beta/RC available, point at the latest one. -->
<if-testing-installer released="yes">

<p>
<strong>För att installera uttestningsutgåvan</strong> rekommenderar vi att du
använder utgåvan <strong><humanversion /></strong> av installationsprogrammet, 
efter att ha läst dess <a href="errata">errata</a>.
Följande avbildningar är tillgängliga för <humanversion />:
</p>

<h2>Officiella utgåvan</h2>

<div class="line">
<div class="item col50">
<strong>Nätverksinstallations-cd</strong>
<netinst-images />
</div>

<div class="item col50 lastcol">
<strong>Nätverksinstallations-cd (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<netinst-images-jigdo />
</div>


</div>

<div class="line">
<div class="item col50">
<strong>Kompletta cd-uppsättningar</strong>
<full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>Kompletta dvd-uppsättningar</strong>
<full-dvd-images />
</div>


</div>


<div class="line">
<div class="item col50">
<strong>Kompletta cd-uppsättningar (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>Kompletta dvd-uppsättningar (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-dvd-jigdo />
</div>


</div>

<div class="line">
<div class="item col50">
<strong>Kompletta blu-ray-uppsättningar (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<full-bd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>Andra avbildningar (netboot, usb-minne, osv.)</strong>
<other-images />
</div>
</div>

<p>
Alternativt kan du använda en <b>ögonblicksbild</b> av uttestningsutgåvan.
De avbildningar som byggs varje vecka genererar kompletta uppsättningar av
avbildningar medans dom dagliga endast genererar några få avbildningar.
</p>

<div class="warning">

<p>
Dessa ögonblicksbilder installerar uttestningsutgåvan av Debian, men
installeraren baseras på instabila utgåvan (Debian unstable).
</p>

</div>


<h2>Veckans ögonblicksbild</h2>

<div class="line">
<div class="item col50">
<strong>komplett cd-uppsättning</strong>
<devel-full-cd-images />
</div>

<div class="item col50 lastcol">
<strong>komplett dvd-uppsättning</strong>
<devel-full-dvd-images />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>komplett cd-uppsättning (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<strong>komplett dvd-uppsättning (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-dvd-jigdo />
</div>
</div>

<div class="line">
<div class="item col50">
<strong>Kompletta blu-ray-uppsättningar
 (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-full-bd-jigdo />
</div>
</div>

</if-testing-installer>

<h2>Aktuella dagliga avbildningar</h2>
<div class="line">
<div class="item col50">
<strong>cd-avbildning för nätinstallation</strong>
<devel-small-cd-images />
</div>

<div class="item col50 lastcol">
<strong>cd-avbildning för nätinstallation (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong>
<devel-small-cd-jigdo />
</div>

</div>

<div class="line">
<div class="item col50">
<strong>andra avbildningar (nätstart, usb-pinne, etc.)</strong>
<devel-other-images />
</div>
</div>


<p>
<strong>Anmärkningar</strong>
</p>
<ul>
#        <li>Innan du hämtar de dagligen byggda avbildningarna bör du se om
#        det finns några
#        <a href="https://wiki.debian.org/DebianInstaller/Today">kända problem</a>.
#        </li>
    <li>En arkitektur kan vara (tillfälligt) utesluten ur översikten
        över dagliga avbildningar om dagliga avbildningar inte är
        (pålitligt) tillgängliga.</li>
        <li>Verifieringsfiler (<tt>SHA512SUMS</tt>, <tt>SHA256SUMS</tt> och
        andra) finns tillgängliga i samma mapp som avbildningarna för
        installationsavbildningarna.</li>
    <li>Vi rekommenderar att jigdo används för att
        hämta cd- och dvd-avbildningarna.</li>
    <li>
        Endast ett begränsat antal avbildningar av CD och DVD-uppsättningarna
        finns tillgängliga som ISO-filer för direkt nedladdning. De flesta
        användare behöver inte all mjukvara som finns tillgänglig på
        avbildningarna, så för att spara utrymme på nedladdningsservrar och
        speglingar finns endast de kompletta uppsättningarna tillgängliga via
        jigdo.
    </li>
</ul>

<p>
<strong>Efter att du har använt Debian-Installer</strong> ber vi att du sänder en
<a href="https://d-i.debian.org/manual/sv.amd64/ch05s04.html#submit-bug">
installationsrapport</a>,
även om du inte stötte på några problem.
</p>

<h1>Dokumentation</h1>

<p>
<strong>Om du bara läser ett dokument</strong> innan du installerar, läs vår
<a href="https://d-i.debian.org/manual/sv.amd64/apa.html">installationsguide</a>,
en snabb genomgång av installationsprocessen.
Andra nyttiga dokument är bland andra:
</p>

<ul>
<li>Installationsguide:
# <a href="$(HOME)/releases/stable/installmanual">version för
# den nuvarande utgåvan</a> &ndash;
 <a href="$(HOME)/releases/testing/installmanual">utvecklingsversion (testing)</a> &ndash;
    <a href="https://d-i.debian.org/manual/">senaste version (Git)</a>
<br />
innehåller en utförlig installationsvägledning</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">Debian-Installers FAQ</a><br />
ofta ställda frågor med svar</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Debian-Installers Wiki</a><br />
dokumentation som underhålls av användarna</li>
</ul>

<h1>Kontakta oss</h1>

<p>
<a href="https://lists.debian.org/debian-boot/">Sändlistan debian-boot</a> är
det primära forumet för diskussioner om och arbete på Debian-Installer.
</p>

<p>
Vi har dessutom en IRC-kanal, #debian-boot på <tt>irc.debian.org</tt>.
Kanalen används främst för utveckling, men ibland även för support.
Om du inte får ett svar bör du använda sändlistan istället.
</p>
