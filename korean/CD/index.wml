#use wml::debian::cdimage title="데비안 CD/DVD" BARETITLE=true
#use wml::debian::release_info
#use wml::debian::translation-check translation="a05068516728944fc749fdff35f34cb5dc885d01" maintainer="Seunghun Han (kkamagui)"

<p>데비안 CD/DVD나 USB 메모리를 찾고 있다면, 아래의 선택 가능한 옵션을
보세요.
문제가 생기면 <a href="faq/">데비안 CD/DVD에 대한 FAQ</a>를 보세요.</p>

<div class="tip">
<p>여러분이 그저 데비안을 설치하고 컴퓨터를 인터넷에 연결하고 싶다면,
다운로드하는 크기가 작은 <a href="netinst/">네트워크 설치</a> 미디어를 고려해 보세요.
</p> </div>

<div class="tip">
<p>i386 및 AMD64 아키텍처라면 모든 CD/DVD 이미지를
<a href="https://www.debian.org/CD/faq/#write-usb">USB 메모리</a>에 넣어서
사용할 수도 있습니다.</div>

<ul>

  <li><a href="http-ftp/">HTTP를 이용해서 CD/DVD를 받으세요.</a>
  수많은 미러 서버가 HTTP를 통해 직접 다운로드하는 링크를 제공하며 브라우저나
  명령행 도구로 다운로드할 수 있습니다.
  </li>

  <li><a href="vendors/">데비안 미디어를 사세요.</a> 가격이 쌉니다. 참고로 우리는
  이를 통해 어떤 이득도 얻지 않습니다!
  여러분이 종량제 인터넷을 쓴다면, 데비안 미디어를 사는 것이 유일한
  선택지입니다.
  여러분이 아주 느린 인터넷을 쓰고 있다면, 역시 미디어를 사는 걸 고려하는게
  좋습니다. 이미지를 모두 다운로드하는데 아주 오래 걸리거든요.
  </li>

  <li><a href="jigdo-cd/">jigdo로 CD/DVD 이미지를 받으세요.</a>
  "jigdo"의 체계는 전세계의 300개 데비안 미러 서버 중 가장 빠른 것을
  고를 수 있게 해줍니다.
  이 기능을 사용하면 미러 서버를 손쉽게 선택할 수 있고, 오래된 이미지를 최신
  이미지로 쉽게 "업그레이드"할 수 있죠.
  또, <em>모든</em> 아키텍처의 데비안 DVD 이미지를 다운로드할 수 있는 유일한
  방법이기도 합니다.
  </li>

  <li><a href="torrent-cd/">비트토런트로 CD/DVD 이미지를 받으세요.</a>
  비트토런트는 우리 서버의 부하를 최소화하면서, 수많은 사용자들이
  협력해서 동시에 이미지를 다운로드하게 해주는 친구와 친구를 연결하는
  시스템입니다. 다만, 몇몇 아키텍처만 DVD 이미지가 제공됩니다.
  </li>

  <li><a href="live/">HTTP, FTP나 비트토런트(BitTorrent)를 써서 라이브
  이미지를 받으세요.</a>
  라이브 이미지는 표준 이미지의 새로운 대안으로 제공되며, 데비안을 먼저 사용해
  본 후 설치할 수 있습니다.
  </li>

</ul>

<p>공식 CD/DVD 릴리스는 서명이 되어 있어서 믿을 수 있는지
<a href="verify">검증</a>할 수 있습니다.</p>

<p>데비안은 다른 컴퓨터 아키텍처에서도 가능합니다. 참고로 여러분의 컴퓨터에 맞는
이미지를 가지고 있는지 보세요!
대부분의 사람은 <q>AMD64</q>용 이미지, 64비트 PC 호환 시스템을 위한 이미지가
필요합니다.
여러분이 여러분만의 디스크를 만들었다면, <a
href="artwork/">데비안 디스크 커버를 위한 삽화</a>에도 관심있을 겁니다.
</p>

      <div class="cdflash" id="latest">"안정" CD/DVD 이미지의 최신 공식 릴리스:
        <strong><current-cd-release></strong>.
      <br><small>("시험" 배포판의 스냅샷은 매주 만들어짐)</small></div>

<p>설치와 관련된 알려진 문제는
<a href="$(HOME)/releases/stable/debian-installer/">설치 정보</a>
페이지에서 찾을 수 있습니다.<br>
</p>
