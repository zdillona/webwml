# Translation of others.sk.po
# Copyright (C)
# This file is distributed under the same license as the webwml package.
# Ivan Masár <helix84@centrum.sk>, 2009, 2011, 2012, 2013, 2017.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"Report-Msgid-Bugs-To: debian-www@lists.debian.org\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2017-07-12 16:39+0200\n"
"Last-Translator: Ivan Masár <helix84@centrum.sk>\n"
"Language-Team: x\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Virtaal 0.7.1\n"

#: ../../english/devel/join/nm-steps.inc:7
msgid "New Members Corner"
msgstr "Kútik Nových členov"

#: ../../english/devel/join/nm-steps.inc:10
msgid "Step 1"
msgstr "Krok 1"

#: ../../english/devel/join/nm-steps.inc:11
msgid "Step 2"
msgstr "Krok 2"

#: ../../english/devel/join/nm-steps.inc:12
msgid "Step 3"
msgstr "Krok 3"

#: ../../english/devel/join/nm-steps.inc:13
msgid "Step 4"
msgstr "Krok 4"

#: ../../english/devel/join/nm-steps.inc:14
msgid "Step 5"
msgstr "Krok 5"

#: ../../english/devel/join/nm-steps.inc:15
msgid "Step 6"
msgstr "Krok 6"

#: ../../english/devel/join/nm-steps.inc:16
msgid "Step 7"
msgstr "Krok 7"

#: ../../english/devel/join/nm-steps.inc:19
msgid "Applicants' checklist"
msgstr "Kontrolný zoznam uchádzača"

#: ../../english/devel/website/tc.data:11
msgid ""
"See <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/</a> "
"(only available in French) for more information."
msgstr ""
"Ďalšie informácie nájdete na <a href=\"m4_HOME/intl/french/\">http://www."
"debian.org/intl/french/</a> (iba vo francúzštine)."

#: ../../english/devel/website/tc.data:12
#: ../../english/devel/website/tc.data:14
#: ../../english/devel/website/tc.data:15
#: ../../english/devel/website/tc.data:16
#: ../../english/events/merchandise.def:145
msgid "More information"
msgstr "Ďalšie informácie"

#: ../../english/devel/website/tc.data:13
msgid ""
"See <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/</"
"a> (only available in Spanish) for more information."
msgstr ""
"Ďalšie informácie nájdete na <a href=\"m4_HOME/intl/spanish/\">http://www."
"debian.org/intl/spanish/</a> (iba v španielčine)."

#: ../../english/distrib/pre-installed.defs:18
msgid "Phone"
msgstr "Telefón"

#: ../../english/distrib/pre-installed.defs:19
msgid "Fax"
msgstr "Fax"

#: ../../english/distrib/pre-installed.defs:21
msgid "Address"
msgstr "Adresa"

#: ../../english/events/merchandise.def:13
msgid "Products"
msgstr "Produkty"

#: ../../english/events/merchandise.def:16
msgid "T-shirts"
msgstr "Tričká"

#: ../../english/events/merchandise.def:19
msgid "hats"
msgstr "klobúky"

#: ../../english/events/merchandise.def:22
msgid "stickers"
msgstr "samolepky"

#: ../../english/events/merchandise.def:25
msgid "mugs"
msgstr "hrnčenky"

#: ../../english/events/merchandise.def:28
msgid "other clothing"
msgstr "iné oblečenie"

#: ../../english/events/merchandise.def:31
msgid "polo shirts"
msgstr "golfové tričká"

#: ../../english/events/merchandise.def:34
msgid "frisbees"
msgstr "frisbee"

#: ../../english/events/merchandise.def:37
msgid "mouse pads"
msgstr "podložky pod myš"

#: ../../english/events/merchandise.def:40
msgid "badges"
msgstr "odznaky"

#: ../../english/events/merchandise.def:43
msgid "basketball goals"
msgstr "basketbalové koše"

#: ../../english/events/merchandise.def:47
msgid "earrings"
msgstr "náušnice"

#: ../../english/events/merchandise.def:50
msgid "suitcases"
msgstr "kufre"

#: ../../english/events/merchandise.def:53
msgid "umbrellas"
msgstr "dáždniky"

#: ../../english/events/merchandise.def:56
msgid "pillowcases"
msgstr "obliečky na vankúše"

#: ../../english/events/merchandise.def:59
msgid "keychains"
msgstr "zväzky kľúčov"

#: ../../english/events/merchandise.def:62
msgid "Swiss army knives"
msgstr "Švajčiarske nožíky"

#: ../../english/events/merchandise.def:65
msgid "USB-Sticks"
msgstr "Kľúčenky USB"

#: ../../english/events/merchandise.def:80
msgid "lanyards"
msgstr "Šnúrky na krk"

#: ../../english/events/merchandise.def:83
msgid "others"
msgstr "iné"

#: ../../english/events/merchandise.def:90
msgid "Available languages:"
msgstr ""

#: ../../english/events/merchandise.def:107
msgid "International delivery:"
msgstr ""

#: ../../english/events/merchandise.def:118
msgid "within Europe"
msgstr ""

#: ../../english/events/merchandise.def:122
msgid "Original country:"
msgstr ""

#: ../../english/events/merchandise.def:187
msgid "Donates money to Debian"
msgstr "Daruje peniaze Debianu"

#: ../../english/events/merchandise.def:192
msgid "Money is used to organize local free software events"
msgstr ""
"Peniaze sa využívajú na organizovanie miestnych akcií slobodného softvéru"

#: ../../english/logos/index.data:6
msgid "With&nbsp;``Debian''"
msgstr "S&nbsp;`„Debianom“"

#: ../../english/logos/index.data:9
msgid "Without&nbsp;``Debian''"
msgstr "Bez&nbsp;`„Debianu“"

#: ../../english/logos/index.data:12
msgid "Encapsulated PostScript"
msgstr "Zapúzdrený PostScript"

#: ../../english/logos/index.data:18
msgid "[Powered by Debian]"
msgstr "[Funguje na Debiane]"

#: ../../english/logos/index.data:21
msgid "[Powered by Debian GNU/Linux]"
msgstr "[Funguje na Debian GNU/Linux]"

#: ../../english/logos/index.data:24
msgid "[Debian powered]"
msgstr "[Funguje na Debiane]"

#: ../../english/logos/index.data:27
msgid "[Debian] (mini button)"
msgstr "[Debian] (minitlačidlo)"

#: ../../english/mirror/submit.inc:7
msgid "same as the above"
msgstr "rovnako ako vyššie"

#: ../../english/women/profiles/profiles.def:24
msgid "How long have you been using Debian?"
msgstr "Ako dlho už používate Debian?"

#: ../../english/women/profiles/profiles.def:27
msgid "Are you a Debian Developer?"
msgstr "Ste vývojár Debianu?"

#: ../../english/women/profiles/profiles.def:30
msgid "What areas of Debian are you involved in?"
msgstr "V akých oblastiach Debianu sa angažujete?"

#: ../../english/women/profiles/profiles.def:33
msgid "What got you interested in working with Debian?"
msgstr "Čo vás zaujalo natoľko, že ste začali pracovať s Debianom?"

#: ../../english/women/profiles/profiles.def:36
msgid ""
"Do you have any tips for women interested in getting more involved with "
"Debian?"
msgstr ""
"Máte nejaké tipy pre ženy, ktoré majú záujem viac sa angažovať v Debiane?"

#: ../../english/women/profiles/profiles.def:39
msgid ""
"Are you involved with any other women in technology group? Which one(s)?"
msgstr ""
"Angažujete sa v iných skupinách žien v technologickom odvetví? V ktorých?"

#: ../../english/women/profiles/profiles.def:42
msgid "A bit more about you..."
msgstr "Niečo viac o vás..."

#~ msgid "Where:"
#~ msgstr "Kde:"

#~ msgid "Specifications:"
#~ msgstr "Špecifikácie:"

#~ msgid "Architecture:"
#~ msgstr "Architektúra:"

#~ msgid "Who:"
#~ msgstr "Kto:"

#~ msgid "Wanted:"
#~ msgstr "Hľadá sa:"

#~ msgid "URL"
#~ msgstr "URL"

#~ msgid "Version"
#~ msgstr "Verzia"

#~ msgid "Status"
#~ msgstr "Stav"

#~ msgid "Package"
#~ msgstr "Balík"

#~ msgid "ALL"
#~ msgstr "VŠETKY"

#~ msgid "Unknown"
#~ msgstr "Neznáme"

#~ msgid "??"
#~ msgstr "??"

#~ msgid "BAD?"
#~ msgstr "ZLÉ?"

#~ msgid "OK?"
#~ msgstr "OK?"

#~ msgid "BAD"
#~ msgstr "ZLÉ"

#~ msgid "OK"
#~ msgstr "OK"

#~ msgid "Old banner ads"
#~ msgstr "Staré reklamné bannery"

#~ msgid "Download"
#~ msgstr "Stiahnuť"

#~ msgid "Unavailable"
#~ msgstr "Nedostupné"

#~ msgid "<void id=\"d-i\" />Unknown"
#~ msgstr "<void id=\"d-i\" />Neznáme"

#~ msgid "No images"
#~ msgstr "Bez obrazov"

#~ msgid "No kernel"
#~ msgstr "Bez jadra"

#~ msgid "Not yet"
#~ msgstr "Zatiaľ nie"

#~ msgid "Building"
#~ msgstr "Zostavuje"

#~ msgid "Booting"
#~ msgstr "Zavádza sa"

#~ msgid "sarge (broken)"
#~ msgstr "sarge (nefunguje)"

#~ msgid "sarge"
#~ msgstr "sarge"

#~ msgid "Working"
#~ msgstr "Funguje"
