#use wml::debian::template title="Debian-Logos" BARETITLE=true
#include "$(ENGLISHDIR)/logos/index.data"
#use wml::debian::translation-check translation="97732ca8593e39ce8b981adf7f81657417b62c73"
# $Id$
# Translator: Joerg Friedrich <Joerg.Dieter.Friedrich@uni-konstanz.de>
# Updated: Thimo Neubauer <thimo@debian.org>
# Updated: Martin Schulze <joey@debian.org>
# Updated: Holger Wansing <linux@wansing-online.de>, 2011, 2012, 2015, 2017.
# Updated: Holger Wansing <hwansing@mailbox.org>, 2020.

<p>Debian hat zwei Logos. Das <a href="#open-use">offizielle Logo</a> (auch
   bekannt als <q>Logo zur freien Verwendung</q>) enthält den wohlbekannten
   Debian-<q>Wirbel</q> und repräsentiert am Besten die visuelle Identität
   des Debian-Projekts. Es existiert auch ein separates
   <a href="#restricted-use">Logo mit eingeschränkten Bedingungen für die
   Verwendung</a>, das nur vom Debian-Projekt und seinen Mitgliedern genutzt
   werden darf. Um sich auf Debian zu beziehen, bevorzugen Sie bitte das
   offizielle Logo.</p>

<hr>

<table cellspacing="0" cellpadding="5" width="100%">
<colgroup span="2">
<col width="65%" />
<col width="35%" />
</colgroup>
<tr>
<th colspan="2"><a name="open-use">Debian-Logo zur freien Verwendung</a></th>
</tr>
<tr>
<td>

<p>Das Debian-Logo zur freien Verwendung gibt es in zwei Varianten,
   mit und ohne &ldquo;Debian&rdquo;-Label.</p>

<p>Die Debian-Logos zur freien Verwendung stehen unter
   Copyright (c) 1999 <a href="https://www.spi-inc.org/">Software in the
   Public Interest, Inc.</a>, und werden unter den Bedingungen der
   <a href="https://www.gnu.org/copyleft/lgpl.html">GNU Lesser General Public
   License</a> (Version 3 oder irgendeiner späteren Version) oder, nach Ihrer
   Wahl, der <a href="https://creativecommons.org/licenses/by-sa/3.0/">Creative
   Commons Attribution-ShareAlike 3.0 Unported License</a> veröffentlicht.</p>

<p>Bemerkung: Wir würden es begrüßen, wenn das Bild ein Link nach
<a href="$(HOME)">https://www.debian.org/</a>
wäre, falls Sie es für eine Webseite verwenden.</p>

</td>
<td>
<openlogotable>
</td>
</tr>
</table>

<hr />

<table cellspacing="0" cellpadding="5" width="100%">
<colgroup span="2">
<col width="65%" />
<col width="35%" />
</colgroup>
<tr>
<th colspan="2"><a name="restricted-use">Debian-Logo mit eingeschränkten Nutzungsbedingungen</a></th>
</tr>
<tr>
<td>
<h3>Lizenz für das Debian-Logo mit eingeschränkten Nutzungsbedingungen</h3>

<p>Copyright (c) 1999 Software in the Public Interest</p>
<ol>
   <li>Dieses Logo darf nur verwendet werden, wenn:
       <ul>
          <li>das Produkt, für das es verwendet wird, nach einer
	  auf www.debian.org veröffentlichten Methode erstellt
	  wurde (z.B. für die Erstellung offizieller CDs) oder</li>
          <li>wir die Erlaubnis zur Benutzung geben.</li>
       </ul>
   </li>
   <li>Das Logo darf auch verwendet werden, wenn ein offizieller Teil
   von Debian (nach den Regeln von 1.) einen Teil eines anderen Produkts
   darstellt, solange klar ist, dass nur dieser Teil offiziell
   autorisiert ist.</li>
   <li>Wir behalten uns das Recht vor, einem Produkt die Lizenz zu entziehen.</li>
</ol>
<p>Es wurde die Erlaubnis gegeben, das <q>Debian-Logo mit eingeschränkten
   Nutzungsbedingungen</q> auf
Kleidungsstücken (Hemden, T-Shirts, Hüte, usw.) zu
verwenden, so lange sie von einem Debian-Entwickler produziert werden
und nicht für Profit verkauft werden.</p>
</td>
<td>
<officiallogotable>
</td>
</tr>
</table>

<hr />

<h2>Über die Debian-Logos</h2>
<p>
Die Debian-Logos wurden bei einer Abstimmung der Debian-Entwickler 1999
ausgewählt. Sie wurden von <a href="mailto:rsilva@debian.org">Raul Silva</a>
entworfen. Der rote Farbton in der Schrift ist nominell <strong>Rubine Red 2X CVC</strong>.
Aktuelles Equivalent dazu sind entweder PANTONE Strong Red C (Rendering in RGB als #CE0056)
oder PANTONE Rubine Red C (Rendering in RGB als #CE0058).
Weitere Details über die Logos und Gründe dafür, warum PANTONE Rubine Red 2X CVC
ersetzt wurde sowie Infos über andere Equivalente der roten Farbe finden Sie auf der
<a href="https://wiki.debian.org/DebianLogo">Logo-Seite</a> im Debian-Wiki.</p>

<h2>Andere werbewirksame Grafiken</h2>

<h3>Debian-Buttons</h3>

<p><img class="ico" src="button-1.gif" alt="[Debian GNU/Linux]">Dies ist der erste Button für das
Projekt. Die Lizenz ist die gleiche wie die für das Logo zur freien Verwendung. Dieser
Button wurde von <a href="mailto:csmall@debian.org">Craig
Small</a> entworfen.</p>

<p>Hier sind einige weitere Button, die für Debian erstellt wurden:</p>
<br />
<morebuttons>

<h3>Debians Vielfältigkeits-Logo</h3>

<p>Wir haben eine Variation des Debian-Logos, um die Vielfältigkeit innerhalb
unserer Gemeinschaft zu unterstreichen, es nennt sich Debian Diversity Logo:
<br/>
<img src="diversity-2019.png" alt="[Debian Diversity logo]" />
<br/>
Das Logo wurde von <a href="https://wiki.debian.org/ValessioBrito">Valessio Brito</a>
erstellt und ist unter der GPLv3 lizensiert.
Der Quellcode (SVG-Format) ist im <a href="https://gitlab.com/valessiobrito/artwork">Git-Repository</a>
des Autors verfügbar.
<br />
</p>

<h3>Hexagonaler Debian-Sticker</h3>

<p>Dies ist ein Sticker, der den
<a href="https://github.com/terinjokes/StickerConstructorSpec">Spezifikationen
für hexagonale Sticker</a> entspricht:
<br/>
<img src="hexagonal.png" alt="[Debian GNU/Linux]" />
<br/>
Die Quelldatei (im SVG-Format) und ein Makefile zum Erstellen von
png- und svg-Vorschaubildern sind im
<a href="https://salsa.debian.org/debian/debian-flyers/tree/master/hexagonal-sticker">Debian-Flyers-Repository</a>
zu finden.

Die Lizenz dieses Stickers ist gleichwertig zu der Lizenz des offiziellen Logos.
Der Sticker wurde erstellt von
<a href="mailto:valhalla@trueelena.org">Elena Grandi</a>.</p>
<br />

#
# Logo Schriftart: Poppl Laudatio Condensed
#
# https://lists.debian.org/debian-www/2003/08/msg00261.html
#

<hr />

<p><small> Dies ist die deutsche Übersetzung der englischen
<a href="index.en.html">Originallizenzen</a>. In
Zweifelsfällen ist das englische Original maßgeblich.</small></p>
