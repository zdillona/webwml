# translation of templates.po to Arabic
# Isam Bayazidi <isam@bayazidi.net>, 2004, 2005.
# Ossama M. Khayat <okhayat@yahoo.com>, 2005.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2022-05-25 11:00+0000\n"
"Last-Translator: Med <medeb@protonmail.com>\n"
"Language-Team: Arabic <debian-l10n-arabic@lists.debian.org>\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && "
"n%100<=10 ? 3 : n%100>=11 ? 4 : 5;\n"
"X-Generator: Poedit 2.4.2\n"

#: ../../english/template/debian/basic.wml:19
#: ../../english/template/debian/navbar.wml:11
msgid "Debian"
msgstr "دبيان"

#: ../../english/template/debian/basic.wml:48
msgid "Debian website search"
msgstr "البحث في موقع دبيان"

#: ../../english/template/debian/common_translation.wml:4
msgid "Yes"
msgstr "نعم"

#: ../../english/template/debian/common_translation.wml:7
msgid "No"
msgstr "لا"

#: ../../english/template/debian/common_translation.wml:10
msgid "Debian Project"
msgstr "مشروع دبيان"

#: ../../english/template/debian/common_translation.wml:13
msgid ""
"Debian is an operating system and a distribution of Free Software. It is "
"maintained and updated through the work of many users who volunteer their "
"time and effort."
msgstr ""
"دبيان هو نظام تشغيل وتوزيعة تعتمد على البرمجيات الحرة. يقوم بإدارته وتحديثه "
"العديد من المستخدمين المتطوعين بوقتهم وجهدهم."

#: ../../english/template/debian/common_translation.wml:16
msgid "debian, GNU, linux, unix, open source, free, DFSG"
msgstr "دبيان، جنو، لينكس، يونكس، مصدر مفتوح، حر، DFSG"

#: ../../english/template/debian/common_translation.wml:19
msgid "Back to the <a href=\"m4_HOME/\">Debian Project homepage</a>."
msgstr "العودة إلى <a href=\"m4_HOME/\">الصفحة الرئيسية لمشروع دبيان</a>."

#: ../../english/template/debian/common_translation.wml:22
#: ../../english/template/debian/links.tags.wml:149
msgid "Home"
msgstr "الرئيسية"

#: ../../english/template/debian/common_translation.wml:25
msgid "Skip Quicknav"
msgstr "تخطي القائمة"

#: ../../english/template/debian/common_translation.wml:28
msgid "About"
msgstr "عن"

#: ../../english/template/debian/common_translation.wml:31
msgid "About Debian"
msgstr "عن دبيان"

#: ../../english/template/debian/common_translation.wml:34
msgid "Contact Us"
msgstr "راسلنا"

#: ../../english/template/debian/common_translation.wml:37
msgid "Legal Info"
msgstr "معلومات قانونية"

#: ../../english/template/debian/common_translation.wml:40
msgid "Data Privacy"
msgstr "سياسة الخصوصية"

#: ../../english/template/debian/common_translation.wml:43
msgid "Donations"
msgstr "تبرعات"

#: ../../english/template/debian/common_translation.wml:46
msgid "Events"
msgstr "أحداث"

#: ../../english/template/debian/common_translation.wml:49
msgid "News"
msgstr "الأخبار"

#: ../../english/template/debian/common_translation.wml:52
msgid "Distribution"
msgstr "توزيعة"

#: ../../english/template/debian/common_translation.wml:55
msgid "Support"
msgstr "الدعم"

#: ../../english/template/debian/common_translation.wml:58
msgid "Pure Blends"
msgstr "الأخلاط النقية"

#: ../../english/template/debian/common_translation.wml:61
#: ../../english/template/debian/links.tags.wml:46
msgid "Developers' Corner"
msgstr "رُكن المطورين"

#: ../../english/template/debian/common_translation.wml:64
msgid "Documentation"
msgstr "التوثيق"

#: ../../english/template/debian/common_translation.wml:67
msgid "Security Information"
msgstr "معلومات أمنية"

#: ../../english/template/debian/common_translation.wml:70
msgid "Search"
msgstr "بحث"

#: ../../english/template/debian/common_translation.wml:73
msgid "none"
msgstr "لا شيء"

#: ../../english/template/debian/common_translation.wml:76
msgid "Go"
msgstr "انطلق"

#: ../../english/template/debian/common_translation.wml:79
msgid "worldwide"
msgstr "العالم"

#: ../../english/template/debian/common_translation.wml:82
msgid "Site map"
msgstr "خريطة الموقع"

#: ../../english/template/debian/common_translation.wml:85
msgid "Miscellaneous"
msgstr "متفرقات"

#: ../../english/template/debian/common_translation.wml:88
#: ../../english/template/debian/links.tags.wml:104
msgid "Getting Debian"
msgstr "الحصول على دبيان"

#: ../../english/template/debian/common_translation.wml:91
msgid "The Debian Blog"
msgstr "مدونة دبيان"

#: ../../english/template/debian/common_translation.wml:94
msgid "Debian Micronews"
msgstr "أخبار هامشية"

#: ../../english/template/debian/common_translation.wml:97
msgid "Debian Planet"
msgstr "كوكب دبيان"

#: ../../english/template/debian/common_translation.wml:100
msgid "Last Updated"
msgstr "آخر تعديل"

#: ../../english/template/debian/ddp.wml:6
msgid ""
"Please send all comments, criticisms and suggestions about these web pages to "
"our <a href=\"mailto:debian-doc@lists.debian.org\">mailing list</a>."
msgstr ""
"يرجى إرسال التعليقات، والانتقادات والاقتراحات حول صفحات الوِب هذه إلى <a href="
"\"mailto:debian-doc@lists.debian.org\">قائمتنا البريدية</a>."

#: ../../english/template/debian/fixes_link.wml:11
msgid "not needed"
msgstr "غير مطلوب"

#: ../../english/template/debian/fixes_link.wml:14
msgid "not available"
msgstr "غير متوفر"

#: ../../english/template/debian/fixes_link.wml:17
msgid "N/A"
msgstr "غ‌م"

#: ../../english/template/debian/fixes_link.wml:20
msgid "in release 1.1"
msgstr "في الإصدار 1.1"

#: ../../english/template/debian/fixes_link.wml:23
msgid "in release 1.3"
msgstr "في الإصدار 1.3"

#: ../../english/template/debian/fixes_link.wml:26
msgid "in release 2.0"
msgstr "في الإصدار 2.0"

#: ../../english/template/debian/fixes_link.wml:29
msgid "in release 2.1"
msgstr "في الإصدار 2.1"

#: ../../english/template/debian/fixes_link.wml:32
msgid "in release 2.2"
msgstr "في الإصدار 2.2"

#: ../../english/template/debian/footer.wml:84
msgid ""
"See our <a href=\"m4_HOME/contact\">contact page</a> to get in touch. Web "
"site source code is <a href=\"https://salsa.debian.org/webmaster-team/webwml"
"\">available</a>."
msgstr ""
"للتواصل معنا راجع <a href=\"m4_HOME/contact\">صفحة الاتصال</a>. الشفرة "
"المصدرية للموقع <a href=\"https://salsa.debian.org/webmaster-team/webwml"
"\">متوفرة</a>."

#: ../../english/template/debian/footer.wml:87
msgid "Last Modified"
msgstr "آخر تعديل"

#: ../../english/template/debian/footer.wml:90
msgid "Last Built"
msgstr "آخر بناء"

#: ../../english/template/debian/footer.wml:93
msgid "Copyright"
msgstr "حقوق النشر"

#: ../../english/template/debian/footer.wml:96
msgid "<a href=\"https://www.spi-inc.org/\">SPI</a> and others;"
msgstr "<a href=\"https://www.spi-inc.org/\">SPI</a> و آخرون؛"

#: ../../english/template/debian/footer.wml:99
msgid "See <a href=\"m4_HOME/license\" rel=\"copyright\">license terms</a>"
msgstr "راجع <a href=\"m4_HOME/license\" rel=\"copyright\">بنود الترخيص</a>"

#: ../../english/template/debian/footer.wml:102
msgid ""
"Debian is a registered <a href=\"m4_HOME/trademark\">trademark</a> of "
"Software in the Public Interest, Inc."
msgstr ""
"دبيان <a href=\"m4_HOME/trademark\">علامة تجارية</a> لـ Software in the "
"Public Interest, Inc."

#: ../../english/template/debian/languages.wml:196
#: ../../english/template/debian/languages.wml:232
msgid "This page is also available in the following languages:"
msgstr "هذه الصفحة متوفرة أيضا باللغات التالية:"

#: ../../english/template/debian/languages.wml:265
msgid "How to set <a href=m4_HOME/intro/cn>the default document language</a>"
msgstr "كيفية تعيين <a href=m4_HOME/intro/cn>لغة مبدئية للمستند</a>"

#: ../../english/template/debian/languages.wml:323
msgid "Browser default"
msgstr "الإعدادات المبدئية للمتصفح"

#: ../../english/template/debian/languages.wml:323
msgid "Unset the language override cookie"
msgstr "تجاوز الكوكي الذي يفرض اللغة"

#: ../../english/template/debian/links.tags.wml:4
msgid "Debian International"
msgstr "دبيان الدولية"

#: ../../english/template/debian/links.tags.wml:7
msgid "Partners"
msgstr "الشركاء"

#: ../../english/template/debian/links.tags.wml:10
msgid "Debian Weekly News"
msgstr "أخبار دبيان الأسبوعية"

#: ../../english/template/debian/links.tags.wml:13
msgid "Weekly News"
msgstr "الأخبار الأسبوعية"

#: ../../english/template/debian/links.tags.wml:16
msgid "Debian Project News"
msgstr "أخبار مشروع دبيان"

#: ../../english/template/debian/links.tags.wml:19
msgid "Project News"
msgstr "أخبار المشروع"

#: ../../english/template/debian/links.tags.wml:22
msgid "Release Info"
msgstr "معلومات عن الإصدار"

#: ../../english/template/debian/links.tags.wml:25
msgid "Debian Packages"
msgstr "حزم دبيان"

#: ../../english/template/debian/links.tags.wml:28
msgid "Download"
msgstr "تنزيل"

#: ../../english/template/debian/links.tags.wml:31
msgid "Debian&nbsp;on&nbsp;CD"
msgstr "دبيان&nbsp;على&nbsp;القرص المضغوط CD"

#: ../../english/template/debian/links.tags.wml:34
msgid "Debian Books"
msgstr "كتب دبيان"

#: ../../english/template/debian/links.tags.wml:37
msgid "Debian Wiki"
msgstr "ويكي دبيان"

#: ../../english/template/debian/links.tags.wml:40
msgid "Mailing List Archives"
msgstr "أرشيف القوائم البريدية"

#: ../../english/template/debian/links.tags.wml:43
msgid "Mailing Lists"
msgstr "القوائم البريدية"

#: ../../english/template/debian/links.tags.wml:49
msgid "Social Contract"
msgstr "العقد الاجتماعي"

#: ../../english/template/debian/links.tags.wml:52
msgid "Code of Conduct"
msgstr "قواعد السلوك"

#: ../../english/template/debian/links.tags.wml:55
msgid "Debian 5.0 - The universal operating system"
msgstr "دبيان 5.0 - نظام التشغيل الشامل"

#: ../../english/template/debian/links.tags.wml:58
msgid "Site map for Debian web pages"
msgstr "خريطة صفحات موقع دبيان"

#: ../../english/template/debian/links.tags.wml:61
msgid "Developer Database"
msgstr "قاعدة بيانات المطورين"

#: ../../english/template/debian/links.tags.wml:64
msgid "Debian FAQ"
msgstr "أسئلة دبيان الشائعة"

#: ../../english/template/debian/links.tags.wml:67
msgid "Debian Policy Manual"
msgstr "كتيّب سياسة دبيان"

#: ../../english/template/debian/links.tags.wml:70
msgid "Developers' Reference"
msgstr "مرجع المطورين"

#: ../../english/template/debian/links.tags.wml:73
msgid "New Maintainers' Guide"
msgstr "دليل المشرفين الجدد"

#: ../../english/template/debian/links.tags.wml:76
msgid "Release Critical Bugs"
msgstr "العلاّت الحرجة"

#: ../../english/template/debian/links.tags.wml:79
msgid "Lintian Reports"
msgstr "تقارير Lintian"

#: ../../english/template/debian/links.tags.wml:83
msgid "Archives for users' mailing lists"
msgstr "أرشيف القوائم البريدية للمستخدمين"

#: ../../english/template/debian/links.tags.wml:86
msgid "Archives for developers' mailing lists"
msgstr "أرشيف القوائم البريدية للمطورين"

#: ../../english/template/debian/links.tags.wml:89
msgid "Archives for i18n/l10n mailing lists"
msgstr "أرشيف القوائم البريدية الخاصة بالترجمة والتوطين i18n/l10n"

#: ../../english/template/debian/links.tags.wml:92
msgid "Archives for ports' mailing lists"
msgstr "أرشيف القوائم البريدية للنُّقول"

#: ../../english/template/debian/links.tags.wml:95
msgid "Archives for mailing lists of the Bug tracking system"
msgstr "أرشيف القوائم البريدية لنظام تتبّع العلاّت"

#: ../../english/template/debian/links.tags.wml:98
msgid "Archives for miscellaneous mailing lists"
msgstr "أرشيف القوائم البريدية المتفرقة"

#: ../../english/template/debian/links.tags.wml:101
msgid "Free Software"
msgstr "البرمجيات الحرة"

#: ../../english/template/debian/links.tags.wml:107
msgid "Development"
msgstr "تطوير"

#: ../../english/template/debian/links.tags.wml:110
msgid "Help Debian"
msgstr "مساعدة دبيان"

#: ../../english/template/debian/links.tags.wml:113
msgid "Bug reports"
msgstr "تقارير العلاّت"

#: ../../english/template/debian/links.tags.wml:116
msgid "Ports/Architectures"
msgstr "النُّقول/البُنى"

#: ../../english/template/debian/links.tags.wml:119
msgid "Installation manual"
msgstr "كتيّب التثبيت"

#: ../../english/template/debian/links.tags.wml:122
msgid "CD vendors"
msgstr "باعة الأقراص المضغوطة"

#: ../../english/template/debian/links.tags.wml:125
msgid "CD/USB ISO images"
msgstr "صور CD/USB ISO"

#: ../../english/template/debian/links.tags.wml:128
msgid "Network install"
msgstr "التثبيت الشبَكي"

#: ../../english/template/debian/links.tags.wml:131
msgid "Pre-installed"
msgstr "سابق التثبيت"

#: ../../english/template/debian/links.tags.wml:134
msgid "Debian-Edu project"
msgstr "مشروع Debian-Edu"

#: ../../english/template/debian/links.tags.wml:137
msgid "Salsa &ndash; Debian Gitlab"
msgstr ""

#: ../../english/template/debian/links.tags.wml:140
msgid "Quality Assurance"
msgstr "ضمان الجودة"

#: ../../english/template/debian/links.tags.wml:143
msgid "Package Tracking System"
msgstr "نظام تتبّع الحزم"

#: ../../english/template/debian/links.tags.wml:146
msgid "Debian Developer's Packages Overview"
msgstr "عرض عام لحزم أحد مطوري دبيان"

#: ../../english/template/debian/navbar.wml:10
msgid "Debian Home"
msgstr "صفحة دبيان الرئيسية"

#: ../../english/template/debian/recent_list.wml:7
msgid "No items for this year."
msgstr "لا عناصر هذا العام."

#: ../../english/template/debian/recent_list.wml:11
msgid "proposed"
msgstr "مقترح"

#: ../../english/template/debian/recent_list.wml:15
msgid "in discussion"
msgstr "في نقاش"

#: ../../english/template/debian/recent_list.wml:19
msgid "voting open"
msgstr "التصويت مفتوح"

#: ../../english/template/debian/recent_list.wml:23
msgid "finished"
msgstr "انتهى"

#: ../../english/template/debian/recent_list.wml:26
msgid "withdrawn"
msgstr "مسحوب"

#: ../../english/template/debian/recent_list.wml:30
msgid "Future events"
msgstr "أحداث مرتقبة"

#: ../../english/template/debian/recent_list.wml:33
msgid "Past events"
msgstr "أحداث ماضية"

#: ../../english/template/debian/recent_list.wml:37
msgid "(new revision)"
msgstr "(مراجعة جديدة)"

#: ../../english/template/debian/recent_list.wml:329
msgid "Report"
msgstr "تقرير"

#: ../../english/template/debian/redirect.wml:6
msgid "Page redirected to <newpage/>"
msgstr "أعيد توجيه الصفحة إلى <newpage/>"

#: ../../english/template/debian/redirect.wml:14
msgid ""
"This page has been renamed to <url <newpage/>>, please update your links."
msgstr "أعيدت تسمية هذه الصفحة إلى <url <newpage/>>، المرجو تحديت هذه الوصلة."

#. given a manual name and an architecture, join them
#. if you need to reorder the two, use "%2$s ... %1$s", cf. printf(3)
#: ../../english/template/debian/release.wml:7
msgid "<void id=\"doc_for_arch\" />%s for %s"
msgstr "<void id=\"doc_for_arch\" />%s لـ %s"

#: ../../english/template/debian/translation-check.wml:37
msgid ""
"<em>Note:</em> The <a href=\"$link\">original document</a> is newer than this "
"translation."
msgstr ""
"<em>ملاحظة:</em> <a href=\"$link\">المستند الأصلي</a> احدث من هذه الترجمة."

#: ../../english/template/debian/translation-check.wml:43
msgid ""
"Warning! This translation is too out of date, please see the <a href=\"$link"
"\">original</a>."
msgstr ""
"تنبيه! هذه الترجمة قديمة، يرجى مراجعة <a href=\"$link\"> النسخة الأصلية</a>."

#: ../../english/template/debian/translation-check.wml:49
msgid ""
"<em>Note:</em> The original document of this translation no longer exists."
msgstr "<em>ملاحظة:</em> المستند الأصلي لهذه الترجمة لم يعد موجودا."

#: ../../english/template/debian/translation-check.wml:56
msgid "Wrong translation version!"
msgstr "إصدار الترجمة خاطئ!"

#: ../../english/template/debian/url.wml:4
msgid "URL"
msgstr "مسار"

#: ../../english/template/debian/users.wml:12
msgid "Back to the <a href=\"../\">Who's using Debian? page</a>."
msgstr "العودة إلى <a href=\"./\">صفحة من يستخدم دبيان؟</a>."

#: ../../english/search.xml.in:7
msgid "Debian website"
msgstr "موقع دبيان"

#: ../../english/search.xml.in:9
msgid "Search the Debian website."
msgstr "البحث في موقع دبيان."

#~ msgid ""
#~ "To report a problem with the web site, please e-mail our publicly archived "
#~ "mailing list <a href=\"mailto:debian-www@lists.debian.org\">debian-"
#~ "www@lists.debian.org</a> in English.  For other contact information, see "
#~ "the Debian <a href=\"m4_HOME/contact\">contact page</a>. Web site source "
#~ "code is <a href=\"https://salsa.debian.org/webmaster-team/webwml"
#~ "\">available</a>."
#~ msgstr ""
#~ "للتبليغ عن مشكل في الموقع، راسل <a href=\"mailto:debian-www@lists.debian."
#~ "org\">debian-www@lists.debian.org</a>.  للحصول على معلومات أخرى، راجع <a "
#~ "href=\"m4_HOME/contact\">صفحة الاتصال</a>. الكود المصدري للموقع <a href="
#~ "\"https://salsa.debian.org/webmaster-team/webwml\">متوفر</a>."
