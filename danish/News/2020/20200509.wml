#use wml::debian::translation-check translation="6547bb7720bfba1c2481b95c44642a4a6d3df030"
<define-tag pagetitle>Opdateret Debian 10: 10.4 udgivet</define-tag>
<define-tag release_date>2020-05-09</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.4</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Debian-projektet er stolt over at kunne annoncere den fjerde opdatering af 
dets stabile distribution, Debian <release> (kodenavn <q><codename></q>).
Denne opdatering indeholder primært rettelser af sikkerhedsproblemer i den 
stabile udgave, sammen med nogle få rettelser af alvorlige problemer.  
Sikkerhedsbulletiner er allerede udgivet separat og der vil blive refereret til 
dem, hvor de er tilgængelige.</p>

<p>Bemærk at denne opdatering ikke er en ny udgave af Debian GNU/Linux
<release>, den indeholder blot opdateringer af nogle af de medfølgende pakker.  
Der er ingen grund til at smide gamle <q><codename></q>-medier væk.  Efter en 
installering, kan pakkerne opgradere til de aktuelle versioner ved hjælp af et 
ajourført Debian-filspejl.</p>

<p>Dem der hyppigt opdaterer fra security.debian.org, behøver ikke at opdatere 
ret mange pakker, og de fleste opdateringer fra security.debian.org er indeholdt 
i denne opdatering.</p>

<p>Nye installeringsfilaftryk vil snart være tilgængelige fra de sædvanlige 
steder.</p>

<p>Opdatering af en eksisterende installation til denne revision, kan gøres ved 
at lade pakkehåndteringssystemet pege på et af Debians mange HTTP-filspejle. En 
omfattende liste over filspejle er tilgængelig på:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Forskellige fejlrettelser</h2>

<p>Denne opdatering til den stabile udgave tilføjer nogle få vigtige rettelser 
til følgende pakker:</p>

<table border=0>
<tr><th>Pakke</th>					<th>Årsag</th></tr>
<correction apt-cacher-ng 				"Håndhæver udløsning af sikret kald til serveren i vedligeholdelsesjob [CVE-2020-5202]; tillader .zst-komprimering af tarballs; forøger størrelsen på dekomprimeringslinjebufferen for læsning af opsætningsfil">
<correction backuppc 					"Overfører brugernavnet til start-stop-daemon når der genindlæses, forhindre genindlæsningsfejl">
<correction base-files 					"Opdaterer til punktopdateringen">
<correction brltty 					"Reducerer logmeddelelsers alvorhedsgrad for at undgå at generere for mange meddelelser, når benyttet sammen med nye Orca-versioner">
<correction checkstyle 					"Retter problem med XML External Entity-indsprøjtning [CVE-2019-9658 CVE-2019-10782]">
<correction choose-mirror 				"Opdaterer medfølgende filspejlsliste">
<correction clamav 					"Ny opstrømsudgivelse [CVE-2020-3123]">
<correction corosync 					"totemsrp: Reducerer MTU for at undgå generering af for store pakker">
<correction corosync-qdevice 				"Retter servicestart">
<correction csync2 					"Få HELLO-kommando til at fejle når SSL er krævet">
<correction cups 					"Retter heapbufferoverløb [CVE-2020-3898] og funktionen <q>ippReadIO</q> kan underlæse et extensionfelt [CVE-2019-8842]">
<correction dav4tbsync 					"Ny opstrømsudgivelse, genindfører kompabilitet med nyere versioner af Thunderbird">
<correction debian-edu-config 				"Tilføjer policy-filer til Firefox ESR og Thunderbird for at rette TLS-/SSL-opsætningen">
<correction debian-installer 				"Opdaterer til 4.19.0-9-kernens ABI">
<correction debian-installer-netboot-images 		"Genopbygget mod proposed-updates">
<correction debian-security-support 			"Ny stabil opstrømsudgivelse; opdaterer flere pakkers status; anvender <q>runuser</q> frem for <q>su</q>">
<correction distro-info-data 				"Tilføjer Ubuntu 20.10, og sandsandlig slutdata for support af stretch">
<correction dojo 					"Retter ukorrekt brug af regulært udtryk [CVE-2019-10785]">
<correction dpdk 					"Ny stabil opstrømsudgivelse">
<correction dtv-scan-tables 				"Nyt opstrømsøjebliksbillede; tilføjer alle aktuelle tyske DVB-T2-mux'er og satellitten Eutelsat-5-West-A">
<correction eas4tbsync 					"Ny opstrømsudgivelse, genindfører kompabilitet med nyere versioner af Thunderbird">
<correction edk2 					"Sikkerhedsrettelser [CVE-2019-14558 CVE-2019-14559 CVE-2019-14563 CVE-2019-14575 CVE-2019-14586 CVE-2019-14587]">
<correction el-api 					"Retter stretch- til buster-opgraderinger som involverer Tomcat 8">
<correction fex 					"Retter et potentielt sikkerhedsproblem i fexsrv">
<correction filezilla 					"Retter sårbarhed i forbindelse med søgesti der ikke er tillid til [CVE-2019-5429]">
<correction frr 					"Retter udvidet næste hop-kapabilitet">
<correction fuse 					"Fjerner forældede udevadm-kommandoer fra post-install-scripts; fjern ikke eksplicit fuse.conf ved purge">
<correction fuse3					"Fjerner forældede udevadm-kommandoer fra post-install-scripts; fjern ikke eksplicit fuse.conf ved purge; retter hukommelseslækage i fuse_session_new()">
<correction golang-github-prometheus-common 		"Forlænger testcertifikaters gyldighed">
<correction gosa 					"Erstatter (un)serialize med json_encode/json_decode for at afhjælpe PHP-objektindsprøjtning [CVE-2019-14466]">
<correction hbci4java 					"Understøtter EU-direktiv vedrørende betalingstjenester (PSD2)">
<correction hibiscus 					"Understøtter EU-direktiv vedrørende betalingstjenester (PSD2)">
<correction iputils 					"Korrigerer et problem hvor ping afbrød på ukorrekt vis med en fejlkode når der stadig var adresser som ikke var afprøvet i returværdien fra bibliotekskaldet getaddrinfo()">
<correction ircd-hybrid 				"Anvender dhparam.pem for at undgå nedbrud ved start">
<correction jekyll 					"Tillader anvendelse af ruby-i18n 0.x og 1.x">
<correction jsp-api 					"Retter stretch- til buster-opgraderinger som involverer Tomcat 8">
<correction lemonldap-ng 				"Forhindrer uønsket adgang til administrationsendpoints [CVE-2019-19791]; retter GrantSession-plugin der ikke forhindrede login når to-faktor-autentifikation blev anvendt; retter vilkårlige viderestillinger med OIDC hvis redirect_uri ikke blev anvendt">
<correction libdatetime-timezone-perl 			"Opdaterer medfølgende data">
<correction libreoffice 				"Retter OpenGL-slidetransitioner">
<correction libssh 					"Retter muligt lammelsesangrebsproblem ved håndtering af AES-CTR-nøgler med OpenSSL [CVE-2020-1730]">
<correction libvncserver 				"Retter heapoverløb [CVE-2019-15690]">
<correction linux 					"Ny stabil opstrømsudgivelse">
<correction linux-latest 				"Opdaterer kerne-ABI til 4.19.0-9">
<correction linux-signed-amd64 				"Ny stabil opstrømsudgivelse">
<correction linux-signed-arm64 				"Ny stabil opstrømsudgivelse">
<correction linux-signed-i386 				"Ny stabil opstrømsudgivelse">
<correction lwip 					"Retter bufferoverløb [CVE-2020-8597]">
<correction lxc-templates 				"Ny stabil opstrømsudgivelse; håndterer sprog som kun er UTF-8-kodet">
<correction manila 					"Retter manglende tjek af adgangsrettigheder [CVE-2020-9543]">
<correction megatools 					"Tilføjer understøttelse af mega.nz's nye linkformat">
<correction mew 					"Retter kontrol af SSL-servercertifikaters gyldighed">
<correction mew-beta 					"Retter kontrol af SSL-servercertifikaters gyldighed">
<correction mkvtoolnix 					"Genopbygget for at skærpe afhængighed af libmatroska6v5">
<correction ncbi-blast+ 				"Deaktiverer understøttelse af SSE4.2">
<correction node-anymatch 				"Fjerner unødvendige afhængigheder">
<correction node-dot 					"Forhindrer udførelse af kode efter prototypeforurening [CVE-2020-8141]">
<correction node-dot-prop 				"Retter prototypeforurening [CVE-2020-8116]">
<correction node-knockout 				"Retter escaping med ældre versioner af Internet Explorer [CVE-2019-14862]">
<correction node-mongodb 				"Afviser ugyldige _bsontypes [CVE-2019-2391 CVE-2020-7610]">
<correction node-yargs-parser 				"Retter prototypeforurening [CVE-2020-7608]">
<correction npm 					"Retter adgang til vilkårlig sti [CVE-2019-16775 CVE-2019-16776 CVE-2019-16777]">
<correction nvidia-graphics-drivers 			"Ny stabil opstrømsudgivelse">
<correction nvidia-graphics-drivers-legacy-390xx	"Ny stabil opstrømsudgivelse">
<correction nvidia-settings-legacy-340xx 		"Ny opstrømsudgivelse">
<correction oar 					"Går tilbage til stretchs virkemåde for perl-funktionen Storable::dclone, retter problemer med rekursionsdybde">
<correction opam 					"Foretrækker mccs over aspcud">
<correction openvswitch 				"Retter vswitchd-afbrydelse når en port tilføjes og controller'en er nede">
<correction orocos-kdl 					"Retter strengkonvertering med Python 3">
<correction owfs 					"Fjerner defekte Python 3-pakker">
<correction pango1.0 					"Retter nedbrud i pango_fc_font_key_get_variations() når nøgle er null">
<correction pgcli 					"Tilføjer manglende afhængighed af python3-pkg-resources">
<correction php-horde-data 				"Retter sårbarhed i forbindelse med autentificeret fjernudførelse af kode [CVE-2020-8518]">
<correction php-horde-form 				"Retter sårbarhed i forbindelse med autentificeret fjernudførelse af kode [CVE-2020-8866]">
<correction php-horde-trean 				"Retter sårbarhed i forbindelse med autentificeret fjernudførelse af kode [CVE-2020-8865]">
<correction postfix 					"Ny stabil opstrømsudgivelse; retter panik med Postfix' multi-Milter-opsætning under MAIL FROM; retter d/init.d-kørende ændring så den igen fungerer med multi-instance">
<correction proftpd-dfsg 				"Retter problem med hukommelsestilgang i keyboard-interative-kode i mod_sftp; håndterer DEBUG-, IGNORE-, DISCONNECT- og UNIMPLEMENTED-meddelelser på korrekt vis i keyboard-interactive-tilstand">
<correction puma 					"Retter problem med lammelsesangreb [CVE-2019-16770]">
<correction purple-discord 				"Retter nedbrud i ssl_nss_read">
<correction python-oslo.utils 				"Retter lækage af følsomme oplysninger gennem mistral-logninger [CVE-2019-3866]">
<correction rails 					"Retter mulig udførelse af skripter på tværs af websteder gennem Javascript-escapehelper [CVE-2020-5267]">
<correction rake 					"Retter kommandoindsprøjtningssårbarhed [CVE-2020-8130]">
<correction raspi3-firmware 				"Retter dtb-navneuoverensstemmelse i z50-raspi-firmware; retter boot på Raspberry Pi-familie 1 og 0">
<correction resource-agents 				"Retter ethmonitor viser ikke grænseflader uden tildelt IP-adresse; fjerner ikke længere krævet xen-toolstack-patch; retter ikke-standard anvendelse i ZFS-agent">
<correction rootskel 					"Deaktiverer understøttelse af flere konsoller hvis preseeding er i brug">
<correction ruby-i18n 					"Retter generering af gemspec">
<correction rubygems-integration 			"Undgår udfasningsadvarsler når brugere installerer en nyere version af Rubygems vha. <q>gem update --system</q>">
<correction schleuder 					"Forbedrer patch til håndtering af encoding-fejl opstået i tidligere version; skifter standardencoding til UTF-8; lader x-add-key håndtere mails med vedhæftede, quoted-printable-encoded nøgler; retter x-attach-listkey med mails oprettet af Thunderbird som indeholder beskyttede headere">
<correction scilab 					"Retter biblioteksindlæsning med OpenJDK 11.0.7">
<correction serverspec-runner 				"Understøtter Ruby 2.5">
<correction softflowd 					"Retter defekt flowopsamling hvilket kunne medføre overløb i flowtabel og 100 procents CPU-forbrug">
<correction speech-dispatcher 				"Retter default-pulseaudiolatency der udløser <q>skrattende</q> udgangssignal">
<correction spl-linux 					"Retter deadlock">
<correction sssd 					"Retter optagetløkket i sssd_be når LDAP-forbindelse er sporadisk">
<correction systemd 					"Ved autorisering gennem re-resolve callback/userdata i PolicyKit i stedet for at cache dem [CVE-2020-1712]; installerer 60-block.rules i udev-udeb og initramfs-tools">
<correction taglib 					"Retter korruptionsproblemer med OGG-filer">
<correction tbsync 					"Ny opstrømsudgivelse, genindfører kompabilitet med nyere versioner af Thunderbird">
<correction timeshift 					"Retter forudsigelig anvendelse af midlertidig mappe [CVE-2020-10174]">
<correction tinyproxy 					"Opsætter kun PIDDIR hvis PIDFILE er en streng som ikke har en længde på nul">
<correction tzdata 					"Ny stabil opstrømsudgivelse">
<correction uim 					"Afregistrerer moduler som ikke er installeret, retter en regression i en tidligere upload">
<correction user-mode-linux 				"Retter opbygningsfejl med aktuelle stabile kerner">
<correction vite 					"Retter nedbrud når der er flere end 32 elementer">
<correction waagent 					"Ny opstrømsudgave; understøtter samtidig installering med cloud-init">
<correction websocket-api 				"Retter stretch- til buster-opgraderinger som involverer Tomcat 8">
<correction wpa 					"Forsøg ikke at genkende PSK-uoverensstemmelse under PTK-rekeying; tjek for FT-understøttelse ved valg af FT-suiter; retter MAC-randomiseringsproblem med nogle kort">
<correction xdg-utils 					"xdg-open: Retter pcmanfm-tjek og håndtering af mapper med mellemrum i deres navne; xdg-screensaver: Renser vinduenavn før det sendes over D-Bus; xdg-mime: Opretter config-mappe hvis den ikke allerede findes">
<correction xtrlock 					"Retter blokering af (nogle) multitouch-enhed mens låst [CVE-2016-10894]">
<correction zfs-linux 					"Retter potentielle problemer med deadlock">
</table>


<h2>Sikkerhedsopdateringer</h2>

<p>Denne revision tilføjer følgende sikkerhedsopdateringer til den stabile 
udgave.  Sikkerhedsteamet har allerede udgivet bulletiner for hver af de nævnte
opdateringer:</p>

<table border=0>
<tr><th>Bulletin-id</th>  <th>Pakke(r)</th></tr>
<dsa 2020 4616 qemu>
<dsa 2020 4617 qtbase-opensource-src>
<dsa 2020 4618 libexif>
<dsa 2020 4619 libxmlrpc3-java>
<dsa 2020 4620 firefox-esr>
<dsa 2020 4623 postgresql-11>
<dsa 2020 4624 evince>
<dsa 2020 4625 thunderbird>
<dsa 2020 4627 webkit2gtk>
<dsa 2020 4629 python-django>
<dsa 2020 4630 python-pysaml2>
<dsa 2020 4631 pillow>
<dsa 2020 4632 ppp>
<dsa 2020 4633 curl>
<dsa 2020 4634 opensmtpd>
<dsa 2020 4635 proftpd-dfsg>
<dsa 2020 4636 python-bleach>
<dsa 2020 4637 network-manager-ssh>
<dsa 2020 4638 chromium>
<dsa 2020 4639 firefox-esr>
<dsa 2020 4640 graphicsmagick>
<dsa 2020 4641 webkit2gtk>
<dsa 2020 4642 thunderbird>
<dsa 2020 4643 python-bleach>
<dsa 2020 4644 tor>
<dsa 2020 4645 chromium>
<dsa 2020 4646 icu>
<dsa 2020 4647 bluez>
<dsa 2020 4648 libpam-krb5>
<dsa 2020 4649 haproxy>
<dsa 2020 4650 qbittorrent>
<dsa 2020 4651 mediawiki>
<dsa 2020 4652 gnutls28>
<dsa 2020 4653 firefox-esr>
<dsa 2020 4654 chromium>
<dsa 2020 4655 firefox-esr>
<dsa 2020 4656 thunderbird>
<dsa 2020 4657 git>
<dsa 2020 4658 webkit2gtk>
<dsa 2020 4659 git>
<dsa 2020 4660 awl>
<dsa 2020 4661 openssl>
<dsa 2020 4663 python-reportlab>
<dsa 2020 4664 mailman>
<dsa 2020 4665 qemu>
<dsa 2020 4666 openldap>
<dsa 2020 4667 linux-signed-amd64>
<dsa 2020 4667 linux-signed-arm64>
<dsa 2020 4667 linux-signed-i386>
<dsa 2020 4667 linux>
<dsa 2020 4669 nodejs>
<dsa 2020 4671 vlc>
<dsa 2020 4672 trafficserver>
</table>


<h2>Fjernede pakker</h2>

<p>Følgende pakker er blevet fjernet på grund af omstændigheder uden for vores 
kontrol:</p>

<table border=0>
<tr><th>Pakke</th>		<th>Årsag</th></tr>
<correction getlive 		"Defekt på grund af Hotmail-ændringer">
<correction gplaycli 		"Defekt på grund af Google API-ændringer">
<correction kerneloops 		"Opstrønstjeneste ikke længere tilgængelig">
<correction lambda-align2 	"[arm64 armel armhf i386 mips64el ppc64el s390x] Defekt på ikke-amd64-arkitekturer">
<correction libmicrodns 	"Sikkerhedsproblemer">
<correction libperlspeak-perl 	"Sikkerhedsproblemer; vedligeholdes ikke">
<correction quotecolors 	"Inkompatibel med nyere Thunderbird-versioner">
<correction torbirdy 		"Inkompatibel med nyere Thunderbird-versioner">
<correction ugene 		"Ikke-fri; kan ikke opbygges">
<correction yahoo2mbox 		"Defekt gennem flere år">
</table>


<h2>Debian Installer</h2>

<p>Installeringsprogrammet er opdateret for at medtage rettelser indført i stable, 
i denne punktopdatering.</p>


<h2>URL'er</h2>

<p>Den komplette liste over pakker, som er ændret i forbindelse med denne 
revision:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Den aktuelle stabile distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Foreslåede opdateringer til den stabile distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Oplysninger om den stabile distribution (udgivelsesbemærkninger, fejl, 
osv.):</p>

<div class="center">
  <a href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Sikkerhedsannonceringer og -oplysninger:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>


<h2>Om Debian</h2>

<p>Debian-projektet er en organisation af fri software-udviklere som frivilligt
bidrager med tid og kræfter, til at fremstille det helt frie styresystem Debian
GNU/Linux.</p>


<h2>Kontaktoplysninger</h2>

<p>For flere oplysninger, besøg Debians websider på 
<a href="$(HOME)/">https://www.debian.org/</a> eller send e-mail på engelsk til
&lt;press@debian.org&gt; eller kontakt holdet bag den stabile udgave på 
&lt;debian-release@debian.org&gt;.</p>
