#use wml::debian::translation-check translation="7ce619292bb7825470d6cb611887d68f405623ff"
<define-tag pagetitle>Debian 11 <q>bullseye</q> udgivet</define-tag>
<define-tag release_date>2021-08-14</define-tag>
#use wml::debian::news

<p>Efter to år, en måned og ni dages udvikling, er Debian-projektet stolt over 
at kunne præsentere sin nye stabile version 11 (kodenavn <q>bullseye</q>), som 
vil blive understøttet i de næste fem år takket være det kombinerede arbejde, 
der udføres af 
<a href="https://security-team.debian.org/">Debian Security-holdet</a> og 
<a href="https://wiki.debian.org/LTS">Debian Long Term Support</a>-holdet.</p>

<p>Debian 11 <q>bullseye</q> leveres med flere skrivebordsapplikationer og 
-miljøer.  Blandt andre er der nu følgende skrivebordsmiljøer:</p>

<ul>
<li>Gnome 3.38,</li>
<li>KDE Plasma 5.20,</li>
<li>LXDE 11,</li>
<li>LXQt 0.16,</li>
<li>MATE 1.24,</li>
<li>Xfce 4.16.</li>
</ul>

<p>Denne udgave indeholder flere end 11.294 nye pakker, så der i alt er 59.551 
pakker, sammen med en betydeligt reduktion af over 9.519 pakker, som blev 
markeret som <q>obsolete</q> (forældede) og er blevet fjernet.  42.821 pakker 
blev opdateret og 5.434 pakker forblev uændrede.</p>

<p><q>bullseye</q> er vores første udgivelse, som indeholder en Linux-kerne, der 
har understøttelse af filsystemet exFAT og benytter det som standard til at 
mount'e exFAT-filsystemer.  Som følge heraf er det ikke længere at krævet at 
benytte implementering af filsystem-i-brugerrummet, som leveres gennem pakken 
exfat-fuse.  Værktøjer til oprettelse og kontrol af et exFAT-filsystem leveres 
gennem pakke exfatprogs.</p>

<p>De fleste moderne printere er i stand til at benytte driverløs udskrivning og 
scanning, uden at der er behov for leverandørspecifikke drivere (som ofte er 
ikke-frie.  <q>bullseye</q> indeholder en ny pakke, ipp-usb, der anvender den 
leverandørneutale protokol IPP-over-USB, som understøttes af mange moderne 
printere.  Dermed kan en USB-enhed behandles som en netværksenhed.  SANEs  
officielle driverløse backend leveres af sane-escl i libsane1, der anvender 
eSCL-protokollen.</p>

<p>Systemd i <q>bullseye</q> aktiverer som standard sin vedvarende 
journal-funktionlitet, med et implicit fallback til volatile storage.  Dermed 
kan brugere, som ikke er afhængige af særlig funktionalitet, afinstallere 
traditionelle logningsdæmoner og skifte til kun at anvende systemds journal.</p>

<p>Debian Med-holdet har deltaget i kampen mod COVID-19, ved at pakke software 
til forskning i virussen på sekvensniveuet, og til at bekæmpe pandemien med 
værktøjer som anvendes i epidemiologi; arbejdet fortsætter med fokus på 
maskinlæringsværktøjer på begge områder.  Holdets arbejde med Quality Assurance 
og Continuous Integration er kritisk for at kunne have konsekvent reproducerbare 
resultater, som krævet af videnskaben.  Debian Med Blend har en række 
applikationer som er udførelseskritiske, hvilket nu kan drage nytte af SIMD 
Everywhere.  For at installere pakker, der vedligeholdes af Debian Med-holdet, 
installeres metapakkerne navngivet med-*, som er i version 3.6.x.</p>

<p>Kinesisk, japansk, koreansk og mange andre sprog, har nu en ny Fcitx 
5-inddatametode, som er efterfølgeren til den poplære Fcitx4 i <q>buster</q>; 
denne nye version har meget bedre understøttelse af addons i Wayland 
(standard-display manager).</p>

<p>Debian 11 <q>bullseye</q> indeholder talrige opdaterede softwarepakker (over 
72 procent af alle pakker i den tidligere udgave), så som:</p>

<ul>
<li>Apache 2.4.48</li>
<li>BIND DNS Server 9.16</li>
<li>Calligra 3.2</li>
<li>Cryptsetup 2.3</li>
<li>Emacs 27.1</li>
<li>GIMP 2.10.22</li>
<li>GNU Compiler Collection 10.2</li>
<li>GnuPG 2.2.20</li>
<li>Inkscape 1.0.2</li>
<li>LibreOffice 7.0</li>
<li>Linux kernel 5.10 series</li>
<li>MariaDB 10.5</li>
<li>OpenSSH 8.4p1</li>
<li>Perl 5.32</li>
<li>PHP 7.4</li>
<li>PostgreSQL 13</li>
<li>Python 3, 3.9.1</li>
<li>Rustc 1.48</li>
<li>Samba 4.13</li>
<li>Vim 8.2</li>
<li>flere end 59.000 andre softwarepakker, der er parate til at blive 
installeret, opbygget fra flere end 30.000 kildekodepakker.</li>
</ul>

<p>Med det brede udvalg af pakker og den traditionelle brede understøttelse af 
arkitekturer, lever Debian endnu en gang op til sin målsætning om at være 
<q>det universelle styresystem</q>.  Det er velegnet til mange forskellige 
formål: fra skriverbordssystemer til netbooks; fra udviklingservere til 
klyngesystemer; og til database-, web og storageservere.  På samme tid, sørger 
yderligere kvalitetssikringtiltag så som automatisk installerings- og 
opgraderestest af alle pakker i Debian arkiv, at <q>bullseye</q> opfylder de 
høje forventninger, som brugerne har til en stabil Debian-udgave.</p>

<p>I alt ni arkitekturer er understøttet:
64-bit PC / Intel EM64T / x86-64 (<code>amd64</code>),
32-bit PC / Intel IA-32 (<code>i386</code>),
64-bit little-endian Motorola/IBM PowerPC (<code>ppc64el</code>),
64-bit IBM S/390 (<code>s390x</code>),
til ARM, <code>armel</code>
og <code>armhf</code> til ældre og nyere 32 bit-hardware,
samt <code>arm64</code> til 64 bit-arkitekturen <q>AArch64</q>,
og til MIPS, <code>mipsel</code>-arkitekturen (little-endian) til 32 
bit-hardware og <code>mips64el</code>-arkitekturen til 64 bit-hardware 
(little-endian).</p>


<h3>Hvad med en prøvetur?</h3>

<p>Hvis du blot ønsker at prøve Debian 11 <q>bullseye</q> uden at installere det, 
kan du benytte et af de tilgængelige <a href="$(HOME)/CD/live/">liveimages</a>, 
som indlæser og afvikler et komplet styresystem i kun læsning-tilstand ved hjælp 
af din computers hukommelse.</p>

<p>Disse liveimages findes til arkitekturerne <code>amd64</code> og
<code>i386</code>, og er tilgængelige til DVD'er, USB-pinde og 
netboot-opsætninger.  Brugeren kan afprøve forskellige skrivebordsmiljøer: 
GNOME, KDE Plasma, LXDE, LXQt, MATE og Xfce.  Debian Live <q>bullseye</q> 
har et standardliveaftryk, så det også er muligt at prøve et grundlæggende 
Debian-system, uden en grafisk brugerflade.</p>

<p>Hvis du bliver glad for styresystemet, har du mulighed for at installere fra 
liveimaget til din computers harddisk.  Liveimaget indeholder det uafhængige 
installeringsprogram Calamares, samt den almindelige Debian Installer.  Flere 
oplysninger er tilgængelige i afsnittene 
<a href="$(HOME)/releases/bullseye/releasenotes">udgivelsesbemærkninger</a> og 
<a href="$(HOME)/CD/live/">liveinstalleringsimages</a> på Debian websted.</p>

<p>For at installere Debian 11 <q>bullseye</q> direkte på din computers harddisk, 
kan du vælge blandt forskellige medier, så som Blu-ray Disc, DVD, CD, USB-pind 
eller ved hjælp af en netværksforbindelse.  Flere skrivebordsmiljøer &ndash; 
Cinnamon, GNOME, KDE Plasma Desktop and Applications, LXDE, LXQt, MATE og Xfce 
&ndash; kan blive installeret fra disse images.  Desuden er der 
<q>multi-architecture</q>-CD'er, hvor man kan vælge installering blandt flere 
forskellige arkitekturer fra en enkelt skive.  Ellers kan du altid oprette et 
startbart USB-medie (se <a href="$(HOME)/releases/bullseye/installmanual">\
Installeringshåndbogen</a> for flere oplysninger).</p>

# Translators: some text taken from /devel/debian-installer/News/2021/20210802
<p>Der har været megen udvikling i Debian Installer, som har givet forbedret 
hardwareunderstøttelse samt anden ny funktionalitet.</p>

<p>I nogle tilfælde kan en succesrig installering stadig have problemer med 
skærmen, når det nye system startes; i de tilfælde er der 
<a href="https://www.debian.org/releases/bullseye/amd64/ch06s04#completing-installed-system">\
et par workarounds</a> som kan hjælpe med at logge på alligevel.  Der er også en 
<a href="https://www.debian.org/releases/bullseye/amd64/ch06s04#completing-installed-system">\
isenkram-baseret procedure</a>, som giver brugerne mulighed for at opdage og 
rette manglende firmware på deres systemer, på en automatiseret måde.  Her er 
man naturligvis nødt til at overveje fordele og ulemper ved at anvende 
værktøjet, da det er meget muligt at der er behov for at installere ikke-frie 
pakker.</p>

<p>Desuden er de 
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/">\
ikke-frie installeringsaftryk, hvor firmwarepakker medfølger</a>, blevet 
forbedret så de kan tage højde for behovet for firmware i det installerede 
system (fx firmware til grafikkort fra AMD eller Nvidia, eller nyere 
generationer af Intels lydhardware).</p>

<p>Til skybrugere tilbyder Debian direkte understøttelse af mange de bedst 
kendte skyplatforme.  Officielle Debian-aftryk vælges let på hvert 
imagemarkedsplads.  Debian udgiver også 
<a href="https://cloud.debian.org/images/openstack/current/">forhåndsopbyggede
OpenStack-images</a> til arkitekturerne <code>amd64</code> og 
<code>arm64</code>, klar til hentning og brug i lokale skyopsætninger.</p>

<p>Debian kan nu installere på 76 sprog, hvoraf de fleste er tilgængelige i 
både tekst-baserede og grafisk brugergrænsflader.</p>

<p>Installeringsimages kan hentes lige nu ved hjælp af 
<a href="$(HOME)/CD/torrent-cd/">bittorrent</a> (den anbefalede metode),
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a> eller 
<a href="$(HOME)/CD/http-ftp/">HTTP</a>; se
<a href="$(HOME)/CD/">Debian på CD'er</a> for flere oplysninger. <q>bullseye</q> 
bliver også snart tilgængelig på fysisk DVD, CD-ROM og Blu-ray Disc fra talrige 
<a href="$(HOME)/CD/vendors">forhandlere</a>.</p>


<h3>Opgradering af Debian</h3>

<p>Opgradering til Debian 11 fra den tidligere udgave, Debian 10 (kodenavn 
<q>buster</q>), håndteres automatisk af pakkehåndteringsværktøjet APT for de 
fleste opsætningers vedkommende.</p>

<p>I <q>bullseye</q> hedder sikkerhedssuiten nu bullseye-security, og brugerne 
bør tilpasse deres source-list-filer i APT tilsvarende, når der opgraderes.  
Hvis APT-opsætningen også involverer pinning eller 
<code>APT::Default-Release</code>, er det muligt at det også skal tilpasses.  Se 
<a href="https://www.debian.org/releases/bullseye/amd64/release-notes/ch-information#security-archive">\
Changed security archive layout</a>-afsnittet i udgivelesbemærkningerne for 
flere oplysninger.</p>

<p>Hvis der foretages en fjernopgradering, så vær opmærksom på afsnittet 
<a href="$(HOME)/releases/bullseye/amd64/release-notes/ch-information#ssh-not-available">\
No new SSH connections possible during upgrade</a>.</p>

<p>Som altid kan Debian-systemer opgraderes smertefrit, på stedet og uden 
tvungen nedetid, men vi anbefaler kraftigt, at man læser 
<a href="$(HOME)/releases/bullseye/releasenotes">udgivelsesbemærkningerne</a> 
samt 
<a href="$(HOME)/releases/bullseye/installmanual">installeringsvejledningen</a>, 
for at være opmærksom på eventuelle problemer, samt for at få detaljeret 
vejledning i installering og opgradering.  I ugerne efter udgivelsen, vil 
udgivelsesbemærkningerne blive løbende forbedret og oversat til flere sprog.</p>


<h2>Om Debian</h2>

<p>Debian er et frit styresystem, der udvikles af tusindvis af frivillige fra 
hele verden, som samarbejder via internettet.  Debian-projektets hovedstyrker 
er dets fundament af frivillige, dets dedikation til Debians sociale kontrakt 
og fri software, samt dets engagement i at tilbyde det bedst mulige styresystem.  
Denne nye udgave er endnu et vigtigt skridt i den retning.</p>


<h2>Kontaktoplysninger</h2>

<p>For flere oplysninger, besøg Debians websider på 
<a href="$(HOME)/">https://www.debian.org/</a> eller send e-mail på engelsk til
&lt;press@debian.org&gt;.</p>


















