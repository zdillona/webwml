#use wml::debian::cdimage title="Live install-aftryk"
#use wml::debian::release_info
#use wml::debian::installer
#use wml::debian::translation-check translation="8c86ac02236495359e82eed0e3b9e29905514fd7"
#include "$(ENGLISHDIR)/releases/images.data"

<p>Et <q>live install</q>-aftryk indeholder et Debian-system, der kan boote uden 
at ændre nogen filer på harddisken og som også gør det muligt at installere 
Debian fra aftrykkets indhold.</p>

<p><a name="choose_live"><strong>Er et liveaftryk velegnet til mit formål?</strong></a>  Her er nogle ting 
at overveje, som vil hjælpe dig med at træffe beslutningen.</p>

<ul>
    <li><strong>Launcher:</strong>  Ud over mulighed for at installere via tekst 
	og GUI i bootmenuen, indeholder skrivebordsvarianten også en launcher på 
	skrivebordet, der kan anvendes til at installere mens man kører 
	liveaftrykket.</li>
    <li><strong>Varianter:</strong>  Liveaftrykkene findes i flere varianter 
	(<q>flavors</q>), hvor flere af den giver mulighed for at vælge mellem 
	skrivebordsmiljøer (GNOME, KDE, LXDE, Xfce, Cinnamon og MATE).  For 
	mange brugere vil det indledningsvise pakkevalg være passende, og der 
	kan efterfølgende over netværket, installeres flere pakker efter 
	behov.</li>
    <li><strong>Arkitektur:</strong>  Kun aftryk til de to mest populære 
	arkitekturer, 32-bits-pc (i386) og 64-bits-pc (amd64), er i øjeblikket 
	tilgængelige.</li>
    <li><strong>Installerering:</strong> Begyndende med Debian 10 Buster, 
	indeholder livefilaftrykkene den slutbrugervenlige 
	<a href="https://calamares.io">Calamares Installer</a>, et 
	distributionsuafhængigt installeringsframework, som et alternativ til 
	vores velkendte 
	<a href="$(HOME)/devel/debian-installer">Debian-Installer</a>.</li>
    <li><strong>Størrelse:</strong>  Hvert aftryk er meget mindre end et komplet 
	sæt DVD-aftryk, men større end netværksinstalleringsmediet.</li>
    <li><strong>Sprog:</strong>  Aftrykkene indeholder ikke det komplette udvalg 
	af sprogunderstøttelsespakker.  Hvis man har brug for 
	indtastningsmetoder, skrifttyper eller supplerende sprogpakker til sit 
	sprog, skal disse installeres efterfølgende.</li>
</ul>

<p>Følgende live install-aftryk kan hentes:</p>

<ul>
    <li>Officielle <q>live install</q>-aftryk indeholdende den <q>stabile</q> 
	udgave &ndash; <a href="#live-install-stable">se nedenfor</a></li>
</ul>


<h2 id="live-install-stable">Officielle live install-aftryk indeholdende den <q>stabile</q> udgave</h2>

<p>Tilgængelige i forskellige varianter, med forskellige størrelser som 
beskrevet oven for, er disse aftryk beregnet til at afprøve et Debian-system
bestående af et standardudvalg af pakker og dernæst installering af den fra
det samme medie.</p>

<div class="line">
<div class="item col50">
<p><strong>DVD/USB (via <a href="$(HOME)/CD/torrent-cd">\
BitTorrent</a>)</strong></p>
<p><q>Hybride</q> ISO-aftryksfiler, beregnet til at blive brændt på 
DVD-R(W)-medier, og også til USB-nøgler af en passende størrelse.  Hvis du har 
mulighed for at anvende BitTorrent, så vær venlig at gøre det, da det reducerer 
belastningen på vores servere.</p>
	  <stable-live-install-bt-cd-images />
</div>

<div class="item col50 lastcol">
<p><strong>DVD/USB</strong></p>
<p><q>Hybride</q> ISO-aftryksfiler, beregnet til at blive brændt på DVD-R(W)- 
og CD-R(W)-medier, afhængigt af størrelsen, og også til USB-nøgler af en 
passende størrelse:</p>
	  <stable-live-install-iso-cd-images />
</div>
</div>

<p>For oplysninger om hvad disse filer er og hvordan de anvendes, se vores
<a href="../faq/">ofte stillede spørgsmål</a>.</p>

<p>Hvis du har i sinde at installere Debian fra det hentede filaftryk, så sørg 
for at kigge på de <a href="$(HOME)/releases/stable/installmanual">detaljerede 
oplysninger om installeringsprocessen</a>.</p>

<p>Se <a href="$(HOME)/devel/debian-live">siden om Debian Live Project page</a> 
for flere oplysninger om Debian Live-systemer, som er tilgængelige via disse 
aftryk.</p>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
#
<h2><a name="firmware">Uofficielle cd-/dvd-liveaftryk med medfølgende ikke-fri firmware</a></h2>

<div id="firmware_nonfree" class="important">

<p>Hvis noget af hardwaren i dit system <strong>kræver at ikke-fri firmware skal 
indlæses</strong> sammen med enhedsdriveren, kan du anvende en 
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
tarball med almindelige firmwarepakker</a> eller hente et 
<strong>uofficielt</strong> aftryk, som indehodler denne 
<strong>ikke-frie</strong> firmware.  Vejledning i hvordan en tarball anvendes 
og generelle oplysninger om indlæsning af firmware under en installering, finder 
man i <a href="../../releases/stable/amd64/ch06s04">Installation Guide</a>.</p>

<p><a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">\
uofficielle installeringsaftryk til udgaven <q>stable</q> med medfølgende firmware</a></p>

</div>
