#use wml::debian::translation-check translation="8be684d647389ee3db99d941206fa9b5cbef2621" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder blev opdaget i evince, et simpelt program til visning af 
dokumenter.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000159">CVE-2017-1000159</a>

    <p>Tobias Mueller rapporterede at DVI-eksportfunktionen i evince var sårbar 
    over for en kommandindsprøjtningssårbarhed gennem særligt fremstillede 
    filnavne.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11459">CVE-2019-11459</a>

    <p>Andy Nguyen rapporterede at funktionerne tiff_document_render() og 
    tiff_document_get_thumbnail() i TIFF-dokumentbackend'en ikke håndterede 
    fejl fra TIFFReadRGBAImageOriented(), førende til blotlæggelse af 
    uinitialiseret hukommelse, når der blev behandlet 
    TIFF-billederfiler.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1010006">CVE-2019-1010006</a>

    <p>En bufferoverløbssårbarhed i TIFF-backend'en, kunne føre til 
    lammelsesangreb eller potentielt udførelse af vilkårlig kode, hvis en 
    særligt fremstillet PDF-fil blev åbnet.</p></li>

</ul>

<p>I den gamle stabile distribution (stretch), er disse problemer rettet
i version 3.22.1-3+deb9u2.</p>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 3.30.2-3+deb10u1. The stable distribution is only affected by
<a href="https://security-tracker.debian.org/tracker/CVE-2019-11459">CVE-2019-11459</a>.</p>

<p>Vi anbefaler at du opgraderer dine evince-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende evince, se
dens sikkerhedssporingssidede på:
<a href="https://security-tracker.debian.org/tracker/evince">https://security-tracker.debian.org/tracker/evince</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4624.data"
