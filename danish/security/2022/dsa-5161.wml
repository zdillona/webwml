#use wml::debian::translation-check translation="e92eab10cf723e2d5c0c43d72db58a57da07845b" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i Linux-kernen, hvilke kunne føre til en 
rettighedsforøgelse, lammelsesangreb eller informationslækager.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0494">CVE-2022-0494</a>

    <p>scsi_ioctl() var sårbar over for en informationslækage, der kun kunne 
    udnyttes af brugere med kapabiliteterne CAP_SYS_ADMIN eller 
    CAP_SYS_RAWIO.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0854">CVE-2022-0854</a>

    <p>Ali Haider opdagede en potentiel informationslækage i undersystemet DMA. 
    På systemer hvor funktionaliteten swiotlb er nødvendig, kunne det være 
    muligt for en lokal bruger at læse følsomme oplysninger.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1012">CVE-2022-1012</a>

    <p>Randomiseringen ved beregning af port-offset i IP-implementeringen blev 
    forbedret.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1729">CVE-2022-1729</a>

    <p>Norbert Slusarek opdagede en kapløbstilstand i undersystemet perf, 
    hvilken kunne medføre lokal rettighedsforøgelse til root.  
    Standardopsætningen i Debian forhindrer udnyttelse af fejlen, med mindre 
    eftergivende indstillinger er taget i anvendelse i 
    kernel.perf_event_paranoid sysctl.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1786">CVE-2022-1786</a>

    <p>Kyle Zeng opdagede en anvendelse efter frigivelse i undersystemet 
    io_uring, hvilke kunne medføre lokal rettighedsforøgelse til root.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1789">CVE-2022-1789</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-1852">CVE-2022-1852</a>

    <p>Yongkang Jia, Gaoning Pan og Qiuhao Li opdagede to 
    NULL-pointer-dereferencer i KVM's håndtering af CPU-instruktioner, 
    medførende lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32250">CVE-2022-32250</a>

    <p>Aaron Adams opdagede en anvendelse efter frigivelse i Netfilter, hvilken 
    kunne medføre lokal rettighedsforøgelse til root.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1972">CVE-2022-1972</a>

    <p>Ziming Zhang opdagede en skrivning udenfor grænserne i Netfilter, hvilken 
    kunne medføre lokal rettighedsforøgelse til root.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1974">CVE-2022-1974</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-1975">CVE-2022-1975</a>

    <p>Duoming Zhou opdagede at NFC netlink-grænsefladen var sårbar over for 
    lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21499">CVE-2022-21499</a>

    <p>Man opdagede at kernedebuggeren kunne anvendes til at omgå UEFI Secure 
    Boot-begrænsninger.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28893">CVE-2022-28893</a>

    <p>Felix Fu opdagede en anvendelse efter frigivelse i implmenteringen af 
    protokollen Remote Procedure Call (SunRPC), hvilken kunne medføre 
    lammelsesangreb eller en informationslækage.</p></li>

</ul>

<p>I den stabile distribution (bullseye), er disse problemer rettet i
version 5.10.120-1.</p>

<p>Vi anbefaler at du opgraderer dine linux-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende linux, se
dens sikkerhedssporingssidede på:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5161.data"
