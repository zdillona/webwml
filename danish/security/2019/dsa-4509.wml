#use wml::debian::translation-check translation="b917e690cbacd447496fcc36bb3b22df5d6873b2" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er fundet i Apache HTTPD-serveren.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9517">CVE-2019-9517</a>

    <p>Jonathan Looney rapporterede at en ondsindet klient kunne iværksætte 
    lammelsesangreb (udmattelse af h2-workers) ved at oversvømme en forbindelse 
    med forespørgsler, og ikke læse svar på TCP-forbindelsen.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10081">CVE-2019-10081</a>

    <p>Craig Young rapporterede at HTTP/2-PUSH kunne føre til en overskrivning 
    af hukommelse i den push'ende forespørgsels pool, førende til 
    nedbrud.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10082">CVE-2019-10082</a>

    <p>Craig Young rapporterede at man kunne få HTTP/2-sessionhåndteringen kunne 
    til at læse hukommelse efter den var frigivet, under nedlukning af en 
    forbindelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10092">CVE-2019-10092</a>

    <p>Matei <q>Mal</q> Badanoiu rapporterede om en begrænset sårbarhed i 
    forbindelse med udførelse af skripter på tværs af websteder på 
    mod_proxys fejlside.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10097">CVE-2019-10097</a>

    <p>Daniel McCarney rapporterede at når mod_remoteip var opsat til at anvende 
    en mellemliggende proxyserver, som der er tillid til, ved hjælp af 
    <q>PROXY</q>-protokollen, kunne en særligt fremstillet PROXY-header udløse 
    et stakbrugeroverløb eller en NULL-pointerdeference.  Sårbarheden kunne kun 
    udløses af en proxy, man har tillid til, og ikke af HTTP-klienter, men ikke 
    har tillid til.  Problemet påvirker ikke stretch-udgaven.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10098">CVE-2019-10098</a>

    <p>Yukitsugu Sasaki rapporterede om en potentiel åben 
    viderestilling-sårbarhed i modulet mod_rewrite.</p></li>

</ul>

<p>I den gamle stabile distribution (stretch), er disse problemer rettet
i version 2.4.25-3+deb9u8.</p>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 2.4.38-3+deb10u1.</p>

<p>Vi anbefaler at du opgraderer dine apache2-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende apache2, se
dens sikkerhedssporingssidede på:
<a href="https://security-tracker.debian.org/tracker/apache2">\
https://security-tracker.debian.org/tracker/apache2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4509.data"
