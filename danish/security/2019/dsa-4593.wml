#use wml::debian::translation-check translation="4b462ea192fc355c557625a4edd8b02668ca91dd" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Man opdagede at freeimage, et grafikbibliotek, var påvirket af følgende to 
sikkerhedsproblemer:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12211">CVE-2019-12211</a>

    <p>Heapbufferoverløb forårsaget af ugyldig memcpy i PluginTIFF.  Fejlen 
    kunne udnyttes af fjernangribere til at udløse et lammelsesangreb eller 
    anden ikke-angivet påvirkning ved hjælp af fabrikerede TIFF-data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12213">CVE-2019-12213</a>

    <p>Stakudmattelse forårsaget af uønsket rekursivitet i PluginTIFF.  Fejlen 
    kunne udnyttes af fjernangribere til at udløse et lammelsesangreb ved 
    hjælp af fabrikerede TIFF-data.</p></li>

</ul>

<p>I den gamle stabile distribution (stretch), er disse problemer rettet
i version 3.17.0+ds1-5+deb9u1.</p>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 3.18.0+ds2-1+deb10u1.</p>

<p>Vi anbefaler at du opgraderer dine freeimage-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende freeimage, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/freeimage">\
https://security-tracker.debian.org/tracker/freeimage</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4593.data"
