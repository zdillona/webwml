#use wml::debian::template title="Hvordan man anvender Debians søgemaskine"
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<p>Debian-projektet har sin egen søgemaskine på adressen
<a href="https://search.debian.org/">https://search.debian.org/</a>.
Her er nogle råd til hvordan den anvendes, samt nogle simple søgninger foruden 
mere komplekse søgninger med boolske operatorer.</p>

<div class="row">
  <!-- left column -->
  <div class="column column-left">
    <div style="text-align: left">
      <h3>Simpel søgning</h3>
      <p>Den simpelste måde at anvende søgemaskinene på, er at skrive et enkelt 
         ord i søgefeltet og trykke på [Enter].  Alternativt kan du klikke på 
	 knappen <em>Søg</em>.  Søgemaskinen præsenterer dernæst en oversigt 
	 over alle de sider på vores websted, som indeholder ordet.  Ved de 
	 fleste søgninger skulle det give gode resultater.</p>
      <p>Alternativt kan du søge efter mere end ét ord.  Igen skulle du gerne se 
	 en oversigt over alle de sider på Debians websted, som indeholder alle 
	 de angivne ord.  For at søge efter sætninger, sættes de i anførselstegn
	 (").  Bemærk at søgemaskinen ikke kender forskel på små og store 
         bogstaver, hvorfor en søgning efter <code>gcc</code> finder både 
	 <q>gcc</q> og <q>GCC</q>.</p> 
      <p>Under søgefeltet kan du angive, hvor mange resultater pr. side, du 
	 ønsker at se.  Det er også muligt at vælge et andet sprog; Debians 
	 websteds søgemotor understøtter næsten 40 forskellige sprog.</p>
    </div>
  </div>

<!-- right column -->
  <div class="column column-right">
    <div style="text-align: left">
      <h3>Boolsk søgning</h3>
      <p>Hvis en simpel søgning ikke er tilstrækkelig, kan du anvende boolske 
	 søgeoperatorer.  Du kan vælge mellem <em>AND</em> (og), <em>OR</em> 
	 (eller) og <em>NOT</em> (ikke), eller kombinere dem alle tre.  Sørg 
	 for at skrive dem alle med store bogstaver, så søgemaskinen genkender 
	 dem.</p>
      <ul>
        <li><b>AND</b> (og) kombinerer to ord og finder sider, der indeholder 
	    dem begge.  Eksempelvis finder <code>gcc AND patch</code> alle sider 
	    som indeholder begge, både <q>gcc</q> og <q>patch</q>.  I denne 
	    situation får du de samme resultater, som når der søges efter 
	    <code>gcc patch</code>, men en eksplicit <code>AND</code> kan være 
	    nyttig i kombination med andre operatorer.</li>
        <li><b>OR</b> (eller) giver resultater hvis et af ordene findes på en 
	    side.  <code>gcc OR patch</code> finder enhver side der indeholder 
	    enten <q>gcc</q> eller <q>patch</q>.</li>
        <li><b>NOT</b> (ikke) anvendes til at fravælge søgeord fra resultaterne. 
	    Eksempelvis finder <code>gcc NOT patch</code> alle sider der 
	    indeholder <q>gcc</q>, men ikke <q>patch</q>.  
	    <code>gcc AND NOT patch</code> giver dig de samme resultater, men 
	    søgning efter <code>NOT patch</code> er ikke understøttet.</li>
        <li><b>(...)</b> kan anvendes til at gruppere udtryk.  Eksempelvis 
	    finder <code>(gcc OR make) NOT patch</code> alle sider, der 
	    indeholder enten <q>gcc</q> eller <q>make</q>, men ikke indeholder 
	    <q>patch</q>.</li>
      </ul>
    </div>
  </div>
</div>
