#use wml::debian::translation-check translation="4fcc70e9176cd63d44d1565ff55a606c9c42f4ee" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Trois problèmes de sécurité ont été découverts dans python3.5 :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3177">CVE-2021-3177</a>

<p>Python 3.x avait un dépassement de tampon dans PyCArg_repr dans
_ctypes/callproc.c qui pourrait conduire à l’exécution de code à distance dans
certaines applications en Python qui acceptent des nombres en virgule flottante
comme entrées non fiables. Cela se produit à cause d’une utilisation non sûre de
sprintf.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3426">CVE-2021-3426</a>

<p>L’exécution de <q>pydoc -p</q> permet à d’autres utilisateurs locaux d’extraire
des fichiers arbitraires. L’URL <q>/getfile?key=path</q> permet de lire un fichier
arbitraire sur le système de fichiers.</p>

<p>Le correctif supprime la fonction <q>getfile</q> du module pydoc qui pourrait
être détournée pour lire des fichiers arbitraires sur le disque (vulnérabilité
de traversée de répertoires).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23336">CVE-2021-23336</a>

<p>Python3.5 est vulnérable à un empoisonnement du cache web à l’aide de
urllib.parse.parse_qsl et urllib.parse.parse_qs en utilisant un masquage de
paramètre d’appel de vecteur. Quand l’attaquant peut séparer les paramètres
de requête en utilisant un point-virgule (;), il peut provoquer une différence
dans l’interprétation de la requête entre le mandataire (exécuté avec la
configuration par défaut) et le serveur. Cela peut aboutir à des requêtes
malveillantes mises en cache comme étant entièrement sûres, puisque le
mandataire habituellement ne voit pas le point-virgule comme un séparateur,
et par conséquent ne l’inclura pas dans une clé de cache d’un paramètre non
mis en clé.</p>

<p><b>Attention, modification d’API !</b> </p>

<p> Veuillez vérifier que vos logiciels fonctionnent correctement s’ils
utilisent urllib.parse.parse_qs ou urllib.parse.parse_qsl, cgi.parse ou
cgi.parse_multipart.</p>

<p>Les versions précédentes de Python permettaient l’utilisation de <q>;</q> et
<q>&amp;</q> comme séparateurs de paramètres de requête dans urllib.parse.parse_qs
et urllib.parse.parse_qsl. À cause de problèmes de sécurité, et pour se
conformer avec les nouvelles recommandations du W3C, cela a été modifié pour
permettre seulement un unique séparateur avec <q>&amp;</q> comme valeur par défaut.
Cette modification affecte aussi cgi.parse et cgi.parse_multipart
puisqu’ils utilisent les fonctions affectées en interne. Pour plus de détails,
veuillez consulter leur documentation respective.</p>


<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 3.5.3-1+deb9u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python3.5.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de python3.5, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/python3.5">\
https://security-tracker.debian.org/tracker/python3.5</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2619.data"
# $Id: $
