#use wml::debian::translation-check translation="041224bb5e1edc4a1aa97c56ae08c7b6968b3805" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux problèmes ont été découverts dans bluez, un paquet avec des outils et
des démons Bluetooth. Un problème concerne une attaque de type <q>homme du
milieu</q> (man in the middle) lors de l’appairage de sécurité, l’autre concerne
une divulgation d'informations due à un contrôle d’accès incorrect.</p>

<p>Pour corriger complètement ces deux problèmes, le noyau doit aussi être mis
à jour. Pour Debian 9 Stretch, cela a été déjà publié.</p>


<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 5.43-2+deb9u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bluez.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de bluez, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/bluez">\
https://security-tracker.debian.org/tracker/bluez</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2692.data"
# $Id: $
