#use wml::debian::translation-check translation="b733960cc6f8e493d63073cc582a0f4db42aa46b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un problème dans l’outil en ligne de
commande pour le service de haute disponibilité
<a href="https://clusterlabs.org/pacemaker/">Pacemaker</a>. Des attaquants
locaux pouvaient exécuter des commandes à l’aide d’une injection de code
d’interpréteur dans l’outil en ligne de commande <tt>crm history</tt>,
permettant éventuellement une augmentation de droits.</p>
<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35459">CVE-2020-35459</a>

<p>Un problème a été découvert dans crmsh de ClusterLabs jusqu’à la
version 4.2.1. Des attaquants locaux capables d’appeler <tt>crm history</tt>
(quand <code>crm</code> était en cours d’exécution) pouvaient exécuter des
commandes à l’aide d’une injection de code d’interpréteur dans l’outil en ligne
de commande <tt>crm history</tt>, permettant éventuellement une augmentation de
droits.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2.3.2-4+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets crmsh.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2533.data"
# $Id: $
