#use wml::debian::translation-check translation="62c5438af2063746de2ae8f9062538f4c368c5a4" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>runc, tel qu’utilisé dans Docker et d’autres produits, permettait de
contourner les restrictions d’AppArmor et SELinux, et par conséquent, une image
Docker malveillante pouvait rompre l’isolation.</p>

<p>Cette mise à jour fournit des correctifs relatifs à SELinux dans la
bibliothèque golang-github-opencontainers-selinux, qui seront exploités dans la
mise à jour de sécurité à venir de runc.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1.0.0~rc1+git20170621.5.4a2974b-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets golang-github-opencontainers-selinux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de golang-github-opencontainers-selinux,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/golang-github-opencontainers-selinux">\
https://security-tracker.debian.org/tracker/golang-github-opencontainers-selinux</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3322.data"
# $Id: $
