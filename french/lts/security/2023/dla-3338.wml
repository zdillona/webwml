#use wml::debian::translation-check translation="3c4503673737f21b82b2494eccce42d1590b5391" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans git, un système de gestion
de versions rapide et extensible.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-22490">CVE-2023-22490</a>

<p>yvvdwf a trouvé un vulnérabilité d’exfiltration de données lors d’un clonage
local d’un dépôt malveillant même en utilisant un transport non local.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23946">CVE-2023-23946</a>

<p>Joern Schneeweisz a trouvé une vulnérabilité de traversée de répertoires dans
git-apply, par laquelle un chemin en dehors de l’arbre de travail pouvait être
écrasé en tant qu’utilisateur actif.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1:2.20.1-2+deb10u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets git.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de git,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/git">\
https://security-tracker.debian.org/tracker/git</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3338.data"
# $Id: $
