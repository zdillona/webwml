#use wml::debian::translation-check translation="594452bc604209e41c5b15e82b22ac6fcce5964e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Cette mise à jour corrige un certain nombre de violations d’accès mémoire et
d’autres échecs de validation d’entrée qui pouvaient être déclenchés en
fournissant des fichiers spécialement contrefaits à exiv2.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11591">CVE-2017-11591</a>

<p>Il existait une exception de virgule flottante dans la fonction
<code>Exiv2::ValueType</code> qui pouvait conduire à une attaque distante par
déni de service à l’aide d’une entrée contrefaite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14859">CVE-2017-14859</a>

<p>Un déréférencement non valable d’adresse mémoire a été découvert dans
<code>Exiv2::StringValueBase::read</code> dans <code>value.cpp</code>. La
vulnérabilité provoquait une erreur de segmentation et un plantage d'application,
amenant un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14862">CVE-2017-14862</a>

<p>Un déréférencement non valable d’adresse mémoire a été découvert dans
<code>Exiv2::DataValue::read</code> dans <code>value.cpp</code>. La
vulnérabilité provoquait une erreur de segmentation et un plantage d'application,
amenant un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14864">CVE-2017-14864</a>

<p>Un déréférencement non valable d’adresse mémoire a été découvert dans
<code>Exiv2::getULong</code> dans <code>types.cpp</code>. La vulnérabilité
provoquait une erreur de segmentation et un plantage d'application,
amenant un déni de service.</p></li>


<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17669">CVE-2017-17669</a>

<p>Une lecture hors limites de tampon de tas existait dans la fonction
<code>Exiv2::Internal::PngChunk::keyTXTChunk</code> de
<code>pngchunk_int.cpp</code>. Un fichier PNG contrefait pouvait conduire a une
attaque distante par déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18005">CVE-2017-18005</a>

<p>Exiv2 avait un déréférencement de pointeur NULL dans la fonction
<code>Exiv2::DataValue::toLong</code> dans <code>value.cpp</code>, relatif à
des métadonnées contrefaites dans un fichier TIFF.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8976">CVE-2018-8976</a>

<p><code>jpgimage.cpp</code> permettait à des attaquants distants de provoquer un
déni de service (lecture hors limites pour <code>image.cpp</code>
<code>Exiv2::Internal::stringFormat</code>) à l'aide d'un fichier contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17581">CVE-2018-17581</a>

<p><code>CiffDirectory::readDirectory()</code> dans <code>crwimage_int.cpp</code>
avait une consommation de pile excessive à cause d’une fonction récursive,
conduisant à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19107">CVE-2018-19107</a>

<p><code>Exiv2::IptcParser::decode</code> dans <code>iptc.cpp</code> (appelée
à partir de <code>psdimage.cpp</code> dans le lecteur d’image PSD) pouvait être
victime d’un déni de service (lecture hors limites de tampon de tas) provoqué
par un dépassement d'entier à l'aide d'un fichier d’image PSD contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19108">CVE-2018-19108</a>

<p><code>Exiv2::PsdImage::readMetadata</code> dans <code>psdimage.cpp</code>
dans le lecteur d’image PSD pouvait être victime d’un déni de service (boucle
infinie) provoqué par un dépassement d'entier à l'aide d'un fichier d’image PSD
contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19535">CVE-2018-19535</a>

<p><code>PngChunk::readRawProfile</code> dans <code>pngchunk_int.cpp</code>
pouvait provoquer un déni de service (plantage d'application dû à une lecture
hors limites de tampon de tas) à l'aide d'un fichier PNG contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20097">CVE-2018-20097</a>

<p>Une erreur de segmentation dans
<code>Exiv2::Internal::TiffParserWorker::findPrimaryGroups</code> de
<code>tiffimage_int.cpp</code>. Une entrée contrefaite pouvait conduire à une
attaque distante par déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13110">CVE-2019-13110</a>

<p>Un dépassement d'entier et une lecture hors limites dans
<code>CiffDirectory::readDirectory</code> permettait à un attaquant de provoquer
un déni de service (<code>SIGSEGV</code>) à l'aide d'un fichier d’image CRW
contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13112">CVE-2019-13112</a>

<p>Une allocation de mémoire non contrôlée pour
<code>PngChunk::parseChunkContent</code> permettait à un attaquant de provoquer
un déni de service (plantage dû à une exception <code>std::bad_alloc</code>)
à l'aide d'un fichier d’image PNG contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13114">CVE-2019-13114</a>

<p><code>http.c</code> permettait à un serveur HTTP malveillant de provoquer un
déni de service (plantage dû à un déréférencement de pointeur <code>NULL</code>)
en renvoyant une réponse contrefaite avec un caractère espace manquant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13504">CVE-2019-13504</a>

<p>Une lecture hors limites existait dans
<code>Exiv2::MrwImage::readMetadata</code> dans <code>mrwimage.cpp</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14369">CVE-2019-14369</a>

<p><code>Exiv2::PngImage::readMetadata()</code> dans <code>pngimage.cpp</code>
permettait à des attaquants de provoquer un déni de service (lecture hors limites
de tampon basé sur le tas) à l'aide d'un fichier image contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14370">CVE-2019-14370</a>

<p>Une lecture hors limites existait dans
<code>Exiv2::MrwImage::readMetadata()</code> dans <code>mrwimage.cpp</code>.
Elle pouvait aboutir à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17402">CVE-2019-17402</a>

<p>Exiv2 permet des attaquants de déclencher un plantage dans
<code>Exiv2::getULong</code> dans <code>types.cpp</code> lorsque appelé à partir
de <code>Exiv2::Internal::CiffDirectory::readDirectory</code> dans
<code>crwimage_int.cpp</code>, car il n’existait pas de validation de relations
entre la taille totale et les position et taille.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-18771">CVE-2020-18771</a>

<p>Exiv2 avait une lecture globale excessive de tampon dans
<code>Exiv2::Internal::Nikon1MakerNote::print0x0088</code> dans
<code>nikonmn_int.cpp</code> qui pouvait amener à une fuite d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-29458">CVE-2021-29458</a>

<p>Une lecture hors limites a été découverte dans Exiv2. Une lecture hors
limites était provoquée quand Exiv2 était utilisé pour écrire des métadonnées
dans un fichier image contrefait. Un attaquant pouvait éventuellement exploiter
la vulnérabilité pour provoquer un déni de service par plantage d’Exiv2, s’il
pouvait pousser la victime à exécuter Exiv2 sur un fichier image contrefait.
Remarque : ce bogue était seulement provoqué lors de l’écriture de métadonnées,
ce qui est moins couramment utilisé comme opération d’Exiv2 que la lecture de
métadonnées. Par exemple, pour déclencher le bogue dans une application en
ligne de commande d’ Exiv2, il était besoin d’ajouter un argument supplémentaire
dans la ligne de commande, telle une insertion.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32815">CVE-2021-32815</a>

<p>Un échec d’assertion était provoqué quand Exiv2 était utilisé pour modifier
les métadonnées d’un fichier image contrefait. Un attaquant pouvait
éventuellement exploiter la vulnérabilité pour provoquer un déni de service
s’il pouvait pousser la victime à exécuter Exiv2 sur un fichier image contrefait.
Remarque : ce bogue était seulement provoqué lors de l’écriture de métadonnées,
ce qui était moins couramment utilisé comme opération d’Exiv2 que la lecture de
métadonnées. Par exemple, pour déclencher le bogue dans une application en
ligne de commande d’ Exiv2, il était besoin d’ajouter un argument supplémentaire
dans la ligne de commande, tel que <code>fi</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-34334">CVE-2021-34334</a>

<p>Une boucle infinie était provoquée quand Exiv2 était utilisé pour lire les
métadonnées d’un fichier image contrefait. Un attaquant pouvait éventuellement
exploiter la vulnérabilité pour provoquer un déni de service s’il pouvait
pousser la victime à exécuter Exiv2 sur un fichier image contrefait.

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-37620">CVE-2021-37620</a>

<p>Une lecture hors limites était provoquée quand Exiv2 était utilisé pour lire
les métadonnées d’un fichier image contrefait. Un attaquant pouvait
éventuellement exploiter la vulnérabilité pour provoquer un déni de service s’il
pouvait pousser la victime à exécuter Exiv2 sur un fichier image contrefait.

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-37621">CVE-2021-37621</a>

<p>Une boucle infinie était déclenchée quand Exiv2 était utilisé pour écrire
des métadonnées dans un fichier image contrefait. Un attaquant pouvait
éventuellement exploiter la vulnérabilité pour provoquer un déni de service s’il
pouvait pousser la victime à exécuter Exiv2 sur un fichier image contrefait.
Remarque : ce bogue était seulement provoqué lors de l’écriture d’un profil
ICC d’image, ce qui est une opération moins couramment utilisée d’Exiv2 qui
requiert une option supplémentaire de ligne de commande (<code>-p C</code>).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-37622">CVE-2021-37622</a>

<p>Une boucle infinie était provoquée quand Exiv2 était utilisé pour modifier
les métadonnées d’un fichier image contrefait. Un attaquant pouvait
éventuellement exploiter cette vulnérabilité pour provoquer un déni de service
s’il pouvait pousser la victime à exécuter Exiv2 sur un fichier image
contrefait. Remarque :ce bogue était uniquement déclenché lors de la suppression
de données IPTC, ce qui est une opération moins couramment utilisée d’Exiv2 qui
requiert une option supplémentaire de ligne de commande (<code>-d I rm</code>).</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 0.25-4+deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets exiv2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de exiv2,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/exiv2">\
https://security-tracker.debian.org/tracker/exiv2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3265.data"
# $Id: $
