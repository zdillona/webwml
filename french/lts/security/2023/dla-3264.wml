#use wml::debian::translation-check translation="240be263c340ce9e2e828742419ff8a1cf8666d5" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité potentielle de
téléchargement de fichier par réflexion (RFD) dans ruby-sinatra, une
bibliothèque de Ruby pour écrire des applications HTTP. Un en-tête HTTP
Content-Disposition était incorrectement dérivé d’un nom de fichier
potentiellement fourni par un utilisateur.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-45442">CVE-2022-45442</a>

<p>Sinatra est un langage dédié à la création d’applications web avec Ruby. Un
problème a été découvert dans Sinatra, versions 2.0 avant 2.2.3 et 3.0
avant 3.0.4. Une application est vulnérable à une attaque de téléchargement de
fichier par réflexion (RFD) qui règle l’en-tête Content-Disposition d’une
réponse quand le nom de fichier est dérivé d’une entrée fournie par un
utilisateur. Les version 2.2.3 et 3.0.4 fournissent des correctifs pour ce
problème.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2.0.5-4+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby-sinatra.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3264.data"
# $Id: $
