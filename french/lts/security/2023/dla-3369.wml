#use wml::debian::translation-check translation="c4590e1589be60681f13b2b550d698e53d970e3f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans runc, l’environnement
d’exécution du projet Open Container, qui est souvent utilisé dans des
environnements de virtualisation tels que Docker. Des images malveillantes ou
des offres groupées OCI pouvaient rompre l’isolation.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16884">CVE-2019-16884</a>

<p>runc, tel qu’utilisé dans Docker et d’autres produits, permettait le
contournement de restrictions d’AppArmor et SELinux parce que
libcontainer/rootfs_linux.go vérifiait mal les cibles de montage, et par
conséquent une image Docker malveillante pouvait monter un volume sur un
répertoire /proc.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19921">CVE-2019-19921</a>

<p>runc avait un contrôle incorrect d’accès conduisant à une élévation de
privilèges, relative à libcontainer/rootfs_linux.go. Pour exploiter cela, un
attaquant devait être capable de déployer deux conteneurs avec des
configurations d’environnement de montage de volume personnalisées et d’être
capable d’exécuter des images personnalisées. (Cette vulnérabilité n’affectait
pas Docker à cause d’un détail d’implémentation qui bloquait cette attaque.)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30465">CVE-2021-30465</a>

<p>runc permettait une évasion du système de fichiers du conteneur à l’aide
d’une traversée de répertoires. Pour exploiter tette vulnérabilité, un attaquant
devait être capable de créer plusieurs conteneurs avec une configuration plutôt
spécifique. Le problème se produisait à l’aide d’une attaque par échange de
liens symboliques qui reposait sur une situation de compétition.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-29162">CVE-2022-29162</a>

<p><q>runc exec --cap</q> créait des processus avec des capacités de processus
Linux héritables non vides, créant un environnement Linux atypique et
autorisant des programmes avec des capacités de fichier héritables d’élever ces
capacités à l’ensemble permis lors d’un execve(2). Ce bogue n’affectait pas le bac
à sable de sécurité du conteneur car l’ensemble héritable ne contenait jamais
plus de capacités que celles incluses dans l’ensemble permis du conteneur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-27561">CVE-2023-27561</a>

<p><a href="https://security-tracker.debian.org/tracker/CVE-2019-19921">CVE-2019-19921</a> a été réintroduit par le correctif pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2021-30465">CVE-2021-30465</a>.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1.0.0~rc6+dfsg1-3+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets runc.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de runc,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/runc">\
https://security-tracker.debian.org/tracker/runc</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3369.data"
# $Id: $
