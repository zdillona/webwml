#use wml::debian::translation-check translation="8e5713a25237c23b851db6079bbf7082732be9fe" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans QEMU, un émulateur
rapide de processeur, qui pouvaient aboutir à un déni de service, une fuite
d'informations ou éventuellement à l’exécution de code arbitraire.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14394">CVE-2020-14394</a>

<p>Un défaut de boucle infinie a été découvert dans l’émulation du contrôleur
xHCI USB de QEMU lors du calcul de la longueur de l’anneau TRB (Transfer Request
Block). Ce défaut permettait a un invité privilégié de planter le processus
QEMU dans l’hôte, aboutissant à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-17380">CVE-2020-17380</a>/<a href="https://security-tracker.debian.org/tracker/CVE-2021-3409">CVE-2021-3409</a>

<p>Un dépassement de tampon de tas a été découvert dans QEMU dans la prise en
charge de l’émulation du périphérique SDHCI. Cela pouvait se produire lors d’un
transfert SDMA multibloc à l’aide de la routine sdhci_sdma_transfer_multi_blocks()
dans hw/sd/sdhci.c. Un invité ou un processus pouvait utiliser ce défaut pour
planter le processus QEMU dans l’hôte, aboutissant à une possibilité de déni de
service ou éventuellement à une exécution de code arbitraire avec les privilèges
du processus QEMU dans l’hôte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29130">CVE-2020-29130</a>

<p>slirp.c avait une lecture excessive de tampon parce qu’il essayait de lire
un certain montant de données d’en-tête même s’il excédait la taille totale du
paquet.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3592">CVE-2021-3592</a>

<p>Un problème d’initialisation non valable de pointeur a été découvert dans
l’implémentation de réseautage SLiRP de QEMU. Le défaut existait dans la
fonction bootp_input() et pouvait se produire lors du traitement d’un paquet
udp plus petit que la taille de la structure <q>bootp_t</q>. Un invité
malveillant pouvait utiliser ce défaut pour divulguer 10 octets de mémoire de
tas non initialisés de l’hôte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3593">CVE-2021-3593</a>

<p>Un problème d’initialisation non valable de pointeur a été découvert dans
l’implémentation de réseautage SLiRP de QEMU. Le défaut existait dans la
fonction udp6_input() et pouvait se produire lors du traitement d’un paquet
udp plus petit que la taille de la structure <q>udphdr</q>. Ce problème pouvait
conduire à un accès en lecture hors limites accès ou un divulgation indirecte
de la mémoire de l’hôte à l’invité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3594">CVE-2021-3594</a>

<p>Un problème d’initialisation non valable de pointeur a été découvert dans
l’implémentation de réseautage SLiRP de QEMU. Le défaut existait dans la
fonction udp_input() et pouvait se produire lors du traitement d’un paquet
udp plus petit que la taille de la structure <q>udphdr</q>. Ce problème pouvait
conduire à un accès en lecture hors limites accès ou un divulgation indirecte
de la mémoire de l’hôte à l’invité.</p></li>


<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3595">CVE-2021-3595</a>

<p>Un problème d’initialisation non valable de pointeur a été découvert dans
l’implémentation de réseautage SLiRP de QEMU. Le défaut existait dans la
fonction tftp_input() et pouvait se produire lors du traitement d’un paquet
udp plus petit que la taille de la structure <q>tftp_t</q>. Ce problème pouvait
conduire à un accès en lecture hors limites accès ou un divulgation indirecte
de la mémoire de l’hôte à l’invité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0216">CVE-2022-0216</a>

<p>Une vulnérabilité d’utilisation de mémoire après libération a été découverte
dans l’émulation du contrôleur hôte de bus SCSI LSI53C895A de QEMU. Le défaut
se produisait lors du traitement de messages répétés pour annuler la requête
SCSI à l’aide de la fonction lsi_do_msgout. Ce défaut permettait à un
utilisateur privilégié malveillant dans le client de planter le processus QEMU
dans l’hôte, aboutissant à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1050">CVE-2022-1050</a>

<p>Un défaut a été découvert dans l’implémentation de QEMU du périphérique
paravirtuel RDMA de VMWare. Ce défaut permettait à un pilote contrefait
d’invité d’exécuter des commandes HW quand les tampons partagés n’étaient pas
encore alloués, conduisant potentiellement à une condition d’utilisation de
mémoire après libération. Remarque : PVRDMA est désactivé dans Buster, mais
cela avait été corrigé préventivement au cas où cela changerait dans le futur.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1:3.1+dfsg-8+deb10u10.</p>

<p>Nous vous recommandons de mettre à jour vos paquets qemu.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de qemu,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/qemu">\
https://security-tracker.debian.org/tracker/qemu</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3362.data"
# $Id: $
