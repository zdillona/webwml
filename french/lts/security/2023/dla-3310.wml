#use wml::debian::translation-check translation="e2814a24d7dd17053dd363a279ee035a112f4a39" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Jan-Niklas Sohn, travaillant pour Zero Day Initiative de Trend Micro, a
découvert une vulnérabilité dans le serveur X d’X.Org. Un défaut d'utilisation
potentielle de mémoire après libération pouvait aboutir à une élévation des
droits si le serveur X était exécuté sans privilèges ou à une exécution de code
à distance lors de sessions X SSH retransmises.</p>


<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2:1.20.4-1+deb10u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets xorg-server.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de xorg-server,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/xorg-server">\
https://security-tracker.debian.org/tracker/xorg-server</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3310.data"
# $Id: $
