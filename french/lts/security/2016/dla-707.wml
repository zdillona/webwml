#use wml::debian::translation-check translation="55d1ac616e9ec6fe42ad1680e45c2ce133b85547" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>La restriction noexec de sudo pourrait être contournée si une
application, lancée à l'aide de sudo, exécutait les fonctions system(),
popen() ou wordexp() de la bibliothèque C avec un argument fourni par
l'utilisateur. Un utilisateur local autorisé à exécuter une application de
ce type à l'aide de sudo avec la restriction noexec pourrait éventuellement
utiliser ce défaut pour exécuter des commandes arbitraires avec des
privilèges augmentés.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7032">CVE-2016-7032</a>

<p>contournement de noexec au moyen de system() et popen()</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-7076">CVE-2016-7076</a>

<p>contournement de noexec au moyen de wordexp()</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 1.8.5p2-1+nmu3+deb7u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sudo.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-707.data"
# $Id: $
