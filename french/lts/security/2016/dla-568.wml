#use wml::debian::translation-check translation="ce41d997301872adfc27a79ea546429856226b67" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans WordPress, un outil de
blog. Le projet « Common Vulnerabilities and Exposures » (CVE) identifie
les problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5387">CVE-2016-5387</a>

<p>WordPress permet à des attaquants distants de contourner des
restrictions d'accès voulues et de retirer un attribut de catégorie d'un
envoi à l'aide de vecteurs non précisés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5832">CVE-2016-5832</a>

<p>L'outil de personnalisation dans WordPress permet à des attaquants
distants de contourner des restrictions de redirection voulues à l'aide de
vecteurs non précisés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5834">CVE-2016-5834</a>

<p>Une vulnérabilité de script intersite (XSS) dans la fonction
wp_get_attachment_link dans wp-includes/post-template.php dans WordPress
permet à des attaquants distants d'injecter un script web arbitraire ou du
code HTML à l'aide d'un nom de pièce jointe contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5835">CVE-2016-5835</a>

<p>WordPress permet à des attaquants distants d'obtenir des informations
sensibles de l'historique des modifications en exploitant la capacité de
lire un envoi liée à wp-admin/includes/ajax-actions.php et
wp-admin/revision.php.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5838">CVE-2016-5838</a>

<p>WordPress permet à des attaquants distants de contourner des
restrictions voulues de modification de mot de passe en exploitant la
connaissance d'un cookie.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5839">CVE-2016-5839</a>

<p>WordPress permet à des attaquants distants de contourner le mécanisme de
protection sanitize_file_name à l'aide de vecteurs non précisés.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 3.6.1+dfsg-1~deb7u11.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wordpress.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-568.data"
# $Id: $
