#use wml::debian::translation-check translation="f7fa639c88b1840f09568990ed50be9e3a5bc55b" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Jan-Niklas Sohn a découvert deux écritures en mémoire hors limites dans
les extensions Xkb ProcXkbSetGeometry et ProcXkbSetDeviceInfo du serveur
X.Org Server. Ces problèmes pouvaient être exploités par un attaquant pour
provoquer un déni de service, une élévation de privilèges ou l'exécution de
code arbitraire.</p>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans la
version 2:1.20.4-1+deb10u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets xorg-server.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de xorg-server, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/xorg-server">\
https://security-tracker.debian.org/tracker/xorg-server</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3068.data"
# $Id: $
