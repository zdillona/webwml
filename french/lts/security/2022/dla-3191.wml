#use wml::debian::translation-check translation="e2f9e1bbf6b5b0c0de57041f09c5f70ea2ba7364" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes ont été découverts dans Django, un cadriciel de
développement web basé sur Python :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45452">CVE-2021-45452</a>

<p>Storage.save permettait une traversée de répertoires si des noms de fichier
contrefaits lui étaient directement passés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-22818">CVE-2022-22818</a>

<p>La balise de patron {% debug %} n’encodait pas correctement le contenu
actuel. Cela pourrait conduire à une vulnérabilité de script intersite (XSS).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23833">CVE-2022-23833</a>

<p>Le HTTP MultiPartParser avait un problème par lequel certaines entrées pour
des formulaires multiparties pourraient aboutir à une boucle infinie lors de
l’analyse de fichiers téléversés.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1:1.11.29-1+deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-django.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3191.data"
# $Id: $
