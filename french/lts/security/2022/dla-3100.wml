#use wml::debian::translation-check translation="07a6f8cab18e1216c9cf36a9cc4c9176311d89ca" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Gson, une bibliothèque Java qui peut être utilisée pour convertir des
objets Java dans leurs représentations JSON et vice versa, était vulnérable
à un défaut de désérialisation. Une application pouvait désérialiser des
données non fiables sans vérifier suffisamment la validité des données
résultantes, permettant à un attaquant de contrôler l'état ou le flux
de l'exécution. Cela pouvait conduire à un déni de service ou même à
l'exécution de code arbitraire.</p>

<p>Pour Debian 10 Buster, ce problème a été corrigé dans la version
2.8.5-3+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libgoogle-gson-java.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libgoogle-gson-java,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libgoogle-gson-java">\
https://security-tracker.debian.org/tracker/libgoogle-gson-java</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3100.data"
# $Id: $
