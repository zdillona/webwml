#use wml::debian::translation-check translation="d78d16f5edda87c389c585a4166c1f20aa200f3f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans inetutils, une
collection de programmes courants pour le réseau.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0053">CVE-2019-0053</a>

<p>Le client telnet d’inetutils ne valide pas suffisamment les variables
d’environnement, cela peut conduire à des dépassements de pile. Ce problème est
limité à une exploitation locale à partir d’interpréteurs restreints.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40491">CVE-2021-40491</a>

<p>Le client ftp d’inetutils avant la version 2.2 ne valide pas les adresses
renvoyées par les réponses PASV/LSPV pour être sûr qu’elles correspondent à
l’adresse du serveur. Un serveur malveillant peut exploiter ce défaut pour
obtenir des services dans le réseau privé du client. Cela est similaire au
<a href="https://security-tracker.debian.org/tracker/CVE-2020-8284">CVE-2020-8284</a>
de curl.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39028">CVE-2022-39028</a>

<p>Le serveur telnet d’inetutils jusqu’à la version 2.3 avait un déréférencement
de pointeur NULL qu’un client pourrait déclencher en envoyant 0xff 0xf7 ou
0xff 0xf8. Dans une installation typique, l’application telnetd planterait, mais
le service telnet serait toujours disponible à travers inetd. Cependant, si
l’application telnetd plantait souvent dans un intervalle de temps réduit, le
service telnet deviendrait non disponible après qu’inetd a journalisé une erreur
« telnet/tcp failing (looping), service terminated ».</p></li>

</ul>

<p>Pour Debian 10 « Buster », ces problèmes ont été corrigés dans
la version 2:1.9.4-7+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets inetutils.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de inetutils,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/inetutils">\
https://security-tracker.debian.org/tracker/inetutils</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3205.data"
# $Id: $
