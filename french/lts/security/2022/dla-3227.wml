#use wml::debian::translation-check translation="fe299b8add85900ac8afce5770929c6b529cbad8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité potentielle de script intersite (XSS) a été découverte dans
ruby-rails-html-sanitizer, une bibliothèque pour nettoyer (ou <q>assainir</q>)
du HTML pour le rendu dans les applications web Ruby on Rails.</p>

<p>Pour Debian 10 « Buster », ce problème a été corrigé dans
la version 1.0.4-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby-rails-html-sanitizer.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ruby-rails-html-sanitizer,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ruby-rails-html-sanitizer">\
https://security-tracker.debian.org/tracker/ruby-rails-html-sanitizer</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3227.data"
# $Id: $
