#use wml::debian::translation-check translation="f105b78e2e3e2536315656a5dd1a68d4c1ffeb82" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux problèmes ont été découverts dans libvncserver, une bibliothèque
pour écrire son propre serveur VNC.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25708">CVE-2020-25708</a>

<p>Du fait de l'absence de certaines vérifications, il pouvait se produire
une division par zéro qui pouvait avoir pour conséquence un déni de
service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29260">CVE-2020-29260</a>

<p>Du fait d'une fuite de mémoire dans la fonction rfbClientCleanup(), un
attaquant distant pouvait être capable de provoquer un déni de service.</p>


<p>
<p>Pour Debian 10 Buster, ces problèmes ont été corrigés dans la version
0.9.11+dfsg-1.3+deb10u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libvncserver.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libvncserver,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libvncserver">\
https://security-tracker.debian.org/tracker/libvncserver</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3125.data"
# $Id: $
