#use wml::debian::translation-check translation="ba75adefcfb853727d458925afc6bdfe5646710b" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité d'injection de commande a été découverte dans FreeCAD,
un modélisateur 3D paramétrique, lors de l'importation de fichiers DWG avec
des noms de fichier contrefaits.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans la version
0.18~pre1+dfsg1-5+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets freecad.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de freecad, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/freecad">\
https://security-tracker.debian.org/tracker/freecad</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3076.data"
# $Id: $
