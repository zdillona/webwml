#use wml::debian::translation-check translation="8cafbb05bd1a613769436c7a5b0faa861b0d3798" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux problèmes ont été découverts dans SQLite, une base de données
embarquée répandue :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35525">CVE-2020-35525</a> :

<p>Correction d'un problème potentiel de déréférencement de pointeur NULL
dans le traitement d'une requête INTERSEC.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35527">CVE-2020-35527</a >:

<p>Correction d'un problème d'accès hors limites qui pouvait être exploité
au moyen de la directive ALTER TABLE dans des vues qui comportent des
clauses FROM imbriquées.</p></li>
</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 3.27.2-3+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sqlite3.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3107.data"
# $Id: $
