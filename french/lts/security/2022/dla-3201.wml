#use wml::debian::translation-check translation="078e40cfcc533d7bd177f136412ee2afb2132e0e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Yuchen Zeng et Eduardo Vela ont découvert un dépassement de tampon dans
NTFS-3G, un pilote NTFS d’écriture et lecture pour FUSE, dû à une validation
incorrecte de certaines métadonnées de NTFS. Un utilisateur local peut exploiter
ce défaut pour une élévation locale aux privilèges de superutilisateur.</p>


<p>Pour Debian 10 « Buster », ce problème a été corrigé dans
la version 1:2017.3.23AR.3-3+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ntfs-3g.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ntfs-3g,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ntfs-3g">\
https://security-tracker.debian.org/tracker/ntfs-3g</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3201.data"
# $Id: $
