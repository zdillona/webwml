#use wml::debian::translation-check translation="94f4e61838274d148d8b8fc8151d6bdf7fccfa4d" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été découvertes dans le serveur HTTP Apache :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44224">CVE-2021-44224</a>

<p>Lorsqu'il opérait comme mandataire de réacheminement, Apache dépendait
d'une configuration vulnérable à un déni de service ou à une contrefaçon de
requête côté serveur (SSRF).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44790">CVE-2021-44790</a>

<p>Un dépassement de tampon dans mod_lua pouvait avoir pour conséquences un
déni de service ou éventuellement l'exécution de code arbitraire.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 2.4.25-3+deb9u12.</p>

<p>Nous vous recommandons de mettre à jour vos paquets apache2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de apache2, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/apache2">\
https://security-tracker.debian.org/tracker/apache2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2907.data"
# $Id: $
