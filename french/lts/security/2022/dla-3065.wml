#use wml::debian::translation-check translation="b20b8045a7efabbbd560f323acd00a580b85e570" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pouvaient conduire à une élévation de privilèges, à un déni de service ou à
des fuites d'informations.</p>

<p>Malheureusement, cette mise à jour n'est pas disponible pour
l'architecture armel.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1108">CVE-2018-1108</a>

<p>Le pilote random pouvait générer des octets aléatoires au moyen de
/dev/random et de l'appel système getrandom() avant d'avoir recueilli
suffisamment d'entropie pour qu'ils ne soient pas prévisibles. Cela pouvait
compromettre la confidentialité et l'intégrité des communications chiffrées.</p>

<p>Le correctif original pour ce problème a dû être annulé parce qu'il
provoquait une suspension du processus d'amorçage sur plusieurs systèmes.
Dans cette version, le pilote random a été mis à jour, ce qui lui donne une
meilleure efficacité pour recueillir l'entropie sans avoir besoin d'un
générateur de nombres aléatoires (RNG) matériel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4149">CVE-2021-4149</a>

<p>Hao Sun a signalé un défaut dans le pilote du système de fichiers Btrfs.
Il y avait un potentiel déséquilibre de verrouillage dans un chemin
d'erreur. Un utilisateur local pouvait exploiter ce défaut pour un déni de
service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39713">CVE-2021-39713</a>

<p>L'outil syzbot a découvert une situation de compétition dans le
sous-système d'ordonnancement du réseau qui pouvait conduire à une
utilisation de mémoire après libération. Un utilisateur local pouvait
exploiter cela pour un déni de service (corruption de mémoire ou plantage)
ou éventuellement pour une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0494">CVE-2022-0494</a>

<p>Le scsi_ioctl() était vulnérable à une fuite d'informations exploitable
uniquement par les utilisateurs dotés des capacités CAP_SYS_ADMIN ou
CAP_SYS_RAWIO.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0812">CVE-2022-0812</a>

<p>Le transport RDMA pour NFS (xprtrdma) calculait de façon incorrecte la
taille des en-têtes de message ce qui pouvait conduire à une fuite
d'informations sensibles entre les serveurs et les clients NFS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0854">CVE-2022-0854</a>

<p>Ali Haider a découvert une potentielle fuite d'informations dans le
sous-système DMA. Sur les systèmes où la fonction swiotlb est nécessaire,
cela pouvait permettre à un utilisateur local de lire des informations
sensibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1011">CVE-2022-1011</a>

<p>Jann Horn a découvert un défaut dans l'implémentation de FUSE (système de
fichiers en espace utilisateur). Un utilisateur local autorisé à monter des
systèmes de fichiers FUSE pouvait exploiter cela pour provoquer une
utilisation de mémoire après libération et lire des informations sensibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1012">CVE-2022-1012</a>

<p>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-32296">CVE-2022-32296</a></p>

<p>Moshe Kol, Amit Klein et Yossi Gilad ont découvert une faiblesse dans la
randomisation de la sélection de port source TCP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1016">CVE-2022-1016</a>

<p>David Bouman a découvert un défaut dans le sous-système netfilter dans
lequel la fonction nft_do_chain n'initialisait pas les données de registre à
partir desquelles ou vers lesquelles les expressions nf_tables peuvent lire
ou écrire. Un attaquant local peut tirer avantage de cela pour lire des
informations sensibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1198">CVE-2022-1198</a>

<p>Duoming Zhou a découvert une situation de compétition dans le pilote
radio amateur 6pack qui pouvait conduire à une utilisation de mémoire après
libération. Un utilisateur local pouvait exploiter cela pour provoquer un
déni de service (corruption de mémoire ou plantage) ou éventuellement une
élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1199">CVE-2022-1199</a>

<p>Duoming Zhou a découvert des situations de compétition dans le protocole
radio amateur AX.25, qui pouvaient conduire à une utilisation de mémoire
après libération ou à un déréférencement de pointeur NULL. Un utilisateur
local pouvait exploiter cela pour provoquer un déni de service (corruption
de mémoire ou plantage) ou éventuellement une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1353">CVE-2022-1353</a>

<p>L'outil TCS Robot a découvert une fuite d'informations dans le
sous-système PF_KEY. Un utilisateur local peut recevoir un message netlink
quand un démon IPsec s'inscrit sur le noyau, et cela pouvait comprendre
des informations sensibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1516">CVE-2022-1516</a>

<p>Un défaut de déréférencement de pointeur NULL dans l'implémentation de
l'ensemble X.25 des protocoles réseau standardisés peut avoir pour
conséquence un déni de service.</p>

<p>Ce pilote est désactivé dans les configurations officielles du noyau de
Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1729">CVE-2022-1729</a>

<p>Norbert Slusarek a découvert une situation de compétition dans le
sous-système perf qui pouvait avoir pour conséquence une élévation locale
de privilèges vers ceux du superutilisateur. Les réglages par défaut dans
Debian évitent son exploitation à moins qu'une configuration plus
permissive ait été appliquée dans le sysctl kernel.perf_event_paranoid.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1734">CVE-2022-1734</a>

<p>Duoming Zhou a découvert des situations de compétition dans le pilote NFC
nfcmrvl qui pouvait conduire à une utilisation de mémoire après libération,
une double libération de zone de mémoire ou un déréférencement de pointeur
NULL. Un utilisateur local pouvait exploiter ces défauts pour un déni de
service (plantage ou corruption de mémoire) ou éventuellement pour une
élévation de privilèges.</p>

<p>Ce pilote est désactivé dans les configurations officielles du noyau de
Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1974">CVE-2022-1974</a>

<p>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-1975">CVE-2022-1975</a></p>

<p>Duoming Zhou a découvert que l'interface Netlink NFC était vulnérable à
un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2153">CVE-2022-2153</a>

<p><q>kangel</q> a signalé un défaut dans l'implémentation de KVM pour les
processeurs x86 qui pouvait conduire à un déréférencement de pointeur NULL.
Un utilisateur local autorisé à accéder à /dev/kvm pouvait exploiter cela
pour provoquer un déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21123">CVE-2022-21123</a>

<p>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-21125">CVE-2022-21125</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-21166">CVE-2022-21166</a></p>

<p>Divers chercheurs ont découvert des défauts dans les processeurs x86
d'Intel, communément appelées vulnérabilités <q>MMIO Stale Data</q>. Elles
sont similaires aux problèmes Microarchitectural Data Sampling (MDS)
précédemment publiés et pouvaient être exploitées par des utilisateurs
locaux pour divulguer des informations sensibles.</p>

<p>Pour certain processeurs, les atténuations pour ces problèmes nécessitent
un microcode mis à jour. Un paquet intel-microcode mis à jour pourra être
fourni ultérieurement. Le microcode mis à jour du processeur peut aussi
être disponible sous la forme d'un élément de la mise à jour des
microprogrammes du système (<q>BIOS</q>).</p>

<p>Plus d'informations sur la mitigation sont disponibles sur la page
<https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/processor_mmio_stale_data.html>
ou dans le paquet linux-doc-4.9.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23036">CVE-2022-23036</a>

<p>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-23037">CVE-2022-23037</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-23038">CVE-2022-23038</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-23039">CVE-2022-23039</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2022-23040">CVE-2022-23040</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-23041">CVE-2022-23041</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-23042">CVE-2022-23042</a> (XSA-396)</p>

<p>Demi Marie Obenour et Simon Gaiser de Invisible Things Lab ont découvert
des défauts dans plusieurs interfaces du périphérique de paravirtualisation
de Xen. Ces pilotes utilisent mal l'API de <q>grant table</q> de Xen d'une
manière qui pouvait être exploitée par un dorsal de périphérique malveillant
pour provoquer une corruption de données, des fuites d'informations
sensibles ou un déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23960">CVE-2022-23960</a>
<p>Des chercheurs de VUSec ont découvert que le tampon <q>Branch History</q>
dans les processeurs Arm pouvait être exploité pour créer des attaques par
canal auxiliaire d'information avec une exécution spéculative. Ce problème
est semblable à Spectre variante 2, mais demande des palliatifs
supplémentaires sur certains processeurs.</p>

<p>Cela pouvait être exploité pour obtenir des informations sensibles à
partir d'un contexte de sécurité différent, comme à partir de l'espace
utilisateur vers le noyau ou à partir d'un client KVM vers le noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24958">CVE-2022-24958</a>

<p>Un défaut a été découvert dans le sous-système USB Gadget qui pouvait
conduire à une utilisation de mémoire après libération. Un utilisateur local
autorisé à configurer des gadgets USB pouvait exploiter cela pour provoquer
un déni de service (plantage ou corruption de mémoire) ou éventuellement
pour une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26490">CVE-2022-26490</a>

<p>Des dépassements de tampon dans le pilote du cœur de ST21NFCA de
STMicroelectronics peuvent avoir pour conséquence un déni de service ou une
élévation de privilèges.</p>

<p>Ce pilote est désactivé dans les configurations officielles du noyau de
Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26966">CVE-2022-26966</a>

<p>Un défaut a été découvert dans le pilote réseau USB sr9700. Un
utilisateur local capable de brancher un périphérique USB conçu pour
l'occasion pouvait utiliser cela pour divulguer des informations sensibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-27223">CVE-2022-27223</a>

<p>Un défaut a été découvert dans le pilote du contrôleur USB mode Gadget
udc-xilinx. Sur les systèmes utilisant ce pilote, un hôte USB malveillant
pouvait exploiter cela pour provoquer un déni de service (plantage ou
corruption de mémoire) ou éventuellement pour exécuter du code arbitraire.</p>

<p>Ce pilote est désactivé dans les configurations officielles du noyau de
Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28356">CVE-2022-28356</a>

<p><q>Beraphin</q> a découvert que le pilote ANSI/IEEE 802.2 LLC type 2 ne
réalisait pas correctement le compte de références sur certains chemins
d'erreur. Un attaquant local peut tirer avantage de ce défaut pour
provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28390">CVE-2022-28390</a>

<p>Une vulnérabilité de double libération de zone de mémoire a été
découverte dans le pilote d'interface CPC-USB/ARM7 CAN/USB d'EMS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30594">CVE-2022-30594</a>

<p>Jann Horn a découvert un défaut dans l'interaction entre les
sous-systèmes ptrace et seccomp. Un processus mis dans un bac à sable en
utilisant seccomp(), mais encore autorisé à utiliser ptrace(), pouvait
exploiter cela pour supprimer les restrictions de seccomp.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32250">CVE-2022-32250</a>

<p>Aaron Adams a découvert une utilisation de mémoire après libération dans
Netfilter qui peut avoir pour conséquence une élévation locale de
privilèges vers ceux du superutilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-33981">CVE-2022-33981</a>

<p>Yuan Ming de l'université de Tsinghua a signalé une situation de
compétition dans le pilote de lecteur de disquette impliquant l'utilisation
du iotcl FDRAWCMD, qui pouvait conduire à une utilisation de mémoire après
libération. Un utilisateur local ayant accès à un lecteur de disquette
pouvait exploiter cela pour provoquer un déni de service (plantage ou
corruption de mémoire) ou éventuellement une élévation de privilèges. Cet
ioctl est maintenant désactivé par défaut.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 4.9.320-2.</p>

<p>Pour l'architecture armhf, cette mise à jour active des implémentations
optimisées de plusieurs algorithmes de chiffrement et de CRC. Pour au moins
AES, cela devrait supprimer une attaque par canal auxiliaire temporelle qui
pouvait conduire à la fuite d'informations sensibles.</p>

<p>Cette mise à jour comprend beaucoup plus de corrections de bogues à
partir des mises à jour de stable 4.9.304 à 4.9.320 incluse. Le pilote
random a été rétroporté à partir de Linux 5.19 et corrige de nombreux
problèmes de performance et d'exactitude. Certains changements devraient
être visibles :</p>

<p> – La taille de la source d'entropie est maintenant de 256 bits au lieu
de 4096. Pour le permettre, vous pouvez avoir besoin d'ajuster la
configuration des systèmes de surveillance système ou de recueil d'entropie
dans l'espace utilisateur.</p>

<p> – Sur les systèmes sans générateur de nombres aléatoires matériel (RNG),
le noyau enregistrera beaucoup plus d'utilisations de /dev/urandom avant
qu'il soit complètement initialisé. Ces utilisations étaient auparavant
sous-estimées et ce n'est pas une régression.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3065.data"
# $Id: $
