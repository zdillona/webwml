#use wml::debian::translation-check translation="e76e9a7d15849251c4d36efca791457c3961e3e5" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Jakub Wilk a découvert qu'APT, le gestionnaire de paquet de haut niveau,
ne réalisait pas correctement les vérifications d'authentification pour les
paquets source téléchargés par la commande « apt-get source ». Ce problème
n'affecte que les cas où les paquets source sont téléchargés avec cette
commande ; il n'affecte pas l'installation et la mise à niveau normale des
paquets Debian.
(<a href="https://security-tracker.debian.org/tracker/CVE-2014-0478">CVE-2014-0478</a>)</p>

<p>APT gérait incorrectement l'option de configuration Verify-Host. Si un
attaquant distant était capable de réaliser une attaque de type « homme du
milieu », ce défaut pourrait éventuellement être utilisé pour voler
l'identité du dépôt. Cela ne concerne que les systèmes qui utilisent les
sources d'APT pour des connexions https (cela nécessite que le paquet
apt-transport-https soit installé).
(<a href="https://security-tracker.debian.org/tracker/CVE-2011-3634">CVE-2011-3634</a>)</p>

<p>Pour Debian 6 <q>Squeeze</q>, ces problèmes ont été corrigés dans la
version 0.8.10.3+squeeze2 d'apt.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2014/dla-0005.data"
# $Id: $
