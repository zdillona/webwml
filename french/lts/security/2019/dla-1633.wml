#use wml::debian::translation-check translation="244380ed6414a14f265795cff6ac8dab1d04e3a3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs défauts ont été corrigés dans SQLite, un moteur de base de données
SQL.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-2518">CVE-2017-2518</a>

<p>Un bogue d’utilisation de mémoire après libération dans l’optimiseur de
requêtes peut provoquer un dépassement de tampon et un plantage d'application
à l'aide d'une construction SQL contrefaite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-2519">CVE-2017-2519</a>

<p>Une taille insuffisante du compte de référence dans les objets Table
pourrait conduire à déni de service ou l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-2520">CVE-2017-2520</a>

<p>L’interface sqlite3_value_text() renvoyait un tampon trop petit pour contenir
la chaîne compète plus le zéro terminal quand l’entrée était un zeroblob. Cela
pourrait conduire à l’exécution de code arbitraire ou à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10989">CVE-2017-10989</a>

<p>SQLite gérait incorrectement les blobs RTree sous-dimensionnés dans une base
de données contrefaite menant à une lecture hors limites de tampon basé sur le
tas ou éventuellement à un autre impact non précisé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8740">CVE-2018-8740</a>

<p>Les bases de données dont le schéma est corrompu, utilisant une construction
CREATE TABLE AS pourraient causer un déréférencement de pointeur NULL.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 3.8.7.1-1+deb8u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sqlite3.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1633.data"
# $Id: $
