#use wml::debian::translation-check translation="244380ed6414a14f265795cff6ac8dab1d04e3a3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été trouvées dans le composant journald de
systemd qui peuvent conduire à un plantage ou à l’exécution de code.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16864">CVE-2018-16864</a>

<p>Une allocation de mémoire sans limites, qui pourrait conduire à un conflit de
pile avec une autre région de mémoire, a été découverte dans systemd-journald
lorsque plusieurs entrées sont envoyées à la socket de journal. Un attaquant
local, ou distant si systemd-journal-remote est utilisé, peut utiliser ce défaut
pour planter systemd-journald ou exécuter du code avec les privilèges de
journald .</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16865">CVE-2018-16865</a>

<p>Une allocation de mémoire sans limites, qui pourrait conduire à un conflit de
pile avec une autre région de mémoire, a été découverte dans systemd-journald
lorsqu’un programme avec des arguments longs de ligne de commande appelle
syslog. Un attaquant local peut utiliser ce défaut pour planter systemd-journald
ou augmenter ses privilèges. Les versions jusqu’à la version 240 sont
vulnérables.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 215-17+deb8u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets systemd.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1639.data"
# $Id: $
