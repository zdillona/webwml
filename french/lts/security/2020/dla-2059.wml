#use wml::debian::translation-check translation="42774c430ea914d4aebadbc6608dd7ba13b4eae7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans git, un système de
gestion de versions distribué, rapide et évolutif.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1348">CVE-2019-1348</a>

<p>Il a été signalé que l'option --export-marks de fast-import de git est
aussi exposée au moyen de la fonctionnalité export-marks=... de la commande
in-stream, permettant l'écrasement de chemins arbitraires.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1387">CVE-2019-1387</a>

<p>Les noms des sous modules ne sont pas validés de façon suffisamment
stricte, permettant des attaques très ciblées au moyen de l'exécution
distante de code lors de la réalisation de clones récursifs.</p></li>

</ul>

<p>En complément, cette mise à jour corrige un certain nombre de problèmes
de sécurité qui existent seulement si git fonctionne sur un système de
fichiers NTFS (<a href="https://security-tracker.debian.org/tracker/CVE-2019-1349">CVE-2019-1349</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-1352">CVE-2019-1352</a>
et <a href="https://security-tracker.debian.org/tracker/CVE-2019-1353">CVE-2019-1353</a>).</p>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1:2.1.4-2.1+deb8u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets git.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2059.data"
# $Id: $
