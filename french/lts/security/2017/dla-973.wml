#use wml::debian::translation-check translation="adc5cbd36ecf754028e80bbdee567a58bca03b81" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités de déni de service ont été identifiées dans
strongSwan, une suite IKE/IPsec utilisant le projet de test à données
aléatoires OSS-Fuzz de Google.</p>
<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9022">CVE-2017-9022</a>

<p>Les clés publiques RSA passées au greffon gmp ne sont pas suffisamment
validées avant la tentative de vérification de signature, aussi une entrée
non valable pourrait mener à une exception de virgule flottante et au
plantage du processus. Un certificat avec une clé publique préparée de façon
appropriée envoyé par un pair pourrait être utilisé pour une attaque par
déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9023">CVE-2017-9023</a>

<p>Les types ASN.1 CHOICE ne sont pas correctement gérés par l'analyseur
ASN.1 lors de l'analyse de certificats X.509 avec des extensions qui
utilisent ces types. Cela pourrait conduire à une boucle infinie du
processus lors de l'analyse d'un certificat spécialement contrefait.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 4.5.2-1.5+deb7u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets strongswan.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-973.data"
# $Id: $
