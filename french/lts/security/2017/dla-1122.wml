#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité de sécurité a été découverte dans Asterisk, une boîte
à outils au code source ouvert d’autocommutateur (PBX) et de téléphonie, qui
pourrait conduire à une exécution de commande non autorisée.</p>

<p>Le module app_minivm possédait une option de configuration de programme
<q>externnotify</q> exécutée par l’application de plan de numérotation
MinivmNotify. L’application utilise le nom d’identifiant et le numéro d’appelant
comme partie de la chaîne construite passée à l’interpréteur du système
d’exploitation pour son interprétation et exécution. Un nom et numéro
contrefaits, dans la mesure où ils peuvent provenir d'une source non fiable,
permettaient une injection de commande arbitraire d’interpréteur.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1:1.8.13.1~dfsg1-3+deb7u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets asterisk.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1122.data"
# $Id: $
