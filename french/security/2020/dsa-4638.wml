#use wml::debian::translation-check translation="6fbca817d300a20809fec1f06bb2ae3c92689156" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le navigateur web
Chromium.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19880">CVE-2019-19880</a>

<p>Richard Lorenz a découvert un problème dans la bibliothèque sqlite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19923">CVE-2019-19923</a>

<p>Richard Lorenz a découvert un problème de lecture hors limites dans la
bibliothèque sqlite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19925">CVE-2019-19925</a>

<p>Richard Lorenz a découvert un problème dans la bibliothèque sqlite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19926">CVE-2019-19926</a>

<p>Richard Lorenz a découvert une erreur d'implémentation dans la
bibliothèque sqlite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6381">CVE-2020-6381</a>

<p>Le National Cyber Security Centre du Royaume-Uni a découvert un problème
de dépassement d'entier dans la bibliothèque JavaScript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6382">CVE-2020-6382</a>

<p>Soyeon Park et Wen Xu ont découvert une erreur de type dans la
bibliothèque JavaScript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6383">CVE-2020-6383</a>

<p>Sergei Glazunov a découvert une erreur de type dans la bibliothèque
JavaScript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6384">CVE-2020-6384</a>

<p>David Manoucheri a découvert un problème d'utilisation de mémoire après
libération dans WebAudio.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6385">CVE-2020-6385</a>

<p>Sergei Glazunov a découvert une erreur d'application de politique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6386">CVE-2020-6386</a>

<p>Zhe Jin a découvert un problème d'utilisation de mémoire après
libération dans le traitement de la parole.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6387">CVE-2020-6387</a>

<p>Natalie Silvanovich a découvert une erreur d'écriture hors limites dans
l'implémentation de WebRTC.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6388">CVE-2020-6388</a>

<p>Sergei Glazunov a découvert une erreur de lecture hors limites dans
l'implémentation de WebRTC.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6389">CVE-2020-6389</a>

<p>Natalie Silvanovich a découvert une erreur d'écriture hors limites dans
l'implémentation de WebRTC.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6390">CVE-2020-6390</a>

<p>Sergei Glazunov a découvert une erreur de lecture hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6391">CVE-2020-6391</a>

<p>Michał Bentkowski a découvert une validation insuffisante des entrées
non dignes de confiance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6392">CVE-2020-6392</a>

<p>La Microsoft Edge Team a découvert une erreur d'application de
politique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6393">CVE-2020-6393</a>

<p>Mark Amery a découvert une erreur d'application de politique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6394">CVE-2020-6394</a>

<p>Phil Freo a découvert une erreur d'application de politique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6395">CVE-2020-6395</a>

<p>Pierre Langlois a découvert une erreur de lecture hors limites dans la
bibliothèque JavaScript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6396">CVE-2020-6396</a>

<p>William Luc Ritchie a découvert une erreur dans la bibliothèque skia.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6397">CVE-2020-6397</a>

<p>Khalil Zhani a découvert une erreur d'interface utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6398">CVE-2020-6398</a>

<p>pdknsk a découvert une variable non initialisée dans la bibliothèque
pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6399">CVE-2020-6399</a>

<p>Luan Herrera a découvert une erreur d'application de politique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6400">CVE-2020-6400</a>

<p>Takashi Yoneuchi a découvert une erreur dans le partage de ressources
d'origine croisée (CORS).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6401">CVE-2020-6401</a>

<p>Tzachy Horesh a découvert une validation insuffisante des entrées
d'utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6402">CVE-2020-6402</a>

<p>Vladimir Metnew a découvert une erreur d'application de politique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6403">CVE-2020-6403</a>

<p>Khalil Zhani a découvert une erreur d'interface utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6404">CVE-2020-6404</a>

<p>kanchi a découvert une erreur dans Blink/Webkit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6405">CVE-2020-6405</a>

<p>Yongheng Chen et Rui Zhong ont découvert un problème de lecture hors
limites dans la bibliothèque sqlite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6406">CVE-2020-6406</a>

<p>Sergei Glazunov a découvert un problème d'utilisation de mémoire après
libération.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6407">CVE-2020-6407</a>

<p>Sergei Glazunov a découvert une erreur de lecture hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6408">CVE-2020-6408</a>

<p>Zhong Zhaochen a découvert une erreur d'application de politique de
partage de ressources d'origine croisée (CORS).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6409">CVE-2020-6409</a>

<p>Divagar S et Bharathi V ont découvert une erreur dans l'implémentation
d'omnibox.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6410">CVE-2020-6410</a>

<p>evil1m0 a découvert une erreur d'application de politique.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6411">CVE-2020-6411</a>

<p>Khalil Zhani a découvert une validation insuffisante des entrées
d'utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6412">CVE-2020-6412</a>

<p>Zihan Zheng a découvert une validation insuffisante des entrées
d'utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6413">CVE-2020-6413</a>

<p>Michał Bentkowski a découvert une erreur dans Blink/Webkit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6414">CVE-2020-6414</a>

<p>Lijo A.T a découvert une erreur d'application de politique de navigation
sécurisée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6415">CVE-2020-6415</a>

<p>Avihay Cohen a découvert une erreur d'implémentation dans la
bibliothèque JavaScript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6416">CVE-2020-6416</a>

<p>Woojin Oh a découvert une validation insuffisante des entrées non dignes
de confiance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6418">CVE-2020-6418</a>

<p>Clement Lecigne a découvert une erreur de type dans la bibliothèque
JavaScript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6420">CVE-2020-6420</a>

<p>Taras Uzdenov a découvert une erreur d'application de politique.</p></li>

</ul>

<p>Pour la distribution oldstable (Stretch), le suivi de sécurité pour
chromium est terminé.</p>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 80.0.3987.132-1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets chromium.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de chromium, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4638.data"
# $Id: $
