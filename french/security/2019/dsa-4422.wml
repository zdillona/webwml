#use wml::debian::translation-check translation="9bf43d9ebe1fe8f0f648cd5c0431b530d1105d92" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le serveur web HTTP
Apache.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17189">CVE-2018-17189</a>

<p>Gal Goldshtein de F5 Networks a découvert une vulnérabilité de déni de
service dans mod_http2. En envoyant des requêtes mal formées, le flux
http/2 pour cette requête occupait inutilement un processus du serveur
nettoyant les données entrantes, avec pour conséquence un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17199">CVE-2018-17199</a>

<p>Diego Angulo de ImExHS a découvert que mod_session_cookie ne respecte
pas la date d'expiration.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0196">CVE-2019-0196</a>

<p>Craig Young a découvert que le traitement des requêtes http/2 dans
mod_http2 pourrait être utilisé pour accéder à de la mémoire libérée dans
une comparaison de chaîne lors de la détermination de la méthode d'une
requête, et donc traiter la requête incorrectement.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0211">CVE-2019-0211</a>

<p>Charles Fol a découvert une augmentation de droits d'un processus fils
avec des droits inférieurs vers le processus parent exécuté en tant que
superutilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0217">CVE-2019-0217</a>

<p>Une situation de compétition dans mod_auth_digest lorsqu'il est exécuté
dans un serveur multitâche (threaded server) pourrait permettre à un
utilisateur avec une identité valable de s'identifier avec un autre nom
d'utilisateur, en contournant les restrictions de contrôle d'accès
configurées. Ce problème a été découvert par Simon Kappel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0220">CVE-2019-0220</a>

<p>Bernhard Lorenz de Alpha Strike Labs GmbH a signalé que la normalisation
d'URL était gérée de manière incohérente. Quand le composant chemin d'une
URL de requête contient plusieurs barres obliques consécutives (« / »),
des directives telles que LocationMatch et RewriteRule doivent les compter
comme dupliquées dans les expressions rationnelles tandis que d'autres
aspects du traitement des serveurs les écraseront de façon implicite.</p></li>

</ul>

<p>Pour la distribution stable (Stretch), ces problèmes ont été corrigés
dans la version 2.4.25-3+deb9u7.</p>

<p>Cette mise à jour fournit aussi des corrections de bogues qui étaient
prévues pour inclusion dans la prochaine mise à jour de stable, y compris
un correctif pour une régression provoquée par un correctif de sécurité
dans la version 2.4.25-3+deb9u6.</p>

<p>Nous vous recommandons de mettre à jour vos paquets apache2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de apache2, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/apache2">\
https://security-tracker.debian.org/tracker/apache2</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4422.data"
# $Id: $
