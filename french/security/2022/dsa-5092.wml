#use wml::debian::translation-check translation="14d91c5e8b991574bd42901de0e30d4eb4a8bb7e" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pouvaient conduire à une élévation de privilèges, un déni de service ou à
des fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43976">CVE-2021-43976</a>

<p>Zekun Shen et Brendan Dolan-Gavitt ont découvert un défaut dans la
fonction mwifiex_usb_recv() du pilote USB WiFi-Ex de Marvell. Un attaquant
capable de se connecter à un périphérique USB contrefait pouvait tirer
avantage de ce défaut pour provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0330">CVE-2022-0330</a>

<p>Sushma Venkatesh Reddy a découvert une absence de vidage de TLB du GPU
dans le pilote i915, ayant pour conséquences un déni de service ou une
élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0435">CVE-2022-0435</a>

<p>Samuel Page et Eric Dumazet ont signalé un dépassement de pile dans le
module réseau pour le protocole TIPC (Transparent Inter-Process
Communication), ayant pour conséquences un déni de service ou
éventuellement l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0516">CVE-2022-0516</a>

<p>Une vérification insuffisante dans le sous-système KVM pour s390x
pouvait permettre un accès non autorisé à la mémoire en lecture ou en
écriture.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0847">CVE-2022-0847</a>

<p>Max Kellermann a découvert un défaut dans les attributs de traitement de
tampon de tube. Un attaquant pouvait tirer avantage de ce défaut pour une
élévation locale de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-22942">CVE-2022-22942</a>

<p>Le mauvais traitement du descripteur de fichier dans le pilote de GPU
virtuel de VMware (vmwgfx) pouvait avoir pour conséquences une fuite
d'informations ou une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24448">CVE-2022-24448</a>

<p>Lyu Tao a signalé un défaut dans l'implémentation de NFS dans le noyau
Linux lors du traitement de requêtes pour ouvrir un répertoire sur un
fichier ordinaire, qui pouvait avoir pour conséquence une fuite
d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24959">CVE-2022-24959</a>

<p>Une fuite de mémoire a été découverte dans la fonction
yam_siocdevprivate() du pilote YAM pour AX.25, qui pouvait avoir pour
conséquence un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-25258">CVE-2022-25258</a>

<p>Szymon Heidrich a signalé que le sous-système USB Gadget manque de
certaines validations des requêtes du descripteur d'OS de l'interface,
avec pour conséquence une corruption de mémoire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-25375">CVE-2022-25375</a>

<p>Szymon Heidrich a signalé que le gadget USB RNDIS manque de validation
de la taille de la commande RNDIS_MSG_SET, avec pour conséquence une fuite
d'informations à partir de la mémoire du noyau.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 5.10.92-2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5092.data"
# $Id: $
