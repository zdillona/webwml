#use wml::debian::translation-check translation="facb505d95d838491acb3aab03c314a249510e68" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans lighttpd, un serveur
web rapide ayant une empreinte mémoire minimale.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-37797">CVE-2022-37797</a>

<p>Une requête HTTP non valable (initialisation de connexion de websocket)
peut provoquer un déréférencement de pointeur NULL dans le module wstunnel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41556">CVE-2022-41556</a>

<p>Une fuite de ressource dans mod_fastcgi et mod_scgi pouvait conduire à
un déni de service après un grand nombre de mauvaises requêtes HTTP.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 1.4.59-1+deb11u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets lighttpd.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de lighttpd, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/lighttpd">\
https://security-tracker.debian.org/tracker/lighttpd</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5243.data"
# $Id: $
