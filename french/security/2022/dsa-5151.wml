#use wml::debian::translation-check translation="5bc655b04ad10666ec6447e58622dd561e17e87e" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans smarty3,
le moteur de compilation de modèles pour PHP. Les auteurs de modèles
peuvent exécuter des méthodes PHP statiques restreintes ou même du code PHP
arbitraire en confectionnant une chaîne mathématique malveillante ou en
choisissant des noms de <q>{block}</q> ou de fichier <q>{include}</q> non
valables. Si une chaîne mathématique était passée à la fonction math en
tant que données fournies par l'utilisateur, des utilisateurs distants
étaient capables également d'exécuter du code PHP arbitraire.</p>

<p>Pour la distribution oldstable (Buster), ces problèmes ont été corrigés
dans la version 3.1.33+20180830.1.3a78a21f+selfpack1-1+deb10u1.</p>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 3.1.39-2+deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets smarty3.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de smarty3, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/smarty3">\
https://security-tracker.debian.org/tracker/smarty3</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5151.data"
# $Id: $
