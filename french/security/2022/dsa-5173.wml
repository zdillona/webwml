#use wml::debian::translation-check translation="56cd72821e38080a5918e1d806721388e90fd0f6" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pouvaient conduire à une élévation de privilèges, à un déni de service ou à
des fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4197">CVE-2021-4197</a>

<p>Eric Biederman a signalé que des vérifications de permission incorrectes
dans l'implémentation de la migration de processus de groupe de contrôle
peuvent permettre à un attaquant local d'élever ses privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0494">CVE-2022-0494</a>

<p>Le scsi_ioctl() était vulnérable à une fuite d'informations exploitable
uniquement par les utilisateurs dotés des capacités CAP_SYS_ADMIN ou
CAP_SYS_RAWIO.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0812">CVE-2022-0812</a>

<p>Le transport RDMA pour NFS (xprtrdma) calculait de façon incorrecte la
taille des en-têtes de message ce qui pouvait conduire à une fuite
d'informations sensibles entre les serveurs et les clients NFS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0854">CVE-2022-0854</a>

<p>Ali Haider a découvert une potentielle fuite d'informations dans le
sous-système DMA. Sur les systèmes où la fonction swiotlb est nécessaire,
cela pouvait permettre à un utilisateur local de lire des informations
sensibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1011">CVE-2022-1011</a>

<p>Jann Horn a découvert un défaut dans l'implémentation de FUSE (système de
fichiers en espace utilisateur). Un utilisateur local autorisé à monter des
systèmes de fichiers FUSE pouvait exploiter cela pour provoquer une
utilisation de mémoire après libération et lire des informations sensibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1012">CVE-2022-1012</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2022-32296">CVE-2022-32296</a>

<p>Moshe Kol, Amit Klein et Yossi Gilad ont découvert une faiblesse dans la
randomisation de la sélection de port de source TCP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1016">CVE-2022-1016</a>

<p>David Bouman a découvert un défaut dans le sous-système netfilter dans
lequel la fonction nft_do_chain n'initialisait pas les données de registre à
partir desquelles ou vers lesquelles les expressions nf_tables peuvent lire
ou écrire. Un attaquant local peut tirer avantage de cela pour lire des
informations sensibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1048">CVE-2022-1048</a>

<p>Hu Jiahui a découvert une situation de compétition dans le sous-système
son qui peut avoir pour conséquence une utilisation de mémoire après
libération. Un utilisateur local autorisé à accéder au périphérique son PCM
peut tirer avantage de ce défaut pour planter le système ou éventuellement
pour obtenir une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1195">CVE-2022-1195</a>

<p>Lin Ma a découvert des situations de compétition dans les pilotes radio
amateur 6pack et mkiss, qui pouvaient conduire à une utilisation de mémoire
après libération. Un utilisateur local pouvait exploiter cela pour
provoquer un déni de service (corruption de mémoire ou plantage) ou
éventuellement pour obtenir une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1198">CVE-2022-1198</a>

<p>Duoming Zhou a découvert une situation de compétition dans le pilote
radio amateur 6pack qui pouvait conduire à une utilisation de mémoire après
libération. Un utilisateur local pouvait exploiter cela pour provoquer un
déni de service (corruption de mémoire ou plantage) ou éventuellement une
élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1199">CVE-2022-1199</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2022-1204">CVE-2022-1204</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2022-1205">CVE-2022-1205</a>

<p>Duoming Zhou a découvert des situations de compétition dans le protocole
radio amateur AX.25, qui pouvaient conduire à une utilisation de mémoire
après libération ou à un déréférencement de pointeur NULL. Un utilisateur
local pouvait exploiter cela pour provoquer un déni de service (corruption
de mémoire ou plantage) ou éventuellement une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1353">CVE-2022-1353</a>

<p>L'outil TCS Robot a découvert une fuite d'informations dans le
sous-système PF_KEY. Un utilisateur local peut recevoir un message netlink
quand un démon IPsec s'inscrit sur le noyau, et cela pouvait comprendre
des informations sensibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1419">CVE-2022-1419</a>

<p>Minh Yuan a découvert une situation de compétition dans le pilote de GPU
virtuel vgem qui peut conduire à une utilisation de mémoire après
libération. Un utilisateur local autorisé à accéder au périphérique GPU peut
exploiter cela pour provoquer un déni de service (plantage ou corruption de
mémoire) ou éventuellement une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1516">CVE-2022-1516</a>

<p>Un défaut de déréférencement de pointeur NULL dans l'implémentation de
l'ensemble X.25 des protocoles réseau standardisés peut avoir pour
conséquence un déni de service.</p>

<p>Ce pilote est désactivé dans les configurations officielles du noyau de
Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1652">CVE-2022-1652</a>

<p>Minh Yuan a découvert une situation de compétition dans le pilote de
lecteur de disquette qui peut conduire à une utilisation de mémoire après
libération. Un utilisateur local autorisé à accéder à un lecteur de
disquette peut exploiter cela pour provoquer un déni de service
(plantage ou corruption de mémoire) ou éventuellement une élévation de
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1729">CVE-2022-1729</a>

<p>Norbert Slusarek a découvert une situation de compétition dans le
sous-système perf qui pouvait avoir pour conséquence une élévation locale
de privilèges vers ceux du superutilisateur. Les réglages par défaut dans
Debian évitent son exploitation à moins qu'une configuration plus
permissive ait été appliquée dans le sysctl kernel.perf_event_paranoid.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1734">CVE-2022-1734</a>

<p>Duoming Zhou a découvert des situations de compétition dans le pilote
NFC nfcmrvl qui pouvaient conduire à une utilisation de mémoire après
libération, une double libération de zone de mémoire ou un déréférencement
de pointeur NULL. Un utilisateur local pouvait exploiter des défauts pour
un déni de service (plantage ou corruption de mémoire) ou éventuellement
pour une élévation de privilèges.</p>

<p>Ce pilote est désactivé dans les configurations officielles du noyau de
Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1974">CVE-2022-1974</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2022-1975">CVE-2022-1975</a>

<p>Duoming Zhou a découvert que l'interface Netlink NFC était vulnérable à
un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2153">CVE-2022-2153</a>

<p><q>kangel</q> a signalé un défaut dans l'implémentation de KVM pour les
processeurs x86 qui pouvait conduire à un déréférencement de pointeur NULL.
Un utilisateur local autorisé à accéder à /dev/kvm pouvait exploiter cela
pour provoquer un déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21123">CVE-2022-21123</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2022-21125">CVE-2022-21125</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2022-21166">CVE-2022-21166</a>

<p>Divers chercheurs ont découvert des défauts dans les processeurs x86
d'Intel, communément appelées vulnérabilités <q>MMIO Stale Data</q>. Elles
sont similaires aux problèmes Microarchitectural Data Sampling (MDS)
précédemment publiés et pouvaient être exploitées par des utilisateurs
locaux pour divulguer des informations sensibles.</p>

<p>Pour certain processeurs, les atténuations pour ces problèmes nécessitent
un microcode mis à jour. Un paquet intel-microcode mis à jour pourra être
fourni ultérieurement. Le microcode mis à jour du processeur peut aussi
être disponible sous la forme d'un élément de la mise à jour des
microprogrammes du système (<q>BIOS</q>)./p>

<p>Plus d'informations sur la mitigation sont disponibles sur la page
<https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/processor_mmio_stale_data.html>
ou dans le paquet linux-doc-4.19</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23960">CVE-2022-23960</a>

<p>Des chercheurs de VUSec ont découvert que le tampon <q>Branch History</q>
dans les processeurs Arm pouvait être exploité pour créer des attaques par
canal auxiliaire d'information avec une exécution spéculative. Ce problème
est semblable à Spectre variante 2, mais demande des palliatifs
supplémentaires sur certains processeurs./p>

<p>Cela était atténué auparavant pour les architectures ARM 32 bits (armel
et armhf) et est aussi désormais atténué pour l'architecture ARM 64 bits
(arm64).</p>

<p>Cela pouvait être exploité pour obtenir des informations sensibles à
partir d'un contexte de sécurité différent, comme à partir de l'espace
utilisateur vers le noyau ou à partir d'un client KVM vers le noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26490">CVE-2022-26490</a>

<p>Des dépassements de tampon dans le pilote du cœur de ST21NFCA de
STMicroelectronics peuvent avoir pour conséquence un déni de service ou une
élévation de privilèges./p>

<p>Ce pilote est désactivé dans les configurations officielles du noyau de
Debian./p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-27666">CVE-2022-27666</a>

<p><q>valis</q> a signalé un possible dépassement de tampon dans le code de
transformation IPsec ESP. Un utilisateur local peut tirer avantage de ce
défaut pour provoquer un déni de service ou une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28356">CVE-2022-28356</a>

<p><q>Beraphin</q> a découvert que le pilote ANSI/IEEE 802.2 LLC type 2 ne
réalisait pas correctement le compte des références sur certains chemins
d'erreurs. Un attaquant local peut tirer avantage de ce défaut pour
provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28388">CVE-2022-28388</a>

<p>Une vulnérabilité de double libération de zone de mémoire a été
découverte dans le pilote d'interface USB2CAN de 8devices.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28389">CVE-2022-28389</a>

<p>Une vulnérabilité de double libération de zone de mémoire a été
découverte dans le pilote de l'interface CAN BUS Analyzer de Microchip.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28390">CVE-2022-28390</a>

<p>Une vulnérabilité de double libération de zone de mémoire a été
découverte dans le pilote d'interface CPC-USB/ARM7 CAN/USB d'EMS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-29581">CVE-2022-29581</a>

<p>Kyle Zeng a découvert un bogue de compte de références dans le filtre
réseau cls_u32 qui peut conduire à une utilisation de mémoire après
libération. Un utilisateur local peut exploiter cela pour provoquer un déni
de service (plantage ou corruption de mémoire) ou éventuellement une
élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30594">CVE-2022-30594</a>

<p>Jann Horn a découvert un défaut dans l'interaction entre les
sous-systèmes ptrace et seccomp. Un processus mis dans un bac à sable en
utilisant seccomp(), mais encore autorisé à utiliser ptrace(), pouvait
exploiter cela pour supprimer les restrictions de seccomp./p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32250">CVE-2022-32250</a>

<p>Aaron Adams a découvert une utilisation de mémoire après libération dans
Netfilter qui peut avoir pour conséquence une élévation locale de
privilèges vers ceux du superutilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-33981">CVE-2022-33981</a>

<p>Yuan Ming de l'université de Tsinghua a signalé une situation de
compétition dans le pilote de lecteur de disquette impliquant l'utilisation
du iotcl FDRAWCMD, qui pouvait conduire à une utilisation de mémoire après
libération. Un utilisateur local ayant accès à un lecteur de disquette
pouvait exploiter cela pour provoquer un déni de service (plantage ou
corruption de mémoire) ou éventuellement une élévation de privilèges. Cet
ioctl est maintenant désactivé par défaut./p></li>

</ul>

<p>Pour la distribution oldstable (Buster), ces problèmes ont été corrigés
dans la version 4.19.249-2.</p>

<p>À cause d'un problème dans le service de signature (Cf. bogue Debian
nº 1012741), le module vport-vxlan ne peut pas être chargé pour le noyau
signé pour amd64 dans cette mise à jour.</p>

<p>Cette mise à jour corrige aussi une régression dans le sous-système
d'ordonnanceur réseau (bogue nº 1013299).</p>

<p>Pour les architectures Arm 32 bits (armel et armhf), cette mise à jour
active des implémentations optimisées de plusieurs algorithmes de
chiffrement et de CRC. Pour au moins AES, cela devrait supprimer une attaque
par canal auxiliaire temporelle qui pouvait conduire à la fuite
d'informations sensibles.</p>

<p>Cette mise à jour comprend beaucoup plus de corrections de bogues à
partir des mises à jour de stable 4.9.304 à 4.9.320 incluse. Le pilote
random a été rétroporté à partir de Linux 5.19 et corrige de nombreux
problèmes de performance et d'exactitude. Certains changements devraient
être visibles :</p>

<ul>

<li>La taille de la source d'entropie est maintenant de 256 bits au lieu
de 4096. Pour le permettre, vous pouvez avoir besoin d'ajuster la
configuration des systèmes de surveillance système ou de recueil d'entropie
dans l'espace utilisateur.</li>

<li>Sur les systèmes sans générateur de nombres aléatoires matériel (RNG),
le noyau enregistrera beaucoup plus d'utilisations de /dev/urandom avant
qu'il soit complètement initialisé. Ces utilisations étaient auparavant
sous-estimées et ce n'est pas une régression.</li>

</ul>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5173.data"
# $Id: $
