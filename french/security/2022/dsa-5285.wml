#use wml::debian::translation-check translation="3e39e450fa3e6314b6c30ab396de64010ccf812e" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans Asterisk,
un autocommutateur téléphonique privé (PBX) au code source ouvert. Des
dépassements de tampon et d'autres erreurs de programmations pouvaient être
exploités pour la divulgation d'informations ou l'exécution de code
arbitraire.</p>

<p>Un soin particulier doit être pris lors de la mise à niveau vers cette
nouvelle version amont. Certains fichiers et options de configuration ont
été modifiés afin de remédier à certaines vulnérabilités de sécurité.
Principalement, le récepteur TLS pjsip TLS n'accepte désormais que des
connexions TLSv1.3 dans la configuration par défaut. Cela peut être annulé
en ajoutant <q>method=tlsv1_2</q> au transport dans pjsip.conf. Voir aussi
<a href="https://issues.asterisk.org/jira/browse/ASTERISK-29017">\
https://issues.asterisk.org/jira/browse/ASTERISK-29017</a>.</p>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 1:16.28.0~dfsg-0+deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets asterisk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de asterisk, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/asterisk">\
https://security-tracker.debian.org/tracker/asterisk</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5285.data"
# $Id: $
