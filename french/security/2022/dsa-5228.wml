#use wml::debian::translation-check translation="c5e6056b6cfe4c47b624ccc6e664ff19613d23b7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans gdk-pixbuf, la
bibliothèque GDK Pixbuf.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44648">CVE-2021-44648</a>

<p>Sahil Dhar a signalé une vulnérabilité de dépassement de tas lors du
décodage du flux de données d'image compressé avec lzw. Cela peut avoir
pour conséquences l'exécution de code arbitraire ou un déni de service lors
du traitement d'une image GIF mal formée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-46829">CVE-2021-46829</a>

<p>Pedro Ribeiro a signalé une vulnérabilité de dépassement de tas lors de
la composition ou le nettoyage d'images fixes dans des fichiers GIF. Cela
peut avoir pour conséquences l'exécution de code arbitraire ou un déni de
service lors du traitement d'une image GIF mal formée.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 2.42.2+dfsg-1+deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets gdk-pixbuf.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de gdk-pixbuf, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/gdk-pixbuf">\
https://security-tracker.debian.org/tracker/gdk-pixbuf</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5228.data"
# $Id: $
