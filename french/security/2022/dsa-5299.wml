#use wml::debian::translation-check translation="e2025ff809f329753f1a9f1bfabdb104ec91427e" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans OpenEXR,
des outils en ligne de commande et une bibliothèque pour le format d'image
OpenEXR. Des dépassements de tampon ou des lectures hors limites pouvaient
conduire à un déni de service (plantage d'application) lors du traitement
d'un fichier image mal formé.</p>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 2.5.4-2+deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openexr.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openexr, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/openexr">\
https://security-tracker.debian.org/tracker/openexr</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5299.data"
# $Id: $
