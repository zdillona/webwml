#use wml::debian::translation-check translation="42b6b660e613679ceb4726fb1a5aebaa47b68d96"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Múltiples investigadores han descubierto vulnerabilidades en la manera en que
los diseños del procesador Intel han implementado el traspaso especulativo de datos
alojados en estructuras temporales de microarquitectura («buffers»). Este
defecto podría permitir que un atacante que controle un proceso sin privilegios
lea información sensible, incluyendo información del núcleo y de todos los demás
procesos en ejecución en el sistema, o cruzar los límites entre huésped y anfitrión para leer
datos de la memoria del anfitrión.</p>

<p>Consulte <a href="https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/mds.html">\
https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/mds.html</a>
para más detalles.</p>

<p>Para resolver completamente estas vulnerabilidades es necesario instalar también
una actualización del microcódigo de la CPU. Se proporcionará un paquete intel-microcode
actualizado (solo disponible en Debian non-free) mediante otro DSA. El
microcódigo de CPU actualizado podría estar disponible también como parte de una actualización del firmware
del sistema ("BIOS").</p>

<p>Además, esta actualización incluye la corrección de una regresión que provoca
abrazos mortales en el controlador de loopback y que fue introducida por la actualización
a la 4.9.168 incluida en la última versión de Stretch.</p>

<p>Para la distribución «estable» (stretch), estos problemas se han corregido en
la versión 4.9.168-1+deb9u2.</p>

<p>Le recomendamos que actualice los paquetes de linux.</p>

<p>Para información detallada sobre el estado de seguridad de linux, consulte su página
en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4444.data"
