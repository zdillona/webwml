#use wml::debian::translation-check translation="d948bd9589feece12bc709aa1b03a99f72526fc6"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en el núcleo Linux que
pueden dar lugar a elevación de privilegios, a denegación de servicio o a fugas de
información.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3016">CVE-2019-3016</a>

    <p>Se descubrió que la implementación de KVM para x86 no
    siembre realizaba vaciados («flushes») de la TLB cuando era necesario si estaba habilitada
    la funcionalidad «vaciado de TLB paravirtualizado» («paravirtualised TLB flush»). Esto podía dar lugar a revelación de
    información sensible dentro de un huésped VM.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19462">CVE-2019-19462</a>

    <p>La herramienta syzkaller encontró una ausencia de comprobación de errores en la biblioteca
    <q>relay</q>, utilizada para implementar varios ficheros bajo debugfs. Un usuario
    local autorizado a acceder a debugfs podría usar esto para provocar denegación
    de servicio (caída) o, posiblemente, elevación de privilegios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-0543">CVE-2020-0543</a>

    <p>Investigadores de la VU Amsterdam descubrieron que en algunas CPU Intel
    con soporte de las instrucciones RDRAND y RDSEED, parte de un valor
    aleatorio generado por estas instrucciones puede ser usado en una ejecución
    especulativa posterior en cualquier núcleo de la misma CPU física.
    Dependiendo de cómo utilicen las aplicaciones estas instrucciones, un
    usuario local o un huésped VM podrían usar esto para obtener información
    sensible, como claves criptográficas de otros usuarios o de otras VM.</p>

    <p>Esta vulnerabilidad se puede mitigar mediante una actualización de microcódigo, ya sea
    como parte del firmware del sistema (BIOS) o a través del paquete
    intel-microcode de la sección non-free del archivo de Debian. Esta actualización del núcleo
    solo proporciona mecanismos para informar de la vulnerabilidad y la opción para
    inhabilitar la mitigación si esta no es necesaria.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10711">CVE-2020-10711</a>

    <p>Matthew Sheets informó de problemas de desreferencia de punteros NULL en el
    subsistema SELinux al recibir paquetes CIPSO con categoría nula. Un
    atacante remoto puede aprovechar este defecto para provocar denegación de
    servicio (caída). Tenga en cuenta que este problema no afecta a los paquetes
    binarios distribuidos en Debian ya que en ellos CONFIG_NETLABEL no está habilitado.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10732">CVE-2020-10732</a>

    <p>Se encontró una fuga de información de la memoria privada del núcleo al espacio de usuario
    en la implementación del volcado de la memoria de procesos del espacio de usuario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10751">CVE-2020-10751</a>

    <p>Dmitry Vyukov informó de que el subsistema SELinux no gestionaba
    correctamente la validación de múltiples mensajes, lo que podía permitir que un atacante
    con privilegios eludiera las restricciones SELinux de netlink.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10757">CVE-2020-10757</a>

    <p>Fan Yang informó de un defecto en la manera en que mremap manejaba las páginas gigantes DAX
    que permitía que un usuario local elevara sus privilegios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12114">CVE-2020-12114</a>

    <p>Piotr Krysiuk descubrió una condición de carrera entre las operaciones
    umount y pivot_root en el núcleo del sistema de archivos (vfs). Un usuario local
    con capacidad CAP_SYS_ADMIN en cualquier espacio de nombres de usuario podía usar
    esto para provocar denegación de servicio (caída).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12464">CVE-2020-12464</a>

    <p>Kyungtae Kim informó de una condición de carrera en el núcleo USB que podía
    dar lugar a un «uso tras liberar». No está clara la forma de explotar
    esto, pero podría dar lugar a denegación de servicio (caída o
    corrupción de memoria) o a elevación de privilegios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12768">CVE-2020-12768</a>

    <p>Se descubrió un fallo en la implementación de KVM para procesadores AMD
    que podía dar lugar a filtraciones del contenido de la memoria. No está claro el impacto de esto
    en la seguridad.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12770">CVE-2020-12770</a>

    <p>Se descubrió que el controlador sg (SCSI «generic») no
    liberaba correctamente recursos internos en un caso concreto de error.
    Un usuario local autorizado a acceder a un dispositivo sg podía, posiblemente, usar
    esto para provocar denegación de servicio (agotamiento de recursos).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13143">CVE-2020-13143</a>

    <p>Kyungtae Kim informó de una potencial escritura fuera de límites en la memoria dinámica («heap»)
    del subsistema de dispositivos («gadget») USB. Un usuario local autorizado a escribir en
    el sistema de archivos de configuración de gadgets podía usar esto para provocar
    denegación de servicio (caída o corrupción de memoria) o, potencialmente,
    elevación de privilegios.</p></li>

</ul>

<p>Para la distribución «estable» (buster), estos problemas se han corregido
en la versión 4.19.118-2+deb10u1. Esta versión corrige también algunos fallos
relacionados que no tienen identificativo CVE propio y una regresión en la
cabecera &lt;linux/swab.h&gt; de UAPI introducida en la versión anterior
(fallo #960271).</p>

<p>Le recomendamos que actualice los paquetes de linux.</p>

<p>Para información detallada sobre el estado de seguridad de linux, consulte su
página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4699.data"
