#use wml::debian::translation-check translation="2592e40c5d7143a6f575ff96f6127ba4fb3f18d5"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han encontrado varias vulnerabilidades en el servidor Apache HTTPD.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1927">CVE-2020-1927</a>

    <p>Fabrice Perez informó de que ciertas configuraciones de mod_rewrite tienen
    propensión a una redirección abierta.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1934">CVE-2020-1934</a>

    <p>Chamal De Silva descubrió que el módulo mod_proxy_ftp usa
    memoria no inicializada al actuar como proxy hacia un backend FTP malicioso.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9490">CVE-2020-9490</a>

    <p>Felix Wilhelm descubrió que un valor de la cabecera 'Cache-Digest' preparado de una
    manera determinada en una petición HTTP/2 podría provocar una caída en el
    momento en el que, posteriormente, el servidor intente hacer un PUSH HTTP/2 de un recurso.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11984">CVE-2020-11984</a>

    <p>Felix Wilhelm informó de un fallo de desbordamiento de memoria en el módulo
    mod_proxy_uwsgi que podría dar lugar a revelación de información o, potencialmente,
    a ejecución de código remoto.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11993">CVE-2020-11993</a>

    <p>Felix Wilhelm informó de que, cuando estaban habilitados el rastreo («trace») o la depuración para el
    módulo HTTP/2, ciertos patrones de tráfico periférico podían provocar el registro («logging»)
    de sentencias en la conexión equivocada, dando lugar a un uso concurrente de
    áreas de memoria («memory pools»).</p></li>

</ul>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 2.4.38-3+deb10u4.</p>

<p>Le recomendamos que actualice los paquetes de apache2.</p>

<p>Para información detallada sobre el estado de seguridad de apache2, consulte su página
en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/apache2">https://security-tracker.debian.org/tracker/apache2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4757.data"
