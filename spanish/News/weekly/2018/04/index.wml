#use wml::debian::projectnews::header PUBDATE="2018-12-11" SUMMARY="Le damos la bienvenida a las DPN, Publicado Debian 9.6, Rescate de paquetes, Compilaciones reproducibles ingresa en la Software Conservancy, Rust disponible en 14 arquitecturas, Decisión del CTTE, Calendario de la próxima congelación, «Bits» del equipo antiacoso, Se acercan las Buster BSP, Informes, Peticiones de ayuda, No solo código, Enlaces rápidos de los medios sociales de Debian"
#use wml::debian::acronyms
#use wml::debian::translation-check translation="09966b2f1ad834f67126c0f4a6f131d6f995ec06"
#use wml::debian::translation-check translation="b03ec83ecc1e06d008afc98e83bdbd410ab50bd0"

# $Rev$
# Status: [content-frozen]


## substitute XXX with the number (expressed in letter) of the issue.
## please note that the var issue is not automagically localized, so
## translators need to put it directly in their language!
## example: <intro issue="fourth" />

## Use &#35; to escape # for IRC channels
## Use &#39 for single quotes

<intro issue="cuarta" />
<toc-display/>

<toc-add-entry name="newdpn">¡Le damos la bienvenida a las noticias del proyecto Debian!</toc-add-entry>

<p>Esperamos que disfrute con esta edición de las DPN.</p>

<p>Para otras noticias, consulte el blog oficial de Debian 
<a href="https://bits.debian.org">Bits from Debian</a> y siga
<a href="https://micronews.debian.org">https://micronews.debian.org</a>, que
también alimenta (vía RSS) al perfil @debian en varias redes sociales.</p>
<p> Al final de estas Noticias del proyecto hemos añadido una sección <b>Enlaces rápidos</b> con
enlaces a una selección de las noticias publicadas a través de nuestros otros medios.</p>

<p>El equipo de seguridad de Debian publica diariamente avisos
(<a href="$(HOME)/security/2018/">Avisos de seguridad de 2018</a>). 
Léalos con atención y suscríbase a la <a href="https://lists.debian.org/debian-security-announce/">lista de
correo de seguridad</a>.</p>

<toc-add-entry name="internal">Noticias internas/Acontecimientos</toc-add-entry>

<p><b>Debian 9 actualizado: publicada la versión 9.6</b></p>
<p>El proyecto Debian <a href="https://www.debian.org/News/2018/20181110">anunció</a>
la sexta actualización de su distribución «estable» Debian 9 (nombre en clave <q>Stretch</q>) a la versión 9.6
el 10 de noviembre de 2018.</p>

#<p>The Debian project also <a href="https://www.debian.org/News/2018/2018MMDD">announced</a>
#the YYYY update of its oldstable distribution Debian 8 (codename <q>Jessie</q>) on DD Month 2018
#to point release 8.Y.</p>

<p>Esta nueva versión añadió correcciones de problemas de seguridad, junto con unos pocos
ajustes para problemas graves. Los avisos de seguridad se han publicado ya
de forma independiente, y hacemos referencia a ellos donde corresponde.

Puede actualizar una instalación existente a esta nueva versión
haciendo que el sistema de gestión de paquetes apunte a una de las muchas réplicas HTTP de Debian.
En la dirección siguiente puede encontrar el listado completo de réplicas: <a href="https://www.debian.org/mirror/list">https://www.debian.org/mirror/list</a>.
</p>



<p><b>Rescate de paquetes</b></p>

<p>Tobias Frost <a href="https://lists.debian.org/debian-devel-announce/2018/09/msg00003.html">anunció</a>
un nuevo añadido al manual de referencia del desarrollador de Debian, sobre rescate de paquetes.
<a href="https://www.debian.org/doc/manuals/developers-reference/ch05.en.html#package-salvaging">El rescate de paquetes</a>
permite que paquetes que oficialmente no son huérfanos ni están abandonados sean mantenidos por otros
desarrolladores o desarrolladoras o por nuevos contribuidores o contribuidoras una vez considerados algunos factores de elegibilidad.</p>

<p>Este proceso difiere de la gestión de paquetes de MIA en que permite que los paquetes
descuidados u olvidados vuelvan al redil. Hay un conjunto de <a href="https://wiki.debian.org/PackageSalvaging">directrices</a>
que resumen las fases del proceso, junto con información adicional
y una sección con preguntas frecuentes, disponibles en la wiki de Debian.</p>


<p><b>¡Compilaciones reproducibles ingresa en la Software Conservancy!</b></p>

<p><a href="https://reproducible-builds.org/">Compilaciones reproducibles</a> ha ingresado en la <a href="https://sfconservancy.org/about/">Software Freedom Conservancy</a>, una organización sin ánimo de lucro
501(c)(3) que ayuda en la promoción, desarrollo y defensa de proyectos de software libre
y de código abierto (FLOSS por sus siglas en inglés). A través de la SFC, los proyectos miembro pueden recibir donaciones
reservadas para el beneficio de un proyecto FLOSS específico.</p>

<p>El proyecto Compilaciones reproducibles, que <a href="https://wiki.debian.org/ReproducibleBuilds/History">empezó como un proyecto</a> dentro de la comunidad Debian,
es crítico también para el propio trabajo de Conservancy en materia de conformidades: una compilación que no se puede
verificar puede contener código que desencadene responsabilidades diferentes en cuanto a conformidad con la licencia
de las que el receptor espera.</p>

<p>Junto con su ingreso en Conservancy, Compilaciones reproducibles también recibirá una donación de 300.000 dólares estadounidenses
de la <a href="https://handshake.org/">Handshake Foundation</a>, lo que impulsará los esfuerzos del proyecto para asegurar la
salud y la usabilidad futuras del software libre.</p>

<p><b>Rust disponible en 14 arquitecturas de Debian</b></p>

<p>John Paul Adrian Glaubitz anunció que <a href="https://lists.debian.org/debian-devel-announce/2018/11/msg00000.html">Rust está disponible en 14 arquitecturas Debian</a>
y expresó su agradecimiento a los muchos contribuidores y contribuidoras que ayudaron a conseguirlo.
Las arquitecturas añadidas <a href="https://buildd.debian.org/status/package.php?p=rustc&amp;suite=unstable">más recientemente</a>
a la relación de arquitecturas soportadas son: mips, mips64el, mipsel y powerpcspe.</p>
<p>Este trabajo es el resultado del
esfuerzo conjunto de muchas personas con talento y de la labor en el proyecto original LLVM, que
ha corregido muchísimos fallos en los backends MIPS y PowerPC, y que ha añadido soporte
para la variante PowerPCSPE.</p>

<p><b>Documentación de los y las titulares de los derechos de autor en debian/copyright</b></p>


<p>El equipo de FTP ha publicado algunas <a href="https://lists.debian.org/debian-devel-announce/2018/10/msg00004.html">aclaraciones</a>
con respecto a la atribución de derechos de autor en debian/copyright. Algunos de los puntos principales son:</p>

<ul>
<li>Salvo que una licencia establezca explícitamente que las atribuciones de derechos de autor solo son aplicables a
las distribuciones de código fuente, se aplican igualmente a las distribuciones de código fuente y a las de binarios. Los derechos de autor
se deben documentar en debian/copyright por razones de conformidad con la licencia.</li>

<li>Tenga presentes las <a href="https://www.debian.org/doc/debian-policy/ch-archive.html#copyright-considerations">2.3 Consideraciones sobre los derechos de autor</a>: todo paquete debe acompañarse de
copias literales de su información de derechos de autor y de su licencia de distribución en el fichero
/usr/share/doc/package/copyright.</li>

<li>En raras ocasiones, los administradores y administradoras de FTP han determinado, por un lado, que no es factible la
atribución completa de los derechos de autor y, por otro, que, dada la naturaleza del paquete, un aviso de
derechos de autor adecuado no necesita listar a todos sus titulares; 
no debería asumirse que esta tolerancia sea aplicable a otros paquetes.</li>
</ul>

<p>El equipo de FTP afirma que documentar los titulares de los derechos de autor en debian/copyright es
una buena idea.</p>

<p><b>Decisión del CTTE sobre secuencias de parches específicos para cada proveedor</b></p>

<p>El comité técnico <a href="https://lists.debian.org/debian-devel-announce/2018/11/msg00004.html">aprobó una resolución</a> sobre el uso de
secuencias de parches específicos para cada proveedor en el archivo de Debian, en síntesis:</p>

<p>El comité reconoce la necesidad de que los paquetes se comporten de forma diferente cuando se compilan
en distribuciones diferentes, pero esto se debería hacer utilizando paquetes fuente
distintos o como parte del proceso de compilación, por medio de prácticas actuales o futuras,
como parches con comportamiento condicional o aplicación de parches durante la compilación
en lugar de hacerlo en tiempo de desempaquetado de los fuentes.</p>

<p>Puesto que en la actualidad varios paquetes utilizan esta funcionalidad, es necesario un
periodo de transición razonable. No obstante, se considerarán paquetes con fallos desde
el momento en que se acepte esta resolución, pero no con la severidad suficiente
como para justificar su inmediata eliminación de Debian.</p>

<p>Tras la publicación de Buster, la presencia de secuencias de parches específicos para cada proveedor
será una violación de una directiva MUST en la normativa de Debian.</p>

<p>Por lo tanto, el comité resuelve:
Cualquier uso de la funcionalidad de dpkg de secuencias de parches específicos para cada proveedor es un fallo para paquetes
en el archivo de Debian (incluyendo contrib y non-free).</p>

<p>Tras la publicación de Buster se prohíbe el uso de la funcionalidad de secuencias de parches específicos para cada
proveedor en el archivo de Debian.</p>

<p>Para información adicional y acceso al debate original consulte el <a href="https://bugs.debian.org/904302">fallo #904302</a>.</p>

<p><b>Equipo responsable de la publicación: calendario de la próxima congelación, formas de ayudar</b></p>

<p>El equipo responsable de la publicación se está <a href="https://lists.debian.org/debian-devel-announce/2018/09/msg00004.html">preparando</a> para la fase inicial de la congelación para buster.</p>

<p>Se pide a los desarrolladores y desarrolladoras que repasen sus planes y evalúen calendarios
realistas para completar los cambios e incluirlos en buster.</p>

<p>Los cambios pueden ser alojados en «experimental» para evitar perturbaciones. Tenga presente
que otros voluntarios pueden no tener la misma capacidad para trabajar en pos de los objetivos de usted.
Sugerimos que los fallos pendientes de corregir se corrijan por medio de <a href="https://wiki.debian.org/NonMaintainerUpload">NMU</a> hoy mejor que mañana.</p>

<p>El calendario oficial de congelación para buster es:</p>
<ul>
 <li>2019-01-12 - Congelación de transiciones («Transition freeze»)</li>
 <li>2019-02-12 - Congelación débil («Soft-freeze»)</li>
 <li>2019-03-12 - Congelación total («Full-freeze»)</li></ul>

<p>Para información detallada sobre las fases de la congelación y lo que suponen para usted, consulte el calendario
y la <a href="https://release.debian.org/buster/freeze_policy.html">normativa de congelación</a> para buster.</p>

<p>Si le gustaría ayudarnos a conseguir que buster salga a tiempo y tiene capacidad para
ayudar a corregir fallos críticos para la publicación (RC, por sus siglas en inglés: «release critical») en «pruebas» antes de la congelación de transiciones, puede hacerlo ahora
consultando la <a href="https://udd.debian.org/bugs/">lista de fallos RC</a> o uniéndose al canal irc &#35;debian-bugs en irc.oftc.net.</p>

<p><b>«Bits» del equipo antiacoso</b></p>


<p>El <a href="https://wiki.debian.org/AntiHarassment">equipo antiacoso de Debian</a> es el punto de contacto para cualquier miembro
de la comunidad que quiera ayudar a crear un entorno más cordial y
respetuoso en Debian, y también es el punto de contacto al que dirigir informes o preocupaciones
sobre comportamientos inadecuados o abusos. El equipo emitirá de forma habitual
breves informes a la comunidad.</p>

<p>Si ve algún comportamiento que considera que merece atención, por favor, háganoslo
saber. No espere hasta que el problema se haga demasiado grande; podemos ayudar
a reducir de forma amistosa la intensidad del problema o actuar como mediadores. Los miembros también pueden remitir
información que no requiera que se tome ninguna acción, pero sí archivarla para el caso de que el
problema se agrave en el futuro.</p>

<p>Puede ponerse en contacto con el equipo en <a href="mailto:antiharassment@debian.org">antiharassment@debian.org</a>.</p> 

<p>Algunas notas destacadas sobre nuestra actividad reciente:</p> 

<p>Una solicitud de intervención en un conflicto sobre un paquete considerado ofensivo,
emitimos nuestra recomendación: fallo <a href="https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=907199">#907199</a></p>

<p>Tuvimos una solicitud sobre eliminación de mensajes de lista de correos y respondimos.</p>

<p>Varios asistentes a la DebConf 18 actuaron como un equipo antiacoso 
que gestionó conflictos entre asistentes y una posible violación del código de conducta,
mediando en problemas menores y aconsejando durante la celebración de la conferencia.</p>

<p>Durante la DebConf18 se envió varias veces vía micronews un recordatorio general
sobre el CdC.</p>

<p>Hemos estado involucrados en los debates acerca de la normativa de la DebConf sobre fotografías,
y tenemos previsto hacer pronto una propuesta.</p>

<p><b>Nueva estudiante en prácticas para Outreachy</b></p>

<p>Debian da la bienvenida a <a href="https://bits.debian.org/2018/11/welcome-outreachy-intern-2018-2019.html">Anastasia Tsikoza como nuestra nueva estudiante en prácticas para Outreachy</a>. El programa
Outreachy proporciona periodos de prácticas para personas pertenecientes a grupos tradicionalmente
infrarrepresentados en la tecnología. Anastasia, cuyos mentores serán Paul Wise y Raju Devidas,
trabajará en la mejora de la integración de las distribuciones derivadas de Debian con la infraestructura
y la comunidad de Debian.</p>



<p><b>Noticias varias para desarrolladores y desarrolladoras</b></p>

<p>Paul Wise <a href="https://lists.debian.org/debian-devel-announce/2018/10/msg00002.html">publicó</a> una nueva edición de Noticias varias para desarrolladores y desarrolladoras, la número 46 («Misc Developer News &#35;46»). Los titulares
incluyen:</p>

<p>Abierto el periodo para pruebas beta de debhelper compat level 12, que se espera que pase a «estable» en Debian buster.</p>

<p>Disponible una nueva adaptación de la variante <q>riscv64</q> de RISC-V (64 bits little endian) en Debian Ports.</p>

<p>Con la reciente publicación de debcargo 2.0.0 en crates.io, puede crear paquetes Debian desde su contenedor Rust preferido y subirlos al archivo de Debian.</p>

<p>Publicado devscripts 2.18.5 con nuevas funcionalidades de uscan como la verificación de etiquetas firmadas en git y la asignación de un valor «auto» a dversionmangle.</p>

<p>Llamamiento de Chris Lamb pidiendo más voluntarios para el equipo de FTP.</p>


<toc-add-entry name="events">Próximos eventos y crónicas</toc-add-entry>

<p><b>Próximos eventos</b></p>

<p><b>MiniDebConf Marsella 2019</b></p>
<p>
Del 25 al 26 de mayo tendrá lugar una miniDebConf en Marsella (Francia), con
dos días de charlas, charlas relámpago, fiesta de firma de claves, comida, y ... cerveza.
Lea el <a href="https://france.debian.net/pipermail/minidebconf/2018-November/000159.html">anuncio</a>
y visite la <a href="https://wiki.debian.org/DebianEvents/fr/2019/Marseille">página wiki del evento</a>,
donde puede conocer todos los detalles, registrarse para el evento y ayudar en la
organización.</p>

<p><b>¡Fiestas caza-(fallos)-Buster!</b></p>

<p>Hay muchas «fiestas caza-fallos» (BSP, por sus siglas en inglés) en nuestros calendarios, que se celebrarán próximamente y cuyos
esfuerzos se centrarán en abordar y corregir fallos RC, los cuales retrasarían
la publicación de Debian 10 (buster). Las BSP están abiertas a todo el que quiera y tenga
la capacitación para implicarse. ¡Acérquese y ayúdenos a hacer de esta versión un éxito!</p>

<p><b>Países Bajos, Venlo, 12 al 13 de enero de 2019</b></p>

<p>A celebrar en Transceptor Technology e insign.it.</p>

<p>Reciba nuestra bienvenida si quiere contribuir a Debian, cualquiera que sea su
nivel de experiencia. No necesita estar contribuyendo ya a Debian.
Simplemente intentar reproducir un fallo y documentar su experiencia es
de por sí útil.</p>

<p><a href="https://lists.debian.org/debian-devel-announce/2018/11/msg00005.html">Anuncio de la BSP de Venlo</a></p>
<p><a href="https://wiki.debian.org/BSP/2019/01/nl/Venlo">Wiki de la BSP de Venlo</a></p>


<p><b>Canadá, Montreal, 19 al 20 de enero de 2019</b></p>

<p>A celebrar en Eastern Bloc, Montreal, Canadá.</p>

<p>A diferencia de la que organizamos para la publicación de Stretch, esta BSP se celebrará a lo largo de un
fin de semana completo, por lo que esperamos que personas procedentes de otras provincias de Canadá y de los
Estados Unidos de América puedan venir.</p>
<p>Puede registrarse en la página wiki, en la que encontrará información
sobre transporte, alojamiento, comida y otras cuestiones útiles. El proyecto Debian
podría financiar los gastos de asistencia a esta BSP.</p>

<p><a href="https://lists.debian.org/debian-devel-announce/2018/12/msg00000.html">Anuncio de la BSP de Montreal</a></p>
<p><a href="https://wiki.debian.org/BSP/2019/01/ca/Montreal">Wiki de la BSP de Montreal</a></p>


#<p>
#venue yet unknon
#There will be a <a href="https://wiki.debian.org/BSP/2019/01/us/Austin">Bug
#Squashing Party</a> on 25&ndash;27 January 2019, held in Austin, Texas, USA.
#</p>

<p><b>Alemania, Bonn, 22 al 24 de febrero de 2019</b></p>

<p>Tarent solutions GmbH, Rochussstr. 2, 53123 Bonn, Alemania.</p>

<p>La BSP está programada justo entre la congelación débil y la congelación total, representando,
por lo tanto, una oportunidad perfecta para un esprint caza-fallos RC realmente
eficiente y concentrado.</p>

<p>El lugar dispone de sitio para veinte personas, ubicaciones separadas para quienes
prefieran trabajar en equipos pequeños y también sitio para socializar.</p>

<p><a href="https://lists.debian.org/debian-devel-announce/2018/11/msg00001.html">Anuncio de la BSP de Bonn</a></p>
<p><a href="https://wiki.debian.org/BSP/2019/02/de/Bonn">Wiki de la BSP de Bonn</a></p>


<p><b>Austria, Salzburgo, 5 al 7 de abril de 2019</b></p>

<p>Oficinas de Conova Communications GmbH [CONOVA], ubicadas cerca del
aeropuerto W.A. Mozart en Salzburgo.</p>

<p>Nos alegra invitarle a la sexta fiesta caza-fallos Debian a celebrar en
Salzburgo, Austria.</p> 

<p>Es necesaria una breve inscripción en la página wiki [BSPSBG] para facilitar la
organización del evento. En la misma página encontrará información
sobre transporte, alojamiento (financiado) y otras cuestiones útiles.</p>

<p><a href="https://lists.debian.org/debian-devel-announce/2018/11/msg00006.html">Anuncio de la BSP de Salzburgo</a></p>
<p><a href="https://wiki.debian.org/BSP/2019/04/Salzburg">Wiki de la BSP de Salzburgo</a></p>


<toc-add-entry name="reports">Informes</toc-add-entry>
## It's easier to link to the monthly reports for the LTS section and the RB links rather than
# summarize each report or number of packages. Feel free to input this information if you need to fill
# the issue out
#
<p><b>Informes mensuales de Freexian sobre LTS</b></p>

<p>Freexian publica <a href="https://raphaelhertzog.com/tag/Freexian+LTS/">informes mensuales</a>
sobre el trabajo de los contribuidores remunerados para el soporte a largo plazo de Debian.
</p>

<p><b>Estado actual de Compilaciones reproducibles</b></p>

<p>Siga en el <a
href="https://reproducible-builds.org/blog/">blog de
Compilaciones reproducibles</a> los informes mensuales de su trabajo en el ciclo de vida de <q>Buster</q>.
</p>


<toc-add-entry name="help">Se necesita ayuda</toc-add-entry>

##<p><b>Teams needing help</b></p>
## Teams needing help
## $Link to the email from $Team requesting help


<p><b>Paquetes que necesitan ayuda</b></p>

## link= link to the mail report from wnpp@debian.org to debian-devel ML
## orphaned= number of packages orphaned according to $link
## rfa= number of packages up for adoption according to $link

<wnpp link="https://lists.debian.org/debian-devel/2018/11/msg00733.html"
	orphaned="1311"
	rfa="157" />

<p><b>Fallos para principiantes</b></p>

## check https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=newcomer and add outstanding + forwarded + pending upload
<p>
Debian tiene una etiqueta de clasificación de fallos llamada <q>newcomer</q> («principiante»), usada para marcar fallos adecuados para que los nuevos
contribuidores los utilicen como punto de entrada para trabajar en paquetes específicos.

En la actualidad hay <a href="https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=newcomer">212</a>
fallos disponibles marcados <q>newcomer</q>.
</p>

<toc-add-entry name="morethancode">No solo código</toc-add-entry>

<p>Carl Chenet opinó: <a href="https://carlchenet.com/you-think-the-visual-studio-code-binary-you-use-is-a-free-software-think-again/">¿piensa que el Visual Studio Code binario que utiliza es software
libre? Piénselo de nuevo.</a> Señala algunas de las prácticas de licenciamiento utilizadas en
relación con la licencia MIT, una licencia de software libre permisiva.</p>

<p>Elana Hashman comparte información sobre los <a href="https://hashman.ca/pygotham-2018/">recursos de una charla en PyGotham 2018</a>
titulada «The Black Magic of Python Wheels» («La magia negra de Python Wheels»), basada en dos años de trabajo con
auditwheel y con la plataforma manylinux.</p>

<p>Benjamin Mako Hill habla acerca de <a href="https://mako.cc/copyrighteous/what-we-lose-when-we-move-from-social-to-market-exchange">qué perdemos cuando nos desplazamos del intercambio social al de mercado</a>,
sobre el intercambio de dinero como remuneración por algo frente al intercambio de hospitalidad.</p>

<p>Molly de Blanc compartió cómo llegó a organizar un programa de donación <a href="http://deblanc.net/blog/2018/11/24/conservancy-match/">Conservancy Match</a>
a beneficio de la Software Freedom Conservancy.</p>


<toc-add-entry name="code">Programas, programadores y contribuidores</toc-add-entry>
<p><b>Nuevos y nuevas responsables de paquetes desde el 19 de agosto</b></p>
##Run the ./new-package-maintainers script locally
##./new-package-maintainers YYYMMDD

<p>Damos la bienvenida a: Pedro Loami Barbosa dos Santos, Alexandros Afentoulis,
David Kunz, Helen Koike, Andreas Schwarz, Miriam Retka, GreaterFire, Birger
Schacht, Simon Spöhel, Guillaume Pernot, Joachim Nilsson, Mujeeb Rahman K, Timo
Röhling, Hashem Nasarat, Christophe Courtois, Matheus Faria, Oliver Dechant,
Johan Fleury, Gabriel Filion, Baptiste Beauplat, Bastian Germann, Markus
Wurzenberger, Jeremy Finzel, Mangesh Divate, Jonas Schäfer, Julian Rüth,
Scarlett Moore, Tiago Stürmer Daitx, Tommi Höynälänmaa, Romuald Brunet,
Gerardo Ballabio, Stewart Ferguson, Julian Schauder, Chen-Ying Kuo, Denis
Danilov, David Lamparter y Kienan Stewart.</p>

<p><b>Nuevos mantenedores y mantenedoras de Debian</b></p>
##Run the ./dd-dm-from-keyring.pl script locally for new DM and DD
##./dd-dm-from-keyring.pl YYYMMDD

<p>Damos la bienvenida a: Sagar Ippalpalli, Kurt Kremitzki, Michal Arbet, Peter Wienemann,
Alexis Bienvenüe y Gard Spreemann.</p>

<p><b>Nuevos desarrolladores y desarrolladoras de Debian</b></p>
<p>Damos la bienvenida a: Joseph Herlant, Aurélien Couderc, Dylan Aïssi, Kunal Mehta,
Ming-ting Yao Wei, Nicolas Braud-Santoni, Pierre-Elliott Bécue, Stephen Gelman,
Daniel Echeverry y Dmitry Bogatov.</p>


<p><b>Contribuidores y contribuidoras</b></p>

## Visit the link below and pull the information manually.

<p>
En la página de <a href="https://contributors.debian.org/">contribuidores y contribuidoras a Debian</a> de
2018 hay 1.603 personas y 19 equipos listados.
</p>

<p><b>Estadísticas</b></p>
##Pull this information from sources.d.o
##https://sources.debian.org/stats/sid/
##https://sources.debian.org/stats/buster/
<p><b><em>buster</em></b></p>
<ul style="list-style-type:none">
<li>Ficheros fuente: 11.885.550</li>
<li>Paquetes fuente: 28.697</li>
<li>Disco usado: 252.791.492 kB</li>
<li>Ctags: 17.452.645</li>
<li>Líneas de código fuente: 1.044.492.396</li>
</ul>

<p><b><em>sid</em></b></p>
<ul>
<li>Ficheros fuente: 20.019.227</li>
<li>Paquetes fuente: 33.533</li>
<li>Disco usado: 381.351.424 kB</li>
<li>Ctags: 42.219.156</li>
<li>Líneas de código fuente: 1.759.157.606</li>
</ul>


<p><b>Debates</b></p>

<p>Aurélien Couderc, usuario de Debian, preguntó sobre <a href="https://lists.debian.org/debian-devel/2018/09/msg00220.html">incrementar una época y reusar un nombre de paquete</a>,
lo que condujo a un debate acerca de solicitar cambios en los proyectos originales en base a normativas internas
de Debian y a su efecto en los usuarios. En el debate se mencionan varias
alternativas, así como inconvenientes de incrementar un número de versión.</p>


<p>Pétùr, usuario de Debian, pidió ayuda con un <a href="https://lists.debian.org/debian-user/2018/09/msg00311.html">fichero con permisos extraños, imposible de borrar</a>.
El debate derivó rápidamente hacia problemas con permisos, inodos, fsck y cables SATA
defectuosos.</p>


<p>Subhadip Ghosh, usuario de Debian, preguntó: <a href="https://lists.debian.org/debian-user/2018/09/msg00700.html">¿Por qué Debian permite por omisión todo el tráfico entrante?</a></p>


<p><b>Trucos y consejos</b></p>

<p>Jonathan McDowell continúa con su serie de escritos sobre domótica con
<a href="https://www.earth.li/~noodles/blog/2018/10/heating-automation.html">Control de la calefacción con Home Assistant</a> y <a href="https://www.earth.li/~noodles/blog/2018/09/netlink-arp-presence.html">Uso de ARP vía netlink como detector
de presencia.</a></p>

<p>Antoine Beaupré compartió consejos para <a href="https://anarc.at/blog/2018-10-04-archiving-web-sites/">archivar sitios web</a> utilizando herramientas libremente
disponibles y algo de conocimiento.</p>

<p>Sergio Alberti <a href="https://sergioalberti.gitlab.io//gsoc/debian/2018/09/24/reveng.html">compartió</a> una guía sobre <a href="https://reverse-engineering-ble-devices.readthedocs.io/en/latest/">ingeniería inversa de dispositivos bluetooth de baja energía</a>.</p>

<p>Petter Reinholdtsen <a href="http://people.skolelinux.org/pere/blog/VLC_in_Debian_now_can_do_bittorrent_streaming.html">comparte que VLC en Debian ahora puede hacer transmisiones («streaming») bittorrent</a>.</p>

<p>Laura Arjona Reina encontró un pequeño marco de fotos digital y volvió a ponerlo
en uso: <a href="https://larjona.wordpress.com/2018/09/22/handling-an-old-digital-photo-frame-ax203-with-debian-and-gphoto2/">gestión de un viejo marco de fotos digital (AX203) con Debian (y gphoto2)</a>.</p>


<p><b>Érase una vez en Debian</b></p>

## Items pulled from the Timeline https://timeline.debian.net
## Jump to any random year/ same month/ same week.
## Provide link and link description.
## This may work better with a script at some point, but for now let us see
## what the ease of work is.

## Format - YYYY-MM-DD text

<ul>
<li>2009-12-08 <a href="https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=560000">Mika Tiainen informó del fallo Debian &#35;560000</a></li>
<li>2016-12-10 <a href="https://miniconf.debian.or.jp/index.en.html">La MiniDebconf 2016 se celebra en Tokio, Japón</a></li>
<li>1996-12-12 <a href="https://lists.debian.org/debian-devel-announce/2014/08/msg00012.html">Publicado Debian 1.2 (Rex)</a></li>
<li>2014-12-13 <a href="https://wiki.debian.org/BSP/2014/12/nl/Tilburg">Fiesta «caza-fallos» (Bug Squashing Party) en Tilburgo, Países Bajos</a></li>
<li>2016-12-13 <a href="https://reproducible-builds.org/events/berlin2016/">Debian coorganiza y patrocina la Cumbre de compilaciones reproducibles en Berlín, Alemania</a></li>
</ul>


<toc-add-entry name="outside">Noticias del exterior</toc-add-entry>

<p>
La Cumbre mundial de Creative Commons se celebrará en Lisboa, Portugal, del 9 al 11 de mayo de 2019. La recepción de propuestas está abierta hasta el 10 de diciembre de 2018.
Visite <a href="https://summit.creativecommons.org/">https://summit.creativecommons.org/</a> para más información y para registrarse.
</p>

<toc-add-entry name="quicklinks">Enlaces rápidos de los medios sociales de Debian</toc-add-entry>

<p>
Este es un extracto de
<a href="https://micronews.debian.org">micronews.debian.org</a>, del
que hemos eliminado los temas ya comentados en esta edición de las DPN.
Si suele seguir <b>micronews.debian.org</b> o el perfil <b>@debian</b> en
alguna red social (Pump.io, GNU Social, Mastodon o Twitter), puede
saltarse esta sección. Los temas se muestran sin formatear y en orden de fecha decreciente 
(las noticias más recientes, primero).
</p>

<p>
<b>Noviembre</b></p>
<ul>
<li>«Bits» del líder del proyecto @Debian (noviembre 2018)
<a href="https://lists.debian.org/debian-devel-announce/2018/11/msg00007.html">https://lists.debian.org/debian-devel-announce/2018/11/msg00007.html</a>
</li>

<li>¡Tubería de integración continua («CI pipeline») Debian para mantenedores y mantenedoras de Debian!
<a href="https://salsa.debian.org/salsa-ci-team/pipeline/blob/master/README.md">https://salsa.debian.org/salsa-ci-team/pipeline/blob/master/README.md</a>
</li>

<li>¡Feliz cumpleaños, @Fedora!
<a href="https://fedoramagazine.org/celebrate-fifteen-years-fedora/">https://fedoramagazine.org/celebrate-fifteen-years-fedora/</a>
</li>

<li>Transición a Perl 5.28 en marcha, ¡es previsible que se den numerosos problemas de instalación en sid durante los próximos días!
<a href="https://lists.debian.org/debian-devel-announce/2018/10/msg00006.html">https://lists.debian.org/debian-devel-announce/2018/10/msg00006.html</a>
</li>
</ul>


<p>
<b>Octubre</b></p>
<ul>
<li>«Bits» del líder del proyecto Debian (octubre 2018)
<a href="https://lists.debian.org/debian-devel-announce/2018/10/msg00005.html">https://lists.debian.org/debian-devel-announce/2018/10/msg00005.html</a>
</li>

<li>«Bandas de Salsa» («Salsa ribbons»), por Chris Lamb
<a href="https://chris-lamb.co.uk/posts/salsa-ribbons">https://chris-lamb.co.uk/posts/salsa-ribbons</a>
</li>

<li>[debian-installer] Llamamiento para la actualización de traducciones para Buster
<a href="https://lists.debian.org/debian-i18n/2018/10/msg00002.html">https://lists.debian.org/debian-i18n/2018/10/msg00002.html</a>
</li>

<li>«Bits» de la MicroDebConf Brasilia 2018
<a href="http://blog.kanashiro.xyz/debconf/2018/09/28/microdebconf-bsb.html">http://blog.kanashiro.xyz/debconf/2018/09/28/microdebconf-bsb.html</a>
</li>
</ul>


<p>
<b>Septiembre</b></p>
<ul>
<li>«Bits» del líder del proyecto Debian (septiembre 2018)
<a href="https://lists.debian.org/debian-devel-announce/2018/09/msg00005.html">https://lists.debian.org/debian-devel-announce/2018/09/msg00005.html</a>
</li>

<li>Petición de mentores e ideas para proyectos para la próxima ronda de Outreachy
<a href="https://lists.debian.org/debian-outreach/2018/09/msg00030.html">https://lists.debian.org/debian-outreach/2018/09/msg00030.html</a>
</li>

<li>Repositorios de seguridad de Debian en Japón permanecen en línea a pesar de terremoto de magnitud 6,7
<a href="https://henrich-on-debian.blogspot.com/2018/09/earthquake-struck-hokkaido-and-caused.html">https://henrich-on-debian.blogspot.com/2018/09/earthquake-struck-hokkaido-and-caused.html</a>
</li>
</ul>


<p>
<b>Agosto</b></p>
<ul>
<li>El FISL19 se celebrará en Porto Alegre antes del DebCamp del próximo año
<a href="https://debconf19.debconf.org/news/2018-08-23-fisl19/">https://debconf19.debconf.org/news/2018-08-23-fisl19/</a>
</li>

<li>«Bits» del líder del proyecto Debian (agosto 2018)
<a href="https://lists.debian.org/debian-devel-announce/2018/08/msg00008.html">https://lists.debian.org/debian-devel-announce/2018/08/msg00008.html</a>
</li>
</ul>


<toc-add-entry name="continuedpn">¿Quiere seguir leyendo DPN?</toc-add-entry>
<continue-dpn />

<p><a href="https://lists.debian.org/debian-news/">Suscríbase o cancele su suscripción</a> a la lista de correo de Noticias Debian.</p>

#use wml::debian::projectnews::footer editor="el equipo de publicidad («Publicity Team») con contribuciones de Jean-Pierre Giraud, Justin B Rye, Laura Arjona Reina"
# Please add the contributors to the /dpn/CREDITS file
# Translators may also add a translator="foo, bar, baz" to the previous line
