<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>golang-go.crypto was recently updated with a fix for <a href="https://security-tracker.debian.org/tracker/CVE-2020-9283">CVE-2020-9283</a>. This in
turn requires all packages that use the affected code to be recompiled in order
to pick up the security fix.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9283">CVE-2020-9283</a>

    <p>SSH signature verification could cause Panic when given
    invalid Public key.</p></li>

</ul>

<p>For Debian 9 stretch, this problem has been fixed in version
0.10.2+dfsg-6+deb9u1.</p>

<p>We recommend that you upgrade your packer packages.</p>

<p>For the detailed security status of packer please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/packer">https://security-tracker.debian.org/tracker/packer</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2455.data"
# $Id: $
