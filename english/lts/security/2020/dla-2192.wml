<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The JSON gem through 2.2.0 for Ruby, as used in Ruby 2.1 has an
unsafe object creation vulnerability.
This is quite similar to <a href="https://security-tracker.debian.org/tracker/CVE-2013-0269">CVE-2013-0269</a>, but does not rely on poor
garbage-collection behavior within Ruby. Specifically, use of JSON
parsing methods can lead to creation of a malicious object within
the interpreter, with adverse effects that are application-dependent.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.1.5-2+deb8u10.</p>

<p>We recommend that you upgrade your ruby2.1 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2192.data"
# $Id: $
