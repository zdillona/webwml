<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that SNMP Trap Translator does not drop privileges as configured and does not properly escape shell commands in certain functions. A remote attacker, by sending a malicious crafted SNMP trap, could possibly execute arbitrary shell code with the privileges of the process or cause a Denial of Service condition.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.4-1+deb9u1.</p>

<p>We recommend that you upgrade your snmptt packages.</p>

<p>For the detailed security status of snmptt please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/snmptt">https://security-tracker.debian.org/tracker/snmptt</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2393.data"
# $Id: $
