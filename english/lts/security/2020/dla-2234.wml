<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>There were several CVE bugs reported against src:netqmail.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2005-1513">CVE-2005-1513</a>

    <p>Integer overflow in the stralloc_readyplus function in qmail,
    when running on 64 bit platforms with a large amount of virtual
    memory, allows remote attackers to cause a denial of service
    and possibly execute arbitrary code via a large SMTP request.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2005-1514">CVE-2005-1514</a>

    <p>commands.c in qmail, when running on 64 bit platforms with a
    large amount of virtual memory, allows remote attackers to
    cause a denial of service and possibly execute arbitrary code
    via a long SMTP command without a space character, which causes
    an array to be referenced with a negative index.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2005-1515">CVE-2005-1515</a>

    <p>Integer signedness error in the qmail_put and substdio_put
    functions in qmail, when running on 64 bit platforms with a
    large amount of virtual memory, allows remote attackers to
    cause a denial of service and possibly execute arbitrary code
    via a large number of SMTP RCPT TO commands.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3811">CVE-2020-3811</a>

    <p>qmail-verify as used in netqmail 1.06 is prone to a
    mail-address verification bypass vulnerability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3812">CVE-2020-3812</a>

    <p>qmail-verify as used in netqmail 1.06 is prone to an
    information disclosure vulnerability. A local attacker can
    test for the existence of files and directories anywhere in
    the filesystem because qmail-verify runs as root and tests
    for the existence of files in the attacker's home directory,
    without dropping its privileges first.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.06-6.2~deb8u1.</p>

<p>We recommend that you upgrade your netqmail packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2234.data"
# $Id: $
