<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Christopher Ertl found that multiple functions in ipmitool neglect
proper checking of the data received from a remote LAN party, which may
lead to buffer overflows and potentially to remote code execution on the
ipmitool side.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.8.14-4+deb8u1.</p>

<p>We recommend that you upgrade your ipmitool packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2098.data"
# $Id: $
