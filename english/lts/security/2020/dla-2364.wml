<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in netty, a Java NIO
client/server socket framework.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20444">CVE-2019-20444</a>

    <p>HttpObjectDecoder.java in Netty before 4.1.44 allows an HTTP header
    that lacks a colon, which might be interpreted as a separate header
    with an incorrect syntax, or might be interpreted as an "invalid
    fold."</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20445">CVE-2019-20445</a>

    <p>HttpObjectDecoder.java in Netty before 4.1.44 allows a Content-Length
    header to be accompanied by a second Content-Length header, or by a
    Transfer-Encoding header.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-7238">CVE-2020-7238</a>

    <p>Netty 4.1.43.Final allows HTTP Request Smuggling because it
    mishandles Transfer-Encoding whitespace (such as a
    [space]Transfer-Encoding:chunked line) and a later Content-Length
    header. This issue exists because of an incomplete fix for
    <a href="https://security-tracker.debian.org/tracker/CVE-2019-16869">CVE-2019-16869</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11612">CVE-2020-11612</a>

    <p>The ZlibDecoders in Netty 4.1.x before 4.1.46 allow for unbounded
    memory allocation while decoding a ZlibEncoded byte stream. An
    attacker could send a large ZlibEncoded byte stream to the Netty
    server, forcing the server to allocate all of its free memory to a
    single decoder.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1:4.1.7-2+deb9u2.</p>

<p>We recommend that you upgrade your netty packages.</p>

<p>For the detailed security status of netty please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/netty">https://security-tracker.debian.org/tracker/netty</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2364.data"
# $Id: $
