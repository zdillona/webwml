<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>rubyzip gem version 1.2.1 and earlier contains a Directory Traversal
vulnerability in Zip::File component that can result in write
arbitrary files to the filesystem.</p>

<p>This attack appear to be exploitable via if a site allows
uploading of .zip files, an attacker can upload a malicious file
that contains symlinks or files with absolute pathnames "../"
to write arbitrary files to the filesystem..</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.2.0-1.1+deb9u1.</p>

<p>We recommend that you upgrade your ruby-zip packages.</p>

<p>For the detailed security status of ruby-zip please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ruby-zip">https://security-tracker.debian.org/tracker/ruby-zip</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2307.data"
# $Id: $
