<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The following vulnerabilities have been discovered in the WebKitGTK
web engine:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23529">CVE-2023-23529</a>

    <p>An anonymous researcher discovered that processing maliciously
    crafted web content may lead to arbitrary code execution. Apple is
    aware of a report that this issue may have been actively
    exploited.</p></li>

</ul>

<p>For Debian 10 buster, this problem has been fixed in version
2.38.5-1~deb10u1.</p>

<p>We recommend that you upgrade your webkit2gtk packages.</p>

<p>For the detailed security status of webkit2gtk please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">https://security-tracker.debian.org/tracker/webkit2gtk</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3320.data"
# $Id: $
