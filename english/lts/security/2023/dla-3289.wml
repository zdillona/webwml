<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities were found in dojo, a modular JavaScript toolkit,
that could result in information disclosure.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-4051">CVE-2020-4051</a>

    <p>The Dijit Editor's LinkDialog plugin of dojo 1.14.0 to 1.14.7 is
    vulnerable to cross-site scripting (XSS) attacks.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23450">CVE-2021-23450</a>

    <p>Prototype pollution vulnerability via the <code>setObject()</code> function.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1.14.2+dfsg1-1+deb10u3.</p>

<p>We recommend that you upgrade your dojo packages.</p>

<p>For the detailed security status of dojo please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/dojo">https://security-tracker.debian.org/tracker/dojo</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3289.data"
# $Id: $
