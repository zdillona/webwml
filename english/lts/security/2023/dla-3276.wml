<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Igor Ponomarev discovered that LAVA, a continuous integration system for
deploying operating systems onto physical and virtual hardware for
running tests, was susceptible to denial of service via recursive XML
entity expansion.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2019.01-5+deb10u2.</p>

<p>We recommend that you upgrade your lava packages.</p>

<p>For the detailed security status of lava please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/lava">https://security-tracker.debian.org/tracker/lava</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3276.data"
# $Id: $
