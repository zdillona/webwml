<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities have been found in the ClamAV antivirus toolkit,
which could result in arbitrary code execution or information disclosure
when parsing maliciously crafted HFS+ or DMG files.</p>

<p>For Debian 10 buster, these problems have been fixed in version
0.103.8+dfsg-0+deb10u1.</p>

<p>We recommend that you upgrade your clamav packages.</p>

<p>For the detailed security status of clamav please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/clamav">https://security-tracker.debian.org/tracker/clamav</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3328.data"
# $Id: $
