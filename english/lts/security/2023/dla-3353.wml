<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A security issue has been discovered in xfig, a diagramming tool for the
interactive generation of figures under X11.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40241">CVE-2021-40241</a>:

    <p>A potential buffer overflow exists in the file src/w_help.c at line 55.
    Specifically, the length of the string returned by getenv("LANG") may become
    very long and cause a buffer overflow while executing the sprintf() function.
    This vulnerability could potentially allow an attacker to execute arbitrary
    code or cause a denial-of-service condition.</p></li>

</ul>

<p>For Debian 10 buster, this problem has been fixed in version
1:3.2.7a-3+deb10u1.</p>

<p>We recommend that you upgrade your xfig packages.</p>

<p>For the detailed security status of xfig please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/xfig">https://security-tracker.debian.org/tracker/xfig</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3353.data"
# $Id: $
