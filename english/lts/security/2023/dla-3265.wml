<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update fixes a number of memory access violations and other input
validation failures that can be triggered by passing specially crafted files to
exiv2.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11591">CVE-2017-11591</a>

    <p>There is a Floating point exception in the <code>Exiv2::ValueType</code> function that
    will lead to a remote denial of service attack via crafted input.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14859">CVE-2017-14859</a>

    <p>An Invalid memory address dereference was discovered in
    <code>Exiv2::StringValueBase::read</code> in <code>value.cpp</code>. The vulnerability causes a
    segmentation fault and application crash, which leads to denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14862">CVE-2017-14862</a>

    <p>An Invalid memory address dereference was discovered in
    <code>Exiv2::DataValue::read</code> in <code>value.cpp</code>. The vulnerability causes a
    segmentation fault and application crash, which leads to denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14864">CVE-2017-14864</a>

    <p>An Invalid memory address dereference was discovered in <code>Exiv2::getULong</code> in
    <code>types.cpp</code>. The vulnerability causes a segmentation fault and application
    crash, which leads to denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17669">CVE-2017-17669</a>

    <p>There is a heap-based buffer over-read in the
    <code>Exiv2::Internal::PngChunk::keyTXTChunk</code> function of <code>pngchunk_int.cpp</code>. A
    crafted PNG file will lead to a remote denial of service attack.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18005">CVE-2017-18005</a>

    <p>Exiv2 has a Null Pointer Dereference in the <code>Exiv2::DataValue::toLong</code>
    function in <code>value.cpp</code>, related to crafted metadata in a TIFF file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8976">CVE-2018-8976</a>

    <p><code>jpgimage.cpp</code> allows remote attackers to cause a denial of service
    (<code>image.cpp</code> <code>Exiv2::Internal::stringFormat</code> out-of-bounds read) via a crafted
    file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17581">CVE-2018-17581</a>

    <p><code>CiffDirectory::readDirectory()</code> at <code>crwimage_int.cpp</code> has excessive stack
    consumption due to a recursive function, leading to Denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19107">CVE-2018-19107</a>

    <p><code>Exiv2::IptcParser::decode</code> in <code>iptc.cpp</code> (called from <code>psdimage.cpp</code> in the PSD
    image reader) may suffer from a denial of service (heap-based buffer
    over-read) caused by an integer overflow via a crafted PSD image file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19108">CVE-2018-19108</a>

    <p><code>Exiv2::PsdImage::readMetadata</code> in <code>psdimage.cpp</code> in the PSD image reader may
    suffer from a denial of service (infinite loop) caused by an integer
    overflow via a crafted PSD image file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19535">CVE-2018-19535</a>

    <p><code>PngChunk::readRawProfile</code> in <code>pngchunk_int.cpp</code> may cause a denial of service
    (application crash due to a heap-based buffer over-read) via a crafted PNG
    file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20097">CVE-2018-20097</a>

    <p>There is a SEGV in <code>Exiv2::Internal::TiffParserWorker::findPrimaryGroups</code> of
    <code>tiffimage_int.cpp</code>. A crafted input will lead to a remote denial of service
    attack.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13110">CVE-2019-13110</a>

    <p>A <code>CiffDirectory::readDirectory</code> integer overflow and out-of-bounds read
    allows an attacker to cause a denial of service (<code>SIGSEGV</code>) via a crafted CRW
    image file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13112">CVE-2019-13112</a>

    <p>A <code>PngChunk::parseChunkContent</code> uncontrolled memory allocation allows an
    attacker to cause a denial of service (crash due to an <code>std::bad_alloc</code>
    exception) via a crafted PNG image file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13114">CVE-2019-13114</a>

    <p><code>http.c</code> allows a malicious http server to cause a denial of service (crash
    due to a <code>NULL</code> pointer dereference) by returning a crafted response that
    lacks a space character.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13504">CVE-2019-13504</a>

    <p>There is an out-of-bounds read in <code>Exiv2::MrwImage::readMetadata</code> in
    <code>mrwimage.cpp</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14369">CVE-2019-14369</a>

    <p><code>Exiv2::PngImage::readMetadata()</code> in <code>pngimage.cpp</code> allows attackers to cause a
    denial of service (heap-based buffer over- read) via a crafted image file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14370">CVE-2019-14370</a>

    <p>There is an out-of-bounds read in <code>Exiv2::MrwImage::readMetadata()</code> in
    <code>mrwimage.cpp</code>. It could result in denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17402">CVE-2019-17402</a>

    <p>Exiv2 allows attackers to trigger a crash in <code>Exiv2::getULong</code> in <code>types.cpp</code>
    when called from <code>Exiv2::Internal::CiffDirectory::readDirectory</code> in
    <code>crwimage_int.cpp</code>, because there is no validation of the relationship of the
    total size to the offset and size.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-18771">CVE-2020-18771</a>

    <p>Exiv2 has a global buffer over-read in
    <code>Exiv2::Internal::Nikon1MakerNote::print0x0088</code> in <code>nikonmn_int.cpp</code> which can
    result in an information leak.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-29458">CVE-2021-29458</a>

    <p>An out-of-bounds read was found in Exiv2. The out-of- bounds read is
    triggered when Exiv2 is used to write metadata into a crafted image file.
    An attacker could potentially exploit the vulnerability to cause a denial
    of service by crashing Exiv2, if they can trick the victim into running
    Exiv2 on a crafted image file. Note that this bug is only triggered when
    writing the metadata, which is a less frequently used Exiv2 operation than
    reading the metadata. For example, to trigger the bug in the Exiv2
    command-line application, you need to add an extra command-line argument
    such as insert.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32815">CVE-2021-32815</a>

    <p>The assertion
    failure is triggered when Exiv2 is used to modify the metadata of a
    crafted image file. An attacker could potentially exploit the
    vulnerability to cause a denial of service, if they can trick the
    victim into running Exiv2 on a crafted image file. Note that this bug
    is only triggered when modifying the metadata, which is a less
    frequently used Exiv2 operation than reading the metadata. For
    example, to trigger the bug in the Exiv2 command-line application, you
    need to add an extra command-line argument such as <code>fi</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-34334">CVE-2021-34334</a>

    <p>An infinite loop is triggered when Exiv2 is used to read the metadata of a
    crafted image file. An attacker could potentially exploit the vulnerability
    to cause a denial of service, if they can trick the victim into running
    Exiv2 on a crafted image file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-37620">CVE-2021-37620</a>

    <p>An out-of-bounds read is triggered when Exiv2 is used to read the metadata
    of a crafted image file. An attacker could potentially exploit the
    vulnerability to cause a denial of service, if they can trick the victim
    into running Exiv2 on a crafted image file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-37621">CVE-2021-37621</a>

    <p>An infinite loop is triggered when Exiv2 is used to print the metadata of a
    crafted image file. An attacker could potentially exploit the vulnerability
    to cause a denial of service, if they can trick the victim into running
    Exiv2 on a crafted image file. Note that this bug is only triggered when
    printing the image ICC profile, which is a less frequently used Exiv2
    operation that requires an extra command line option (<code>-p C</code>).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-37622">CVE-2021-37622</a>

    <p>An infinite loop is triggered when Exiv2 is used to modify the metadata of
    a crafted image file. An attacker could potentially exploit the
    vulnerability to cause a denial of service, if they can trick the victim
    into running Exiv2 on a crafted image file. Note that this bug is only
    triggered when deleting the IPTC data, which is a less frequently used
    Exiv2 operation that requires an extra command line option (<code>-d I rm</code>).</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
0.25-4+deb10u4.</p>

<p>We recommend that you upgrade your exiv2 packages.</p>

<p>For the detailed security status of exiv2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/exiv2">https://security-tracker.debian.org/tracker/exiv2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3265.data"
# $Id: $
