<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update includes the changes in tzdata 2018i. Notable
changes are:</p>

<ul>
  <li>Qyzylorda, Kazakhstan moved from +06 to +05 on 2018-12-21. A new
      zone Asia/Qostanay has been added, because Qostanay, Kazakhstan
      didn't move.</li>
  <li>Metlakatla, Alaska observes PST this winter only.</li>
  <li>São Tomé and Príncipe switched from +01 to +00 on 2019-01-01.</li>
</ul>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2018i-0+deb8u1.</p>

<p>We recommend that you upgrade your tzdata packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1625.data"
# $Id: $
