<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An information leak issue was discovered in phpMyAdmin. An attacker
can read any file on the server that the web server's user can
access. This is related to the mysql.allow_local_infile PHP
configuration. When the AllowArbitraryServer configuration setting is
set to false (default), the attacker needs a local MySQL account. When
set to true, the attacker can exploit this with the use of a rogue
MySQL server.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
4:4.2.12-2+deb8u5.</p>

<p>We recommend that you upgrade your phpmyadmin packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1692.data"
# $Id: $
