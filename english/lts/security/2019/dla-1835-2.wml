<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The update issued as DLA-1835-1 caused a regression in the http.client
library in Python 3.4 which was broken by the patch intended to fix
<a href="https://security-tracker.debian.org/tracker/CVE-2019-9740">CVE-2019-9740</a> and <a href="https://security-tracker.debian.org/tracker/CVE-2019-9947">CVE-2019-9947</a>.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
3.4.2-1+deb8u4.</p>

<p>We recommend that you upgrade your python3.4 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1835-2.data"
# $Id: $
