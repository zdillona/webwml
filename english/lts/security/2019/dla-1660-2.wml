<define-tag description>LTS regression update</define-tag>
<define-tag moreinfo>

<p>It was discovered that the fix for the security vulnerability released for
rssh in 2.3.4-4+deb8u2 via DLA-1660-1 introduced a regression that blocked
scp(1) of multiple files from a server using rssh.</p>

<p>Please see <a href="https://bugs.debian.org/921655">#921655</a> for more
information.</p>

<p>For Debian 8 <q>Jessie</q>, this issue has been addressed in rssh version
2.3.4-4+deb8u3.</p>

<p>We recommend that you upgrade your rssh packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1660-2.data"
# $Id: $
