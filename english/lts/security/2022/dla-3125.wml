<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two issues have been found in libvncserver, a library to write one's own
VNC server.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25708">CVE-2020-25708</a>

     <p>Due to some missing checks, a divide by zero could happen, which could
     result in a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29260">CVE-2020-29260</a>

     <p>Due to a memory leak in function rfbClientCleanup() a remote attacker
     might be able to cause a denial of service.</p>


<p>
<p>For Debian 10 buster, these problems have been fixed in version
0.9.11+dfsg-1.3+deb10u5.</p>

<p>We recommend that you upgrade your libvncserver packages.</p>

<p>For the detailed security status of libvncserver please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libvncserver">https://security-tracker.debian.org/tracker/libvncserver</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3125.data"
# $Id: $
