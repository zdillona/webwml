<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Matthias Gerstner discovered that the --join option of Firejail, a
sandbox to restrict an application environment, was susceptible to
local privilege escalation to root.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
0.9.58.2-2+deb9u1.</p>

<p>We recommend that you upgrade your firejail packages.</p>

<p>For the detailed security status of firejail please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/firejail">https://security-tracker.debian.org/tracker/firejail</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3061.data"
# $Id: $
