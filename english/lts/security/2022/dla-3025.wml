<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a user-after-free vulnerability in irssi,
the popular terminal-based IRC client.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13045">CVE-2019-13045</a>

    <p>Irssi before 1.0.8, 1.1.x before 1.1.3, and 1.2.x before 1.2.1, when
    SASL is enabled, has a use after free when sending SASL login to the
    server.</p></li>
</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
1.0.7-1~deb9u2.</p>

<p>We recommend that you upgrade your irssi packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3025.data"
# $Id: $
