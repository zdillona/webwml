<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Cache poisoning vulnerabilities were found in node-tar, a Node.js module
used to read and write portable tar archives, which may result in
arbitrary file creation or overwrite.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-37701">CVE-2021-37701</a>

    <p>It was discovered that node-tar performed insufficient symlink
    protection, thereby making directory cache vulnerable to poisoning
    using symbolic links.</p>

    <p>Upon extracting an archive containing a directory `foo/bar` followed
    with a symbolic link `foo\\bar` to an arbitrary location, node-tar
    would extract arbitrary files into the symlink target, thus allowing
    arbitrary file creation and overwrite.</p>

    <p>Moreover, on case-insensitive filesystems, a similar issue occurred
    with a directory `FOO` followed with a symbolic link `foo`.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-37712">CVE-2021-37712</a>

    <p>Similar to <a href="https://security-tracker.debian.org/tracker/CVE-2021-37701">CVE-2021-37701</a>, a specially crafted tar archive
    containing two directories and a symlink with names containing
    unicode values that normalized to the same value, would bypass
    node-tar's symlink checks on directories, thus allowing arbitrary
    file creation and overwrite.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
4.4.6+ds1-3+deb10u2.</p>

<p>We recommend that you upgrade your node-tar packages.</p>

<p>For the detailed security status of node-tar please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/node-tar">https://security-tracker.debian.org/tracker/node-tar</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3237.data"
# $Id: $
