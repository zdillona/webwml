<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The c_rehash script does not properly sanitise shell metacharacters to prevent
command injection. This script is executed by update-ca-certificates, from
ca-certificates, to re-hash certificates in /etc/ssl/certs/. An attacker able
to place files in this directory could execute arbitrary commands with the
privileges of the script.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.1.0l-1~deb9u6.</p>

<p>We recommend that you upgrade your openssl packages.</p>

<p>For the detailed security status of openssl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/openssl">https://security-tracker.debian.org/tracker/openssl</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3008.data"
# $Id: $
