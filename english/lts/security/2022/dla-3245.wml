<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2978">CVE-2022-2978</a>

    <p><q>butt3rflyh4ck</q>, Hao Sun, and Jiacheng Xu reported a flaw in the
    nilfs2 filesystem driver which can lead to a use-after-free.  A
    local use might be able to exploit this to cause a denial of
    service (crash or memory corruption) or possibly for privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3521">CVE-2022-3521</a>

    <p>The syzbot tool found a race condition in the KCM subsystem
    which could lead to a crash.</p>

    <p>This subsystem is not enabled in Debian's official kernel
    configurations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3524">CVE-2022-3524</a>

    <p>The syzbot tool found a race condition in the IPv6 stack which
    could lead to a memory leak.  A local user could exploit this to
    cause a denial of service (memory exhaustion).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3564">CVE-2022-3564</a>

    <p>A flaw was discovered in the Bluetooh L2CAP subsystem which
    would lead to a use-after-free.  This might be exploitable
    to cause a denial of service (crash or memory corruption) or
    possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3565">CVE-2022-3565</a>

    <p>A flaw was discovered in the mISDN driver which would lead to a
    use-after-free.  This might be exploitable to cause a denial of
    service (crash or memory corruption) or possibly for privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3594">CVE-2022-3594</a>

    <p>Andrew Gaul reported that the r8152 Ethernet driver would log
    excessive numbers of messages in response to network errors.  A
    remote attacker could possibly exploit this to cause a denial of
    service (resource exhaustion).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3621">CVE-2022-3621</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-3646">CVE-2022-3646</a>

    <p>The syzbot tool found flaws in the nilfs2 filesystem driver which
    can lead to a null pointer dereference or memory leak.  A user
    permitted to mount arbitrary filesystem images could use these to
    cause a denial of service (crash or resource exhaustion).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3628">CVE-2022-3628</a>

    <p>Dokyung Song, Jisoo Jang, and Minsuk Kang reported a potential
    heap-based buffer overflow in the brcmfmac Wi-Fi driver.  A user
    able to connect a malicious USB device could exploit this to cause
    a denial of service (crash or memory corruption) or possibly for
    privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3640">CVE-2022-3640</a>

    <p>A flaw was discovered in the Bluetooh L2CAP subsystem which
    would lead to a use-after-free.  This might be exploitable
    to cause a denial of service (crash or memory corruption) or
    possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3643">CVE-2022-3643</a> (XSA-423)

    <p>A flaw was discovered in the Xen network backend driver that would
    result in it generating malformed packet buffers.  If these
    packets were forwarded to certain other network devices, a Xen
    guest could exploit this to cause a denial of service (crash or
    device reset).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3649">CVE-2022-3649</a>

    <p>The syzbot tool found flaws in the nilfs2 filesystem driver which
    can lead to a use-after-free.  A user permitted to mount arbitrary
    filesystem images could use these to cause a denial of service
    (crash or memory corruption) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4378">CVE-2022-4378</a>

    <p>Kyle Zeng found a flaw in procfs that would cause a stack-based
    buffer overflow.  A local user permitted to write to a sysctl
    could use this to cause a denial of service (crash or memory
    corruption) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-20369">CVE-2022-20369</a>

    <p>A flaw was found in the v4l2-mem2mem media driver that would lead
    to an out-of-bounds write.  A local user with access to such a
    device could exploit this for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-29901">CVE-2022-29901</a>

    <p>Johannes Wikner and Kaveh Razavi reported that for Intel
    processors (Intel Core generation 6, 7 and 8), protections against
    speculative branch target injection attacks were insufficient in
    some circumstances, which may allow arbitrary speculative code
    execution under certain microarchitecture-dependent conditions.</p>

    <p>More information can be found at
    <a href="https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/advisory-guidance/return-stack-buffer-underflow.html">https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/advisory-guidance/return-stack-buffer-underflow.html</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-40768">CVE-2022-40768</a>

    <p><q>hdthky</q> reported that the stex SCSI adapter driver did not fully
    initialise a structure that is copied to user-space.  A local user
    with access to such a device could exploit this to leak sensitive
    information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41849">CVE-2022-41849</a>

    <p>A race condition was discovered in the smscufx graphics driver,
    which could lead to a use-after-free.  A user able to remove the
    physical device while also accessing its device node could exploit
    this to cause a denial of service (crash or memory corruption) or
    possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41850">CVE-2022-41850</a>

    <p>A race condition was discovered in the hid-roccat input driver,
    which could lead to a use-after-free.  A local user able to access
    such a device could exploit this to cause a denial of service
    (crash or memory corruption) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42328">CVE-2022-42328</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-42329">CVE-2022-42329</a> (XSA-424)

    <p>Yang Yingliang reported that the Xen network backend driver did
    not use the proper function to free packet buffers in one case,
    which could lead to a deadlock.  A Xen guest could exploit this to
    cause a denial of service (hang).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42895">CVE-2022-42895</a>

    <p>Tamás Koczka reported a flaw in the Bluetooh L2CAP subsystem
    that would result in reading uninitialised memory.  A nearby
    attacker able to make a Bluetooth connection could exploit
    this to leak sensitive information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42896">CVE-2022-42896</a>

    <p>Tamás Koczka reported flaws in the Bluetooh L2CAP subsystem that
    can lead to a use-after-free.  A nearby attacker able to make a
    Bluetooth SMP connection could exploit this to cause a denial of
    service (crash or memory corruption) or possibly for remote code
    execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43750">CVE-2022-43750</a>

    <p>The syzbot tool found that the USB monitor (usbmon) driver allowed
    user-space programs to overwrite the driver's data structures.  A
    local user permitted to access a USB monitor device could exploit
    this to cause a denial of service (memory corruption or crash) or
    possibly for privilege escalation.  However, by default only the
    root user can access such devices.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
4.19.269-1.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>For the detailed security status of linux please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux">https://security-tracker.debian.org/tracker/linux</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3245.data"
# $Id: $
