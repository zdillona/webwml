<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The Drupal project includes a very old version of jQuery. Security
vulnerabilities leading to cross-site scripting attacks in different
components of the jQuery UI libraries were found and fixed for Drupal
version 7.86.</p>

<p>The fixes for said vulnerabilities were backported to the version in
Debian 9 Stretch (7.52).</p>

<p>Drupal is a rich Web content management system; it was included in
Debian until Stretch, but is not present in any newer releases. If you
run a web server with Drupal7, we strongly recommend you to upgrade
the drupal7 package.</p>

<p>For the detailed security status of drupal7 please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/drupal7">https://security-tracker.debian.org/tracker/drupal7</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

<p>--AWNW9msBK4+52Jxu
Content-Type: application/pgp-signature; name="signature.asc"</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2889.data"
# $Id: $
