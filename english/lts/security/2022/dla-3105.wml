<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there were two issues in connman, a daemon for
managing internet connections within embedded devices:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32292">CVE-2022-32292</a>

    <p>In ConnMan through 1.41, remote attackers able to send HTTP requests to
    the gweb component are able to exploit a heap-based buffer overflow in
    received_data to execute code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32292">CVE-2022-32292</a>

    <p>In ConnMan through 1.41, a man-in-the-middle attack against a WISPR HTTP
    query could be used to trigger a use-after-free in WISPR handling, leading
    to crashes or code execution.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, these problems have been fixed in version
1.36-2.1~deb10u3.</p>

<p>We recommend that you upgrade your connman packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3105.data"
# $Id: $
