<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Evgeny Legerov reported a heap-based buffer overflow vulnerability in
the inflate operation in zlib, which could result in denial of service
or potentially the execution of arbitrary code if specially crafted
input is processed.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1:1.2.11.dfsg-1+deb10u2.</p>

<p>We recommend that you upgrade your zlib packages.</p>

<p>For the detailed security status of zlib please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/zlib">https://security-tracker.debian.org/tracker/zlib</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3103.data"
# $Id: $
