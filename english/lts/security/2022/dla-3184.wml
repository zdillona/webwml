<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that libjettison-java, a collection of StAX parsers and
writers for JSON, was vulnerable to a denial-of-service attack, if the
attacker provided untrusted XML or JSON data.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1.4.0-1+deb10u1.</p>

<p>We recommend that you upgrade your libjettison-java packages.</p>

<p>For the detailed security status of libjettison-java please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libjettison-java">https://security-tracker.debian.org/tracker/libjettison-java</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3184.data"
# $Id: $
