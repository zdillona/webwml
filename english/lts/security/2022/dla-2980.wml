<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities have been discovered in zabbix, a network
monitoring solution. An authenticated user can create a link with reflected
Javascript code inside it for graphs, actions and services pages and send it to
other users. The payload can be executed only with a known CSRF token value of
the victim, which is changed periodically and is difficult to predict.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
1:3.0.32+dfsg-0+deb9u3.</p>

<p>We recommend that you upgrade your zabbix packages.</p>

<p>For the detailed security status of zabbix please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/zabbix">https://security-tracker.debian.org/tracker/zabbix</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2980.data"
# $Id: $
