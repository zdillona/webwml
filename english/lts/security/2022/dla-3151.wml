<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were discovered in squid, a Web Proxy cache</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41317">CVE-2022-41317</a>

    <p>Due to inconsistent handling of internal URIs Squid is
    vulnerable to Exposure of Sensitive Information about clients
    using the proxy.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41318">CVE-2022-41318</a>

    <p>Due to an incorrect integer overflow protection Squid SSPI and
    SMB authentication helpers are vulnerable to a Buffer Overflow
    attack.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
4.6-1+deb10u8.</p>

<p>We recommend that you upgrade your squid packages.</p>

<p>For the detailed security status of squid please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/squid">https://security-tracker.debian.org/tracker/squid</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3151.data"
# $Id: $
