<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in BlueZ, the Linux Bluetooth
protocol stack. An attacker could cause a denial-of-service (DoS) or
leak information.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8921">CVE-2019-8921</a>

    <p>SDP infoleak, the vulnerability lies in the handling of a
    SVC_ATTR_REQ by the SDP implementation of BlueZ. By crafting a
    malicious CSTATE, it is possible to trick the server into
    returning more bytes than the buffer actually holds, resulting in
    leaking arbitrary heap data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8922">CVE-2019-8922</a>

    <p>SDP Heap Overflow; this vulnerability lies in the SDP protocol
    handling of attribute requests as well. By requesting a huge
    number of attributes at the same time, an attacker can overflow
    the static buffer provided to hold the response.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41229">CVE-2021-41229</a>

    <p>sdp_cstate_alloc_buf allocates memory which will always be hung in
    the singly linked list of cstates and will not be freed. This will
    cause a memory leak over time. The data can be a very large
    object, which can be caused by an attacker continuously sending
    sdp packets and this may cause the service of the target device to
    crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43400">CVE-2021-43400</a>

    <p>A use-after-free in gatt-database.c can occur when a client
    disconnects during D-Bus processing of a WriteValue call.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0204">CVE-2022-0204</a>

    <p>A heap overflow vulnerability was found in bluez. An attacker with
    local network access could pass specially crafted files causing an
    application to halt or crash, leading to a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39176">CVE-2022-39176</a>

    <p>BlueZ allows physically proximate attackers to obtain sensitive
    information because profiles/audio/avrcp.c does not validate
    params_len.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39177">CVE-2022-39177</a>

    <p>BlueZ allows physically proximate attackers to cause a denial of
    service because malformed and invalid capabilities can be
    processed in profiles/audio/avdtp.c.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
5.50-1.2~deb10u3.</p>

<p>We recommend that you upgrade your bluez packages.</p>

<p>For the detailed security status of bluez please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/bluez">https://security-tracker.debian.org/tracker/bluez</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3157.data"
# $Id: $
