<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>thenify is a Promisify a callback-based function using any-promise.
Affected versions of this package are vulnerable to Arbitrary Code
Execution. The name argument provided to the package can be controlled
by users without any sanitization, and this is provided to the eval
function without any sanitization.</p>

<p>For Debian 10 buster, this problem has been fixed in version
3.3.0-1+deb10u1.</p>

<p>We recommend that you upgrade your node-thenify packages.</p>

<p>For the detailed security status of node-thenify please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/node-thenify">https://security-tracker.debian.org/tracker/node-thenify</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3128.data"
# $Id: $
