<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that GLib, a general-purpose portable utility library,
could be used to print partial contents from arbitrary files. This
could be exploited from setuid binaries linking to GLib for information
disclosure of files with a specific format.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2.58.3-2+deb10u4.</p>

<p>We recommend that you upgrade your glib2.0 packages.</p>

<p>For the detailed security status of glib2.0 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/glib2.0">https://security-tracker.debian.org/tracker/glib2.0</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3110.data"
# $Id: $
