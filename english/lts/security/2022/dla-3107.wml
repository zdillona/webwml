<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Three issues were discovered in SQLite, a popular embedded database:</p>

<ul><li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35525">CVE-2020-35525</a>: Prevent a potential null pointer deference issue in INTERSEC query processing.</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35527">CVE-2020-35527</a>: Prevent an out-of-bounds access issue that could be exploited via ALTER TABLE in views that have a nested FROM clauses.</li>
</ul>

<p>For Debian 10 <q>Buster</q>, these problems have been fixed in version
3.27.2-3+deb10u2.</p>

<p>We recommend that you upgrade your sqlite3 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3107.data"
# $Id: $
