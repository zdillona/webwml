<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in Mailman, a web-based
mailing list manager. An attacker could impersonate more privileged
accounts through different vectors.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43331">CVE-2021-43331</a>

    <p>A crafted URL to the Cgi/options.py user options page can execute
    arbitrary JavaScript for XSS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43332">CVE-2021-43332</a>

    <p>The CSRF token for the Cgi/admindb.py admindb page contains an
    encrypted version of the list admin password. This could
    potentially be cracked by a moderator via an offline brute-force
    attack.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44227">CVE-2021-44227</a>

    <p>A list member or moderator can get a CSRF token and craft an admin
    request (using that token) to set a new admin password or make
    other changes.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1:2.1.23-1+deb9u8.</p>

<p>We recommend that you upgrade your mailman packages.</p>

<p>For the detailed security status of mailman please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/mailman">https://security-tracker.debian.org/tracker/mailman</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3049.data"
# $Id: $
