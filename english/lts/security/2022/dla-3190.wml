<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several issues were found in GRUB2's font handling code, which could
result in crashes and potentially execution of arbitrary code. These
could lead to by-pass of UEFI Secure Boot on affected systems.</p>

<p>Further, issues were found in image loading that could potentially
lead to memory overflows.</p>

<p>For Debian 10 buster, these problems have been fixed in version
2.06-3~deb10u2.</p>

<p>We recommend that you upgrade your grub2 packages.</p>

<p>For the detailed security status of grub2 please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/grub2">https://security-tracker.debian.org/tracker/grub2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3190.data"
# $Id: $
