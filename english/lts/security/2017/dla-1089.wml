<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Some Irssi issues were found:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10965">CVE-2017-10965</a>

	<p>An issue was discovered in Irssi before 1.0.4. When receiving messages with
	invalid time stamps, Irssi would try to dereference a NULL pointer.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10966">CVE-2017-10966</a>

	<p>An issue was discovered in Irssi before 1.0.4. While updating the internal
	nick list, Irssi could incorrectly use the GHashTable interface and free
	the nick while updating it. This would then result in use-after-free
	conditions on each access of the hash table.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.8.15-5+deb7u3.</p>

<p>We recommend that you upgrade your irssi packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1089.data"
# $Id: $
