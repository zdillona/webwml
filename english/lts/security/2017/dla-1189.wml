<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A minor security vulnerability has been discovered in Python 2.7, an
interactive high-level object-oriented language.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000158">CVE-2017-1000158</a>

    <p>CPython (the reference implementation of Python also commonly known
    as simply Python) versions 2.6 and 2.7 are vulnerable to an integer
    overflow and heap corruption, leading to possible arbitrary code
    execution.  The nature of the error has to do with improper handling
    of large strings with escaped characters.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.7.3-6+deb7u4.</p>

<p>We recommend that you upgrade your python2.7 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1189.data"
# $Id: $
