<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The security update announced as DLA-613-1 caused a regression. A
missing null parameter set the $task variable in the rcmail_url()
function to a boolean value which led to service not available errors
when viewing attached images. Updated packages are now available to
correct this issue.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.7.2-9+deb7u8.</p>

<p>We recommend that you upgrade your roundcube packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-613-2.data"
# $Id: $
