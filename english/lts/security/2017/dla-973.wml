<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two denial of service vulnerabilities were identified in strongSwan, an
IKE/IPsec suite, using Google's OSS-Fuzz fuzzing project.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9022">CVE-2017-9022</a>

    <p>RSA public keys passed to the gmp plugin aren't validated sufficiently
    before attempting signature verification, so that invalid input might
    lead to a floating point exception and crash of the process.
    A certificate with an appropriately prepared public key sent by a peer
    could be used for a denial-of-service attack.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9023">CVE-2017-9023</a>

    <p>ASN.1 CHOICE types are not correctly handled by the ASN.1 parser when
    parsing X.509 certificates with extensions that use such types. This could
    lead to infinite looping of the thread parsing a specifically crafted
    certificate.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.5.2-1.5+deb7u9.</p>

<p>We recommend that you upgrade your strongswan packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-973.data"
# $Id: $
