<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A heap-based buffer overflow was discovered in the LZWDecodeCompat
function in tif_lzw.c (LibTIFF 4.0.9 and earlier). This vulnerability
might be leveraged by remote attackers to crash the client via a
crafted TIFF LZW file.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.9.6-11+deb7u11.</p>

<p>We recommend that you upgrade your tiff3 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1378.data"
# $Id: $
