<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Florian Grunow and Birk Kauer of ERNW discovered a path traversal
vulnerability in SquirrelMail, a webmail application, allowing an
authenticated remote attacker to retrieve or delete arbitrary files
via mail attachment.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2:1.4.23~svn20120406-2+deb7u2.</p>

<p>We recommend that you upgrade your squirrelmail packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1344.data"
# $Id: $
