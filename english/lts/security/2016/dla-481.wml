<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This security update fixes a number of security issues in
phpMyAdmin. We recommend you upgrade your phpmyadmin packages.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-1927">CVE-2016-1927</a>

     <p>suggestPassword generates weak passphrases</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2038">CVE-2016-2038</a>

     <p>information disclosure via crafted requests</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2039">CVE-2016-2039</a>

     <p>weak CSRF token values</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2040">CVE-2016-2040</a>

     <p>XSS vulnerabilities in authenticated users</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2041">CVE-2016-2041</a>

     <p>information breach in CSRF token comparison</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2045">CVE-2016-2045</a>

     <p>XSS injection via crafted SQL queries</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2560">CVE-2016-2560</a>

     <p>XSS injection</p></li>

</ul>

<p>Further information about Debian LTS security Advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

<p>For Debian 7 <q>Wheezy</q>, these issues have been fixed in phpmyadmin version 4:3.4.11.1-2+deb7u3</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-481.data"
# $Id: $
