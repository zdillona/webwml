<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that zstd, a compression utility, was vulnerable
to a race condition: it temporarily exposed, during a very short
timeframe, a world-readable version of its input even if the
original file had restrictive permissions.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.1.2-1+deb9u1.</p>

<p>We recommend that you upgrade your libzstd packages.</p>

<p>For the detailed security status of libzstd please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libzstd">https://security-tracker.debian.org/tracker/libzstd</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2573.data"
# $Id: $
