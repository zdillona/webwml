<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>GnuTLS, a portable cryptography library, fails to validate alternate
trust chains in some conditions.  In particular this breaks connecting
to servers that use Let's Encrypt certificates, starting 2021-10-01.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
3.5.8-5+deb9u6.</p>

<p>We recommend that you upgrade your gnutls28 packages.</p>

<p>For the detailed security status of gnutls28 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/gnutls28">https://security-tracker.debian.org/tracker/gnutls28</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2759.data"
# $Id: $
