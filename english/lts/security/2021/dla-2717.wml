<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there were several integer overflow issues in
Redis, a popular key-value database system. Some BITFIELD-related commands
were affected on 32-bit systems.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32761">CVE-2021-32761</a></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
3:3.2.6-3+deb9u5.</p>

<p>We recommend that you upgrade your redis packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2717.data"
# $Id: $
