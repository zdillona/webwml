<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that csync2, a cluster synchronization tool, did not
correctly check for the return value from GnuTLS security routines. It
neglected to repeatedly call this function as required by the design of the
API.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15523">CVE-2019-15523</a>

    <p>An issue was discovered in LINBIT csync2 through 2.0. It does not
    correctly check for the return value GNUTLS_E_WARNING_ALERT_RECEIVED of the
    gnutls_handshake() function. It neglects to call this function again, as
    required by the design of the API.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
2.0-8-g175a01c-4+deb9u2.</p>

<p>We recommend that you upgrade your csync2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2515.data"
# $Id: $
