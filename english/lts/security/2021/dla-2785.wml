<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3702">CVE-2020-3702</a>

    <p>A flaw was found in the driver for Atheros IEEE 802.11n family of
    chipsets (ath9k) allowing information disclosure.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16119">CVE-2020-16119</a>

    <p>Hadar Manor reported a use-after-free in the DCCP protocol
    implementation in the Linux kernel. A local attacker can take
    advantage of this flaw to cause a denial of service or potentially
    to execute arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3444">CVE-2021-3444</a>

<p>, <a href="https://security-tracker.debian.org/tracker/CVE-2021-3600">CVE-2021-3600</a></p>

    <p>Two flaws were discovered in the Extended BPF (eBPF) verifier.  A
    local user could exploit these to read and write arbitrary memory
    in the kernel, which could be used for privilege escalation.</p>

    <p>This can be mitigated by setting sysctl
    kernel.unprivileged_bpf_disabled=1, which disables eBPF use by
    unprivileged users.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3612">CVE-2021-3612</a>

    <p>Murray McAllister reported a flaw in the joystick input subsystem.
    A local user permitted to access a joystick device could exploit
    this to read and write out-of-bounds in the kernel, which could
    be used for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3653">CVE-2021-3653</a>

   <p>Maxim Levitsky discovered a vulnerability in the KVM hypervisor
   implementation for AMD processors in the Linux kernel: Missing
   validation of the `int_ctl` VMCB field could allow a malicious L1
   guest to enable AVIC support (Advanced Virtual Interrupt
   Controller) for the L2 guest. The L2 guest can take advantage of
   this flaw to write to a limited but still relatively large subset
   of the host physical memory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3655">CVE-2021-3655</a>

    <p>Ilja Van Sprundel and Marcelo Ricardo Leitner found multiple flaws
    in the SCTP implementation, where missing validation could lead to
    an out-of-bounds read.  On a system using SCTP, a networked
    attacker could exploit these to cause a denial of service (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3656">CVE-2021-3656</a>

    <p>Maxim Levitsky and Paolo Bonzini discovered a flaw in the KVM
    hypervisor implementation for AMD processors in the Linux
    kernel. Missing validation of the `virt_ext` VMCB field could
    allow a malicious L1 guest to disable both VMLOAD/VMSAVE
    intercepts and VLS (Virtual VMLOAD/VMSAVE) for the L2 guest. Under
    these circumstances, the L2 guest is able to run VMLOAD/VMSAVE
    unintercepted and thus read/write portions of the host's physical
    memory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3679">CVE-2021-3679</a>

    <p>A flaw in the Linux kernel tracing module functionality could
    allow a privileged local user (with CAP_SYS_ADMIN capability) to
    cause a denial of service (resource starvation).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3732">CVE-2021-3732</a>

    <p>Alois Wohlschlager reported a flaw in the implementation of the
    overlayfs subsystem, allowing a local attacker with privileges to
    mount a filesystem to reveal files hidden in the original mount.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3743">CVE-2021-3743</a>

    <p>An out-of-bounds memory read was discovered in the Qualcomm IPC
    router protocol implementation, allowing to cause a denial of
    service or information leak.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3753">CVE-2021-3753</a>

    <p>Minh Yuan reported a race condition in the vt_k_ioctl in
    drivers/tty/vt/vt_ioctl.c, which may cause an out of bounds read
    in vt.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22543">CVE-2021-22543</a>

    <p>David Stevens discovered a flaw in how the KVM hypervisor maps
    host memory into a guest.  A local user permitted to access
    /dev/kvm could use this to cause certain pages to be freed when
    they should not, leading to a use-after-free.  This could be used
    to cause a denial of service (crash or memory corruption) or
    possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33624">CVE-2021-33624</a>

<p>, <a href="https://security-tracker.debian.org/tracker/CVE-2021-34556">CVE-2021-34556</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2021-35477">CVE-2021-35477</a></p>

    <p>Multiple researchers discovered flaws in the Extended BPF (eBPF)
    verifier's protections against information leaks through
    speculation execution.  A local user could exploit these to read
    sensitive information.</p>

    <p>This can be mitigated by setting sysctl
    kernel.unprivileged_bpf_disabled=1, which disables eBPF use by
    unprivileged users.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-35039">CVE-2021-35039</a>

    <p>A flaw was discovered in module signature enforcement.  A custom
    kernel with IMA enabled might have allowed loading unsigned kernel
    modules when it should not have.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-37159">CVE-2021-37159</a>

    <p>A flaw was discovered in the hso driver for Option mobile
    broadband modems.  An error during initialisation could lead to a
    double-free or use-after-free.  An attacker able to plug in USB
    devices could use this to cause a denial of service (crash or
    memory corruption) or possibly to run arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38160">CVE-2021-38160</a>

    <p>A flaw in the virtio_console was discovered allowing data
    corruption or data loss by an untrusted device.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38198">CVE-2021-38198</a>

    <p>A flaw was discovered in the KVM implementation for x86
    processors, that could result in virtual memory protection within
    a guest not being applied correctly.  When shadow page tables are
    used - i.e. for nested virtualisation, or on CPUs lacking the EPT
    or NPT feature - a user of the guest OS might be able to exploit
    this for denial of service or privilege escalation within the
    guest.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38199">CVE-2021-38199</a>

    <p>Michael Wakabayashi reported a flaw in the NFSv4 client
    implementation, where incorrect connection setup ordering allows
    operations of a remote NFSv4 server to cause a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38205">CVE-2021-38205</a>

    <p>An information leak was discovered in the xilinx_emaclite network
    driver.  On a custom kernel where this driver is enabled and used,
    this might make it easier to exploit other kernel bugs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40490">CVE-2021-40490</a>

    <p>A race condition was discovered in the ext4 subsystem when writing
    to an inline_data file while its xattrs are changing. This could
    result in denial of service.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
4.19.208-1~deb9u1.</p>

<p>We recommend that you upgrade your linux-4.19 packages.</p>

<p>For the detailed security status of linux-4.19 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux-4.19">https://security-tracker.debian.org/tracker/linux-4.19</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2785.data"
# $Id: $
