<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that the fix for <a href="https://security-tracker.debian.org/tracker/CVE-2020-25712">CVE-2020-25712</a> in the Xorg X server, addressed
in DLA-2486-1, caused a regression in caribou, making it crash whenever
special (shifted) characters were entered.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
0.4.21-1+deb9u1.</p>

<p>We recommend that you upgrade your caribou packages.</p>

<p>For the detailed security status of caribou please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/caribou">https://security-tracker.debian.org/tracker/caribou</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2675.data"
# $Id: $
