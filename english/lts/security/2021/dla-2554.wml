<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Roman Fiedler discovered a vulnerability in the OverlayFS code in
firejail, a sandbox program to restrict the running environment of
untrusted applications, which could result in root privilege
escalation. This update disables OverlayFS support in firejail.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
0.9.44.8-2+deb9u2.</p>

<p>We recommend that you upgrade your firejail packages.</p>

<p>For the detailed security status of firejail please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/firejail">https://security-tracker.debian.org/tracker/firejail</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2554.data"
# $Id: $
