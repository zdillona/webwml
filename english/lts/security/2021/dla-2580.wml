<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Adminer is an open-source database management in a single PHP file.
In adminer from version 4.0.0 and before 4.7.9 there is a
server-side request forgery vulnerability. Users of Adminer versions
bundling all drivers (e.g. `adminer.php`) are affected.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
4.2.5-3+deb9u2.</p>

<p>We recommend that you upgrade your adminer packages.</p>

<p>For the detailed security status of adminer please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/adminer">https://security-tracker.debian.org/tracker/adminer</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2580.data"
# $Id: $
