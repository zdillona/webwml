<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities were found in Apache Log4j2, a Logging
Framework for Java, which could lead to a denial of service or information
disclosure.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9488">CVE-2020-9488</a>

    <p>Improper validation of certificate with host mismatch in Apache Log4j SMTP
    appender. This could allow an SMTPS connection to be intercepted by a
    man-in-the-middle attack which could leak any log messages sent through
    that appender.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45105">CVE-2021-45105</a>

    <p>Apache Log4j2 did not protect from uncontrolled recursion from
    self-referential lookups. This allows an attacker with control over Thread
    Context Map data to cause a denial of service when a crafted string is
    interpreted.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
2.12.3-0+deb9u1.</p>

<p>We recommend that you upgrade your apache-log4j2 packages.</p>

<p>For the detailed security status of apache-log4j2 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/apache-log4j2">https://security-tracker.debian.org/tracker/apache-log4j2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2852.data"
# $Id: $
