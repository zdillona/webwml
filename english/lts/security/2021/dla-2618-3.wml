<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The security update of smarty3, the compiling PHP template engine, issued as
DLA 2618-1 introduced a regression in the smarty_security class when secure
directories are evaluated. Updated smarty3 packages are now available to
correct this issue.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
3.1.31+20161214.1.c7d42e4+selfpack1-2+deb9u4.</p>

<p>We recommend that you upgrade your smarty3 packages.</p>

<p>For the detailed security status of smarty3 please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/smarty3">https://security-tracker.debian.org/tracker/smarty3</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2618-3.data"
# $Id: $
