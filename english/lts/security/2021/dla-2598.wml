<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Due to improper input validation, Squid is vulnerable to an HTTP
Request Smuggling attack.</p>

<p>This problem allows a trusted client to perform HTTP Request
Smuggling and access services otherwise forbidden by Squid
security controls.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
3.5.23-5+deb9u6.</p>

<p>We recommend that you upgrade your squid3 packages.</p>

<p>For the detailed security status of squid3 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/squid3">https://security-tracker.debian.org/tracker/squid3</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2598.data"
# $Id: $
