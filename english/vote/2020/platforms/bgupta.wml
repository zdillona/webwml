#use wml::debian::template title="Platform for Brian Gupta" BARETITLE="true" NOHEADER="true"
#include "$(ENGLISHDIR)/vote/style.inc"

<h2>Introduction</h2>
<p>
I am running for DPL with a singular goal. The creation of Debian US and EU Foundations. I largely view my candidacy as a referendum on this goal and its details.
</p>
<p>
Operationally, I would reconstitute the DPL-Helpers team to assist the DPL.
</p>
<h2>Rationale</h2>
<p>
The rationale for these changes is to enable Debian to move faster on issues, such as financial, legal and trademark issues. The DPL should have the option of delegating some administrative duties so that they can concentrate on setting the direction of the project, representing the project, and getting buy-in from the members to move important issues forward. IE: Debian Project Leaders should have more time to lead rather than be buried in the set of administrative tasks they currently face, and history has shown that volunteers alone aren&rsquo;t enough.
<p>
Why can&rsquo;t we do this all with volunteers, like we do everything else? Although for many technical tasks, it&rsquo;s fairly easy to do the work as part of our paid day jobs, or as part of other technical roles, we might have because the skills required are many times a natural fit for technologists, for other non-technical tasks, like accounting, lawyering and such, it&rsquo;s very difficult to find enough people to volunteer to do these things, who are able to give them priority. At the very least, the qualified pool of volunteers that are interested in these issues, but are also technologists who are interested in Debian, is much smaller than for technical or creative tasks.
</p>
<p>
Why create our own orgs when we have existing ones? We&rsquo;ve had mixed results over the years working through TOs (Trusted Organizations) that aren&rsquo;t Debian-focused. It&rsquo;s no secret that SPI is Debian&rsquo;s most Trusted TO. It&rsquo;s also fairly well-known that the service quality provided by SPI to Debian has varied greatly over the years.
</p>
<p>
Currently, SPI&rsquo;s service levels have improved greatly, due to their own use of paid help, but there was a time when the service they provided was bad enough that it was hard to justify maintaining the special relationship. Note, this was largely due to limited volunteer cycles, and a wide-ranging mandate, to support many projects.
</p>
<p>
The biggest challenge of late is that SPI has had a philosophical change. SPI has indicated that they no longer believe that Debian, which was SPI&rsquo;s founding member project, is their most important project, and all projects must be treated equally.
</p>
<p>
Some ways that this is manifesting are, 1) the DPL is no longer a special member of SPI invited to all meetings, 2) without informing us, after 10 years of de facto practice, SPI stopped waiving their standard 5% fees on DebConf sponsorship payments, this has cost Debian approx. 16,000 USD since the unannounced change became effective 3) SPI would like to close the Debian Paypal account, for the simple reason that they can&rsquo;t give all member projects their own Paypal accounts. (Another large SPI member project, PostgreSQL, has their own foundations to deal with these issues.)
</p>
<p>
In addition, our historically preferred European Trusted Org, FFIS, has become non-functional for all intents and purposes, to the point we now ask people not to donate through FFIS, and have removed them from the list of current Trusted Organizations.
</p>
<h2>Overview</h2>
<p>
When establishing these foundations, I would start with Debian US, as I am based in the US (New York City). The Foundation&#39;s board would be elected by the Debian Project Members, and the DPL would automatically be a full member of the Foundation&#39;s board. (Initially a temporary board may be assembled by DPL to bootstrap.) Board members must be Debian Project Members but otherwise should have no geographic restrictions.
</p>
<p>
Once the Foundation is operational, an early goal would be for the Foundation to hire a part-time administrator (20 hours per week) who would take direction from the DPL and the board and would be available to assist the DPL, and Debian&#39;s various bureaucratic teams, including but not limited to DPL-helpers, Treasurer, &nbsp;and Trademark.
</p>
<p>
Another goal would be to establish a Debian Europe. France and Switzerland would be at the top of the list of jurisdictions in which to base a foundation, as we have strong TOs there. I would coordinate with both and see if they might want to evolve their organizations into a Europe-wide entity or assist in founding a new one.
</p>
<p>
Over time, I would expect these two entities to become our primary Trusted Organizations in the US and Europe, but don&#39;t expect to end relationships with our current TOs, SPI, Debian France, and Debian.ch.
</p>
<p>
Some similar entities to look at would be FreeBSD Foundation, the United States PostgreSQL Association, and PostgreSQL Europe.
</p>
<p>
If you share my goal to establish these entities, please feel encouraged to reach out and let me know, and indicate if you are able to help.
</p>
<h2>Commitments made during the campaigning period</h2>
<p>
I will perform the following duties that would be expected of any DPL.
</p>
<ul>
  <li>Generating and publishing monthly reports. Bits from the DPL has become an essential source of news for Project Members, and I am excited to continue this tradition.</li>
  <li>Review and authorize delegations
  <li>I plan to immediately reach out to all delegated teams to schedule availability during the following year. So if teams need extra DPL attention at certain times, I'll have reserved it for those teams.</li>
  <li>Make decisions regarding property held in trust for Debian (mostly reviewing and approving expenses and budgets in a timely fashion)</li>
  <li>Supporting the TC in appointing new members
  <li>Add or remove organizations from the list of trusted organizations. At this time I have no pressing actions on this front, other than work to create the new Foundations.</li>
  <li>Solicit advice both privately and, publicly</li>
  <li>Be available to all Developers who need help</li>
</ul>
<p>
In addition, I commit to the following course of action regarding the Foundations:
</p>
<ul>
  <li>Recruit a team of at least three additional DDs to work on the Foundation(s) project with me (open to all comers)</li>
  <li>Work with the team, to do the heavy lifting and research required to have a deep discussion with the Project about our next steps</li>
  <li>I will seek consensus on debian-project regarding the Foundation(s) details, with a goal of building a consensus around creating US and EU Foundations, enabling us to directly engage paid Financial, Legal and administrative services, and staff.</li>
  <li>I will seek a GR and build consensus for any Constitutional changes legally required to implement the Foundations</li>
  <li>I will consult with DDs regarding hiring any staff (and other large expenditures)</li>
</ul>
<p>
In addition, I have the following non-Foundation related goals and plans.:
</p>
<ul>
  <li>I will (re)create dpl-helpers and invite Project Members, and other DPL candidates (past and present) to join. I plan to reinstitute the regular DPL-team meetings on a bi-weekly basis. These meetings will be on IRC and open for all.</li>
  <li>I will work with DAM and Webmasters to separate uploading rights from the path to becoming a DD. Uploading DD is currently handled as the default path, which discourages potential applicants from pursuing Debian Developer status before they are ready to go through the T&amp;S checks</li>
  <li>I will make sure all delegations are compliant w/ the Constitution (we have at least a couple that are non-compliant)</li>
  <li>While recognizing that many people aren't ready for change, I will encourage experimentation in communication technology e.g. - Discourse, and telepresence tools</li>
  <li>I will attend every DebConf during my term (barring travel restrictions).  DebConf is important and it's important that the DPL attends</li>
  <li>I will run again if things are going well and people are happy with my performance. I say this because any former DPL will confirm that it takes time to learn to be a DPL. Allowing the project to leverage that experience via a second term only makes sense.</li>
</ul>
<h2>About me</h2>
<p>
I&#39;ve been involved in free and open source communities for almost 15 years, a Debian Project Member for almost 7 years, and have been leading NYLUG (New York Linux Users Group) for over 10 years. I first became involved in Debian in 2008, as one of the first bid team members of what was to become DebConf10. I&#39;ve been involved in the Treasurer, Trademark and DPL-helpers teams as well. I have done fundraising for Debian and Debconf over the years and helped Debian get a Paypal account through SPI. I expect to have at least 10-15 hours a week to work on DPL-related tasks. I estimate it will take 6-12 months to create the US entity.
</p>

<h2>Rebuttals</h2>
<h3>Jonathan Carter</h3>
I'd like to congratulate Jonathan for running for DPL again as it's less common than it should be.
<p>
<strong>Membership nomenclature:</strong> 
</p>
<p>
I don’t believe we should refuse to use the term Debian Developer anymore because someone hijacked it to falsely represent themselves.
</p>
<p>
As a member of the minority of Debian Developers that joined the Project for contributions unrelated to packaging, when I initially joined the project, I had a strong preference for the term Debian Project Member. However, I came to realize my initial reluctance to use Debian Developer was a manifestation of "Imposter Syndrome", and have grown to use the terms interchangeably, depending on the audience I am addressing. Rarely do I use the term non-uploading Debian Developer, but have, when it's needed for clarity. My choice largely depends on what will be the most understood or useful in context.
</p>
<p>
I was a little dishearted to hear that Jonathan chose to delay his Project Membership to pursue full uploading rights. I don't fault Jonathan for this, as I've seen other people express this preference before, but it seems a sign of a bigger issue, and makes me wonder if today there is a stigma to being a non-uploading Debian Developer, at least by those pursuing membership.
</p>
<p>
My ideal long-term future for Debian would be one where the vast majority of Developers only had upload rights to the packages they maintain, as I do not believe that 1000 people all need upload rights to 60,000 packages, and think these rights should be granted very selectively, and only as needed to Developers that have project-wide responsibilities. For example, to active members of Debian Security. 
</p>
<p>
Ideally such rights would be aligned with rights in salsa. It doesn't make much sense that you can upload a package where you cannot edit the source and you can edit the source of a package (e.g. the Debian group = collab-maint) but not upload it at least to delayed queue.
</p>
<p>
I understand that such a change would be impractical today, and we are very far from achieving a consensus, so it's NOT something I an actively pursuing, but more of a seed for thought, and suspect to get there we’d have to have it be a change we phase in over a long period of time.
</p>
<p>
Clearly we have work here as a project, but I don't think it's going to be resolved by a GR to stop calling ourselves Debian Developers. I'd prefer to continue to leave that choice to individual Developers and work with DAM and Webmaster teams to make sure the process and documentation for application strongly decouple membership and project-wide uploading rights. (Currently it treats non-uploading status as an anomaly/exception, which may be contributing to the perception.)
</p>

<h3>Sruthi Chandran</h3>
<p>
I'd like to congratulate Sruthi for running. Although I have no issues with Sruthi's expressed high-level goals, I would very much like to see specifics in order to fully evaluate them. Sruthi is leading DebConf 22 preparations. Even though this is over two years away, there is a lot of preparatory work, and much of it is deadline-driven. Also, it's generally expected that bid teams for future DebConfs are heavily involved in organizing the ones that happen earlier. I would personally encourage Sruthi to run for DPL after DebConf 22, as that would no longer be competing for her time, and would also give her more experience working in the Debian project.
</p>
