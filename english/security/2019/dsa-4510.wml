<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Nick Roessler and Rafi Rubin discovered that the IMAP and ManageSieve
protocol parsers in the Dovecot email server do not properly validate
input (both pre- and post-login). A remote attacker can take advantage
of this flaw to trigger out of bounds heap memory writes, leading to
information leaks or potentially the execution of arbitrary code.</p>

<p>For the oldstable distribution (stretch), this problem has been fixed
in version 1:2.2.27-3+deb9u5.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 1:2.3.4.1-5+deb10u1.</p>

<p>We recommend that you upgrade your dovecot packages.</p>

<p>For the detailed security status of dovecot please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/dovecot">\
https://security-tracker.debian.org/tracker/dovecot</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4510.data"
# $Id: $
