<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20836">CVE-2018-20836</a>

    <p>chenxiang reported a race condition in libsas, the kernel
    subsystem supporting Serial Attached SCSI (SAS) devices, which
    could lead to a use-after-free.  It is not clear how this might be
    exploited.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1125">CVE-2019-1125</a>

    <p>It was discovered that most x86 processors could speculatively
    skip a conditional SWAPGS instruction used when entering the
    kernel from user mode, and/or could speculatively execute it when
    it should be skipped.  This is a subtype of Spectre variant 1,
    which could allow local users to obtain sensitive information from
    the kernel or other processes.  It has been mitigated by using
    memory barriers to limit speculative execution.  Systems using an
    i386 kernel are not affected as the kernel does not use SWAPGS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1999">CVE-2019-1999</a>

    <p>A race condition was discovered in the Android binder driver,
    which could lead to a use-after-free.  If this driver is loaded, a
    local user might be able to use this for denial-of-service
    (memory corruption) or for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10207">CVE-2019-10207</a>

    <p>The syzkaller tool found a potential null dereference in various
    drivers for UART-attached Bluetooth adapters.  A local user with
    access to a pty device or other suitable tty device could use this
    for denial-of-service (BUG/oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10638">CVE-2019-10638</a>

    <p>Amit Klein and Benny Pinkas discovered that the generation of IP
    packet IDs used a weak hash function, <q>jhash</q>.  This could enable
    tracking individual computers as they communicate with different
    remote servers and from different networks.  The <q>siphash</q>
    function is now used instead.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12817">CVE-2019-12817</a>

    <p>It was discovered that on the PowerPC (ppc64el) architecture, the
    hash page table (HPT) code did not correctly handle fork() in a
    process with memory mapped at addresses above 512 TiB.  This could
    lead to a use-after-free in the kernel, or unintended sharing of
    memory between user processes.  A local user could use this for
    privilege escalation.  Systems using the radix MMU, or a custom
    kernel with a 4 KiB page size, are not affected.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12984">CVE-2019-12984</a>

    <p>It was discovered that the NFC protocol implementation did not
    properly validate a netlink control message, potentially leading
    to a null pointer dereference.  A local user on a system with an
    NFC interface could use this for denial-of-service (BUG/oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13233">CVE-2019-13233</a>

    <p>Jann Horn discovered a race condition on the x86 architecture,
    in use of the LDT.  This could lead to a use-after-free.  A
    local user could possibly use this for denial-of-service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13631">CVE-2019-13631</a>

    <p>It was discovered that the gtco driver for USB input tablets could
    overrun a stack buffer with constant data while parsing the device's
    descriptor.  A physically present user with a specially
    constructed USB device could use this to cause a denial-of-service
    (BUG/oops), or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13648">CVE-2019-13648</a>

    <p>Praveen Pandey reported that on PowerPC (ppc64el) systems without
    Transactional Memory (TM), the kernel would still attempt to
    restore TM state passed to the sigreturn() system call.  A local
    user could use this for denial-of-service (oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14283">CVE-2019-14283</a>

    <p>The syzkaller tool found a missing bounds check in the floppy disk
    driver.  A local user with access to a floppy disk device, with a
    disk present, could use this to read kernel memory beyond the
    I/O buffer, possibly obtaining sensitive information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14284">CVE-2019-14284</a>

    <p>The syzkaller tool found a potential division-by-zero in the
    floppy disk driver.  A local user with access to a floppy disk
    device could use this for denial-of-service (oops).</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 4.19.37-5+deb10u2.</p>

<p>For the oldstable distribution (stretch), these problems will be fixed
soon.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>For the detailed security status of linux please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux">https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4495.data"
# $Id: $
