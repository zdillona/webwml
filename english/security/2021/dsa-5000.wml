<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the OpenJDK Java
runtime, which may result in denial of service, incorrect Kerberos ticket
use, selection of weak ciphers or information disclosure.</p>

<p>The oldstable distribution (buster), needs additional updates to be able
to build 11.0.13. An update will be provided in a followup advisory.</p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 11.0.13+8-1~deb11u1.</p>

<p>We recommend that you upgrade your openjdk-11 packages.</p>

<p>For the detailed security status of openjdk-11 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/openjdk-11">\
https://security-tracker.debian.org/tracker/openjdk-11</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-5000.data"
# $Id: $
