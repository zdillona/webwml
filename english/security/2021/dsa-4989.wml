<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Researchers at the United States of America National Security Agency (NSA)
identified two denial of services vulnerability in strongSwan, an IKE/IPsec
suite.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41990">CVE-2021-41990</a>

    <p>RSASSA-PSS signatures whose parameters define a very high salt length can
    trigger an integer overflow that can lead to a segmentation fault.</p>

    <p>Generating a signature that bypasses the padding check to trigger the crash
    requires access to the private key that signed the certificate.  However,
    the certificate does not have to be trusted.  Because the gmp and the
    openssl plugins both check if a parsed certificate is self-signed (and the
    signature is valid), this can e.g.  be triggered by an unrelated
    self-signed CA certificate sent by an initiator.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41991">CVE-2021-41991</a>

    <p>Once the in-memory certificate cache is full it tries to randomly replace
    lesser used entries. Depending on the generated random value, this could
    lead to an integer overflow that results in a double-dereference and a call
    using out-of-bounds memory that most likely leads to a segmentation fault.</p>

    <p>Remote code execution can't be ruled out completely, but attackers have no
    control over the dereferenced memory, so it seems unlikely at this point.</p></li>

</ul>

<p>For the oldstable distribution (buster), these problems have been fixed
in version 5.7.2-1+deb10u1.</p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 5.9.1-1+deb11u1.</p>

<p>We recommend that you upgrade your strongswan packages.</p>

<p>For the detailed security status of strongswan please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/strongswan">\
https://security-tracker.debian.org/tracker/strongswan</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4989.data"
# $Id: $
