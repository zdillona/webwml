<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in the Xen hypervisor,
which could result in privilege escalation, denial of service or
information leaks.</p>

<p>With the end of upstream support for the 4.11 branch, the version of xen
in the oldstable distribution (buster) is no longer supported. If you
rely on security support for your Xen installation an update to the
stable distribution (bullseye) is recommended.</p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 4.14.3-1~deb11u1.</p>

<p>We recommend that you upgrade your xen packages.</p>

<p>For the detailed security status of xen please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/xen">\
https://security-tracker.debian.org/tracker/xen</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4977.data"
# $Id: $
