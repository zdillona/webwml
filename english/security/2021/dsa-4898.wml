<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in wpa_supplicant and
hostapd.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12695">CVE-2020-12695</a>

    <p>It was discovered that hostapd does not properly handle UPnP
    subscribe messages under certain conditions, allowing an attacker to
    cause a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-0326">CVE-2021-0326</a>

    <p>It was discovered that wpa_supplicant does not properly process P2P
    (Wi-Fi Direct) group information from active group owners. An
    attacker within radio range of the device running P2P could take
    advantage of this flaw to cause a denial of service or potentially
    execute arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27803">CVE-2021-27803</a>

    <p>It was discovered that wpa_supplicant does not properly process
    P2P (Wi-Fi Direct) provision discovery requests. An attacker
    within radio range of the device running P2P could take advantage
    of this flaw to cause a denial of service or potentially execute
    arbitrary code.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 2:2.7+git20190128+0c1e29f-6+deb10u3.</p>

<p>We recommend that you upgrade your wpa packages.</p>

<p>For the detailed security status of wpa please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/wpa">https://security-tracker.debian.org/tracker/wpa</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4898.data"
# $Id: $
