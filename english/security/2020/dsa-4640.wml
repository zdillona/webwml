<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>This update fixes several vulnerabilities in Graphicsmagick: Various memory
handling problems and cases of missing or incomplete input sanitising
may result in denial of service, memory disclosure or the execution
of arbitrary code if malformed media files are processed.</p>

<p>For the oldstable distribution (stretch), these problems have been fixed
in version 1.3.30+hg15796-1~deb9u3.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 1.4~hg15978-1+deb10u1.</p>

<p>We recommend that you upgrade your graphicsmagick packages.</p>

<p>For the detailed security status of graphicsmagick please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/graphicsmagick">\
https://security-tracker.debian.org/tracker/graphicsmagick</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4640.data"
# $Id: $
