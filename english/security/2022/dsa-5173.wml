<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4197">CVE-2021-4197</a>

    <p>Eric Biederman reported that incorrect permission checks in the
    cgroup process migration implementation can allow a local attacker
    to escalate privileges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0494">CVE-2022-0494</a>

    <p>The scsi_ioctl() was susceptible to an information leak only
    exploitable by users with CAP_SYS_ADMIN or CAP_SYS_RAWIO
    capabilities.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0812">CVE-2022-0812</a>

    <p>It was discovered that the RDMA transport for NFS (xprtrdma)
    miscalculated the size of message headers, which could lead to a
    leak of sensitive information between NFS servers and clients.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0854">CVE-2022-0854</a>

    <p>Ali Haider discovered a potential information leak in the DMA
    subsystem. On systems where the swiotlb feature is needed, this
    might allow a local user to read sensitive information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1011">CVE-2022-1011</a>

    <p>Jann Horn discovered a flaw in the FUSE (Filesystem in User-Space)
    implementation. A local user permitted to mount FUSE filesystems
    could exploit this to cause a use-after-free and read sensitive
    information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1012">CVE-2022-1012</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-32296">CVE-2022-32296</a>

    <p>Moshe Kol, Amit Klein, and Yossi Gilad discovered a weakness
    in randomisation of TCP source port selection.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1016">CVE-2022-1016</a>

    <p>David Bouman discovered a flaw in the netfilter subsystem where
    the nft_do_chain function did not initialize register data that
    nf_tables expressions can read from and write to. A local attacker
    can take advantage of this to read sensitive information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1048">CVE-2022-1048</a>

    <p>Hu Jiahui discovered a race condition in the sound subsystem that
    can result in a use-after-free. A local user permitted to access a
    PCM sound device can take advantage of this flaw to crash the
    system or potentially for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1195">CVE-2022-1195</a>

    <p>Lin Ma discovered race conditions in the 6pack and mkiss hamradio
    drivers, which could lead to a use-after-free. A local user could
    exploit these to cause a denial of service (memory corruption or
    crash) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1198">CVE-2022-1198</a>

    <p>Duoming Zhou discovered a race condition in the 6pack hamradio
    driver, which could lead to a use-after-free. A local user could
    exploit this to cause a denial of service (memory corruption or
    crash) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1199">CVE-2022-1199</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-1204">CVE-2022-1204</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-1205">CVE-2022-1205</a>

    <p>Duoming Zhou discovered race conditions in the AX.25 hamradio
    protocol, which could lead to a use-after-free or null pointer
    dereference. A local user could exploit this to cause a denial of
    service (memory corruption or crash) or possibly for privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1353">CVE-2022-1353</a>

    <p>The TCS Robot tool found an information leak in the PF_KEY
    subsystem. A local user can receive a netlink message when an
    IPsec daemon registers with the kernel, and this could include
    sensitive information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1419">CVE-2022-1419</a>

    <p>Minh Yuan discovered a race condition in the vgem virtual GPU
    driver that can lead to a use-after-free. A local user permitted
    to access the GPU device can exploit this to cause a denial of
    service (crash or memory corruption) or possibly for privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1516">CVE-2022-1516</a>

    <p>A NULL pointer dereference flaw in the implementation of the X.25
    set of standardized network protocols, which can result in denial
    of service.</p>

    <p>This driver is not enabled in Debian's official kernel
    configurations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1652">CVE-2022-1652</a>

    <p>Minh Yuan discovered a race condition in the floppy driver that
    can lead to a use-after-free. A local user permitted to access a
    floppy drive device can exploit this to cause a denial of service
    (crash or memory corruption) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1729">CVE-2022-1729</a>

    <p>Norbert Slusarek discovered a race condition in the perf subsystem
    which could result in local privilege escalation to root. The
    default settings in Debian prevent exploitation unless more
    permissive settings have been applied in the
    kernel.perf_event_paranoid sysctl.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1734">CVE-2022-1734</a>

    <p>Duoming Zhou discovered race conditions in the nfcmrvl NFC driver
    that could lead to a use-after-free, double-free or null pointer
    dereference. A local user might be able to exploit these for
    denial of service (crash or memory corruption) or possibly for
    privilege escalation.</p>

    <p>This driver is not enabled in Debian's official kernel
    configurations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1974">CVE-2022-1974</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-1975">CVE-2022-1975</a>

    <p>Duoming Zhou discovered that the NFC netlink interface was
    suspectible to denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2153">CVE-2022-2153</a>

    <p><q>kangel</q> reported a flaw in the KVM implementation for x86
    processors which could lead to a null pointer dereference. A local
    user permitted to access /dev/kvm could exploit this to cause a
    denial of service (crash).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21123">CVE-2022-21123</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-21125">CVE-2022-21125</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-21166">CVE-2022-21166</a>

    <p>Various researchers discovered flaws in Intel x86 processors,
    collectively referred to as MMIO Stale Data vulnerabilities.
    These are similar to the previously published Microarchitectural
    Data Sampling (MDS) issues and could be exploited by local users
    to leak sensitive information.</p>

    <p>For some CPUs, the mitigations for these issues require updated
    microcode.  An updated intel-microcode package may be provided at
    a later date.  The updated CPU microcode may also be available as
    part of a system firmware ("BIOS") update.</p>

    <p>Further information on the mitigation can be found at
    <url "https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/processor_mmio_stale_data.html">
    or in the linux-doc-4.19 package.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23960">CVE-2022-23960</a>

    <p>Researchers at VUSec discovered that the Branch History Buffer in
    Arm processors can be exploited to create information side
    channels with speculative execution.  This issue is similar to
    Spectre variant 2, but requires additional mitigations on some
    processors.</p>

    <p>This was previously mitigated for 32-bit Arm (armel and armhf)
    architectures and is now also mitigated for 64-bit Arm (arm64).</p>

    <p>This can be exploited to obtain sensitive information from a
    different security context, such as from user-space to the kernel,
    or from a KVM guest to the kernel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26490">CVE-2022-26490</a>

    <p>Buffer overflows in the STMicroelectronics ST21NFCA core driver
    can result in denial of service or privilege escalation.</p>

    <p>This driver is not enabled in Debian's official kernel
    configurations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-27666">CVE-2022-27666</a>

    <p><q>valis</q> reported a possible buffer overflow in the IPsec ESP
    transformation code. A local user can take advantage of this flaw
    to cause a denial of service or for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28356">CVE-2022-28356</a>

    <p><q>Beraphin</q> discovered that the ANSI/IEEE 802.2 LLC type 2 driver did
    not properly perform reference counting on some error paths. A
    local attacker can take advantage of this flaw to cause a denial
    of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28388">CVE-2022-28388</a>

    <p>A double free vulnerability was discovered in the 8 devices
    USB2CAN interface driver.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28389">CVE-2022-28389</a>

    <p>A double free vulnerability was discovered in the Microchip CAN
    BUS Analyzer interface driver.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28390">CVE-2022-28390</a>

    <p>A double free vulnerability was discovered in the EMS CPC-USB/ARM7
    CAN/USB interface driver.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-29581">CVE-2022-29581</a>

    <p>Kyle Zeng discovered a reference-counting bug in the cls_u32
    network classifier which can lead to a use-after-free. A local
    user can exploit this to cause a denial of service (crash or
    memory corruption) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30594">CVE-2022-30594</a>

    <p>Jann Horn discovered a flaw in the interaction between ptrace and
    seccomp subsystems. A process sandboxed using seccomp() but still
    permitted to use ptrace() could exploit this to remove the seccomp
    restrictions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32250">CVE-2022-32250</a>

    <p>Aaron Adams discovered a use-after-free in Netfilter which may
    result in local privilege escalation to root.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-33981">CVE-2022-33981</a>

    <p>Yuan Ming from Tsinghua University reported a race condition in
    the floppy driver involving use of the FDRAWCMD ioctl, which could
    lead to a use-after-free. A local user with access to a floppy
    drive device could exploit this to cause a denial of service
    (crash or memory corruption) or possibly for privilege escalation.
    This ioctl is now disabled by default.</p></li>

</ul>

<p>For the oldstable distribution (buster), these problems have been
fixed in version 4.19.249-2.</p>

<p>Due to an issue in the signing service (Cf. Debian bug #1012741), the
vport-vxlan module cannot be loaded for the signed kernel for amd64 in
this update.</p>

<p>This update also corrects a regression in the network scheduler
subsystem (bug #1013299).</p>

<p>For the 32-bit Arm (armel and armhf) architectures, this update
enables optimised implementations of several cryptographic and CRC
algorithms.  For at least AES, this should remove a timing sidechannel 
that could lead to a leak of sensitive information.</p>

<p>This update includes many more bug fixes from stable updates
4.19.236-4.19.249 inclusive, including for bug #1006346.  The random
driver has been backported from Linux 5.19, fixing numerous
performance and correctness issues.  Some changes will be visible:</p>

<ul>

<li>The entropy pool size is now 256 bits instead of 4096.  You may need
  to adjust the configuration of system monitoring or user-space
  entropy gathering services to allow for this.</li>

<li>On systems without a hardware RNG, the kernel may log more uses of
  /dev/urandom before it is fully initialised.  These uses were
  previously under-counted and this is not a regression.</li>

</ul>

<p>We recommend that you upgrade your linux packages.</p>

<p>For the detailed security status of linux please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5173.data"
# $Id: $
