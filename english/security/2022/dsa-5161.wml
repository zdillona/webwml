<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0494">CVE-2022-0494</a>

    <p>The scsi_ioctl() was susceptible to an information leak only
    exploitable by users with CAP_SYS_ADMIN or CAP_SYS_RAWIO
    capabilities.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0854">CVE-2022-0854</a>

    <p>Ali Haider discovered a potential information leak in the DMA
    subsystem. On systems where the swiotlb feature is needed, this
    might allow a local user to read sensitive information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1012">CVE-2022-1012</a>

    <p>The randomisation when calculating port offsets in the IP
    implementation was enhanced.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1729">CVE-2022-1729</a>

    <p>Norbert Slusarek discovered a race condition in the perf subsystem
    which could result in local privilege escalation to root. The
    default settings in Debian prevent exploitation unless more
    permissive settings have been applied in the
    kernel.perf_event_paranoid sysctl.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1786">CVE-2022-1786</a>

    <p>Kyle Zeng discovered a use-after-free in the io_uring subsystem
    which way result in local privilege escalation to root.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1789">CVE-2022-1789</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-1852">CVE-2022-1852</a>

    <p>Yongkang Jia, Gaoning Pan and Qiuhao Li discovered two NULL pointer
    dereferences in KVM's CPU instruction handling, resulting in denial
    of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-32250">CVE-2022-32250</a>

    <p>Aaron Adams discovered a use-after-free in Netfilter which may
    result in local privilege escalation to root.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1974">CVE-2022-1974</a> /
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-1975">CVE-2022-1975</a>

    <p>Duoming Zhou discovered that the NFC netlink interface was
    suspectible to denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2078">CVE-2022-2078</a>

    <p>Ziming Zhang discovered an out-of-bound write in Netfilter which may
    result in local privilege escalation to root.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21499">CVE-2022-21499</a>

    <p>It was discovered that the kernel debugger could be used to bypass
    UEFI Secure Boot restrictions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28893">CVE-2022-28893</a>

    <p>Felix Fu discovered a use-after-free in the implementation of the
    Remote Procedure Call (SunRPC) protocol, which could result in denial of
    service or an information leak.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 5.10.120-1.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>For the detailed security status of linux please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5161.data"
# $Id: $
