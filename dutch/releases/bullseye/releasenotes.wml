#use wml::debian::template title="Debian 11 -- Notities bij de release" BARETITLE=true
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/arches.data"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#use wml::debian::translation-check translation="ce7e85638a047b969574a7abeb8a634c27b9086b"

<if-stable-release release="stretch">
<p><strong>Er wordt nog gewerkt</strong> aan deze versie van de Notities bij de release
voor de nog niet uitgebrachte release Debian 10, codenaam buster. De hier
beschikbare informatie kan onjuist en verouderd zijn en is zeer waarschijnlijk
onvolledig.</p>
</if-stable-release>

<if-stable-release release="buster">
<p><strong>Er wordt nog gewerkt</strong> aan deze versie van de Notities bij de release
voor de nog niet uitgebrachte release Debian 11, codenaam bullseye. De hier
beschikbare informatie kan onjuist en verouderd zijn en is zeer waarschijnlijk
onvolledig.</p>
</if-stable-release>

<p>Om te weten wat nieuw is in Debian 11 kunt u de Notities bij de release
raadplegen voor uw architectuur:</p>

<ul>
<:= &permute_as_list('release-notes/', 'Release Notes'); :>
</ul>

<p>De Notities bij de release bevatten ook instructies voor gebruikers die hun
systeem opwaarderen vanaf eerdere releases.</p>

<p>Als u uw lokalisatie goed heeft ingesteld in uw browser, dan kunt u
bovenstaande link gebruiken om automatisch de juiste HTML-versie te verkrijgen
&mdash; zie <a href="$(HOME)/intro/cn">onderhandelen over webinhoud</a>.
Anders kiest u de juiste architectuur en de gewenste taal en het gewenste
formaat uit onderstaande tabel.</p>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Architectuur</strong></th>
  <th align="left"><strong>Formaat</strong></th>
  <th align="left"><strong>Taal</strong></th>
</tr>
<: &permute_as_matrix_new( file => 'release-notes', langs => \%langsrelnotes,
                           formats => \%formats, arches => \@arches,
                           html_file => 'release-notes/index' ); :>
</table>
</div>
