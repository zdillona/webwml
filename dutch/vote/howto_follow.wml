#use wml::debian::template title="Standaard resolutieprocedure" BARETITLE="true" NOHEADER="true" NOHOMELINK="true"
#use wml::debian::votebar
#use wml::debian::translation-check translation="e74c3fb0292e5eb9c4ffb3b534f6feaac88bf71b"

	<h1>Standaard resolutieprocedure</h1>

	<P>Een voorstel kan door elke ontwikkelaar worden ingediend door een bericht
    te posten op een gekwalificeerde mailinglijst. Een gekwalificeerde
    mailinglijst is elke "voor het publiek leesbare elektronische mailinglijst,
    aangewezen door de gedelegeerde(n) van de projectleider". De mailinglijst
    debian-vote@lists.debian.org is gedelegeerd door de projectsecretaris. Er is
    geen verplichte tekst voor een voorstel, maar hier zijn enkele
    <A HREF="./howto_proposal">richtlijnen</A>.

	<HR>
	<H2>Inleiding</H2>
	<P>Een voorstel wordt als formeel ingediend beschouwd als het werd
    voorgesteld door de projectleider of het technisch comité. Anders heeft het
    voorstel vijf (5) extra sponsors nodig voordat het bespreekbaar is. </P>

	<HR><H2>Discussie</H2>
	<P>Een voorstel blijft ten minste twee (2) weken ter bespreking open staan.
    Daarna kan de oorspronkelijke indiener of een van de vijf sponsors om
    stemming verzoeken. De stemming volgt niet automatisch op de bespreking. De
    projectleider kan toestaan dat de oproep tot stemming tot een week eerder
    wordt gedaan of dat de discussieperiode tot een week extra wordt verlengd.

	<HR><H2>Stemming</H2>
	<P>Na de oproep tot stemming wordt een stembiljet naar de aangewezen
    mailinglijst gestuurd. De ontwikkelaars vullen het stembiljet in en sturen
    het terug naar het in het stembiljet vermelde adres. De stemperiode duurt
    twee weken (maar kan naar goeddunken van de projectleider met een week
    worden verkort of verlengd).</p>

	<P>De Debian stemmachine zal de stem verwerken en een bevestiging
    terugsturen naar de stemmer. Nadat de stemperiode is geëindigd, zal de
    uitslag van de stemming en elke stemmer en zijn/haar stem op de mailinglijst
    worden geplaatst. De volledige tekst van elke stemming zal op de website
    worden geplaatst, met uitzondering van de stemming bij de verkiezing van de
    Debian Projectleider, die als een geheime stemming wordt beschouwd. In dat
    geval neemt de stemmachine maatregelen om de anonimiteit van de stemmer te
    garanderen, terwijl ervoor wordt gezorgd dat elke stemmer nog steeds zijn
    eigen stem kan valideren.</p>

	<HR><H2>Amendementen</H2>
	<P>Een voorstel kan worden gewijzigd indien de indiener van het voorstel en
    alle vijf (5) de sponsors het daarmee eens zijn. Als een van hen bezwaar
    maakt, vormt het amendement het begin van dit proces door eerst zelf vijf
    (5) sponsors rond zich te verzamelen. Op het moment van de stemming worden
    de amendementen opties of vormen van het voorstel waarover moet worden
    gestemd.</p>
