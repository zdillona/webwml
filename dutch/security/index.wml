#use wml::debian::template title="Beveiligingsinformatie" GEN_TIME="yes" MAINPAGE="true"
#use wml::debian::toc
#use wml::debian::recent_list_security
#use wml::debian::translation-check translation="c99908e0effa46ed240da78ee4a1b5ea8e13fcd4"
#include "$(ENGLISHDIR)/releases/info"

# Last Translation Update by $Author$
# Last Translation Update at $Date$

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#keeping-secure">Uw Debian-systeem veilig houden</a></li>
<li><a href="#DSAS">Recente adviezen</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian neemt beveiliging zeer serieus. We behandelen alle beveiligingsproblemen die onder onze aandacht worden gebracht en zorgen ervoor dat deze binnen een redelijke termijn worden opgelost.</p>
</aside>

<p>
De ervaring heeft geleerd dat <q>security through obscurity</q> (veiligheid door ondoorzichtigheid) nooit werkt. Daarom kunnen beveiligingsproblemen sneller en beter worden opgelost door ze openbaar te maken. In dat opzicht behandelt deze pagina de status van Debian met betrekking tot verschillende bekende veiligheidslekken, die mogelijk van invloed kunnen zijn op het Debian-besturingssysteem.
</p>

<p>
Het Debian-project coördineert veel beveiligingsadviezen met andere leveranciers van vrije software, en als gevolg daarvan worden deze adviezen gepubliceerd op dezelfde dag dat een kwetsbaarheid openbaar wordt gemaakt.
</p>

# "reasonable timeframe" might be too vague, but we don't have
# accurate statistics. For older (out of date) information and data
# please read:
# https://www.debian.org/News/2004/20040406  [ Year 2004 data ]
# and (older)
# https://people.debian.org/~jfs/debconf3/security/ [ Year 2003 data ]
# https://lists.debian.org/debian-security/2001/12/msg00257.html [ Year 2001]
# If anyone wants to do up-to-date analysis please contact me (jfs)
# and I will provide scripts, data and database schemas.

<p>
Debian neemt ook deel aan inspanningen om beveiliging te standaardiseren:
</p>

<ul>
  <li>De <a href="#DSAS">Debian beveiligingsberichten</a> zijn <a href="cve-compatibility">CVE-compatibel</a> (bekijk ook de <a href="crossreferences">kruisverwijzingen</a>).</li>
  <li>Debian is vertegenwoordigd in het bestuur van het project <a href="https://oval.cisecurity.org/">"Open Vulnerability Assessment Language"</a>.</li>
</ul>


<h2><a id="keeping-secure">Uw Debian-systeem veilig houden</a></h2>

<p>
Om de recentste beveiligingsberichten van Debian te ontvangen kunt u intekenen op de mailinglijst <a href="https://lists.debian.org/debian-security-announce/">debian-security-announce</a>.
</p>

<p>
Bovendien kunt u <a href="https://packages.debian.org/stable/admin/apt">APT</a> gebruiken om gemakkelijk de nieuwste beveiligingsupdates te krijgen. Om uw Debian-besturingssysteem up-to-date te houden met beveiligingspatches, voegt u de volgende regel toe aan uw bestand <code>/etc/apt/source.list</code>:
</p>

<pre>
deb http://security.debian.org/debian-security <current_release_security_name> main contrib non-free
</pre>

<p>
Voer na het opslaan van de wijzigingen de volgende twee opdrachten uit om de hangende updates te downloaden en te installeren:
</p>

<pre>
apt-get update &amp;&amp; apt-get upgrade
</pre>

<p>
Het beveiligingsarchief wordt ondertekend met de gebruikelijke <a href="https://ftp-master.debian.org/keys.html">signeersleutels voor het Debian-archief</a>.
</p>

<p>
Voor meer informatie over beveiligingsproblemen in Debian, verwijzen wij u naar onze FAQ en onze documentatie:
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="faq">FAQ over beveiliging</a></button> <button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="../doc/user-manuals#securing">Debian beveiligen</a></button></p>


<aside class="light">
  <span class="fa fa-rss fa-5x"></span>
</aside>

<h2><a id="DSAS">Recente adviezen</a></h2>

<p>Deze webpagina's bevatten een beknopt archief van beveiligingsadviezen die werden geplaatst op de mailinglijst <a href="https://lists.debian.org/debian-security-announce/">debian-security-announce</a>.</p>

<p>
<:= get_recent_security_list( '1m', '6', '.', '$(ENGLISHDIR)/security' ) :>
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
title="Debian-beveiligingsadviezen (alleen de titels)" href="dsa">
<link rel="alternate" type="application/rss+xml"
title="Debian-beveiligingsadviezen (samenvattingen)" href="dsa-long">
:#rss#}

<p>De meeste recente Debian-beveiligingsadviezen zijn ook beschikbaar als
<a href="dsa">RDF-bestanden</a>. We bieden ook een enigszins
<a href="dsa-long">langere versie</a> van de bestanden aan, waarin ook de eeste alinea van het beveiligingsadvies opgenomen werd, zodat u makkelijk kunt zien waar het advies over gaat.
</p>

#include "$(ENGLISHDIR)/security/index.include"
<p>Ook oudere beveiligingsadviezen zijn beschikbaar:
<:= get_past_sec_list(); :>

<p>
Debian-distributies zijn niet voor alle beveiligingsproblemen kwetsbaar. Het <a href="https://security-tracker.debian.org/">Debian-beveiligingsvolgsysteem</a> verzamelt alle informatie over de kwetsbaarheidsstatus van Debian-pakketten. Men kan er in zoeken op CVE-naam of op pakketnaam.
</p>
