# translation of webwml/english/po/stats.pot to Dutch
# Templates files for webwml modules
# Copyright (C) 2011-2012 Software in the Public Interest, Inc.
#
# Jeroen Schot <schot@a-eskwadraat.nl>, 2011, 2012.
# Frans Spiesschaert <Frans.Spiesschaert@yucom.be>, 2014, 2017, 2018, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: dutch/po/stats.nl.po\n"
"PO-Revision-Date: 2019-02-24 15:49+0100\n"
"Last-Translator: Frans Spiesschaert <Frans.Spiesschaert@yucom.be>\n"
"Language-Team: Debian Dutch l10n Team <debian-l10n-dutch@lists.debian.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Gtranslator 2.91.7\n"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Debian website vertaalstatistieken"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "Er zijn %d pagina’s te vertalen."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "Er zijn %d bytes te vertalen."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "Er zijn %d strings te vertalen."

#: ../../stattrans.pl:282 ../../stattrans.pl:498
msgid "Wrong translation version"
msgstr "Verkeerde vertalingsversie"

#: ../../stattrans.pl:284
msgid "This translation is too out of date"
msgstr "Deze vertaling is te verouderd"

#: ../../stattrans.pl:286
msgid "The original is newer than this translation"
msgstr "Het origineel is nieuwer dan deze vertaling"

#: ../../stattrans.pl:290 ../../stattrans.pl:498
msgid "The original no longer exists"
msgstr "Het origineel bestaat niet meer"

#: ../../stattrans.pl:474
msgid "hit count N/A"
msgstr "aantal bezoeken n.v.t"

#: ../../stattrans.pl:474
msgid "hits"
msgstr "bezoeken"

#: ../../stattrans.pl:492 ../../stattrans.pl:493
msgid "Click to fetch diffstat data"
msgstr "Klik om de verschil-info op te halen"

#: ../../stattrans.pl:603 ../../stattrans.pl:743
msgid "Created with <transstatslink>"
msgstr "Gemaakt met <transstatslink>"

#: ../../stattrans.pl:608
msgid "Translation summary for"
msgstr "Vertaaloverzicht voor"

#: ../../stattrans.pl:611 ../../stattrans.pl:767 ../../stattrans.pl:813
#: ../../stattrans.pl:856
msgid "Not translated"
msgstr "Niet vertaald"

#: ../../stattrans.pl:611 ../../stattrans.pl:766 ../../stattrans.pl:812
msgid "Outdated"
msgstr "Verouderd"

#: ../../stattrans.pl:611
msgid "Translated"
msgstr "Vertaald"

#: ../../stattrans.pl:611 ../../stattrans.pl:691 ../../stattrans.pl:765
#: ../../stattrans.pl:811 ../../stattrans.pl:854
msgid "Up to date"
msgstr "Bijgewerkt"

#: ../../stattrans.pl:612 ../../stattrans.pl:613 ../../stattrans.pl:614
#: ../../stattrans.pl:615
msgid "files"
msgstr "bestanden"

#: ../../stattrans.pl:618 ../../stattrans.pl:619 ../../stattrans.pl:620
#: ../../stattrans.pl:621
msgid "bytes"
msgstr "bytes"

#: ../../stattrans.pl:628
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"Let op: De lijst van pagina’s is gesorteerd op volgorde van populariteit. "
"Houd de muis over de paginanaam om het aantal bezoeken te zien."

#: ../../stattrans.pl:634
msgid "Outdated translations"
msgstr "Verouderde vertalingen"

#: ../../stattrans.pl:636 ../../stattrans.pl:690
msgid "File"
msgstr "Bestand"

#: ../../stattrans.pl:638
msgid "Diff"
msgstr "Verschill (diff)"

#: ../../stattrans.pl:640
msgid "Comment"
msgstr "Opmerking"

#: ../../stattrans.pl:641
msgid "Diffstat"
msgstr "Verschil-info"

#: ../../stattrans.pl:642
msgid "Git command line"
msgstr "Git commandoregel"

#: ../../stattrans.pl:644
msgid "Log"
msgstr "Logboek"

#: ../../stattrans.pl:645
msgid "Translation"
msgstr "Vertaling"

#: ../../stattrans.pl:646
msgid "Maintainer"
msgstr "Beheerder"

#: ../../stattrans.pl:648
msgid "Status"
msgstr "Status"

#: ../../stattrans.pl:649
msgid "Translator"
msgstr "Vertaler"

#: ../../stattrans.pl:650
msgid "Date"
msgstr "Datum"

#: ../../stattrans.pl:657
msgid "General pages not translated"
msgstr "Algemene pagina’s die niet zijn vertaald"

#: ../../stattrans.pl:658
msgid "Untranslated general pages"
msgstr "Onvertaalde algemene pagina’s"

#: ../../stattrans.pl:663
msgid "News items not translated"
msgstr "Nieuwsberichten die niet zijn vertaald"

#: ../../stattrans.pl:664
msgid "Untranslated news items"
msgstr "Onvertaalde nieuwsberichten"

#: ../../stattrans.pl:669
msgid "Consultant/user pages not translated"
msgstr "Consultant/gebruikerspagina’s die niet zijn vertaald"

#: ../../stattrans.pl:670
msgid "Untranslated consultant/user pages"
msgstr "Onvertaalde consultant/gebruikerspagina’s"

#: ../../stattrans.pl:675
msgid "International pages not translated"
msgstr "Internationale pagina’s die niet zijn vertaald"

#: ../../stattrans.pl:676
msgid "Untranslated international pages"
msgstr "Onvertaalde internationale pagina’s"

#: ../../stattrans.pl:681
msgid "Translated pages (up-to-date)"
msgstr "Vertaalde pagina’s (bijgewerkt)"

#: ../../stattrans.pl:688 ../../stattrans.pl:838
msgid "Translated templates (PO files)"
msgstr "Vertaalde templates (PO-bestanden)"

#: ../../stattrans.pl:689 ../../stattrans.pl:841
msgid "PO Translation Statistics"
msgstr "PO vertaalstatistieken"

#: ../../stattrans.pl:692 ../../stattrans.pl:855
msgid "Fuzzy"
msgstr "Vaag (fuzzy)"

#: ../../stattrans.pl:693
msgid "Untranslated"
msgstr "Onvertaald"

#: ../../stattrans.pl:694
msgid "Total"
msgstr "Totaal"

#: ../../stattrans.pl:711
msgid "Total:"
msgstr "Totaal:"

#: ../../stattrans.pl:745
msgid "Translated web pages"
msgstr "Vertaalde webpagina’s"

#: ../../stattrans.pl:748
msgid "Translation Statistics by Page Count"
msgstr "Vertaalstatistieken op basis van het aantal pagina’s"

#: ../../stattrans.pl:763 ../../stattrans.pl:809 ../../stattrans.pl:853
msgid "Language"
msgstr "Taal"

#: ../../stattrans.pl:764 ../../stattrans.pl:810
msgid "Translations"
msgstr "Vertalingen"

#: ../../stattrans.pl:791
msgid "Translated web pages (by size)"
msgstr "Vertaalde webpagina’s (grootte)"

#: ../../stattrans.pl:794
msgid "Translation Statistics by Page Size"
msgstr "Vertaalstatistieken op basis van paginagrootte"

#~ msgid "Unified diff"
#~ msgstr "Verenigde diff"

#~ msgid "Colored diff"
#~ msgstr "Gekleurde diff"

#~| msgid "Colored diff"
#~ msgid "Commit diff"
#~ msgstr "Commit diff"

#~ msgid "Created with"
#~ msgstr "Gemaakt met"
