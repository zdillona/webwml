#use wml::debian::template title="Debian-consultants" MAINPAGE="true" NOCOMMENTS="yes" GENTIME="yes"
#use wml::debian::translation-check translation="e65bee2d5397d483659bb7eb64a924ecdabd6b39"

# Translator: $Author$
# Last update: $Date$

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian vermeldt consultants als een service aan zijn gebruikers en beveelt geen van de genoemde personen of bedrijven aan. De vermeldingen zijn uitsluitend de verantwoordelijkheid van de betreffende consultants. Raadpleeg de <a href="info">informatiepagina voor consultants</a> voor ons beleid met betrekking tot het toevoegen of bijwerken van een vermelding.</p>
</aside>

<p>Debian is vrije software en biedt gratis hulp aan via verschillende
<a href="../MailingLists/">mailinglijsten</a>. Sommige mensen hebben echter
geen tijd om alles zelf te doen, of hebben bijzondere behoeften en zijn
bereid iemand in te huren om hun systemen te onderhouden of extra
functionaliteit aan hun Debian-systeem toe te voegen. We bieden daarom een lijst aan van mensen, organisaties en bedrijven die ten minste een deel van hun inkomen halen uit het bieden van <strong>betaalde</strong> ondersteuning in verband met Debian.</p>

<p>De lijst is geordend per land; namen binnen elk land worden eenvoudig weergegeven in de volgorde waarin ze zijn ontvangen. Als uw land ontbreekt, controleer dan eventuele buurlanden, aangezien sommige consultants internationaal of op afstand werken.</p>

<p>Er bestaan aanvullende lijsten met consultants voor specifieke toepassingen van Debian:</p>
<ul>
<li><a href="https://wiki.debian.org/DebianEdu/Help/ProfessionalHelp">DebianEdu</a>:
voor het gebruik van Debian in scholen, universiteiten en andere educatieve
instellingen</li>
<li><a href="https://wiki.debian.org/LTS/Funding">Debian LTS</a>:
voor langetermijnbeveiligingsondersteuning voor Debian</li>
</ul>

<hrline>

<h2>Landen met Debian-consultants:</h2>

#include "../../english/consultants/consultant.data"

<hrline>

# left for backwards compatibility - in case someone still links to #policy
# which is fairly unlikely, but hey
<h2><a name="policy"></a>Beleid voor de consultantspagina van Debian</h2>
<p>Zie de <a href="info">informatiepagina over consultants</a> voor informatie
over het toevoegen of bijwerken van een item.</p>
