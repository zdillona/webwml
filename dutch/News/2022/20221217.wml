#use wml::debian::translation-check translation="f878955e279294523a604a4e9bbd3d93dd0f3cc7"
<define-tag pagetitle>Debian 11 is bijgewerkt: 11.6 werd uitgebracht</define-tag>
<define-tag release_date>2022-12-17</define-tag>
#use wml::debian::news

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.6</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Het Debian-project kondigt met genoegen de zesde update aan van zijn
stabiele distributie Debian <release> (codenaam <q><codename></q>).
Deze tussenrelease voegt voornamelijk correcties voor beveiligingsproblemen toe,
samen met een paar aanpassingen voor ernstige problemen. Beveiligingsadviezen
werden reeds afzonderlijk gepubliceerd en, waar beschikbaar, wordt hiernaar
verwezen.</p>

<p>Merk op dat de tussenrelease geen nieuwe versie van Debian <release> is,
maar slechts een update van enkele van de meegeleverde pakketten. Het is niet
nodig om oude media met <q><codename></q> weg te gooien. Na de installatie
kunnen pakketten worden opgewaardeerd naar de huidige versie door een
bijgewerkte Debian-spiegelserver te gebruiken.</p>

<p>Wie regelmatig updates installeert vanuit security.debian.org zal niet veel
pakketten moeten updaten, en de meeste van dergelijke updates zijn opgenomen in
de tussenrelease.</p>

<p>Nieuwe installatie-images zullen binnenkort beschikbaar zijn op de gewone
plaatsen.</p>

<p>Het opwaarderen van een bestaande installatie naar deze revisie kan worden
bereikt door het pakketbeheersysteem naar een van de vele HTTP-spiegelservers
van Debian te verwijzen. Een uitgebreide lijst van spiegelservers is
beschikbaar op:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Oplossingen voor diverse problemen</h2>

<p>Deze update van stable, de stabiele release, voegt een paar belangrijke
correcties toe aan de volgende pakketten:</p>

<table border=0>
<tr><th>Pakket</th>               <th>Reden</th></tr>
<correction awstats "Probleem van cross-site-scripting oplossen [CVE-2022-46391]">
<correction base-files "Updaten van /etc/debian_version voor tussenrelease 11.6">
<correction binfmt-support "binfmt-support.service uitvoeren na systemd-binfmt.service">
<correction clickhouse "Oplossen van problemen van lezen buiten de grenzen [CVE-2021-42387 CVE-2021-42388], bufferoverloopproblemen [CVE-2021-43304 CVE-2021-43305]">
<correction containerd "CRI plugin: Oplossen van goroutine-lek tijdens Exec [CVE-2022-23471]">
<correction core-async-clojure "Bouwfouten in de testsuite oplossen">
<correction dcfldd "SHA1-uitvoer op big-endian architecturen repareren">
<correction debian-installer "Opnieuw bouwen tegen proposed-updates; Linux kernel ABI verhogen naar 5.10.0-20">
<correction debian-installer-netboot-images "Opnieuw bouwen tegen proposed-updates">
<correction debmirror "non-free-firmware toevoegen aan de standaardsectielijst">
<correction distro-info-data "Toevoegen van Ubuntu 23.04, Lunar Lobster; bijwerken van de Debian ELTS einddata; de releasedatum van Debian 8 (jessie) corrigeren">
<correction dojo "Probleem met prototypepollutie oplossen [CVE-2021-23450]">
<correction dovecot-fts-xapian "Genereren van tijdens het bouwen in gebruik zijnde dovecot ABI versie als vereiste">
<correction efitools "Reparatie voor sporadisch voorkomende bouwproblemen door foutieve vereiste in makefile">
<correction evolution "Adresboeken van Google Contacts overzetten naar CalDAV omdat de Google Contacts API uitgezet werd">
<correction evolution-data-server "Adresboeken van Google Contacts overzetten naar CalDAV omdat de Google Contacts API uitgezet werd; herstellen van compatibiliteit met wijzigingen in Gmail OAuth">
<correction evolution-ews "Herstellen van het ophalen van gebruikerscertificaten die bij contacten horen">
<correction g810-led "Toegang tot apparaten beheren met uaccess in plaats van alles wereldwijd schrijfbaar te maken [CVE-2022-46338]">
<correction glibc "Oplossing voor regressie in wmemchr en wcslen op CPU's met AVX2 maar niet met BMI2 (bijv. Intel Haswell)">
<correction golang-github-go-chef-chef "Reparatie voor sporadische testfouten">
<correction grub-efi-amd64-signed "Xen binaire bestanden niet strippen, zodat ze opnieuw werken; lettertypes toevoegen in de bouw van memdisk voor EFI-images; reparatie van bug in code van core-bestand zodat fouten beter afgehandeld worden; Debian SBAT-niveau verhogen naar 4">
<correction grub-efi-arm64-signed "Xen binaire bestanden niet strippen, zodat ze opnieuw werken; lettertypes toevoegen in de bouw van memdisk voor EFI-images; reparatie van bug in code van core-bestand zodat fouten beter afgehandeld worden; Debian SBAT-niveau verhogen naar 4">
<correction grub-efi-ia32-signed "Xen binaire bestanden niet strippen, zodat ze opnieuw werken; lettertypes toevoegen in de bouw van memdisk voor EFI-images; reparatie van bug in code van core-bestand zodat fouten beter afgehandeld worden; Debian SBAT-niveau verhogen naar 4">
<correction grub2 "Xen binaire bestanden niet strippen, zodat ze opnieuw werken; lettertypes toevoegen in de bouw van memdisk voor EFI-images; reparatie van bug in code van core-bestand zodat fouten beter afgehandeld worden; Debian SBAT-niveau verhogen naar 4">
<correction hydrapaper "Ontbrekende vereiste van python3-pil toevoegen">
<correction isoquery "Oplossen van testfout door een wijziging van de Franse vertaling in het pakket iso-codes">
<correction jtreg6 "Nieuw pakket, vereist voor het bouwen van recentere openjdk-11 versies">
<correction lemonldap-ng "Sessievernietigingsdoorgifte verbeteren [CVE-2022-37186]">
<correction leptonlib "Reparatie voor deling door nul [CVE-2022-38266]">
<correction libapache2-mod-auth-mellon "Probleem met open omleiding oplossen [CVE-2021-3639]">
<correction libbluray "Herstellen van BD-J-ondersteuning met de recente updates van Oracle Java">
<correction libconfuse "Reparatie van een heap-gebaseerde buffer over-lezing in cfg_tilde_expand [CVE-2022-40320]">
<correction libdatetime-timezone-perl "Updaten van ingesloten data">
<correction libtasn1-6 "Oplossen van probleem van lezen buiten de grenzen [CVE-2021-46848]">
<correction libvncserver "Oplossing voor geheugenlek [CVE-2020-29260]; ondersteunen van grotere schermformaten">
<correction linux "Nieuwe bovenstroomse stabiele release; ABI verhogen naar 20; [rt] Updaten naar 5.10.158-rt77">
<correction linux-signed-amd64 "Nieuwe bovenstroomse stabiele release; ABI verhogen naar 20; [rt] Updaten naar 5.10.158-rt77">
<correction linux-signed-arm64 "Nieuwe bovenstroomse stabiele release; ABI verhogen naar 20; [rt] Updaten naar 5.10.158-rt77">
<correction linux-signed-i386 "Nieuwe bovenstroomse stabiele release; ABI verhogen naar 20; [rt] Updaten naar 5.10.158-rt77">
<correction mariadb-10.5 "Nieuwe bovenstroomse stabiele release; beveiligingsoplossingen [CVE-2018-25032 CVE-2021-46669 CVE-2022-27376 CVE-2022-27377 CVE-2022-27378 CVE-2022-27379 CVE-2022-27380 CVE-2022-27381 CVE-2022-27382 CVE-2022-27383 CVE-2022-27384 CVE-2022-27386 CVE-2022-27387 CVE-2022-27444 CVE-2022-27445 CVE-2022-27446 CVE-2022-27447 CVE-2022-27448 CVE-2022-27449 CVE-2022-27451 CVE-2022-27452 CVE-2022-27455 CVE-2022-27456 CVE-2022-27457 CVE-2022-27458 CVE-2022-32081 CVE-2022-32082 CVE-2022-32083 CVE-2022-32084 CVE-2022-32085 CVE-2022-32086 CVE-2022-32087 CVE-2022-32088 CVE-2022-32089 CVE-2022-32091]">
<correction mod-wsgi "X-Client-IP header weglaten wanneer deze niet vertrouwd is [CVE-2022-2255]">
<correction mplayer "Oplossen van verschillende veiligheidsproblemen [CVE-2022-38850 CVE-2022-38851 CVE-2022-38855 CVE-2022-38858 CVE-2022-38860 CVE-2022-38861 CVE-2022-38863 CVE-2022-38864 CVE-2022-38865 CVE-2022-38866]">
<correction mutt "Crash van gpgme repareren bij het weergeven van sleutels in een blok met openbare sleutels, en weergave van blokken met openbare sleutels voor oude versies van gpgme">
<correction nano "Oplossing voor crashes en een probleem van mogelijk dataverlies">
<correction nftables "Een fout van off-by-one / dubbele vrijgave repareren">
<correction node-hawk "URL's ontleden met stdlib [CVE-2022-29167]">
<correction node-loader-utils "Oplossen van probleem met prototypepollutie [CVE-2022-37599 CVE-2022-37601], probleem van op reguliere expressie gebaseerde denial of service [CVE-2022-37603]">
<correction node-minimatch "Verbeteren van bescherming tegen op reguliere expressie gebaseerde denial of service [CVE-2022-3517]; reparatie van regressie in patch voor CVE-2022-3517">
<correction node-qs "Probleem met prototypepollutie oplossen [CVE-2022-24999]">
<correction node-xmldom "Probleem met prototypepollutie oplossen [CVE-2022-37616]; voorkomen van invoeging van niet-goed-gevormde nodes [CVE-2022-39353]">
<correction nvidia-graphics-drivers "Nieuwe bovenstroomse release; beveiligingsoplossingen [CVE-2022-34670 CVE-2022-34674 CVE-2022-34675 CVE-2022-34677 CVE-2022-34679 CVE-2022-34680 CVE-2022-34682 CVE-2022-42254 CVE-2022-42255 CVE-2022-42256 CVE-2022-42257 CVE-2022-42258 CVE-2022-42259 CVE-2022-42260 CVE-2022-42261 CVE-2022-42262 CVE-2022-42263 CVE-2022-42264]">
<correction nvidia-graphics-drivers-legacy-390xx "Nieuwe bovenstroomse release; beveiligingsoplossingen [CVE-2022-34670 CVE-2022-34674 CVE-2022-34675 CVE-2022-34677 CVE-2022-34680 CVE-2022-42257 CVE-2022-42258 CVE-2022-42259]">
<correction nvidia-graphics-drivers-tesla-450 "Nieuwe bovenstroomse release; beveiligingsoplossingen [CVE-2022-34670 CVE-2022-34674 CVE-2022-34675 CVE-2022-34677 CVE-2022-34679 CVE-2022-34680 CVE-2022-34682 CVE-2022-42254 CVE-2022-42256 CVE-2022-42257 CVE-2022-42258 CVE-2022-42259 CVE-2022-42260 CVE-2022-42261 CVE-2022-42262 CVE-2022-42263 CVE-2022-42264]">
<correction nvidia-graphics-drivers-tesla-470 "Nieuwe bovenstroomse release; beveiligingsoplossingen [CVE-2022-34670 CVE-2022-34674 CVE-2022-34675 CVE-2022-34677 CVE-2022-34679 CVE-2022-34680 CVE-2022-34682 CVE-2022-42254 CVE-2022-42255 CVE-2022-42256 CVE-2022-42257 CVE-2022-42258 CVE-2022-42259 CVE-2022-42260 CVE-2022-42261 CVE-2022-42262 CVE-2022-42263 CVE-2022-42264]">
<correction omnievents "Toevoegen van ontbrekende vereiste van libjs-jquery aan het pakket omnievents-doc">
<correction onionshare "Oplossen van probleem van denial of service [CVE-2022-21689], HTML-injectie [CVE-2022-21690]">
<correction openvpn-auth-radius "Ondersteuning voor opdracht verify-client-cert">
<correction postfix "Nieuwe bovenstroomse stabiele release">
<correction postgresql-13 "Nieuwe bovenstroomse stabiele release">
<correction powerline-gitstatus "Oplossing voor commando-injectie via kwaadaardige opslagplaatsconfiguratie [CVE-2022-42906]">
<correction pysubnettree "Repareren van modulebouw">
<correction speech-dispatcher "Verkleinen van de espeak-buffer om synth-artefacten te voorkomen">
<correction spf-engine "Oplossing voor het niet starten van pyspf-milter vanwege een ongeldige import-opdracht">
<correction tinyexr "Oplossing voor problemen van heap-overloop [CVE-2022-34300 CVE-2022-38529]">
<correction tinyxml "Reparatie van oneindige lus [CVE-2021-42260]">
<correction tzdata "Updaten van gegevens voor Fiji, Mexico en Palestina; lijst met schrikkelseconden bijwerken">
<correction virglrenderer "Oplossen van probleem van schrijven buiten de grenzen [CVE-2022-0135]">
<correction x2gothinclient "Ervoor zorgen dat het pakket x2gothinclient-minidesktop voorziet in het virtueel pakket lightdm-greeter">
<correction xfig "Probleem van bufferoverloop oplossen [CVE-2021-40241]">
</table>


<h2>Beveiligingsupdates</h2>


<p>Deze revisie voegt de volgende beveiligingsupdates toe aan de stabiele
release. Het beveiligingsteam heeft voor elk van deze updates al een advies
uitgebracht:</p>

<table border=0>
<tr><th>Advies-ID</th>  <th>Pakket</th></tr>
<dsa 2022 5212 chromium>
<dsa 2022 5223 chromium>
<dsa 2022 5224 poppler>
<dsa 2022 5225 chromium>
<dsa 2022 5226 pcs>
<dsa 2022 5227 libgoogle-gson-java>
<dsa 2022 5228 gdk-pixbuf>
<dsa 2022 5229 freecad>
<dsa 2022 5230 chromium>
<dsa 2022 5231 connman>
<dsa 2022 5232 tinygltf>
<dsa 2022 5233 e17>
<dsa 2022 5234 fish>
<dsa 2022 5235 bind9>
<dsa 2022 5236 expat>
<dsa 2022 5239 gdal>
<dsa 2022 5240 webkit2gtk>
<dsa 2022 5241 wpewebkit>
<dsa 2022 5242 maven-shared-utils>
<dsa 2022 5243 lighttpd>
<dsa 2022 5244 chromium>
<dsa 2022 5245 chromium>
<dsa 2022 5246 mediawiki>
<dsa 2022 5247 barbican>
<dsa 2022 5248 php-twig>
<dsa 2022 5249 strongswan>
<dsa 2022 5250 dbus>
<dsa 2022 5251 isc-dhcp>
<dsa 2022 5252 libreoffice>
<dsa 2022 5253 chromium>
<dsa 2022 5254 python-django>
<dsa 2022 5255 libksba>
<dsa 2022 5256 bcel>
<dsa 2022 5257 linux-signed-arm64>
<dsa 2022 5257 linux-signed-amd64>
<dsa 2022 5257 linux-signed-i386>
<dsa 2022 5257 linux>
<dsa 2022 5258 squid>
<dsa 2022 5260 lava>
<dsa 2022 5261 chromium>
<dsa 2022 5263 chromium>
<dsa 2022 5264 batik>
<dsa 2022 5265 tomcat9>
<dsa 2022 5266 expat>
<dsa 2022 5267 pysha3>
<dsa 2022 5268 ffmpeg>
<dsa 2022 5269 pypy3>
<dsa 2022 5270 ntfs-3g>
<dsa 2022 5271 libxml2>
<dsa 2022 5272 xen>
<dsa 2022 5273 webkit2gtk>
<dsa 2022 5274 wpewebkit>
<dsa 2022 5275 chromium>
<dsa 2022 5276 pixman>
<dsa 2022 5277 php7.4>
<dsa 2022 5278 xorg-server>
<dsa 2022 5279 wordpress>
<dsa 2022 5280 grub-efi-amd64-signed>
<dsa 2022 5280 grub-efi-arm64-signed>
<dsa 2022 5280 grub-efi-ia32-signed>
<dsa 2022 5280 grub2>
<dsa 2022 5281 nginx>
<dsa 2022 5283 jackson-databind>
<dsa 2022 5285 asterisk>
<dsa 2022 5286 krb5>
<dsa 2022 5287 heimdal>
<dsa 2022 5288 graphicsmagick>
<dsa 2022 5289 chromium>
<dsa 2022 5290 commons-configuration2>
<dsa 2022 5291 mujs>
<dsa 2022 5292 snapd>
<dsa 2022 5293 chromium>
<dsa 2022 5294 jhead>
<dsa 2022 5295 chromium>
<dsa 2022 5296 xfce4-settings>
<dsa 2022 5297 vlc>
<dsa 2022 5298 cacti>
<dsa 2022 5299 openexr>
</table>



<h2>Het Debian-installatiesysteem</h2>
<p>Het installatiesysteem werd bijgewerkt om de reparaties die met deze tussenrelease in de stabiele release opgenomen werden, toe te voegen.</p>

<h2>URL's</h2>

<p>De volledige lijsten met pakketten die met deze revisie gewijzigd werden:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>De huidige stabiele distributie:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Voorgestelde updates voor de stabiele distributie:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Informatie over de stabiele distributie (notities bij de release, errata, enz.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Beveiligingsaankondigingen en -informatie:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Over Debian</h2>

<p>Het Debian-project is een vereniging van ontwikkelaars van vrije software
die vrijwillig tijd en moeite steken in het produceren van het volledig vrije
besturingssysteem Debian.</p>

<h2>Contactinformatie</h2>

<p>Ga voor verdere informatie naar de webpagina's van Debian op
<a href="$(HOME)/">https://www.debian.org/</a>, stuur een e-mail naar
&lt;press@debian.org&gt;, of neem contact met het release-team voor de stabiele
release op &lt;debian-release@lists.debian.org&gt;.</p>

