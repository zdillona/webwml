msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2016-01-14 09:14+0100\n"
"Last-Translator: Hans Fredrik Nordhaug <hans@nordhaug.priv.no>\n"
"Language-Team: Norwegian Bokmål <i18n-nb@lister.ping.uio.no>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/CD/vendors/vendors.CD.def:14
msgid "Vendor"
msgstr "Leverandør"

#: ../../english/CD/vendors/vendors.CD.def:15
msgid "Allows Contributions"
msgstr "Tillater bidrag"

#: ../../english/CD/vendors/vendors.CD.def:16
msgid "CD/DVD/BD/USB"
msgstr "CD/DVD/BD/USB"

#: ../../english/CD/vendors/vendors.CD.def:17
msgid "Architectures"
msgstr "Arkitekturer"

#: ../../english/CD/vendors/vendors.CD.def:18
msgid "Ship International"
msgstr "Sender til utlandet"

#: ../../english/CD/vendors/vendors.CD.def:19
msgid "Contact"
msgstr "Kontakt"

#. ###################
#. Vendor home page link, Debian page link
#: ../../english/CD/vendors/vendors.CD.def:44
msgid "Vendor Home"
msgstr "Leverandørens hjemmeside"

#: ../../english/CD/vendors/vendors.CD.def:70
msgid "page"
msgstr "side"

#: ../../english/CD/vendors/vendors.CD.def:71
msgid "email"
msgstr "e-post"

#: ../../english/CD/vendors/vendors.CD.def:114
msgid "within Europe"
msgstr "til Europa"

#: ../../english/CD/vendors/vendors.CD.def:118
msgid "To some areas"
msgstr "Til visse områder"

#: ../../english/CD/vendors/vendors.CD.def:122
msgid "source"
msgstr "kildekode"

#: ../../english/CD/vendors/vendors.CD.def:126
msgid "and"
msgstr "og"

#~ msgid "updated monthly"
#~ msgstr "oppdateres månedlig"

#~ msgid "updated twice weekly"
#~ msgstr "oppdateres to ganger i uken"

#~ msgid "updated weekly"
#~ msgstr "oppdateres ukentlig"

#~ msgid "reseller"
#~ msgstr "videreformidler"

#~ msgid "reseller of $var"
#~ msgstr "videreformidler av $var"

#~ msgid "Custom Release"
#~ msgstr "Bestillingsverk"

#~ msgid "vendor additions"
#~ msgstr "tillegg fra leverandør"

#~ msgid "contrib included"
#~ msgstr "contrib inkludert"

#~ msgid "non-free included"
#~ msgstr "non-free inkludert"

#~ msgid "non-US included"
#~ msgstr "non-US inkludert"

#~ msgid "Multiple Distribution"
#~ msgstr "Fler-distribusjon"

#~ msgid "Vendor Release"
#~ msgstr "Leverandørutgave"

#~ msgid "Development Snapshot"
#~ msgstr "Utviklingsversjon"

#, fuzzy
#~ msgid "Official DVD"
#~ msgstr "Offisiell CD"

#~ msgid "Official CD"
#~ msgstr "Offisiell CD"

#~ msgid "Architectures:"
#~ msgstr "Arkitekturer:"

#~ msgid "DVD Type:"
#~ msgstr "DVD-type:"

#~ msgid "CD Type:"
#~ msgstr "CD-type:"

#~ msgid "email:"
#~ msgstr "epost:"

#~ msgid "Ship International:"
#~ msgstr "Sender til utlandet:"

#~ msgid "Country:"
#~ msgstr "Land:"

#~ msgid "Allows Contribution to Debian:"
#~ msgstr "Tar imot bidrag til Debian:"

#~ msgid "URL for Debian Page:"
#~ msgstr "URL for Debiansiden:"

#~ msgid "Vendor:"
#~ msgstr "Leverandør:"
