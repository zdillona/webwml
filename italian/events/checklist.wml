#use wml::debian::template title="Checklist per uno stand"
#use wml::debian::translation-check translation="96da98ca0d7c98bdddb5d60c39c8e06987402075" maintainer="Daniele Cortesi"

<p>Questa lista è stata creata per aiutare le persone ad
organizzare uno stand per il Progetto Debian ad un qualsiasi evento.
Inviate i commenti a <a
href="mailto:events@debian.org">events@debian.org</a>.
</p>

<h3>Organizzare uno stand</h3>

<p>Questa è una lista ordinata. Alcuni punti, comunque, possono
essere eseguiti in parallelo e altri possono essere svolti in ordine
differente.
</p>

<ol>

<li>Chi è a conoscenza di un evento al quale potrebbe partecipare Debian,
può inviare un messaggio a <a href="eventsmailinglists">debian-events-&lt;regione&gt;</a>; questo vale anche
se non si intendei gestire un stand.
</li>

<li>La presenza di uno stand ha senso solamente se ci sono due o più
persone che condividono l'impegno. È possibile chiedere aiuto su
<a href="eventsmailinglists">debian-events-&lt;regione&gt;</a> se non si dispone di assistenza sufficiente.
Se nessun altro vuole aiutare a creare lo stand si valuti attentamente se è
il caso di farlo.
</li>

<li>Adesso che ci sono persone disponibili per aiutare allo stand,
assicurarsi che sia disponibile nello stand almeno una macchina per
mostrare Debian oppure, per questo è disponibile la scatole per eventi
Debian. Chiedere a events@debian.org. Se non ci sono macchini disponibili,
si valuti attentamente se creare lo stand dato che senza macchine sarebbe
piuttosto vuoto.
</li>

<li> Adesso che sono disponibili sia le persone che le macchine
     è ora di contattare gli organizzatori e richiedere uno
	 stand a disposizione per il progetto.
</li>

<li>Si è dimostrato utile in molti eventi coordinare l'evento nel Wiki
Debian tramite l'uso del modulo nella <a
href="https://wiki.debian.org/DebianEvents#Adding_a_new_event">pagina
Eventi Debian</a>.
</li>

<li> Assicuratevi che lo stand sia completo di mobilio.
</li>

<li> Assicuratevi che lo stand abbia una presa di corrente disponibile
e connettività IP.
</li>

<li> Verificate se ci saranno delle pareti a cui poter appendere dei
     poster.
</li>

<li> Se si possono usare dei poster, verificate se siete in grado di
     stampare o farvi spedire qualche grande poster (controllate la
	 pagina <a href="material">materiali per uno stand</a>). Debian
	 può anche rimborsare i costi della stampa di grandi manifesti.
</li>

<li> Verificate se siete in grado di pensare, creare e stampare dei
     volantini così che i visitatori possano prendere qualcosa.
	 (controllate la pagina <a href="material">materiali per uno
	 stand</a>).
</li>

#<li> Se è una grande mostra, controllate se qualcuno vuole
#     produrre dei CD di Debian per i visitatori. Verificate se
#	 riuscite a trovare degli sponsor per pagarne la produzione
#	 così che i visitatori li possano prendere senza pagare.
#	 Assicuratevi che distribuire solamente CD di binari <a
#    href="$(HOME)/CD/vendors/legal">soddisfi</a> la licenza del
#	 software distribuito.
#</li>

<li> Se allo stand sarà presente qualcuno in grado di tenere un
     discorso che descriva Debian in generale o un singolo punto del
	 progetto, chiedete di verificare con gli organizzatori se
	 è possibile accettare il discorso. In ogni caso si
	 pu&ograve; organizzare un incontro presso lo stand, assicuratevi
	 di mostrare l'avviso in anticipo (il primo giorno) di modo che la
	 gente ne sia a conoscenza.
</li>

<li> Anche se non è stato organizzato nessun discorso formale
     è sempre possibile chiedere agli organizzatori qualche
	 stanza per organizzare un incontro fra gente con gli stessi
	 interessi (BOF [dall'inglese ``Birds of a Feather'', ``gente
	 dello stesso tipo'' n.d.t.]). Un BOF è un meeting
	 informale tra sviluppatori e utenti senza una traccia precisa in
	 cui tutti possono fare qualsiasi domanda su un argomento di
	 interesse. Sono abbastanza utili in quanto gli sviluppatori
	 hanno la possibilità di sapere cosa pensano le persone e
	 le persone di avere una risposta alle loro domande.
</li>

<li> Accordatevi con gli organizzatori per quanti tavoli e sedie sono
     richiesti allo stand.
</li>

<li> Assicuratevi che ci siano abbastanza prese multiple nello stand
     perché le macchine siano davvero in grado di essere
	 accese.
</li>

<li> Assicuratevi che ci siano abbastanza cavi di rete e hub/switch
     nello stand per connettere i computer.
</li>

<li> I tavoli spogli non sono belli da vedere, sarebbe meglio
     utilizzare delle tovaglie bianche così che l'intero stand
	 risulti più professionale.
</li>

<li> Se volete vendere qualsiasi cosa (T-shirt, CD, spille, cappelli,
     ecc.), dovete contattare gli organizzatori per scoprire se
	 è permesso in generale. Ci sono degli eventi in cui
	 vendere materiale agli stand non è concesso.
</li>

<li> Se gli organizzatori sono d'accordo nel vendere materiale allo
     stand, dovete controllare che non ci siano leggi di stato o
	 locali che lo proibiscano (specialmente per gli eventi che si
	 tengono di domenica). Gli organizzatori potrebbero essere in
	 grado di aiutare.
</li>

<li> Se vendete del materiale, non dovreste enfatizzare sul fatto di
     fare soldi allo stand.  Potrebbe sembrare strano richiedere 
	 uno stand gratis e quindi fare <q>tanti</q> soldi allo stand.
</li>

<li> In qualche mostra un incontro sociale viene organizzato per gli
     speaker e gli esibitori. Controllate se ne verr&agrave;
	 organizzato uno e come parteciparvi se siete interessati. Come
	 regola di base: un evento sociale di solito è abbastanza
	 carino dato che si incontrano persone provenienti da altri
	 progetti e compagnie e ci si pu&ograve; rilassare fuori da una
	 manifestazione stressante.
</li>

<li> Assicuratevi di portare con voi abbastanza fingerprint della
     vostra chiave GnuPG così che gli altri la possano <a 
	 href="keysigning">firmare</a>.  Non dimenticate inoltre la carta
	 d'identità o la patente di guida per dimostrare la propria
	 identità.
</li>

<li> Se riuscite a recuperare soldi per fare più cose allo
     stand, potete leggere qualche buona idea sul
     <a href="material">merchandising agli stand</a>.
</li>

<li> Quando le persone e le macchine sono disponibili, assicuratevi
     che tutto lo staff possa operare sulle macchine.  Questo richiede
	 un account dimostrativo con una password conosciuta da tutto lo
	 staff dello stand. Questo account dovrebbe essere in grado di
	 eseguire <code>sudo apt-get</code> se le macchine sono connesse
	 in rete e hanno accesso a un mirror Debian.
</li>

<li> It would be nice if one of the people who are staffing the booth
     would write up a short report after the show so <a
     href="$(HOME)/News/weekly/">Debian Weekly News</a> could report about
     the show afterwards.

<li>Sarebbe carino se le persone presenti allo stand
scrivessere un breve resoconto dopo la manifestazione così
da poterlo pubblicare nelle <a href="$(HOME)/News/weekly/">Debian
Weekly News</a>.
</li>

</ol>

<p>Email riguardanti l'organizzazione dello stand e dei discorsi
dovrebbero essere inviate a una delle <a href="booth#ml">liste di 
messaggi</a> così che siano disponibili pubblicamente, archiviate e
possano attrarre più persone interessate a partecipare.
</p>

<p>Se non volete mandare queste email a una lista pubblica potete
sempre inviarle a events@debian.org per ricevere
qualche consiglio utile. In ogni caso noi non contatteremo gli
organizzatori dell'evento facendovi evitare lo sforzo.</p>

<h3>Creare lo stand</h3>

<ul>

<li> Se volete creare uno stand professionale, è sempre meglio
     sapere dove mettere giacche, scarpe superflue, bevande e altro
	 materiale.  Chiedete agli organizzatori dell'evento se ci
	 sarà una piccola stanza disponibile per queste cose.
</li>

<li> Verificate con gli organizzatori dell'evento come potete
     raggiungere lo stand durante la manifestazione prima che sia
	 aperta al pubblico. Certe fiere richiedono degli speciali
	 pass-esibitore.
</li>

<li> Alla manifestazione il numero di sviluppatori per visitatore non
     dovrebbe essere superiore a due (2.0). è carino avere
	 uno stand pieno di persone, ma non è molto invitante se lo
	 stand è già pieno di persone dello staff e i
	 visitatori non riescono ad entrarvi.
</li>

<li> Non dimenticate di fare una foto di gruppo a tutte le persone
     dello staff Debian che danno una mano.  Questo si dimentica molto
	 spesso ma aiuta tantissimo ad associare immagini e nomi e inoltre
	 rende Debian meno anonima.
</li>

<li> A fine giornata o a fine manifestazione, per favore non lasciate
     lo stand pieno di spazzatura come bottiglie vuote o altro.
</li>

<li>Non è carino quando le persone alla stand stanno sedute dietro il
tavolo guardando i propri notebook anziché prestare attenzione ai
visitatori.
</li>

</ul>

