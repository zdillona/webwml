#use wml::debian::translation-check translation="549ed0fce26a03a0a08c5aca5a6c51e4199c3e20" maintainer="galaxico"
<define-tag pagetitle>Το διαδικτυακό DebConf20 κλείνει</define-tag>

<define-tag release_date>2020-08-30</define-tag>
#use wml::debian::news

<p>
Το Σάββατο 29 Αυγούστου του 2020, το ετήσιο Συνέδριο των Προγραμματιστ(ρι)ών 
και Συνεισφερόντων του Debian (Debian Developers and Contributors Conference) 
έφτασε στο τέλος του.
</p>

<p>
Το DebConf20 διοργανώθηκε για πρώτη φορά διαδικτυακά εξαιτίας της πανδημίας του 
κορωνοϊού (COVID-19). 
</p>

<p>
All of the sessions have been streamed, with a variety of ways of participating:
via IRC messaging, online collaborative text documents,
and video conferencing meeting rooms.
</p>

<p>
With more than 850 attendees from 80 different countries and a
total of over 100 event talks, discussion sessions,
Birds of a Feather (BoF) gatherings and other activities,
<a href="https://debconf20.debconf.org">DebConf20</a> was a large success.
</p>

<p>
Όταν έγινε φανερό ότι το DebConf20 επρόκειτο να είναι ένα αποκλειστικά 
διαδικτυακό γεγονός, η ομάδα Βίντεο του DebConf ξόδεψε πολύ χρόνο τους 
επόμενους μήνες για να προσαρμόσει, και σε μερικές περιπτώσεις να βελτιώσει από 
το μηδέν την τεχνολογία που απαιτούνταν για να γίνει ένα διαδικτυακό DebConf 
εφικτό. Με αυτά που μάθαμε από το MiniDebConfOnline στα τέλη Μαΐου, έγιναν 
μερικές προσαρμογές και στη συνέχεια καταλήξαμε σε μια λύση που περιελάμβανε 
τα Jitsi, OBS, Voctomix, SReview, nginx, Etherpad, και ένα πρόσφατα γραμμένο 
web-based frontend για το voctomix ως τα διάφορα στοιχεία του πακέτου εφαρμογών.
</p>

<p>
Όλες οι συνιστώσες της υποδομής Βίντεο είναι ελεύθερο λογισμικό και ολόκληρο 
το setup ρυθμίζεται μέσω του δημόσιου 
<a href="https://salsa.debian.org/debconf-video-team/ansible">ansible</a> 
αποθετηρίου τους.
</p>

<p>
Το <a href="https://debconf20.debconf.org/schedule/">πρόγραμμα</a> του 
DebConf20 περιελάμβανε δύο track σε γλώσσες διαφορετικά από τα Αγγλικά: 
το MiniConf στα ισπανικά, με οχτώ ομιλίες σε δυο μέρες, και το MiniConf στα 
Μαλαισιανά με εννέα ομιλίες σε τρεις μέρες. Πραγματοποιήθηκαν επίσης διάφορες 
ad-hoc δραστηριότητες, που προτάθηκαν από τους συμμετέχοντες στη διάρκεια 
ολόκληρου του συνεδρίου, και οι οποίες μεταδόθηκαν διαδικτυακά ζωντανά και 
βιντεοσκοπήθηκαν. Υπήρξαν επίσης αρκετές συγκεντρώσεις ομάδων στο <a 
href="https://wiki.debian.org/Sprints/">sprint</a> σε συυγκεκριμένες περιοχές 
ανάπτυξης του Debian.
</p>

<p>
Between talks, the video stream has been showing the usual sponsors on the loop, but also
some additional clips including photos from previous DebConfs, fun facts about Debian
and short shout-out videos sent by attendees to communicate with their Debian friends. 
</p>

<p>
For those who were not able to participate, most of the talks and sessions are already
available through the
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/">Debian meetings archive website</a>,
and the remaining ones will appear in the following days.
</p>

<p>
The <a href="https://debconf20.debconf.org/">DebConf20</a> website
will remain active for archival purposes and will continue to offer
links to the presentations and videos of talks and events.
</p>

<p>
Next year, <a href="https://wiki.debian.org/DebConf/21">DebConf21</a> is planned to be held
in Haifa, Israel, in August or September.
</p>

<p>
DebConf is committed to a safe and welcome environment for all participants.
During the conference, several teams (Front Desk, Welcome team and Community team)
have been available to help so participants get their best experience
in the conference, and find solutions to any issue that may arise.
See the <a href="https://debconf20.debconf.org/about/coc/">web page about the Code of Conduct in DebConf20 website</a>
for more details on this.
</p>

<p>
Debian thanks the commitment of numerous <a href="https://debconf20.debconf.org/sponsors/">sponsors</a>
to support DebConf20, particularly our Platinum Sponsors:
<a href="https://www.lenovo.com">Lenovo</a>,
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://google.com/">Google</a>
and 
<a href="https://aws.amazon.com/">Amazon Web Services (AWS)</a>.
</p>

<h2>About Debian</h2>
<p>
The Debian Project was founded in 1993 by Ian Murdock to be a truly
free community project. Since then the project has grown to be one of
the largest and most influential open source projects.  Thousands of
volunteers from all over the world work together to create and
maintain Debian software. Available in 70 languages, and
supporting a huge range of computer types, Debian calls itself the
<q>universal operating system</q>.
</p>

<h2>About DebConf</h2>

<p>
DebConf is the Debian Project's developer conference. In addition to a
full schedule of technical, social and policy talks, DebConf provides an
opportunity for developers, contributors and other interested people to
meet in person and work together more closely. It has taken place
annually since 2000 in locations as varied as Scotland, Argentina, and
Bosnia and Herzegovina. More information about DebConf is available from
<a href="https://debconf.org/">https://debconf.org</a>.
</p>

<h2>About Lenovo</h2>

<p>
As a global technology leader manufacturing a wide portfolio of connected products,
including smartphones, tablets, PCs and workstations as well as AR/VR devices,
smart home/office and data center solutions, <a href="https://www.lenovo.com">Lenovo</a>
understands how critical open systems and platforms are to a connected world.
</p>

<h2>About Infomaniak</h2>

<p>
<a href="https://www.infomaniak.com">Infomaniak</a> is Switzerland's largest web-hosting company,
also offering backup and storage services, solutions for event organizers,
live-streaming and video on demand services.
It wholly owns its datacenters and all elements critical 
to the functioning of the services and products provided by the company 
(both software and hardware). 
</p>

<h2>About Google</h2>

<p>
<a href="https://google.com/">Google</a> is one of the largest technology companies in the
world, providing a wide range of Internet-related services and products such
as online advertising technologies, search, cloud computing, software, and hardware.
</p>

<p>
Google has been supporting Debian by sponsoring DebConf for more than
ten years, and is also a Debian partner sponsoring parts 
of <a href="https://salsa.debian.org">Salsa</a>'s continuous integration infrastructure
within Google Cloud Platform.
</p>

<h2>About Amazon Web Services (AWS)</h2>

<p>
<a href="https://aws.amazon.com">Amazon Web Services (AWS)</a> is one of the world's
most comprehensive and broadly adopted cloud platforms,
offering over 175 fully featured services from data centers globally
(in 77 Availability Zones within 24 geographic regions).
AWS customers include the fastest-growing startups, largest enterprises
and leading government agencies.
</p>

<h2>Contact Information</h2>

<p>For further information, please visit the DebConf20 web page at
<a href="https://debconf20.debconf.org/">https://debconf20.debconf.org/</a>
or send mail to &lt;press@debian.org&gt;.</p>
