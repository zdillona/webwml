msgid ""
msgstr ""
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/index.def:8
msgid "The Universal Operating System"
msgstr "Az Univerzális Operációs rendszer"

#: ../../english/index.def:12
msgid "DC19 Group Photo"
msgstr "DC19 Csoportkép"

#: ../../english/index.def:15
msgid "DebConf19 Group Photo"
msgstr "DebConf19 Csoportkép"

#: ../../english/index.def:19
msgid "Mini DebConf Hamburg 2018"
msgstr "Mini DebConf Hamburg 2018"

#: ../../english/index.def:22
msgid "Group photo of the MiniDebConf in Hamburg 2018"
msgstr "A 2018-as hanmburgi MiniDebConf csoportképe"

#: ../../english/index.def:26
msgid "Screenshot Calamares Installer"
msgstr "Calamares Telepítő képernyőképe"

#: ../../english/index.def:29
msgid "Screenshot from the Calamares installer"
msgstr "Képernyőkép a Calamares telepítőből"

#: ../../english/index.def:33 ../../english/index.def:36
msgid "Debian is like a Swiss Army Knife"
msgstr "A Debian olyan, mint egy svájci bicska"

#: ../../english/index.def:40
msgid "People have fun with Debian"
msgstr "A résztvevők jól érzik magukat a Debiannal"

#: ../../english/index.def:43
msgid "Debian people at Debconf18 in Hsinchu really having fun"
msgstr "A Debian tagjai jól érzik magukat a DebConf18-on, Hszincsuban"

#: ../../english/template/debian/navbar.wml:31
msgid "Bits from Debian"
msgstr "Bitek a Debian-tól"

#: ../../english/template/debian/navbar.wml:31
msgid "Blog"
msgstr "Blog"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews"
msgstr "Micronews"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews from Debian"
msgstr "Mikro hírek a Debian-tól"

#: ../../english/template/debian/navbar.wml:33
msgid "Planet"
msgstr "Planet"

#: ../../english/template/debian/navbar.wml:33
msgid "The Planet of Debian"
msgstr "A Debian bolygója"
