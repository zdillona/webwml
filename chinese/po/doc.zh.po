#
# Yangfl <mmyangfl@gmail.com>, 2017.
# Wenbin Lv <wenbin816@gmail.com>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2019-07-15 20:38+0800\n"
"Last-Translator: Wenbin Lv <wenbin816@gmail.com>\n"
"Language-Team: 汉语 <debian-chinese-gb@lists.debian.org>\n"
"Language: zh\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0\n"
"X-Generator: Gtranslator 3.30.1\n"

#: ../../english/doc/books.data:35
msgid ""
"\n"
"  Debian 9 is the must-have handbook for learning Linux. Start on the\n"
"  beginners level and learn how to deploy the system with graphical\n"
"  interface and terminal.\n"
"  This book provides the basic knowledge to grow and become a 'junior'\n"
"  systems administrator. Start off with exploring the GNOME desktop\n"
"  interface and adjust it to your personal needs. Overcome your fear of\n"
"  using the Linux terminal and learn the most essential commands in\n"
"  administering Debian. Expand your knowledge of system services (systemd)\n"
"  and learn how to adapt them. Get more out of the software in Debian and\n"
"  outside of Debian. Manage your home-network with network-manager, etc.\n"
"  10 percent of the profits on this book will be donated to the Debian\n"
"  Project."
msgstr ""

#: ../../english/doc/books.data:64 ../../english/doc/books.data:174
#: ../../english/doc/books.data:229
msgid ""
"Written by two Debian developers, this free book\n"
"  started as a translation of their French best-seller known as Cahier de\n"
"  l'admin Debian (published by Eyrolles). Accessible to all, this book\n"
"  teaches the essentials to anyone who wants to become an effective and\n"
"  independent Debian GNU/Linux administrator.\n"
"  It covers all the topics that a competent Linux administrator should\n"
"  master, from the installation and the update of the system, up to the\n"
"  creation of packages and the compilation of the kernel, but also\n"
"  monitoring, backup and migration, without forgetting advanced topics\n"
"  like SELinux setup to secure services, automated installations, or\n"
"  virtualization with Xen, KVM or LXC."
msgstr ""

#: ../../english/doc/books.data:86
msgid ""
"The aim of this freely available book is to get you up to\n"
"  speed with Debian. It is comprehensive with basic support\n"
"  for the user who installs and maintains the system themselves (whether\n"
"  in the home, office, club, or school). Some Debian specific information "
"are very old.\n"
"  "
msgstr ""

#: ../../english/doc/books.data:108
msgid ""
"The first French book about Debian is already in its fifth edition. It\n"
"  covers all aspects of the administration of Debian from the installation\n"
"  to the configuration of network services.\n"
"  Written by two Debian developers, this book can be of interest to many\n"
"  people: the beginner wishing to discover Debian, the advanced user "
"looking\n"
"  for tips to enhance his mastership of the Debian tools and the\n"
"  administrator who wants to build a reliable network with Debian."
msgstr ""

#: ../../english/doc/books.data:128
msgid ""
"The book covers topics ranging from concepts of package\n"
"  management over the available tools and how they're used to concrete "
"problems\n"
"  which may occur in real life and how they can be solved. The book is "
"written\n"
"  in German, an English translation is planned. Format: e-book (Online, "
"HTML,\n"
"  PDF, ePub, Mobi), printed book planned.\n"
msgstr ""

#: ../../english/doc/books.data:149
msgid ""
"This book teaches you how to install and configure the system and also how "
"to use Debian in a professional environment.  It shows the full potential of "
"the distribution (in its current version 8) and provides a practical manual "
"for all users who want to learn more about Debian and its range of services."
msgstr ""

#: ../../english/doc/books.data:203
msgid ""
"Written by two penetration researcher - Annihilator, Firstblood.\n"
"  This book teaches you how to build and configure Debian 8.x server system\n"
"  security hardening using Kali Linux and Debian simultaneously.\n"
"  DNS, FTP, SAMBA, DHCP, Apache2 webserver, etc.\n"
"  From the perspective that 'the origin of Kali Linux is Debian', it "
"explains\n"
"  how to enhance Debian's security by applying the method of penetration\n"
"  testing.\n"
"  This book covers various security issues like SSL cerificates, UFW "
"firewall,\n"
"  MySQL Vulnerability, commercial Symantec antivirus, including Snort\n"
"  intrusion detection system."
msgstr ""

#: ../../english/doc/books.def:38
msgid "Author:"
msgstr "作者："

#: ../../english/doc/books.def:41
msgid "Debian Release:"
msgstr "Debian 版本："

#: ../../english/doc/books.def:44
msgid "email:"
msgstr "電子郵箱："

#: ../../english/doc/books.def:48
msgid "Available at:"
msgstr "售於："

#: ../../english/doc/books.def:51
msgid "CD Included:"
msgstr "包括光碟："

#: ../../english/doc/books.def:54
msgid "Publisher:"
msgstr "發行商："

#: ../../english/doc/manuals.defs:28
msgid "Authors:"
msgstr "作者："

#: ../../english/doc/manuals.defs:35
msgid "Editors:"
msgstr "編輯："

#: ../../english/doc/manuals.defs:42
msgid "Maintainer:"
msgstr "維護者："

#: ../../english/doc/manuals.defs:49
msgid "Status:"
msgstr "狀態："

#: ../../english/doc/manuals.defs:56
msgid "Availability:"
msgstr "获得途径："

#: ../../english/doc/manuals.defs:85
msgid "Latest version:"
msgstr "最新版本："

#: ../../english/doc/manuals.defs:101
msgid "(version <get-var version />)"
msgstr "(version <get-var version />)"

#: ../../english/doc/manuals.defs:131 ../../english/releases/arches.data:38
msgid "plain text"
msgstr "純文字"

#: ../../english/doc/manuals.defs:147 ../../english/doc/manuals.defs:157
#: ../../english/doc/manuals.defs:165
msgid ""
"The latest <get-var srctype /> source is available through the <a "
"href=\"https://packages.debian.org/git\">Git</a> repository."
msgstr ""
"最新的 <get-var srctype /> 格式的源代码可由 <a href=\"https://packages."
"debian.org/git\">Git</a> 仓库获得。"

#: ../../english/doc/manuals.defs:149 ../../english/doc/manuals.defs:159
#: ../../english/doc/manuals.defs:167
msgid "Web interface: "
msgstr "网页界面："

#: ../../english/doc/manuals.defs:150 ../../english/doc/manuals.defs:160
#: ../../english/doc/manuals.defs:168
msgid "VCS interface: "
msgstr "VCS 界面："

#: ../../english/doc/manuals.defs:175 ../../english/doc/manuals.defs:179
msgid "Debian package"
msgstr "Debian 套件"

#: ../../english/doc/manuals.defs:184 ../../english/doc/manuals.defs:188
msgid "Debian package (archived)"
msgstr "Debian 套件（已归档）"

#: ../../english/releases/arches.data:36
msgid "HTML"
msgstr "HTML"

#: ../../english/releases/arches.data:37
msgid "PDF"
msgstr "PDF"

#~ msgid ""
#~ "<p>You can visit the current <a href=\"manuals/<get-var doc />/"
#~ "\">development version</a> here, use <a href=\"cvs\">CVS</a> to download "
#~ "the SGML source text."
#~ msgstr ""
#~ "<p>You can visit the current<a href=\"manuals/<get-var doc />/"
#~ "\">development version</a> here,use <a href=\"cvs\">CVS</a> to download "
#~ "the SGML source text."

#~ msgid ""
#~ "CVS sources working copy: <p>Login to the CVS server using the command:"
#~ "<br>\n"
#~ "<pre>cvs -d:pserver:anonymous@cvs.debian.org:/cvs/debian-policy login</"
#~ "pre>\n"
#~ "and just press enter when you are asked for a password. To check out the "
#~ "sources, use the command:<br>\n"
#~ "<pre>cvs -d:pserver:anonymous@cvs.debian.org:/cvs/debian-policy co <get-"
#~ "var doc /></pre>"
#~ msgstr ""
#~ "CVS sources working copy:<p>Login to the CVS server using the command:"
#~ "<br><pre>cvs -d:pserver:anonymous@cvs.debian.org:/cvs/debian-policy "
#~ "login</pre>and just press enter when you are asked for a password.To "
#~ "check out the sources, use the command:<br><pre>cvs -d:pserver:"
#~ "anonymous@cvs.debian.org:/cvs/debian-policy co <get-var doc /></pre>"

#~ msgid ""
#~ "CVS sources working copy: set <code>CVSROOT</code>\n"
#~ "  to <kbd>:ext:<var>userid</var>@cvs.debian.org:/cvs/debian-boot</kbd>,\n"
#~ "  and check out the <kbd>boot-floppies/documentation</kbd> module."
#~ msgstr ""
#~ "取出 CVS 中的原始檔： 將 <code>CVSROOT</code> 設定為\n"
#~ "  <kbd>:ext:<var>userid</var>@cvs.debian.org:/cvs/debian-boot</kbd>,\n"
#~ "  然後 check out <kbd>boot-floppies/documentation</kbd> module。"

#~ msgid "CVS via web"
#~ msgstr "網頁檢視 CVS"

#~ msgid "Language:"
#~ msgstr "語言:"

#~ msgid ""
#~ "The latest <get-var srctype /> source is available through the <a "
#~ "href=\"https://packages.debian.org/cvs\">Cvs</a> repository."
#~ msgstr ""
#~ "最新的 <get-var srctype /> 格式的源代码可由 <a href=\"https://packages."
#~ "debian.org/cvs\">Cvs</a> 仓库获得。"

#~ msgid ""
#~ "The latest <get-var srctype /> source is available through the <a "
#~ "href=\"https://packages.debian.org/subversion\">Subversion</a> repository."
#~ msgstr ""
#~ "最新的 <get-var srctype /> 格式的源代码可由 <a href=\"https://packages."
#~ "debian.org/subversion\">Subversion</a> 仓库获得。"

#~ msgid ""
#~ "The latest <get-var srctype /> source is available through the <a "
#~ "href=\"https://www.debian.org/doc/cvs\">Subversion</a> repository."
#~ msgstr ""
#~ "<get-var srctype /> 的最新源代码可由 <a href=\"https://www.debian.org/doc/"
#~ "cvs\">Subversion</a> 仓库获得。"

#~ msgid ""
#~ "Use <a href=\"cvs\">SVN</a> to download the SGML source text for <get-var "
#~ "ddp_pkg_loc />."
#~ msgstr ""
#~ "使用 <a href=\"cvs\">SVN</a> 來取得 <get-vard dp_pkg_loc /> 的 SGML 原始"
#~ "檔。"
