#use wml::debian::translation-check translation="4924e09e5cb1b4163d7ec7161488354e4167b24c" maintainer="Lev Lamberov"
<define-tag pagetitle>Обновлённый Debian 11: выпуск 11.5</define-tag>
<define-tag release_date>2022-09-10</define-tag>
#use wml::debian::news

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.5</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Проект Debian с радостью сообщает о пятом обновлении своего
стабильного выпуска Debian <release> (кодовое имя <q><codename></q>).
Это обновление в основном содержит исправления проблем безопасности,
а также несколько корректировок серьёзных проблем. Рекомендации по безопасности
опубликованы отдельно и указываются при необходимости.</p>

<p>Заметьте, что это обновление не является новой версией Debian
<release>, а лишь обновлением некоторых включённых в выпуск пакетов. Нет
необходимости выбрасывать старые носители с выпуском <q><codename></q>. После установки
пакеты можно обновить до текущих версий, используя актуальное
зеркало Debian.</p>

<p>Тем, кто часто устанавливает обновления с security.debian.org, не придётся
обновлять много пакетов, большинство обновлений с security.debian.org
включены в данное обновление.</p>

<p>Новые установочные образы будут доступны позже в обычном месте.</p>

<p>Обновление существующих систем до этой редакции можно выполнить с помощью
системы управления пакетами, используя одно из множества HTTP-зеркал Debian.
Исчерпывающий список зеркал доступен на странице:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Исправления различных ошибок</h2>

<p>Данное стабильное обновление вносит несколько важных исправлений для следующих пакетов:</p>

<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction avahi "Исправление отображения URL, содержащих '&amp;' в avahi-discover; не отключать очистку по таймауту при очистке при слежении; исправление аварийных остановок NULL-указателя при попытке разрешить неправильно сформированные имена узлов [CVE-2021-3502]">
<correction base-files "Обновление /etc/debian_version для редакции 11.5">
<correction cargo-mozilla "Новый пакет с исходным кодом для поддержки сборки новых версий firefox-esr и thunderbird">
<correction clamav "Новый стабильный выпуск основной ветки разработки">
<correction commons-daemon "Исправление обнаружения JVM">
<correction curl "Отклонять куки с <q>control bytes</q> [CVE-2022-35252]">
<correction dbus-broker "Исправление ошибки утверждения при отключении одноранговых групп; исправление утечки памяти; исправление разыменования null-указателя [CVE-2022-31213]">
<correction debian-installer "Повторная сборка с учётом proposed-updates; увеличение ABI ядра Linux до версии 5.10.0-18">
<correction debian-installer-netboot-images "Повторная сборка с учётом proposed-updates; увеличение ABI ядра Linux до версии 5.10.0-18">
<correction debian-security-support "Обновление статуса поддержки различных пакетов">
<correction debootstrap "Гарантирование возможности создания chroot-окружений без объединённого каталога usr для старых выпусков и сборочных служб">
<correction dlt-daemon "Исправление двойного освобождения памяти [CVE-2022-31291]">
<correction dnsproxy "Прослушивание по умолчанию локального узла, а не возможно недоступного 192.168.168.1">
<correction dovecot "Исправление возможных проблем безопасности, когда имеются записи настроек в passdb с тем же драйвером и набором аргументов [CVE-2022-30550]">
<correction dpkg "Исправление обработки удаления файлов настройки при обновлении, утечки памяти в обработке удаления при обновлении; Dpkg::Shlibs::Objdump: исправление apply_relocations для работы с символами, имеющими версии; добавление поддержки для ЦП ARCv2; несколько обновлений и исправлений dpkg-fsys-usrunmess">
<correction fig2dev "Исправление двойного освобождения памяти [CVE-2021-37529], отказа в обслуживании [CVE-2021-37530]; прекращение неправильного размещения встроенных изображений в формате eps">
<correction foxtrotgps "Исправление аварийной остановки путём гарантирования, что потоки всегда не имеют ссылок">
<correction gif2apng "Исправление переполнения динамической памяти [CVE-2021-45909 CVE-2021-45910 CVE-2021-45911]">
<correction glibc "Исправление положительного/отрицательного переполнения буфера из-за ошибки на единицу в getcwd() [CVE-2021-3999]; исправление нескольких переполнений в функциях работы с расширенными символами; добавление нескольких оптимизированных EVEX функций работы со строками для исправления проблем с производительностью (до 40%) на ЦП Skylake-X; возможность использование grantpt после выполнения многопоточного форка; гарантировать, что защита libio vtable включена">
<correction golang-github-pkg-term "Исправление сборка на новых ядрах Linux">
<correction gri "Использовать <q>ps2pdf</q> вместо <q>convert</q> для преобразования PS в PDF">
<correction grub-efi-amd64-signed "Новый выпуск основной ветки разработки">
<correction grub-efi-arm64-signed "Новый выпуск основной ветки разработки">
<correction grub-efi-ia32-signed "Новый выпуск основной ветки разработки">
<correction grub2 "Новый выпуск основной ветки разработки">
<correction http-parser "Сброс F_CHUNKED на новом заголовке Transfer-Encoding, исправляющий возможную подделку HTTP-запросов [CVE-2020-8287]">
<correction ifenslave "Исправление настроек связанных интерфейсов">
<correction inetutils "Исправление переполнения буфера [CVE-2019-0053], исчерпания стека, обработки ответов FTP PASV [CVE-2021-40491], отказа в обслуживании [CVE-2022-39028]">
<correction knot "Исправление перехода от IXFR к AXFR при использовании dnsmasq">
<correction krb5 "Использование SHA256 в качестве Pkinit CMS Digest">
<correction libayatana-appindicator "Предоставление совместимости для ПО, зависящего от libappindicator">
<correction libdatetime-timezone-perl "Обновление поставляемых данных">
<correction libhttp-daemon-perl "Улучшение обработки заголовка Content-Length [CVE-2022-31081]">
<correction libreoffice "Поддержка EUR в локали .hr; добавление коэффициента преобразования HRK&lt;-&gt;EUR к Calc и Euro Wizard; исправления безопасности [CVE-2021-25636 CVE-2022-26305 CVE-2022-26306 CVE-2022-26307]; исправление зависания при обращении к адресным книгам Evolution">
<correction linux "Новый стабильный выпуск основной ветки разработки">
<correction linux-signed-amd64 "Новый стабильный выпуск основной ветки разработки">
<correction linux-signed-arm64 "Новый стабильный выпуск основной ветки разработки">
<correction linux-signed-i386 "Новый стабильный выпуск основной ветки разработки">
<correction llvm-toolchain-13 "Новый пакет с исходным кодом для поддержки сборки новых версий firefox-esr и thunderbird">
<correction lwip "Исправление переполнения буфера [CVE-2020-22283 CVE-2020-22284]">
<correction mokutil "Новая версия основной ветки разработки, позволяющая управлять SBAT">
<correction node-log4js "Не создавать по умолчанию файлы, доступные для чтения всем пользователям [CVE-2022-21704]">
<correction node-moment "Исправление отказа в обслуживании из-за регулярного выражения [CVE-2022-31129]">
<correction nvidia-graphics-drivers "Новый выпуск основной ветки разработки; исправления безопасности [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction nvidia-graphics-drivers-legacy-390xx "Новый выпуск основной ветки разработки; исправления безопасности [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction nvidia-graphics-drivers-tesla-450 "Новый выпуск основной ветки разработки; исправления безопасности [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction nvidia-graphics-drivers-tesla-470 "Новый выпуск основной ветки разработки; исправления безопасности [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction nvidia-settings "Новый выпуск основной ветки разработки; исправление кроссплатформенной сборки">
<correction nvidia-settings-tesla-470 "Новый выпуск основной ветки разработки; исправление кроссплатформенной сборки">
<correction pcre2 "Исправление чтения за пределами выделенного буфера памяти [CVE-2022-1586 CVE-2022-1587]">
<correction postgresql-13 "Не разрешать сценариям расширений заменять объекты, которые пока не принадлежат расширению [CVE-2022-2625]">
<correction publicsuffix "Обновление поставляемых данных">
<correction rocksdb "Исправление неправильной инструкции на arm64">
<correction sbuild "Buildd::Mail: поддержка закодированного MIME заголовка Subject:, а также копирования заголовка Content-Type: при пересылке почты">
<correction systemd "Удаление поставляемой копии linux/if_arp.h, исправление ошибок сборки с новыми заголовками ядра; поддержка определения гостевых систем ARM64 Hyper-V; обнаружение виртуальной машины OpenStack как KVM на arm">
<correction twitter-bootstrap4 "Установка CSS map-файлов">
<correction tzdata "Обновление часовых поясов для Ирана и Чили">
<correction xtables-addons "Поддержка и старых, и новых версий security_skb_classify_flow()">
</table>


<h2>Обновления безопасности</h2>


<p>В данный выпуск внесены следующие обновления безопасности. Команда
безопасности уже выпустила рекомендации для каждого
из этих обновлений:</p>

<table border=0>
<tr><th>Идентификационный номер рекомендации</th>  <th>Пакет</th></tr>
<dsa 2022 5175 thunderbird>
<dsa 2022 5176 blender>
<dsa 2022 5177 ldap-account-manager>
<dsa 2022 5178 intel-microcode>
<dsa 2022 5179 php7.4>
<dsa 2022 5180 chromium>
<dsa 2022 5181 request-tracker4>
<dsa 2022 5182 webkit2gtk>
<dsa 2022 5183 wpewebkit>
<dsa 2022 5184 xen>
<dsa 2022 5185 mat2>
<dsa 2022 5187 chromium>
<dsa 2022 5188 openjdk-11>
<dsa 2022 5189 gsasl>
<dsa 2022 5190 spip>
<dsa 2022 5191 linux-signed-amd64>
<dsa 2022 5191 linux-signed-arm64>
<dsa 2022 5191 linux-signed-i386>
<dsa 2022 5191 linux>
<dsa 2022 5192 openjdk-17>
<dsa 2022 5193 firefox-esr>
<dsa 2022 5194 booth>
<dsa 2022 5195 thunderbird>
<dsa 2022 5196 libpgjava>
<dsa 2022 5197 curl>
<dsa 2022 5198 jetty9>
<dsa 2022 5199 xorg-server>
<dsa 2022 5200 libtirpc>
<dsa 2022 5201 chromium>
<dsa 2022 5202 unzip>
<dsa 2022 5203 gnutls28>
<dsa 2022 5204 gst-plugins-good1.0>
<dsa 2022 5205 ldb>
<dsa 2022 5205 samba>
<dsa 2022 5206 trafficserver>
<dsa 2022 5207 linux-signed-amd64>
<dsa 2022 5207 linux-signed-arm64>
<dsa 2022 5207 linux-signed-i386>
<dsa 2022 5207 linux>
<dsa 2022 5208 epiphany-browser>
<dsa 2022 5209 net-snmp>
<dsa 2022 5210 webkit2gtk>
<dsa 2022 5211 wpewebkit>
<dsa 2022 5213 schroot>
<dsa 2022 5214 kicad>
<dsa 2022 5215 open-vm-tools>
<dsa 2022 5216 libxslt>
<dsa 2022 5217 firefox-esr>
<dsa 2022 5218 zlib>
<dsa 2022 5219 webkit2gtk>
<dsa 2022 5220 wpewebkit>
<dsa 2022 5221 thunderbird>
<dsa 2022 5222 dpdk>
</table>


<h2>Удалённые пакеты</h2>

<p>Следующие пакеты были удалены из-за причин, на которые мы не можем повлиять:</p>

<table border=0>
<tr><th>Пакет</th>               <th>Причина</th></tr>
<correction evenement "Несопровождается; требуется только для уже удалённого пакета movim">
<correction php-cocur-slugify "Несопровождается; требуется только для уже удалённого пакета movim">
<correction php-defuse-php-encryption "Несопровождается; требуется только для уже удалённого пакета movim">
<correction php-dflydev-fig-cookies "Несопровождается; требуется только для уже удалённого пакета movim">
<correction php-embed "Несопровождается; требуется только для уже удалённого пакета movim">
<correction php-fabiang-sasl "Несопровождается; требуется только для уже удалённого пакета movim">
<correction php-markdown "Несопровождается; требуется только для уже удалённого пакета movim">
<correction php-raintpl "Несопровождается; требуется только для уже удалённого пакета movim">
<correction php-react-child-process "Несопровождается; требуется только для уже удалённого пакета movim">
<correction php-react-http "Несопровождается; требуется только для уже удалённого пакета movim">
<correction php-respect-validation "Несопровождается; требуется только для уже удалённого пакета movim">
<correction php-robmorgan-phinx "Несопровождается; требуется только для уже удалённого пакета movim">
<correction ratchet-pawl "Несопровождается; требуется только для уже удалённого пакета movim">
<correction ratchet-rfc6455 "Несопровождается; требуется только для уже удалённого пакета movim">
<correction ratchetphp "Несопровождается; требуется только для уже удалённого пакета movim">
<correction reactphp-cache "Несопровождается; требуется только для уже удалённого пакета movim">
<correction reactphp-dns "Несопровождается; требуется только для уже удалённого пакета movim">
<correction reactphp-event-loop "Несопровождается; требуется только для уже удалённого пакета movim">
<correction reactphp-promise-stream "Несопровождается; требуется только для уже удалённого пакета movim">
<correction reactphp-promise-timer "Несопровождается; требуется только для уже удалённого пакета movim">
<correction reactphp-socket "Несопровождается; требуется только для уже удалённого пакета movim">
<correction reactphp-stream "Несопровождается; требуется только для уже удалённого пакета movim">

</table>

<h2>Программа установки Debian</h2>

Программа установки была обновлена с целью включения исправлений, добавленных в
данную редакцию стабильного выпуска.

<h2>URL</h2>

<p>Полный список пакетов, которые были изменены в данной редакции:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Текущий стабильный выпуск:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Предлагаемые обновления для стабильного выпуска:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Информация о стабильном выпуске (информация о выпуске, известные ошибки и т. д.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Анонсы безопасности и информация:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>О Debian</h2>

<p>Проект Debian &mdash; объединение разработчиков свободного программного обеспечения,
которые жертвуют своё время и знания для создания абсолютно свободной
операционной системы Debian.</p>

<h2>Контактная информация</h2>

<p>Более подробную информацию вы можете получить на сайте Debian
<a href="$(HOME)/">https://www.debian.org/</a>, либо отправив письмо по адресу
&lt;press@debian.org&gt;, либо связавшись с командой стабильного выпуска по адресу
&lt;debian-release@lists.debian.org&gt;.</p>
