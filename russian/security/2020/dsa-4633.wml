#use wml::debian::translation-check translation="d3f29d8015c29a9da9f70c50fcc3cb13a49a95c7" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>
<p>В cURL, библиотеке передачи URL, были обнаружены многочисленные
уязвимости.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5436">CVE-2019-5436</a>

    <p>В коде получения данных по TFTP было обнаружено переполнение буфера,
    которое может позволить вызов отказа в обслуживании или выполнение произвольного
    кода. Данная уязвимость касается только предыдущего стабильного выпуска (stretch).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5481">CVE-2019-5481</a>

    <p>Томас Вегас обнаружил повторное освобождение памяти в коде FTP-KRB code,
    вызываемое вредоносным сервером, отправляющим слишком большой блок данных.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5482">CVE-2019-5482</a>

    <p>Томас Вегас обнаружил переполнение буфера, которое может быть вызвано
    использованием небольшого значения размера блока TFTP (значение не по умолчанию).</p></li>

</ul>

<p>В предыдущем стабильном выпуске (stretch) эти проблемы были исправлены
в версии 7.52.1-5+deb9u10.</p>

<p>В стабильном выпуске (buster) эти проблемы были исправлены в
версии 7.64.0-4+deb10u1.</p>

<p>Рекомендуется обновить пакеты curl.</p>

<p>С подробным статусом поддержки безопасности curl можно ознакомиться на
соответствующей странице отслеживания безопасности по адресу
<a href="https://security-tracker.debian.org/tracker/curl">\
https://security-tracker.debian.org/tracker/curl</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4633.data"
