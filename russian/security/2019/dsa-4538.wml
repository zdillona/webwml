#use wml::debian::translation-check translation="e41f5efa353b2bdd34609a011c02c9132873e041" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>
<p>В реализации протокола WPA в wpa_supplication (станция) и hostapd
(точка доступа) были обнаружены две уязвимости.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13377">CVE-2019-13377</a>

    <p>Атака через сторонние каналы с помощью тайминга на процесс установления связи
    WPA3 Dragonfly при использовании кривых Brainpool может использоваться злоумышленником
    для получения пароля.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16275">CVE-2019-16275</a>

    <p>Недостаточная проверка адреса источника для некоторых управляющих фреймов
    в hostapd может приводить к отказу в обслуживании для станций, связанных с
    точкой доступа. Злоумышленник в пределах распространения радиосигнала точки доступа
    может ввести специально сформированный неаутентифицированный фрейм IEEE 802.11
    в точку доступа для вызова отключения связанных станций, что потребует
    повторного подключения к сети.</p></li>

</ul>

<p>В стабильном выпуске (buster) эти проблемы были исправлены в
версии 2:2.7+git20190128+0c1e29f-6+deb10u1.</p>

<p>Рекомендуется обновить пакеты wpa.</p>

<p>С подробным статусом поддержки безопасности wpa можно ознакомиться на
соответствующей странице отслеживания безопасности по адресу
<a href="https://security-tracker.debian.org/tracker/wpa">\
https://security-tracker.debian.org/tracker/wpa</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4538.data"
