#use wml::debian::template title="Posicionamento do Debian sobre patentes de software" BARETITLE="true"
#use wml::debian::translation-check translation="088c528e5eb9d95504ef91aa449795178ac06be1"

<pre><code>Versão: 1.0
Publicado:: 19 de fevereiro de 2012
</code></pre>


<h1>Introdução</h1>

<p>Este documento descreve o posicionamento do Debian em relação a patentes. O
Debian reconhece a ameaça que as patentes impoõem para o software livre e
continua a trabalhar com outros(as) da comunidade Software Livre na defesa
contra patentes.</p>

<p>A política incluída abaixo pretende fornecer uma orientação clara e útil para
os(as) membros(as) da comunidade quando confrontados(as) com a questão das
patentes. De modo que a comunidade Debian possa coordenar sua defesa contra
patentes livre de medo, incerteza e dúvida.
</p>


<h1>Declaração da política</h1>

<ol>

<li><p>O Debian não distribuirá, conscientemente, software onerado por patentes;
     contribuidores(as) Debian não devem empacotar ou distribuir software que
     eles(as) saibam que infringe uma patente.</p></li>

<li><p>O Debian não aceitará uma licença de patente que seja inconsistente com o
    <a href="$(HOME)/social_contract">Contrato Social Debian</a> ou com a
    <a href="$(HOME)/social_contract#guidelines">Definição Debian de Software
    Livre (DFSG)</a>.</p></li>

<li><p>A menos que comunicações relativas a patentes estejam sujeitas à relação
    especial entre advogado-cliente, membros(as) da comunidade podem ser
    forçados(as) a produzir comunicações em ações legais. Além disso,
    preocupações com patentes expressas publicamente podem ser infundadas, mas
    podem criar grande quantidade de medo, incerteza e dúvida por um período.
    Portanto, por favor, prive-se de expressar publicamente preocupações com
    patentes ou de discutir patentes fora de comunicações que não sejam
    legalmente aconselhadas, quando são sujeitas à relação especial entre
    advogado-cliente.</p></li>

<li><p>Riscos devido a patentes afetam toda a comunidade. Se você tem
    preocupações sobre uma patente específica, por favor, não mantenha-as para
    si &mdash; demande aconselhamento jurídico.</p></li>

<li><p>Toda comunicação relativa a uma patente específica deve ser direcionada,
    em inglês, para <a href="mailto:patents@debian.org">patents@debian.org</a>,
    comunicação que será mantida sob as regras da relação especial entre
    advogado-cliente. O risco será avaliado e toda resposta necessária será
    realizada diretamente com os grupos afetados.</p></li>

</ol>


<h1>Informação de contato</h1>

<p>No caso de desejar nos contatar sobre riscos de uma patente específica no
repositório Debian, favor enviar um e-mail em inglês para
<a href="mailto:patents@debian.org">patents@debian.org</a>, mencionando:</p>

<ul>
<li>seu nome,</li>
<li>seu papel no Debian,</li>
<li>o nome dos pacotes ou projetos envolvidos,</li>
<li>o título, número e jurisdição da patente envolvida,</li>
<li>uma descrição de suas preocupações.</li>
</ul>


<h1>Informações adicionais</h1>

<p>Para mais informações sobre patentes e como elas interagem com distribuições
da comunidade Software Livre, nós recomendamos a leitura de nosso
<a href="$(HOME)/reports/patent-faq">FAQ da política de patente de distribuição da comunidade</a>,
um documento cujo objetivo é educar desenvolvedores(as) de software livre, e
especialmente editores(as) de distribuições, sobre os riscos de patentes de
software.</p>


<hr />

<p>O Projeto Debian agradece
ao <a href="http://www.softwarefreedom.org">Software Freedom Law Center</a>
(SFLC) pelo aconselhamento legal sobre essa matéria.</p>
