#use wml::debian::template title="Política de divulgação de vulnerabilidades do Debian"
#use wml::debian::translation-check translation="5a5aa2dd64b9f93c369cb9c01eafa0bef14dafa8"

Este documento é a política de divulgação e embargo de vulnerabilidades do
projeto Debian,
como requerido pelo status do Debian como uma autoridade numeradora CVE
(Sub-CNA, <a href="https://cve.mitre.org/cve/cna/rules.html#section_2-3_record_management_rules">regra 2.3.3</a>).
Por favor, também consulte o <a href="faq">FAQ do time de segurança</a> para
informações adicionais sobre procedimentos de segurança no Debian.

<h2>Processo geral</h2>

<p>Geralmente, o time de segurança do Debian retornará para os relatores(as) de
vulnerabilidade dentro de alguns dias (veja as entradas do FAQ para
<a href="faq#contact">contatando o time de segurança</a>
e <a href="faq#discover">relatando vulnerabilidades</a>).

<p>A maioria do software que é parte do sistema operacional Debian
não foi escrito especificamente para o Debian. O sistema operacional
Debian como um todo também serve como uma fundação para outras distribuições
GNU/Linux. Isto significa que a maior parte das vulnerabilidades que afetam
o Debian também afetam outras distribuições e, em muitos casos, fornecedores(as)
comerciais de software. Como resultado, a divulgação de vulnerabilidades tem
que ser coordenada com outros grupos, não somente com o(a) relator(a) e o Debian
em si.</p>

<p>Um fórum para esta coordenação é a
<a href="https://oss-security.openwall.org/wiki/mailing-lists/distros">lista de distribuições</a>.
O time de segurança do Debian supõe que os(as) pesquisadores(as) de segurança
experientes contatem diretamente a lista de distribuições e os projetos
originais afetados. O time de segurança do Debian oferecerá assistência para
outros relatores(as) se necessário. Antes de envolver outras partes, é obtida
a permissão dos(as) relatores(as).</p>

<h2>Linhas do tempo</h2>

<p>Como mencionado no início, espera-se que a notificação por e-mail do
relatório inicial demore no máximo alguns dias.</p>

<p>Como o tratamento da maioria das vulnerabilidades nos softwares do Debian
requer a coordenação entre vários grupos (desenvolvedores(as) do software,
outras distribuições), o período entre o relatório inicial da vulnerabilidade e
sua divulgação pública varia muito, dependendo do software e das organizações
envolvidas.</p>

<p>A lista de distribuições limita o período de embargo (o período entre o
relatório inicial e a divulgação) em duas semanas. Entretanto, períodos mais
longos não são incomuns, com coordenação adicional antes do compartilhamento
com a lista de distribuições, para acomodar fornecedores(as) com ciclos de
lançamento mensais ou mesmo trimestrais. O tratamento de vulnerabilidades de
protocolo de Internet pode ser ainda mais longo que os outros, como também as
tentativas de desenvolvimento de mitigação de vulnerabilidades de hardware no
software.</p>

<h2>Evitando embargos</h2>

<p>Visto que a coordenação no privado tende a causar muito atrito
e torna difícil o envolvimento dos(as) especialistas adequados(as) à matéria, o
Debian encorajará a divulgação pública de vulnerabilidades mesmo antes de uma
correção ter sido desenvolvida, exceto quando tal abordagem claramente coloca
em perigo os(as) usuários(as) do Debian e outras partes.</p>

<p>O time de segurança do Debian frequentemente pedirá aos(às) relatores(as)
de vulnerabilidades que preencham um relatório de bug público no sistema de
rastreamento de bug apropriado (como o <a href="../Bugs/">sistema de
rastreamento de bug do Debian</a>), fornecendo assistência se necessário.</p>

<p>Um embargo não é necessário para atribuição CVE ou crédito em um
alerta de segurança.</p>

<h2>Atribuição CVE</h2>

<p>O Debian, como um sub-CNA, somente atribui IDs CVE para vulnerabilidades
do Debian. Se uma vulnerabilidade reportada não atende este critério e
portanto está fora do escopo do CNA do Debian, o time de segurança do Debian
buscará uma atribuição do ID CVE de outros CNAs ou guiará o(a) relator(a) na
submissão de sua própria requisição para um ID CVE.</p>

<p>Uma atribuição CVE pelo Debian CNA será tornada pública com a
publicação do alerta de segurança do Debian, ou quando o bug for
registrado no sistema de rastreamento de bugs apropriado.</p>

<h2>Vulnerabilidade vs bug regular</h2>

<p>Devido à ampla gama de software que é parte do sistema
operacional Debian, não é possível fornecer uma orientação sobre o que
constitui uma vulnerabilidade de segurança e sobre o que é somente
um bug de software comum. Quando estiver em dúvida, por favor contate o time
de segurança do Debian.</p>

<h2>Programa de recompensa de caça aos bugs</h2>

<p>O Debian não oferece um programa de recompensa de caça aos bugs.
Terceiros independentes podem encorajar os(as) relatores(as) a contatá-los
sobre vulnerabilidades no sistema operacional Debian, mas eles não são
endossados pelo projeto Debian.</p>

<h2>Vulnerabilidades de infraestrutura do Debian</h2>

<p>Relatórios de vulnerabilidades na infraestrutura do próprio Debian são
administradas de forma semelhante. Se uma vulnerabilidade de infraestrutura não
é o resultado de má configuração, mas uma vulnerabilidade no software que
está sendo usado, a usual coordenação multigrupo será demandada, com
prazos similares aos descritos acima.</p>
