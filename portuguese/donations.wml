#use wml::debian::template title="Como doar para o projeto Debian"
#use wml::debian::translation-check translation="77730b1058b58e76cb29910f6acf353ec5bf9847"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#spi">Software in the Public Interest</a></li>
  <li><a href="#debianfrance">Debian France</a></li>
  <li><a href="#debianch">debian.ch</a></li>
  <li><a href="#debian">Debian</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Obrigado a todos(as) os(as) doadores(as) por apoiarem o Debian com equipamentos ou serviços: <a href="https://db.debian.org/machines.cgi">patrocinadores(as) de hospedagem e hardware</a>, <a href="mirror/sponsors">patrocinadores(as) de espelhos</a>, <a href="partners/">parceiros(as) de desenvolvimento e serviços</a>.<p>
</aside>

<p>
As doações são gerenciadas pelo(a)
<a href="$(HOME)/devel/leader">líder do projeto Debian</a> (DPL)
e possibilitam que o Debian tenha
<a href="https://db.debian.org/machines.cgi">máquinas</a>,
<a href="https://wiki.debian.org/Teams/DSA/non-DSA-HW">outros tipos de hardware</a>,
domínios, certificados criptográficos,
<a href="https://www.debconf.org">a conferência Debian</a>,
<a href="https://wiki.debian.org/MiniDebConf">miniconferências Debian</a>,
<a href="https://wiki.debian.org/Sprints">sprints de desenvolvimento</a>,
presença em outros eventos, etc.
</p>

<p id="default">
Várias <a href="https://wiki.debian.org/Teams/Treasurer/Organizations">organizações</a>
mantêm ativos em confiança para o Debian e recebem doações em nome do Debian.
Esta página lista diferentes métodos de doação para o projeto Debian. O método
mais fácil de doar para o Debian é via PayPal para a
<a href="https://www.spi-inc.org/" title="SPI">Software in the Public Interest</a>,
uma organização sem fins lucrativos que mantém ativos em confiança para o Debian.
</p>

<p style="text-align:center"><button type="button"><span class="fab fa-paypal fa-2x"></span> <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=86AHSZNRR29UU">Doe via PayPal</a></button></p>

<aside class="light">
  <span class="fas fa-gifts fa-5x"></span>
</aside>

<table>
<tr>
<th>Organização</th>
<th>Métodos</th>
<th>Notas</th>
</tr>
<tr>
<td><a href="#spi"><acronym title="Software in the Public Interest">SPI</acronym></a></td>
<td>
 <a href="#spi-paypal">PayPal</a>,
 <a href="#spi-click-n-pledge">Click &amp; Pledge</a>,
 <a href="#spi-cheque">Cheque</a>,
 <a href="#spi-other">Outros</a>
</td>
<td>EUA, sem fins lucrativos isenta de impostos</td>
</tr>
<tr>
<td><a href="#debianfrance">Debian France</a></td>
<td>
 <a href="#debianfrance-bank">Transferência bancária</a>,
 <a href="#debianfrance-paypal">PayPal</a>
</td>
<td>França, sem fins lucrativos isenta de impostos</td>
</tr>
<tr>
<td><a href="#debianch">debian.ch</a></td>
<td>
 <a href="#debianch-bank">Transferência bancária</a>,
 <a href="#debianch-other">Outros</a>
</td>
<td>Suíça, sem fins lucrativos</td>
</tr>
<tr>
<td><a href="#debian">Debian</a></td>
<td>
 <a href="#debian-equipment">Equipamento</a>,
 <a href="#debian-time">Tempo</a>,
 <a href="#debian-other">Outros</a>
</td>
<td></td>
</tr>

# Template:
#<tr>
#<td><a href="#"><acronym title=""></acronym></a></td>
#<td>
# <a href="#"></a>,
# <a href="#"></a> (allows recurring donations),
# <a href="#cheque"></a> (CUR)
#</td>
#<td>, tax-exempt non-profit</td>
#</tr>

</table>

<h2 id="spi">Software in the Public Interest</h2>

<p>
A <a href="https://www.spi-inc.org/" title="SPI">Software in the Public Interest, Inc.</a>
é uma organização sem fins lucrativos isenta de impostos, com sede nos Estados
Unidos da América, fundada por pessoas do Debian em 1997 para ajudar
organizações de software/hardware livre.
</p>

<h3 id="spi-paypal">PayPal</h3>

<p>
As doações individuais e recorrentes podem ser feitas através da
<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=86AHSZNRR29UU">página da SPI</a>
no site web do PayPal. Para fazer uma doação recorrente, marque a caixa
<em>Make this a monthly donation</em>.
</p>

<h4 id="spi-click-n-pledge">Click &amp; Pledge</h4>

<p>
As doações individuais e recorrentes podem ser feitas através da
<a href="https://co.clickandpledge.com/advanced/default.aspx?wid=34115">página da SPI</a>
no site web Click &amp; Pledge. Para fazer uma doação recorrente, escolha à
direita com qual frequência você gostaria de doar (<em>Repeat payment</em>),
role para baixo para <em>Debian Project Donation</em>, digite o valor que
gostaria de doar, clique no item <em>Add to cart</em> e continue o resto do
processo.
</p>

<h3 id="spi-cheque">Cheque</h3>

<p>
As doações podem ser feitas através de cheque ou ordem de pagamento em
dólares americanos (<abbr title="US dollars">USD</abbr>) e
dólares canadenses (<abbr title="Canadian dollars">CAD</abbr>).
Por favor, ponha Debian no campo memo e envie para a SPI no endereço listado
na <a href="https://www.spi-inc.org/donations/">página de doações da SPI</a>.

<h3 id="spi-other">Outros</h3>

<p>
As doações através de transferência bancária e outros métodos também estão
disponíveis. Para algumas partes do mundo pode ser mais fácil fazer a
doação para uma das organizações parceiras da Software in the Public Interest.
Para mais detalhes, por favor, visite a
<a href="https://www.spi-inc.org/donations/">página de doações da SPI</a>.
</p>


<h2 id="debianfrance">Debian França</h2>

<p>
A <a href="https://france.debian.net/">Debian France Association</a> é uma
organização registrada na França nos termos da <q>lei de 1901</q>,
fundada para apoiar e promover o projeto Debian na França.
</p>

<h3 id="debianfrance-bank">Transferência bancária</h3>
<p>
As doações através de transferência bancária estão disponíveis para a conta
bancária listada na
<a href="https://france.debian.net/soutenir/#compte">página de doações da Debian França</a>.
Os recibos estão disponíveis sob demanda: por favor mande um e-mail para
<a href="mailto:donation@france.debian.net">donation@france.debian.net</a>.
</p>

<h3 id="debianfrance-paypal">PayPal</h3>

<p>
As doações podem ser enviadas através da
<a href="https://france.debian.net/galette/plugins/paypal/form">página PayPal da Debian France</a>.
Elas podem ser direcionadas especificamente para a Debian France ou para o
projeto Debian em geral.
</p>

<h2 id="debianch">debian.ch</h2>

<p>
A <a href="https://debian.ch/">debian.ch</a> foi fundada para representar
o projeto debian na Suíça e no Principado de Liechtenstein.
</p>

<h3 id="debianch-bank">Transferência bancária</h3>

<p>
As doações via transferência bancária tanto de bancos suíços quanto
internacionais estão disponíveis para as contas bancárias listadas no
<a href="https://debian.ch/">site web da debian.ch</a>.
</p>

<h3 id="debianch-other">Outros</h3>

<p>
Doações através de outros métodos podem ser obtidas contatando o endereço de
doações listado no <a href="https://debian.ch/">site web</a>.
</p>

# Template:
#<h3 id=""></h3>
#
#<p>
#</p>
#
#<h4 id=""></h4>
#
#<p>
#</p>

<h2 id="debian">Debian</h2>

<p>
O Debian é capaz de aceitar doações diretas de <a href="#debian-equipment">equipamentos</a>,
mas não <a href="#debian-other">outras</a> doações neste momento.
</p>

<h3 id="debian-equipment">Equipamentos e serviços</h3>

<p>
O Debian também conta com a doação de equipamentos e serviços de indivíduos,
empresas, universidades, etc, para manter o Debian conectado ao mundo.
</p>

<p>
Caso sua empresa tenha algumas máquinas ociosas ou equipamentos de reposição
(discos rígidos, controladoras SCSI, placas de rede, etc) disponíveis, por
favor, considere doá-los para o Debian. Por favor, contate nosso(a)
<a href="mailto:hardware-donations@debian.org">delegado(a) de doações e hardware</a>
para detalhes.
</p>

<p>
O Debian mantém uma
<a href="https://wiki.debian.org/Hardware/Wanted">lista de hardware desejado</a>
por vários serviços e grupos dentro do projeto.
</p>

<h3 id="debian-time">Tempo</h3>

<p>
Há várias formas de <a href="$(HOME)/intro/help">ajudar o Debian</a>
usando seu tempo pessoal ou de trabalho.
</p>

<h3 id="debian-other">Outros</h3>

<p>
O Debian não é capaz de aceitar nenhuma criptomoeda neste momento, mas
estamos analisando a possibilidade de suportar esse método de doação.
# Brian Gupta requested we discuss this before including it:
#If you have cryptocurrency to donate, or insights to share, please
#get in touch with <a href="mailto:madduck@debian.org">Martin f. krafft</a>.
</p>
